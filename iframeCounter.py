
import sys
if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Usage:")
        print("  ", sys.argv[0], "input_movie_name", "[out_txt_file]");
    else:
        import subprocess
        cmd = [ 'ffprobe', '-show_frames', sys.argv[1] ]
        out = subprocess.Popen( cmd, stdout=subprocess.PIPE ).communicate()[0]
        out = out.decode('ascii')
        ilist = []
        frameCounter = 1;
        for line in out.splitlines(True):
            if line.startswith("pict_type="):
                if line.startswith("pict_type=I"):
                    ilist.append(frameCounter)
                frameCounter = frameCounter + 1
        if len(sys.argv) < 3:
            print(ilist)
        else:
            f = open(sys.argv[2], "w")
            f.write(", ".join(str(x) for x in ilist))
            f.close()