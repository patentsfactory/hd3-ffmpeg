Projekt R&D w ramach pracy w firmie Patents Factory Ltd.

Kompresja HD3

Opis kompresji: https://bitbucket.org/patentsfactory/hd3-ffmpeg/src/master/Opis%20kompresji%20video%20HD3.pdf

Opis implementacji: https://bitbucket.org/patentsfactory/hd3-ffmpeg/src/master/Dokumentacja%20implementacji%20HD3.pdf

Opis prototypu kamery: https://bitbucket.org/patentsfactory/hd3-ffmpeg/src/master/Kamera%20HD3%20v2.pdf

Repozytorium pomocnicze (do x264): https://bitbucket.org/patentsfactory/hd3-x264

Kod jest modyfikacją FFmpeg (https://github.com/FFmpeg/FFmpeg) więc również jest na licencji GPLv2

Patent: https://www.freepatentsonline.com/EP3585058A1.pdf

Pytania proszę kierować na kaczirosan@gmail.com