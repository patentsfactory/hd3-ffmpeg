/*
 * Copyright (c) 2010 Stefano Sabatini
 * Copyright (c) 2010 Baptiste Coudurier
 * Copyright (c) 2007 Bobby Bingham
 *
 * This file is part of FFmpeg.
 *
 * FFmpeg is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * FFmpeg is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with FFmpeg; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */
#ifndef PF_MOVDESC_H
#define PF_MOVDESC_H

/**
 * @file
 * overlay one video on top of another
 */

#include "libavfilter/avfilter.h"
#include "libavfilter/formats.h"
#include "libavutil/common.h"
#include "libavutil/eval.h"
#include "libavutil/avstring.h"
#include "libavutil/pixdesc.h"
#include "libavutil/imgutils.h"
#include "libavutil/mathematics.h"
#include "libavutil/opt.h"
#include "libavutil/timestamp.h"
#include "libavfilter/internal.h"
#include "libavfilter/dualinput.h"
#include "libavfilter/drawutils.h"
#include "libavfilter/video.h"

#include "../hd3/frame_copy.h"
#include "camera_setup.h"

#define PF_DM_MAIN 0
#define PF_DM_SECOND  1

typedef struct MovdescCalibdata{

} MovdescCalibdata;

typedef struct CalibNode {
    float pixToReal0;
    float pixToReal1;
    float p0;
    float p1;
} CalibNode;


typedef struct MovdescContext {
    const AVClass *class;

    char *mode;
    char *calibdataFilename;
    char *outFilename;
    char *cameraLens;

    int64_t redThreshold;
    int64_t greenThreshold;
    int64_t blueThreshold;

    int64_t redBottomThreshold;
    int64_t greenBottomThreshold;
    int64_t blueBottomThreshold;

    FILE *calibdataFile;
    FILE *outFile;

    MovdescCalibdata calib;
    FrameCopy buffor;

    FFDualInputContext dinput;

    CalibNode nodesX[1000]; // kacziro - workaround
    CalibNode nodesY[1000]; // kacziro - workaround

    FieldOfView firstFov;
    FieldOfView secFov;
} MovdescContext;

static av_cold void movdesc_uninit(AVFilterContext *ctx)
{
    MovdescContext *mdCtx = ctx->priv;

    ff_dualinput_uninit(&mdCtx->dinput);

    if (mdCtx->calibdataFile) {
        fclose(mdCtx->calibdataFile);
    }
    if (mdCtx->outFile) {
        printf("close file\n");
        fclose(mdCtx->outFile);
    }
}

static int movdesc_process_command(AVFilterContext *ctx, const char *cmd, const char *args,
                                   char *res, int res_len, int flags)
{
    return 0;
}

static int movdesc_config_input_second(AVFilterLink *inlink)
{
    return 0;
}

static int movdesc_config_input_main(AVFilterLink *inlink)
{
    return 0;
}
static int movdesc_config_output(AVFilterLink *outlink)
{
    AVFilterContext *ctx = outlink->src;
    MovdescContext *s = ctx->priv;
    int ret;

    if ((ret = ff_dualinput_init(ctx, &s->dinput)) < 0)
        return ret;

    outlink->w = ctx->inputs[PF_DM_MAIN]->w;
    outlink->h = ctx->inputs[PF_DM_MAIN]->h;
    outlink->time_base = ctx->inputs[PF_DM_MAIN]->time_base;
    initFrameCopy(NULL, &s->buffor, outlink->w, outlink->h, AV_PIX_FMT_GBRAP);
    return 0;
}

static AVFrame* movdesc_detect(AVFilterContext *ctx,
                               AVFrame *first, const AVFrame *second)
{
    //match objects
    MovdescContext *mdCtx = ctx->priv;

    if (first->hd3Data && second->hd3Data) {
        int fIndex = first->pts % first->hd3Data->maxNumber;
        int sIndex = second->pts % second->hd3Data->maxNumber;
        if (first->hd3Data->pBoxes[fIndex].pts == first->pts &&
            second->hd3Data->pBoxes[sIndex].pts == second->pts) {
            uint16_t fHist[1000][3][256]; // kacziro - workaround - max: 1000
            uint16_t sHist[1000][3][256];
            memset(fHist, 0, sizeof(fHist));
            memset(sHist, 0, sizeof(sHist));

            for (int i = 0; i < first->hd3Data->pBoxes[fIndex].number; i++) {
                MovementBox *box = first->hd3Data->pBoxes[fIndex].boxes + i;
                for (int y = box->aabb.begin.y; y < box->aabb.end.y; y++) {
                    uint8_t *pFY = first->data[0] + y*first->linesize[0];
                    for (int x = box->aabb.begin.x; x < box->aabb.end.x; x++) {
                        fHist[i][0][pFY[x]]++;
                    }
                }
                for (int y = box->aabb.begin.y/2; y < box->aabb.end.y/2; y++) {
                    uint8_t *pFU = first->data[1] + y*first->linesize[1];
                    uint8_t *pFV = first->data[2] + y*first->linesize[2];
                    for (int x = box->aabb.begin.x/2; x < box->aabb.end.x/2; x++) {
                        fHist[i][1][pFU[x]]++;
                        fHist[i][2][pFV[x]]++;
                    }
                }
            }
            for (int i = 0; i < second->hd3Data->pBoxes[sIndex].number; i++) {
                MovementBox *box = second->hd3Data->pBoxes[fIndex].boxes + i;
                for (int y = box->aabb.begin.y; y < box->aabb.end.y; y++) {
                    uint8_t *pSY = second->data[0] + y*second->linesize[0];
                    for (int x = box->aabb.begin.x; x < box->aabb.end.x; x++) {
                        sHist[i][0][pSY[x]]++;
                    }
                }
                for (int y = box->aabb.begin.y/2; y < box->aabb.end.y/2; y++) {
                    uint8_t *pSU = second->data[1] + y*second->linesize[1];
                    uint8_t *pSV = second->data[2] + y*second->linesize[2];
                    for (int x = box->aabb.begin.x/2; x < box->aabb.end.x/2; x++) {
                        sHist[i][1][pSU[x]]++;
                        sHist[i][2][pSV[x]]++;
                    }
                }
            }
            for (int i = 0; i < first->hd3Data->pBoxes[fIndex].number; i++) {
                int bestJ = 0;
                int minErr = 0;

                minErr += abs(fHist[i][0][0] + fHist[i][1][0] - sHist[0][0][0] - sHist[0][1][0]);
                minErr += abs(fHist[i][1][1] + fHist[i][1][1] - sHist[0][0][1] - sHist[0][1][1]);
                minErr += abs(fHist[i][1][2] + fHist[i][1][2] - sHist[0][0][2] - sHist[0][1][2]);
                for (int v = 1; v < 256-1; v++) {
                    minErr += abs(fHist[i][v-1][0] + fHist[i][v][0] + fHist[i][v+1][0] - sHist[0][v-1][0] - sHist[0][v][0] - sHist[0][v+1][0]);
                    minErr += abs(fHist[i][v-1][1] + fHist[i][v][1] + fHist[i][v+1][1] - sHist[0][v-1][1] - sHist[0][v][1] - sHist[0][v+1][1]);
                    minErr += abs(fHist[i][v-1][2] + fHist[i][v][2] + fHist[i][v+1][2] - sHist[0][v-1][2] - sHist[0][v][2] - sHist[0][v+1][2]);
                }
                minErr += abs(fHist[i][254][0] + fHist[i][255][0] - sHist[0][254][0] - sHist[0][255][0]);
                minErr += abs(fHist[i][254][1] + fHist[i][255][1] - sHist[0][254][1] - sHist[0][255][1]);
                minErr += abs(fHist[i][254][2] + fHist[i][255][2] - sHist[0][254][2] - sHist[0][255][2]);
                for (int j = 1; j < second->hd3Data->pBoxes[sIndex].number; j++) {
                    int err = 0;
                    err += abs(fHist[i][0][0] + fHist[i][1][0] - sHist[j][0][0] - sHist[j][1][0]);
                    err += abs(fHist[i][0][1] + fHist[i][1][1] - sHist[j][0][1] - sHist[j][1][1]);
                    err += abs(fHist[i][0][2] + fHist[i][1][2] - sHist[j][0][2] - sHist[j][1][2]);
                    for (int v = 1; v < 256-1; v++) {
                        err += abs(fHist[i][v-1][0] + fHist[i][v][0] + fHist[i][v+1][0] - sHist[j][v-1][0] - sHist[j][v][0] - sHist[j][v+1][0]);
                        err += abs(fHist[i][v-1][1] + fHist[i][v][1] + fHist[i][v+1][1] - sHist[j][v-1][1] - sHist[j][v][1] - sHist[j][v+1][1]);
                        err += abs(fHist[i][v-1][2] + fHist[i][v][2] + fHist[i][v+1][2] - sHist[j][v-1][2] - sHist[j][v][2] - sHist[j][v+1][2]);
                    }
                    err += abs(fHist[i][254][0] + fHist[i][255][0] - sHist[j][254][0] - sHist[j][255][0]);
                    err += abs(fHist[i][254][1] + fHist[i][255][1] - sHist[j][254][1] - sHist[j][255][1]);
                    err += abs(fHist[i][254][2] + fHist[i][255][2] - sHist[j][254][2] - sHist[j][255][2]);
                    if (err < minErr) {
                        bestJ = j;
                        minErr = err;
                    }
                }
                // best match - calculate distance
//                for () {
//
//                }
            }
        }
    }

    return first;
}
typedef struct PatternPoints {
    Point2u16 red;
    Point2u16 green;
    Point2u16 blue;
    Point2u16 white;
} PatternPoints;

typedef struct PixToReal {
    float pixLen;
    float realLen;
} PixToReal;

typedef enum {
    MOVDESC_PV_RGB, // -30..0..30
    MOVDESC_PV_RBG, // 30..60..90
    MOVDESC_PV_BRG, // 90..120..150
    MOVDESC_PV_BGR, // 150..180..210
    MOVDESC_PV_GBR, // 210..240..270
    MOVDESC_PV_GRB, // 270..300..330
} MovDesc_PatternView;

typedef struct MovDesc_PatternOrientation {
    MovDesc_PatternView permutation;
    float angleX;
} MovDesc_PatternOrientation;

static MovDesc_PatternOrientation movdesc_detectOrientation(PatternPoints *points) {
    MovDesc_PatternOrientation orientation;
    if (points->blue.x <= points->green.x) {
        if (points->green.x < points->red.x) { //bgr
            orientation.permutation = MOVDESC_PV_BGR;
            orientation.angleX = 60*(points->green.x - points->blue.x)/(float)(points->red.x - points->blue.x) +150;
        } else {
            if (points->blue.x < points->red.x) { //brg
                orientation.permutation = MOVDESC_PV_BRG;
                orientation.angleX = 60*(points->red.x - points->blue.x)/(float)(points->green.x - points->blue.x) +90;
            } else { //rbg
                orientation.permutation = MOVDESC_PV_RBG;
                orientation.angleX = 60*(points->blue.x - points->red.x)/(float)(points->green.x - points->red.x) +30;
            }
        }
    } else {
        if (points->red.x <= points->green.x) { //rgb
            orientation.permutation = MOVDESC_PV_RGB;
            orientation.angleX = 60*(points->green.x - points->red.x)/(float)(points->blue.x - points->red.x) -30;
            if (orientation.angleX < 0) {
                orientation.angleX += 360;
            }
        } else {
            if (points->red.x <= points->blue.x) { //grb
                orientation.permutation = MOVDESC_PV_GRB;
                orientation.angleX = 60*(points->red.x - points->green.x)/(float)(points->blue.x - points->green.x) +270;
            } else { //gbr
                orientation.permutation = MOVDESC_PV_GBR;
                orientation.angleX = 60*(points->blue.x - points->green.x)/(float)(points->red.x - points->green.x) +210;
            }
        }
    }
    return orientation;
}

static void movdesc_estimatePixToReal(float *est0, float *est1, CalibNode *nodes, int nodesNb, uint16_t p0, uint16_t p1) {
    int samples = 0;
    *est0 = 0.0f;
    *est1 = 0.0f;
    for (int i = 0; i < nodesNb; i++) {
        for (int j = 0; j < nodesNb; j++) {
            if ((nodes[i].p0 < p0 && p0 < nodes[j].p0) || (nodes[j].p0 < p0 && p0 < nodes[i].p0)) {
                if ((nodes[i].p1 < p1 && p1 < nodes[j].p1) || (nodes[j].p1 < p1 && p1 < nodes[i].p1)) {
                    float dsi = (nodes[i].p0 - p0)*(nodes[i].p0 - p0) + (nodes[i].p1 - p1)*(nodes[i].p1 - p1);
                    float dsj = (nodes[j].p0 - p0)*(nodes[j].p0 - p0) + (nodes[j].p1 - p1)*(nodes[j].p1 - p1);
                    float d = dsi + dsj;
                    *est0 += nodes[i].pixToReal0*dsi/d + nodes[j].pixToReal0*dsj/d;
                    *est1 += nodes[i].pixToReal1*dsi/d + nodes[j].pixToReal1*dsj/d;
                    samples++;
                }
            }
        }
    }
    if (samples == 0) {
        *est0 = -1.0f;
        *est1 = -1.0f;
    } else {
        *est0 /= samples;
        *est1 /= samples;
    }
}
static PixToReal movdesc_calculatePixToRealHorizontal(PatternPoints *points, float armLen, MovDesc_PatternView orientation) {
    PixToReal p2r;
    float pixA = 0.0f;
    p2r.pixLen = 0.0f;

    Point2u16 left;
    Point2u16 center;
    Point2u16 right;

    switch (orientation) {
        case MOVDESC_PV_RGB:
        {
            left = points->red;
            center = points->green;
            right = points->blue;
        }
        break;
        case MOVDESC_PV_RBG:
        {
            left = points->red;
            center = points->blue;
            right = points->green;
        }
        break;
        case MOVDESC_PV_BRG:
        {
            left = points->blue;
            center = points->red;
            right = points->green;
        }
        break;
        case MOVDESC_PV_BGR:
        {
            left = points->blue;
            center = points->green;
            right = points->red;
        }
        break;
        case MOVDESC_PV_GBR:
        {
            left = points->green;
            center = points->blue;
            right = points->red;
        }
        break;
        case MOVDESC_PV_GRB:
        {
            left = points->green;
            center = points->red;
            right = points->blue;
        }
        break;
    }

    p2r.pixLen = sqrt((left.x - right.x)*(left.x - right.x) + (left.y - right.y)*(left.y - right.y));
    pixA = center.x - left.x;// sqrt((left.x - center.x)*(left.x - center.x) + (left.y - center.y)*(left.y - center.y));

    p2r.realLen = sqrtf(((armLen*armLen)/(1.0f - pixA/p2r.pixLen) + 1.0f));
    return p2r;
}
static PixToReal movdesc_calculatePixToRealVertical(PatternPoints *points, float armLen, MovDesc_PatternView orientation) {
    PixToReal p2r;
    float pixA = 0.0f;
    float dist = 0.0f;
    p2r.pixLen = 0.0f;

    Point2u16 bottom;
    Point2u16 middle;

    switch (orientation) {
        case MOVDESC_PV_RGB:
        {
            bottom.y = (points->red.y + points->blue.y)/2;
            bottom.x = points->green.x;
            middle = points->green;
        }
        break;
        case MOVDESC_PV_RBG:
        {
            bottom.y = (points->green.y + points->red.y)/2;
            bottom.x = points->blue.x;
            middle = points->blue;
        }
        break;
        case MOVDESC_PV_BRG:
        {
            bottom.y = (points->blue.y + points->green.y)/2;
            bottom.x = points->red.x;
            middle = points->red;
        }
        break;
        case MOVDESC_PV_BGR:
        {
            bottom.y = (points->blue.y + points->red.y)/2;
            bottom.x = points->green.x;
            middle = points->green;
        }
        break;
        case MOVDESC_PV_GBR:
        {
            bottom.y = (points->green.y + points->red.y)/2;
            bottom.x = points->blue.x;
            middle = points->blue;
        }
        break;
        case MOVDESC_PV_GRB:
        {
            bottom.y = (points->green.y + points->blue.y)/2;
            bottom.x = points->red.x;
            middle = points->red;
        }
        break;
    }

    if (bottom.y < middle.y) {
        Point2u16 tmp = bottom;
        bottom = middle;
        middle = tmp;
    }
    dist = sqrt((bottom.y - points->white.y)*(bottom.y - points->white.y) + (bottom.x - points->white.x)*(bottom.x - points->white.x));
    p2r.pixLen = dist;
    pixA = bottom.y - middle.y;
    p2r.realLen = sqrtf(((armLen*armLen)/(1.0f - pixA/p2r.pixLen) + 1.0f));
    return p2r;
}

static void movdesc_calibrateCamera(PatternPoints *p0, PixToReal *horizontal, PixToReal *vertical, MovDesc_PatternOrientation *orientation) { // only rotation in one direction

    *orientation = movdesc_detectOrientation(p0);
    *horizontal = movdesc_calculatePixToRealHorizontal(p0, 0.51f, orientation->permutation);
    *vertical = movdesc_calculatePixToRealVertical(p0, 0.51f*sqrtf(3)/2.0f, orientation->permutation);
    printf(" orientation: %d [%f]\n", orientation->permutation, orientation->angleX);

}
#define CLIP(X) ( (X) > 255 ? 255 : (X) < 0 ? 0 : X)
#define C(Y) ( (Y) - 16  )
#define D(U) ( (U) - 128 )
#define E(V) ( (V) - 128 )

#define YUV2R(Y, U, V) CLIP(( 298 * C(Y)              + 409 * E(V) + 128) >> 8)
#define YUV2G(Y, U, V) CLIP(( 298 * C(Y) - 100 * D(U) - 208 * E(V) + 128) >> 8)
#define YUV2B(Y, U, V) CLIP(( 298 * C(Y) + 516 * D(U)              + 128) >> 8)
static uint8_t movdesc_findPattern(MovdescContext *mdCtx, AVFrame *frame,
                                   PatternPoints *points, int altTh) {
    uint8_t redTh = mdCtx->redThreshold;
    uint8_t greenTh = mdCtx->greenThreshold;
    uint8_t blueTh = mdCtx->blueThreshold;
    uint8_t bottomThGreen = mdCtx->greenBottomThreshold;
    uint8_t bottomThRed = mdCtx->redBottomThreshold;
    uint8_t bottomThBlue = mdCtx->blueBottomThreshold;
    uint8_t ret = 0;
    int32_t blockSize = 32;
    int64_t xRValue, xGValue, xBValue, xWValue, yRValue, yGValue, yBValue, yWValue;
    int32_t rCount, gCount, bCount, wCount;

    memset(mdCtx->buffor.data[0], 0, mdCtx->buffor.height*mdCtx->buffor.linesize[0]);
    memset(mdCtx->buffor.data[1], 0, mdCtx->buffor.height*mdCtx->buffor.linesize[1]);
    memset(mdCtx->buffor.data[2], 0, mdCtx->buffor.height*mdCtx->buffor.linesize[2]);
    memset(mdCtx->buffor.data[3], 0, mdCtx->buffor.height*mdCtx->buffor.linesize[3]);

    int div = 6;
    // kacziro - todo: check std for many blobs
    switch (frame->format) {
        case AV_PIX_FMT_YUVA420P:
        case AV_PIX_FMT_YUV420P:
        {
            for (int32_t y = 0; y < frame->height; y++) {
                uint8_t *pMaskR = mdCtx->buffor.data[0] + y*mdCtx->buffor.linesize[0];
                uint8_t *pMaskG = mdCtx->buffor.data[1] + y*mdCtx->buffor.linesize[1];
                uint8_t *pMaskB = mdCtx->buffor.data[2] + y*mdCtx->buffor.linesize[2];
                uint8_t *pMaskW = mdCtx->buffor.data[3] + y*mdCtx->buffor.linesize[3];

                uint8_t *pY = frame->data[0] + y*frame->linesize[0];
                uint8_t *pU = frame->data[1] + (y >> 1)*frame->linesize[1];
                uint8_t *pV = frame->data[2] + (y >> 1)*frame->linesize[2];
                for (int x = 0; x < frame->width; x++) {
                    uint8_t b = YUV2B(pY[x], pU[x >> 1], pV[x >> 1]);
                    uint8_t g = YUV2G(pY[x], pU[x >> 1], pV[x >> 1]);
                    uint8_t r = YUV2R(pY[x], pU[x >> 1], pV[x >> 1]);

                    pMaskR[x] = (r > 210)*(g < 240)*(b < 180)*255;
                    pMaskG[x] = (g > 160)*(r < 60)*(b < 175)*255;
                    pMaskB[x] = (b > 250)*(g < 255)*(r < 160)*255;
                    pMaskW[x] = ((1-altTh)*((120 < r && r < 140) * (130 < g && g < 145) * (105 < b && b < 115)) ||
                                    altTh *((165 < r && r < 180) * (180 < g && g < 200) * (140 < b && b < 160)))*255;
                    //pMaskW[x] = (MAX(MAX(r, b), g) > 169)*255;
                }
            }
            //
            xRValue = xGValue = xBValue = xWValue = yRValue = yGValue = yBValue = yWValue = 0;
            rCount = gCount = bCount = wCount = 0;
            for (int32_t by = 0; by < frame->height/blockSize; by++) {
                for (int32_t bx = 0; bx < frame->width/blockSize; bx++) {
                    int32_t rc, gc, bc, wc;
                    int32_t xRV, xGV, xBV, xWV, yRV, yGV, yBV, yWV;
                    rc = gc = bc = wc = 0;
                    xRV = xGV = xBV = xWV = yRV = yGV = yBV = yWV = 0;
                    for (int32_t y = by*blockSize; y < MIN((by+1)*blockSize, frame->height); y++) {
                        uint8_t *pMaskR = mdCtx->buffor.data[0] + y*mdCtx->buffor.linesize[0];
                        uint8_t *pMaskG = mdCtx->buffor.data[1] + y*mdCtx->buffor.linesize[1];
                        uint8_t *pMaskB = mdCtx->buffor.data[2] + y*mdCtx->buffor.linesize[2];
                        uint8_t *pMaskW = mdCtx->buffor.data[3] + y*mdCtx->buffor.linesize[3];
                        for (int32_t x = bx*blockSize; x < MIN((bx+1)*blockSize, frame->width); x++) {
                            uint8_t cond = pMaskR[x]/255;
                            rc += cond;
                            xRV += cond*x;
                            yRV += cond*y;

                            cond = pMaskG[x]/255;
                            gc += cond;
                            xGV += cond*x;
                            yGV += cond*y;

                            cond = pMaskB[x]/255;
                            bc += cond;
                            xBV += cond*x;
                            yBV += cond*y;

                            cond = pMaskW[x]/255;
                            wc += cond;
                            xWV += cond*x;
                            yWV += cond*y;
                        }
                    }
                    if (wc < blockSize*blockSize/div) {
                        for (int32_t y = by*blockSize; y < MIN((by+1)*blockSize, frame->height); y++) {
                            memset(mdCtx->buffor.data[3] + y*mdCtx->buffor.linesize[3], 0, blockSize);
                        }
                    } else {
                        xWValue += xWV;
                        yWValue += yWV;
                        wCount += wc;
                    }
                    if (rc < blockSize*blockSize/div) {
                        for (int32_t y = by*blockSize; y < MIN((by+1)*blockSize, frame->height); y++) {
                            memset(mdCtx->buffor.data[0] + y*mdCtx->buffor.linesize[0], 0, blockSize);
                        }
                    } else {
                        xRValue += xRV;
                        yRValue += yRV;
                        rCount += rc;
                    }
                    if (gc < blockSize*blockSize/div) {
                        for (int32_t y = by*blockSize; y < MIN((by+1)*blockSize, frame->height); y++) {
                            memset(mdCtx->buffor.data[1] + y*mdCtx->buffor.linesize[1], 0, blockSize);
                        }
                    } else {
                        xGValue += xGV;
                        yGValue += yGV;
                        gCount += gc;
                    }
                    if (bc < blockSize*blockSize/div) {
                        for (int32_t y = by*blockSize; y < MIN((by+1)*blockSize, frame->height); y++) {
                            memset(mdCtx->buffor.data[2] + y*mdCtx->buffor.linesize[2], 0, blockSize);
                        }
                    } else {
                        xBValue += xBV;
                        yBValue += yBV;
                        bCount += bc;
                    }
                }
            }
        }
        break;
    }
    if (rCount > 0) {
        points->red.x = xRValue/rCount;
        points->red.y = yRValue/rCount;
    }
    if (gCount > 0) {
        points->green.x = xGValue/gCount;
        points->green.y = yGValue/gCount;
    }
    if (bCount > 0) {
        points->blue.x = xBValue/bCount;
        points->blue.y = yBValue/bCount;
    }
    if (wCount > 0) {
        points->white.x = xWValue/wCount;
        points->white.y = yWValue/wCount;
    }
    printf(" %d) r %d, %d\n %d) g %d, %d\n %d) b %d, %d\n %d) w %d, %d\n",
            rCount, points->red.x, points->red.y,
            gCount, points->green.x, points->green.y,
            bCount, points->blue.x, points->blue.y,
            wCount, points->white.x, points->white.y);
    if (rCount > 0 &&
        gCount > 0 &&
        bCount > 0 &&
        wCount > 0) {
        uint8_t *pY = frame->data[0];
        uint8_t *pU = frame->data[1];
        uint8_t *pV = frame->data[2];

        uint8_t yValue = pY[points->red.y*frame->linesize[0] + points->red.x];
        uint8_t uValue = pU[(points->red.y >> 1)*frame->linesize[1] + (points->red.x >> 1)];
        uint8_t vValue = pV[(points->red.y >> 1)*frame->linesize[2] + (points->red.x >> 1)];
        uint8_t b = YUV2B(yValue, uValue, vValue);
        uint8_t g = YUV2G(yValue, uValue, vValue);
        uint8_t r = YUV2R(yValue, uValue, vValue);

        ret = r > b && r > g;

        if (ret) {
            yValue = pY[points->green.y*frame->linesize[0] + points->green.x];
            uValue = pU[(points->green.y >> 1)*frame->linesize[1] + (points->green.x >> 1)];
            vValue = pV[(points->green.y >> 1)*frame->linesize[2] + (points->green.x >> 1)];
            b = YUV2B(yValue, uValue, vValue);
            g = YUV2G(yValue, uValue, vValue);
            r = YUV2R(yValue, uValue, vValue);
            ret = g > b && g > r;
        }
        if (ret) {
            yValue = pY[points->blue.y*frame->linesize[0] + points->blue.x];
            uValue = pU[(points->blue.y >> 1)*frame->linesize[1] + (points->blue.x >> 1)];
            vValue = pV[(points->blue.y >> 1)*frame->linesize[2] + (points->blue.x >> 1)];
            b = YUV2B(yValue, uValue, vValue);
            g = YUV2G(yValue, uValue, vValue);
            r = YUV2R(yValue, uValue, vValue);
            ret = b > g && b > r;
        }
    }
    return ret;
}

static AVFrame* movdesc_calibrate(AVFilterContext *ctx,
                                  AVFrame *mainFrame, const AVFrame *secondFrame)
{
    MovdescContext *mdCtx = ctx->priv;
    PatternPoints firstPPoints;
    PatternPoints secondPPoints;
    memset(&firstPPoints, 0, sizeof(firstPPoints));
    memset(&secondPPoints, 0, sizeof(secondPPoints));

    uint8_t first = movdesc_findPattern(mdCtx, mainFrame, &firstPPoints, 0);
    uint8_t second = movdesc_findPattern(mdCtx, secondFrame, &secondPPoints, 1);

    if (first && second) {
        char buf[200];
        PixToReal fX;
        PixToReal fY;
        PixToReal sX;
        PixToReal sY;
        MovDesc_PatternOrientation fOrient;
        MovDesc_PatternOrientation sOrient;
        float fDist = 0.0f;
        float sDist = 0.0f;
        movdesc_calibrateCamera(&firstPPoints, &fX, &fY, &fOrient);
        movdesc_calibrateCamera(&secondPPoints, &sX, &sY, &sOrient);

        {
            double fhWidth = mainFrame->width*fX.realLen/fX.pixLen;
            double fhHeight = mainFrame->height*fX.realLen/fX.pixLen;
            double fhDist = fhWidth / tan(((mdCtx->firstFov.horizontal/2)*M_PI)/180);
            double fvDist = fhHeight / tan(((mdCtx->firstFov.vertical/2)*M_PI)/180);
            fDist = (fhDist + fvDist)/2;
            printf("fdist: %f, %f -> [%f]\n", fhDist, fvDist, fDist);
        }
        {
            double shWidth = mainFrame->width*sX.realLen/sX.pixLen;
            double shHeight = mainFrame->height*sX.realLen/sX.pixLen;
            double shDist = shWidth / tan(((mdCtx->secFov.horizontal/2)*M_PI)/180);
            double svDist = shHeight / tan(((mdCtx->secFov.vertical/2)*M_PI)/180);
            sDist = (shDist + svDist)/2;
            printf("sdist: %f, %f -> [%f]\n", shDist, svDist, sDist);
        }
        memset(buf, 0, 200);
        sprintf(buf, "pts: %I64d, %I64d\n prop: %f x %f -> %f x %f\n %hux%hu -> %hux%hu\n %hux%hu -> %hux%hu\n %hux%hu -> %hux%hu\n %hux%hu -> %hux%hu\n%f | %f\n%f -> %f\n",
                      mainFrame->pts, secondFrame->pts,
                      fX.realLen/fX.pixLen, fY.realLen/fY.pixLen, sX.realLen/sX.pixLen, sY.realLen/sY.pixLen,
                      firstPPoints.red.x, firstPPoints.red.y, secondPPoints.red.x, secondPPoints.red.y,
                      firstPPoints.green.x, firstPPoints.green.y, secondPPoints.green.x, secondPPoints.green.y,
                      firstPPoints.blue.x, firstPPoints.blue.y, secondPPoints.blue.x, secondPPoints.blue.y,
                      firstPPoints.white.x, firstPPoints.white.y, secondPPoints.white.x, secondPPoints.white.y,
                      fDist, sDist,
                      fOrient.angleX, sOrient.angleX);
        fwrite(buf, 1, strlen(buf), mdCtx->calibdataFile);
    } else {
        printf("Pattern not found [%d, %d]\n", first, second);
    }

    memset(mainFrame->data[0], 0, mainFrame->height*mainFrame->linesize[0]);
    memset(mainFrame->data[1], 0, (mainFrame->height/2)*mainFrame->linesize[1]);
    memset(mainFrame->data[2], 0, (mainFrame->height/2)*mainFrame->linesize[2]);

    for (int y = 0; y < mainFrame->height; y++) {
        uint8_t *pMask = mdCtx->buffor.data[3] + y*mdCtx->buffor.linesize[3];
        uint8_t *pDst = mainFrame->data[0] + y*mainFrame->linesize[0];

        memcpy(pDst, pMask, mainFrame->width);
    }

    return mainFrame;
}

static int movdesc_filter_frame(AVFilterLink *inlink, AVFrame *inpicref)
{
    MovdescContext *s = inlink->dst->priv;
    av_log(inlink->dst, AV_LOG_DEBUG, "Incoming frame (time:%s) from link #%d\n", av_ts2timestr(inpicref->pts, &inlink->time_base), FF_INLINK_IDX(inlink));
    return ff_dualinput_filter_frame(&s->dinput, inlink, inpicref);
}

static int movdesc_request_frame(AVFilterLink *outlink)
{
    MovdescContext *s = outlink->src->priv;
    return ff_dualinput_request_frame(&s->dinput, outlink);
}
static av_cold int movdesc_init(AVFilterContext *ctx)
{
    pf_cam_setup();
    MovdescContext *mdCtx = ctx->priv;
        //ZEISS_80MM_F2D8
    memset(&mdCtx->firstFov, 0, sizeof(mdCtx->firstFov));
    memset(&mdCtx->secFov, 0, sizeof(mdCtx->secFov));

    if (mdCtx->cameraLens[0] == 'Z' &&
        mdCtx->cameraLens[1] == 'E' &&
        mdCtx->cameraLens[2] == 'I' &&
        mdCtx->cameraLens[3] == 'S' &&
        mdCtx->cameraLens[4] == 'S' &&
        mdCtx->cameraLens[5] == '_') {
            if (mdCtx->cameraLens[6] == '8' &&
                mdCtx->cameraLens[7] == '0' &&
                mdCtx->cameraLens[8] == 'M' &&
                mdCtx->cameraLens[9] == 'M' &&
                mdCtx->cameraLens[10] == '_') {
                if (mdCtx->cameraLens[11] == 'F' &&
                    mdCtx->cameraLens[12] == '2' &&
                    mdCtx->cameraLens[13] == 'D' &&
                    mdCtx->cameraLens[14] == '8' &&
                    mdCtx->cameraLens[15] == 0) {
                        printf("cam lens: %s\n", mdCtx->cameraLens);
                        mdCtx->firstFov = pfLensesProp[PF_LENS_ZEISS_80MM_F2D8].fov;
                        mdCtx->secFov = pfLensesProp[PF_LENS_ZEISS_80MM_F2D8].fov;
                    }
            }
    }

    mdCtx->dinput.repeatlast = 0;
    mdCtx->dinput.process = movdesc_calibrate;
    if (mdCtx->mode != NULL &&
        mdCtx->mode[0] == 'd' &&
        mdCtx->mode[1] == 'e' &&
        mdCtx->mode[2] == 't' &&
        mdCtx->mode[3] == 'e' &&
        mdCtx->mode[4] == 'c' &&
        mdCtx->mode[5] == 't' &&
        mdCtx->mode[6] == 0)
    {
        fpos_t pos;
        int index = 0;
        mdCtx->dinput.process = movdesc_detect;
        if (NULL == mdCtx->outFilename) {
            return 1;
        }
        mdCtx->outFile = fopen(mdCtx->outFilename, "w");
        mdCtx->calibdataFile = fopen(mdCtx->calibdataFile, "r");

        while(!feof(mdCtx->calibdataFile)) {
            PatternPoints firstPPoints;
            PatternPoints secondPPoints;
            float fX, fY, sX, sY;
            float fOrient, sOrient;
            float fDist, sDist;
            int64_t pts0, pts1;
            if (fscanf(mdCtx->calibdataFile, "pts: %I64d, %I64d\n prop: %f x %f -> %f x %f\n %hux%hu -> %hux%hu\n %hux%hu -> %hux%hu\n %hux%hu -> %hux%hu\n %hux%hu -> %hux%hu\n%f | %f\n%f -> %f\n",
                       &pts0, &pts1,
                       &fX, &fY, &sX, &sY,
                       &firstPPoints.red.x, &firstPPoints.red.y, &secondPPoints.red.x, &secondPPoints.red.y,
                       &firstPPoints.green.x, &firstPPoints.green.y, &secondPPoints.green.x, &secondPPoints.green.y,
                       &firstPPoints.blue.x, &firstPPoints.blue.y, &secondPPoints.blue.x, &secondPPoints.blue.y,
                       &firstPPoints.white.x, &firstPPoints.white.y, &secondPPoints.white.x, &secondPPoints.white.y,
                       &fDist, &sDist,
                       &fOrient, &sOrient)) {
                mdCtx->nodesX[index].pixToReal0 = fX;
                mdCtx->nodesX[index].pixToReal1 = sX;
                mdCtx->nodesX[index].p0 = (firstPPoints.red.x + firstPPoints.green.x + firstPPoints.blue.x + firstPPoints.white.x)*0.25f;
                mdCtx->nodesX[index].p1 = (secondPPoints.red.x + secondPPoints.green.x + secondPPoints.blue.x + secondPPoints.white.x)*0.25f;

                mdCtx->nodesY[index].pixToReal0 = fY;
                mdCtx->nodesY[index].pixToReal1 = sY;
                mdCtx->nodesY[index].p0 = (firstPPoints.red.y + firstPPoints.green.y + firstPPoints.blue.y + firstPPoints.white.y)*0.25f;
                mdCtx->nodesY[index].p1 = (secondPPoints.red.y + secondPPoints.green.y + secondPPoints.blue.y + secondPPoints.white.y)*0.25f;
                index++;
            }
            fgetpos(mdCtx->calibdataFile, &pos);
        }
        fclose(mdCtx->calibdataFile);
        mdCtx->calibdataFile = NULL;
    } else {
        printf("Movement description: calibration\n");
        mdCtx->calibdataFile = fopen(mdCtx->calibdataFilename, "w+");
    }

    return 0;
}

static int movdesc_query_formats(AVFilterContext *ctx)
{
    /* overlay formats contains alpha, for avoiding conversion with alpha information loss */
    static const enum AVPixelFormat main_pix_fmts_yuv420[] = {
        AV_PIX_FMT_YUV420P, AV_PIX_FMT_YUVJ420P, AV_PIX_FMT_YUVA420P,
        AV_PIX_FMT_NV12, AV_PIX_FMT_NV21,
        AV_PIX_FMT_NONE
    };
    static const enum AVPixelFormat overlay_pix_fmts_yuv420[] = {
        AV_PIX_FMT_YUVA420P, AV_PIX_FMT_NONE
    };

    AVFilterFormats *main_formats = NULL;
    AVFilterFormats *overlay_formats = NULL;
    int ret;
    if (!(main_formats    = ff_make_format_list(main_pix_fmts_yuv420)) ||
        !(overlay_formats = ff_make_format_list(overlay_pix_fmts_yuv420))) {
            ret = AVERROR(ENOMEM);
            goto fail;
    }

    if ((ret = ff_formats_ref(main_formats   , &ctx->inputs[PF_DM_MAIN]->out_formats   )) < 0 ||
        (ret = ff_formats_ref(overlay_formats, &ctx->inputs[PF_DM_SECOND]->out_formats)) < 0 ||
        (ret = ff_formats_ref(main_formats   , &ctx->outputs[PF_DM_MAIN]->in_formats   )) < 0)
            goto fail;


    return 0;
fail:
    if (main_formats)
        av_freep(&main_formats->formats);
    av_freep(&main_formats);
    if (overlay_formats)
        av_freep(&overlay_formats->formats);
    av_freep(&overlay_formats);
    return ret;
}
#define PF_MD_OFFSET(x) offsetof(MovdescContext, x)
#define PF_MD_FLAGS AV_OPT_FLAG_VIDEO_PARAM|AV_OPT_FLAG_FILTERING_PARAM

static const AVOption movdesc_options[] = {
    { "mode", "Modes: calib - calibration, detect - detection", PF_MD_OFFSET(mode), AV_OPT_TYPE_STRING, {.str = "calib"}, PF_MD_FLAGS },
    { "calibdata", "Calibration data file", PF_MD_OFFSET(calibdataFilename), AV_OPT_TYPE_STRING, {.str = "calib.dat"}, PF_MD_FLAGS },
    { "redTh", "Red threshold", PF_MD_OFFSET(redThreshold), AV_OPT_TYPE_INT, {.i64 = 250}, 0, 255, PF_MD_FLAGS },
    { "greenTh", "Green threshold", PF_MD_OFFSET(greenThreshold), AV_OPT_TYPE_INT, {.i64 = 250}, 0, 255, PF_MD_FLAGS },
    { "blueTh", "Blue threshold", PF_MD_OFFSET(blueThreshold), AV_OPT_TYPE_INT, {.i64 = 250}, 0, 255, PF_MD_FLAGS },
    { "redThB", "Red bottom threshold", PF_MD_OFFSET(redBottomThreshold), AV_OPT_TYPE_INT, {.i64 = 5}, 0, 255, PF_MD_FLAGS },
    { "greenThB", "Green bottom threshold", PF_MD_OFFSET(greenBottomThreshold), AV_OPT_TYPE_INT, {.i64 = 5}, 0, 255, PF_MD_FLAGS },
    { "blueThB", "Blue bottom threshold", PF_MD_OFFSET(blueBottomThreshold), AV_OPT_TYPE_INT, {.i64 = 5}, 0, 255, PF_MD_FLAGS },
    { "out", "Output file - movement description", PF_MD_OFFSET(outFilename), AV_OPT_TYPE_STRING, {.str = "movdesc.dat"}, PF_MD_FLAGS },
    { "camLens", "Type of camera lens; ex. ZEISS_80MM_F2D8", PF_MD_OFFSET(cameraLens), AV_OPT_TYPE_STRING, {.str = "ZEISS_80MM_F2D8"}, PF_MD_FLAGS },
    { NULL }
};

AVFILTER_DEFINE_CLASS(movdesc);

static const AVFilterPad avfilter_vf_movdesc_inputs[] = {
    {
        .name         = "main",
        .type         = AVMEDIA_TYPE_VIDEO,
        .config_props = movdesc_config_input_main,
        .filter_frame = movdesc_filter_frame,
        .needs_writable = 1,
    },
    {
        .name         = "second",
        .type         = AVMEDIA_TYPE_VIDEO,
        .config_props = movdesc_config_input_second,
        .filter_frame = movdesc_filter_frame,
    },
    { NULL }
};

static const AVFilterPad avfilter_vf_movdesc_outputs[] = {
    {
        .name          = "default",
        .type          = AVMEDIA_TYPE_VIDEO,
        .config_props  = movdesc_config_output,
        .request_frame = movdesc_request_frame,
    },
    { NULL }
};

AVFilter ff_vf_movdesc = {
    .name          = "movdesc",
    .description   = NULL_IF_CONFIG_SMALL("Stereo movement description - distance, velocity"),
    .init          = movdesc_init,
    .uninit        = movdesc_uninit,
    .priv_size     = sizeof(MovdescContext),
    .priv_class    = &movdesc_class,
    .query_formats = movdesc_query_formats,
    .process_command = movdesc_process_command,
    .inputs        = avfilter_vf_movdesc_inputs,
    .outputs       = avfilter_vf_movdesc_outputs,
    .flags         = AVFILTER_FLAG_SUPPORT_TIMELINE_INTERNAL,
};
#endif
