#ifndef PF_CAM_SETUP_H
#define PF_CAM_SETUP_H

typedef enum {
    PF_LENS_ZEISS_80MM_F2D8 = 0,
} PF_LENSES;


typedef struct FieldOfView {
    double vertical;
    double horizontal;
} FieldOfView;

typedef struct LensProp {
    FieldOfView fov;
} LensProp;

static LensProp pfLensesProp[1];

static void pf_cam_setup() {
    pfLensesProp[PF_LENS_ZEISS_80MM_F2D8].fov.horizontal = 87.310946;
    pfLensesProp[PF_LENS_ZEISS_80MM_F2D8].fov.vertical = 43.329731;// kacziro - fixme - values are inaccurate (!)
}

#endif
