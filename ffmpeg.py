import sys, os
from subprocess import call
if __name__ == "__main__":
    workingdir = os.path.dirname(__file__)
    if len(workingdir) == 0:
        workingdir = "."
    cmd = ["/".join([workingdir, "ffmpeg.exe"])]
    e = False
    if sys.argv[-1] == "echo":
        e = True
        sys.argv = sys.argv[:-1]
    filename = sys.argv[-1]
    fileext = filename.split(".")
    filename = ".".join(fileext[:-1])
    fileext = fileext[-1]
    sys.argv = sys.argv[:-1]
    sys.argv = sys.argv[1:]
    lastArg = ""
    for arg in sys.argv:
        cmd.append(arg)
        if lastArg[:4] == "-hd3":
            filename = filename + "_" + lastArg[4:] + arg
        lastArg = arg
    cmd.append(".".join([filename,fileext]))
    if True == e:
        print(" ".join(cmd))
    else:
        call(cmd, cwd=workingdir)