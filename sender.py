import threading, msvcrt
import sys, os, time, json
import requests

def readInput(caption, default, timeout = 1):
    class KeyboardThread(threading.Thread):
        def run(self):
            self.timedout = False
            self.input = ''
            while True:
                if msvcrt.kbhit():
                    chr = msvcrt.getche()
                    if ord(chr) >= 32:
                        self.input += str(chr)
                    print('')
                break
                if len(self.input) == 0 and self.timedout:
                    break
    result = default
    it = KeyboardThread()
    it.start()
    it.join(timeout)
    it.timedout = True
    if len(it.input) > 0:
        it.join()
        result = str(it.input)
    return result

def checkRun():
    a = readInput(None, None, 5)
    if a == str(b'q'):
        return False
    return True

def getUrl(server, fun):
    return '/'.join([server, 'm3u', fun])

def checkRequest(server):
    url = getUrl(server, 'is_request/')
    response = None
    try:
        response = requests.get(url)
        if response.ok:
            response = response.json()
            print('check:', response)
            if response["status"] == '0':
                if int(response['request']) > 0:
                    return True
            return False
    except:
        print("EXCEPTION!!")
        return False
    print("error:", response)
    return False

def sendFile(server, file, name):
    files = {name: open(file, 'rb')}
    response = None
    try:
        response = requests.post(getUrl(server, 'put_chunk/'), files=files)
        if response.ok:
            response = response.json()
            if response["status"] == '0':
                return True
    except:
        print("EXCEPTION!!")
        return False
    print("error:", response.text.encode(sys.stdout.encoding, errors='replace'))
    return False
def sendPlaylist(server, content):
    #files = {'playlist': open(file, 'rb')}
    files = {'playlist': content}
    response = None
    try:
        response = requests.post(getUrl(server, 'put_playlist/'), files=files)
        if response.ok:
            response = response.json()
            if response["status"] == '0':
                return True
    except:
        print("EXCEPTION!!")
        return False
    print("error:", response.text.encode(sys.stdout.encoding, errors='replace'))
    return False

def listFiles(path, ext):
    all = os.listdir(path)
    files = []
    for item in all:
        if os.path.isfile(os.path.join(path, item)) and item[-len(ext):] == ext:
            files.append(item)
    return files

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Usage:")
        print("  ", sys.argv[0], "<server_domain>", "[streams_folder]");
    else:
        run = True
        print("press [q] to quit")
        streamsDir = './streams/'
        if len(sys.argv) > 2:
            streamsDir = sys.argv[2]
        server = 'http://' + sys.argv[1]
        while run:
            content = open(os.path.join(streamsDir, 'playlist'), 'rb')
            sleepTime = 4
            files = listFiles(streamsDir, '.ts')
            files.sort()
            print(files)
            if checkRequest(server) and len(files) > 1:
                sleepTime = 0.5
                for f in files[:-1]:
                    print("send", f)
                    res = sendFile(server, os.path.join(streamsDir, f), f)
                    os.remove(os.path.join(streamsDir, f))
                res = sendPlaylist(server, content)
            elif len(files) > 5:
                os.remove(os.path.join(streamsDir, files[0]))
            time.sleep(sleepTime)
            run = checkRun()

