#ifndef HD3_DECODE_H
#define HD3_DECODE_H

#include "hd3/hd3.h"
#include <pthread.h>

typedef struct ScalingArg
{
    x264_image_t imgDst;
    x264_image_t imgSrc;
    int format;
    int method;
    int sbw;
    int sbh;
    int dbw;
    int dbh;
} ScalingArg;
static void* threadScaling(void* arg)
{
    ScalingArg* a = (ScalingArg*)arg;
    scale(&a->imgSrc, a->sbw, a->sbh, &a->imgDst, a->dbw, a->dbh, a->format, a->method);
    return NULL;
}
static int hd3_decode_init(AVCodecContext *ctx,
                           x264_param_t* pParams,
                           int maxDelayedFrames, int xCompression, int yCompression)
{
    int ret = hd3_init(ctx, pParams, maxDelayedFrames, xCompression, yCompression);
    hd3Data.pInputs = (FrameCopy*)malloc(sizeof(FrameCopy));
    initFrameCopy(hd3Data.ctx, hd3Data.pInputs, hd3Data.width, hd3Data.height, ctx->pix_fmt);// pParams->i_csp);
    //if (ctx->hd3.length < maxDelayedFrames)
    //{
    //    FrameBoxes *pTmp = (FrameBoxes*)malloc(maxDelayedFrames*sizeof(FrameBoxes));
    //    memset(pTmp, 0, sizeof(FrameBoxes)*maxDelayedFrames);
    //    memcpy(pTmp, ctx->hd3.allBoxes, sizeof(FrameBoxes)*ctx->hd3.length);
    //    free(ctx->hd3.allBoxes);
    //    ctx->hd3.allBoxes = pTmp;
    //    ctx->hd3.length = maxDelayedFrames;
    //}
    return ret;
}
static void hd3_decode_destroy()
{
    free(hd3Data.pInputs);
    //free(hd3Data.ctx->hd3.allBoxes);
    hd3_destroy();
}

static void copyAABB(x264_picture_t *buffor, AVFrame *frame, AABB rect) {
    if (X264_CSP_I420 == buffor->img.i_csp)
    {
        for (int y = rect.begin.y; y < rect.end.y - rect.begin.y; y+=2)
        {
            memcpy(rect.begin.x + buffor->img.plane[0] +     y*buffor->img.i_stride[0],
                   rect.begin.x + frame->data[0]      +     y*frame->linesize[0],
                   rect.end.x - rect.begin.x);//frame->linesize[0]);
            memcpy(rect.begin.x + buffor->img.plane[0] + (y+1)*buffor->img.i_stride[0],
                   rect.begin.x + frame->data[0] + (y+1)*frame->linesize[0],
                   rect.end.x - rect.begin.x);//frame->linesize[0]);
            memcpy(rect.begin.x/2 + buffor->img.plane[1] + (y/2)*buffor->img.i_stride[1],
                   rect.begin.x/2 + frame->data[1] + (y/2)*frame->linesize[1],
                   (rect.end.x - rect.begin.x)/2);//frame->linesize[1]);
            memcpy(rect.begin.x/2 + buffor->img.plane[2] + (y/2)*buffor->img.i_stride[2],
                   rect.begin.x/2 + frame->data[2] + (y/2)*frame->linesize[2],
                   (rect.end.x - rect.begin.x)/2);//frame->linesize[2]);
        }
    }
    else if (X264_CSP_RGB == buffor->img.i_csp || X264_CSP_BGR == buffor->img.i_csp)
    {
        for (int y = rect.begin.y; y < rect.end.y - rect.begin.y; y++)
        {
            memcpy(rect.begin.x + buffor->img.plane[0] + y*buffor->img.i_stride[0],
                   rect.begin.x + frame->data[0] + y*frame->linesize[0],
                   rect.end.x - rect.begin.x);//frame->linesize[0]);
        }
    }
    else if (X264_CSP_GBRP == buffor->img.i_csp)
    {
        for (int y = rect.begin.y; y < rect.end.y - rect.begin.y; y++)
        {
            memcpy(rect.begin.x + buffor->img.plane[0] + y*buffor->img.i_stride[0],
                   rect.begin.x + frame->data[0] + y*frame->linesize[0],
                   rect.end.x - rect.begin.x);//frame->linesize[0]);
            memcpy(rect.begin.x + buffor->img.plane[1] + y*buffor->img.i_stride[1],
                   rect.begin.x + frame->data[1] + y*frame->linesize[1],
                   rect.end.x - rect.begin.x);//frame->linesize[0]);
            memcpy(rect.begin.x + buffor->img.plane[2] + y*buffor->img.i_stride[2],
                   rect.begin.x + frame->data[2] + y*frame->linesize[2],
                   rect.end.x - rect.begin.x);//frame->linesize[0]);
        }
    }
    else
    {
        av_log(hd3Data.ctx, AV_LOG_ERROR, "Error: not implemented pixel format %d\n", buffor->img.i_csp);
    }
}

static int hd3_decode_update(AVFrame *frame)
{
    static int firstTime = 1;
    if (NULL != frame)
    {
        if (firstTime)
        {
            firstTime = 0;
            AABB r;
            r.begin.x = r.begin.y = 0;
            r.end.x = frame->width;
            r.end.y = frame->height;
            copyAABB(&hd3Data.buffor, frame, r);
        }
        //read boxes
        hd3Data.boxSet->pts = frame->pts;
        switch(hd3SourceType)
        {
            case HD3_SOURCE_TYPE_FILE:
            {
                //kacziro - todo - generate prevBoxSet for HD3_SOURCE_TYPE_FILE
                int iFrame = 0;
                if (0 == readBoxes(hd3Data.boxSet, &hd3Data.readerData, &iFrame, READ_BOXES_METHOD_GRID))
                {
                    fclose(hd3Data.readerData.pBoxSource);
                    hd3Data.readerData.pBoxSource = NULL;
                }
                if (hd3Data.readerData.align)
                {
                    alignBoxes(hd3Data.boxSet, 32*hd3Data.globalXCompression, hd3Data.globalYCompression, hd3Data.height);
                    if (HD3_AGGREGATION_CONST_AREA == hd3Data.aggregationMethod)
                    {
                        aggregation_constArea(hd3Data.boxSet, 32*hd3Data.globalXCompression, hd3Data.globalYCompression);
                    }
                    else if (HD3_AGGREGATION_MAX == hd3Data.aggregationMethod)
                    {
                        aggregation_max(hd3Data.boxSet);
                    }
                }
            }
            break;
            case HD3_SOURCE_TYPE_SEI:
            {
                HD3SEIData* pData = frame->hd3Data ? frame->hd3Data: hd3Data.pSei;
                if (NULL != pData)
                {
                    hd3Data.pSei = pData;
                    if (hd3Data.pSei->maxNumber > 0) {
                        int index = (frame->pts - 1)%hd3Data.pSei->maxNumber;

                        index = frame->pts%hd3Data.pSei->maxNumber;
                        hd3Data.boxSet->number = hd3Data.pSei->pBoxes[index].number;
                        {
                            hd3Data.boxSet->table = hd3Data.pSei->pBoxes[index].table;
                            for (int i = 0; i < hd3Data.pSei->pBoxes[index].number; i++)
                            {
                                hd3Data.boxSet->boxes[i] = hd3Data.pSei->pBoxes[index].boxes[i];
                            }
                        }
                        hd3Data.pSei->pBoxes[index].number = 0; // clear read boxes (for pkg decoding)

                        if (hd3Data.prevBoxSet) {
                            for (int prevI = 0; prevI < hd3Data.prevBoxSet->number; prevI++) { // kacziro - check - it works?
                                copyAABB(&hd3Data.buffor, frame, hd3Data.prevBoxSet->boxes[prevI].aabb);
                            }
                            hd3Data.prevBoxSet->number = hd3Data.boxSet->number;
                            {
                                hd3Data.prevBoxSet->table = hd3Data.boxSet->table;
                                for (int i = 0; i < hd3Data.boxSet->number; i++)
                                {
                                    hd3Data.prevBoxSet->boxes[i] = hd3Data.boxSet->boxes[i];
                                }
                            }
                        }
                    } else {
                        hd3Data.boxSet->number = 0;
                    }
                }
            }
            break;
        }
        hd3Data.pInputs->format = frame->format;
        hd3Data.pInputs->pts = frame->pts;
        hd3Data.pInputs->width = frame->width;
        hd3Data.pInputs->height = frame->height;
        hd3Data.pInputs->linesize[0] = frame->linesize[0];
        hd3Data.pInputs->linesize[1] = frame->linesize[1];
        hd3Data.pInputs->linesize[2] = frame->linesize[2];
        hd3Data.pInputs->linesize[3] = frame->linesize[3];
        if (X264_CSP_I420 == hd3Data.buffor.img.i_csp)
        {
            memcpy(hd3Data.pInputs->data[0], frame->data[0], frame->linesize[0]*frame->height);
            memcpy(hd3Data.pInputs->data[1], frame->data[1], frame->linesize[1]*(frame->height/2));
            memcpy(hd3Data.pInputs->data[2], frame->data[2], frame->linesize[2]*(frame->height/2));
        }
        else if (X264_CSP_RGB == hd3Data.buffor.img.i_csp || X264_CSP_BGR == hd3Data.buffor.img.i_csp)
        {
            memcpy(hd3Data.pInputs->data[0], frame->data[0], frame->linesize[0]*frame->height);
        }
        else if (X264_CSP_GBRP == hd3Data.buffor.img.i_csp)
        {
            memcpy(hd3Data.pInputs->data[0], frame->data[0], frame->linesize[0]*frame->height);
            memcpy(hd3Data.pInputs->data[1], frame->data[1], frame->linesize[1]*frame->height);
            memcpy(hd3Data.pInputs->data[2], frame->data[2], frame->linesize[2]*frame->height);
        }
        else
        {
            av_log(hd3Data.ctx, AV_LOG_ERROR, "Error: not implemented pixel format %d\n", hd3Data.buffor.img.i_csp);
        }

        //hd3Data.step = 0;
    }
    return 1; // always render
}
static uint8_t flags[60000];
static int hd3_decode_render(x264_picture_t* pFrame)
{
    int updateInput = 0;
    if (NULL != pFrame)
    {
        int maxStep = hd3Data.globalXCompression*hd3Data.globalYCompression;
        pFrame->i_pts = hd3Data.pInputs->pts*maxStep + hd3Data.step;

        if (X264_CSP_I420 == hd3Data.buffor.img.i_csp)
        {
            int halfW = hd3Data.buffor.img.i_stride[1];
            int x, y, index;
            for (int by = 0; by < hd3Data.height; by += hd3Data.globalYCompression)
            {
                y = hd3Data.pPixOrder[hd3Data.step].y + by;
                {
                    uint8_t* pYData = hd3Data.buffor.img.plane[0] + hd3Data.buffor.img.i_stride[0]*y;
                    uint8_t* pUData = hd3Data.buffor.img.plane[1] + hd3Data.buffor.img.i_stride[1]*(y/2);
                    uint8_t* pVData = hd3Data.buffor.img.plane[2] + hd3Data.buffor.img.i_stride[2]*(y/2);
                    uint8_t* pInY = hd3Data.pInputs->data[0] + hd3Data.pInputs->linesize[0]*y;
                    uint8_t* pInU = hd3Data.pInputs->data[1] + hd3Data.pInputs->linesize[1]*(y/2);
                    uint8_t* pInV = hd3Data.pInputs->data[2] + hd3Data.pInputs->linesize[2]*(y/2);
                    for (int bx = 0; bx < hd3Data.width; bx += hd3Data.globalXCompression)
                    {
                        x = hd3Data.pPixOrder[hd3Data.step].x + bx;

                        pYData[x] = pInY[x];
                        x /= 2;
                        pUData[x] = pInU[x];
                        pVData[x] = pInV[x];
                    }
                }
            }
        }
        else if (X264_CSP_RGB == hd3Data.buffor.img.i_csp || X264_CSP_BGR == hd3Data.buffor.img.i_csp)
        {
            int x, y;
            for (int by = 0; by < hd3Data.height; by += hd3Data.globalYCompression)
            {
                y = hd3Data.pPixOrder[hd3Data.step].y + by;
                {
                    uint8_t* pRGBData = hd3Data.buffor.img.plane[0] + y*hd3Data.buffor.img.i_stride[0];
                    uint8_t* pIn = hd3Data.pInputs->data[0] + y*hd3Data.pInputs->linesize[0];
                    for (int bx = 0; bx < hd3Data.width; bx += hd3Data.globalXCompression)
                    {
                        x = 3*(hd3Data.pPixOrder[hd3Data.step].x + bx);
                        memcpy(pRGBData + x, pIn + x, 3);
                    }
                }
            }
        }
        else if (X264_CSP_GBRP == hd3Data.buffor.img.i_csp) // to rgb
        {
            int x, y, index;
            for (int by = 0; by < hd3Data.height; by += hd3Data.globalYCompression)
            {
                y = hd3Data.pPixOrder[hd3Data.step].y + by;
                {
                    uint8_t* pGOut = hd3Data.buffor.img.plane[0] + y*hd3Data.buffor.img.i_stride[0];
                    uint8_t* pBOut = hd3Data.buffor.img.plane[1] + y*hd3Data.buffor.img.i_stride[1];
                    uint8_t* pROut = hd3Data.buffor.img.plane[2] + y*hd3Data.buffor.img.i_stride[2];
                    uint8_t* pGIn = hd3Data.pInputs->data[0] + y*hd3Data.pInputs->linesize[0];
                    uint8_t* pBIn = hd3Data.pInputs->data[1] + y*hd3Data.pInputs->linesize[1];
                    uint8_t* pRIn = hd3Data.pInputs->data[2] + y*hd3Data.pInputs->linesize[2];
                    for (int bx = 0; bx < hd3Data.width; bx += hd3Data.globalXCompression)
                    {
                        x = hd3Data.pPixOrder[hd3Data.step].x + bx;
                        pROut[x] = pRIn[x];
                        pGOut[x] = pGIn[x];
                        pBOut[x] = pBIn[x];
                    }
                }
            }
        }
        else
        {
            av_log(hd3Data.ctx, AV_LOG_ERROR, "Error: not implemented pixel format %d\n", hd3Data.buffor.img.i_csp);//pFrame->img.i_csp);
        }
        //copy movement
        int y = hd3Data.step/hd3Data.globalXCompression; // kacziro - fixme - x,y for less box compression + interpolation (dynamic quant?)
        int x = hd3Data.step - y*hd3Data.globalXCompression;

        int threadsNumber = 1;// av_cpu_count();
        pthread_t threads[1];
        ScalingArg threadsArg[1];

        uint8_t xReduction, yReduction;
        int sbx, sby, dbx, dby;

        memset(flags, 0, 60000);

        for (int i = 0; i < hd3Data.boxSet->number;)
        {
            memset(threads, 0, sizeof(pthread_t)*threadsNumber);
            for (int thNr = 0; thNr < threadsNumber && i < hd3Data.boxSet->number; thNr++, i++)
            {
                threadsArg[thNr].imgSrc.i_csp = threadsArg[thNr].imgDst.i_csp = hd3Data.buffor.img.i_csp;
                threadsArg[thNr].imgDst.i_stride[0] = hd3Data.buffor.img.i_stride[0];
                threadsArg[thNr].imgDst.i_stride[1] = hd3Data.buffor.img.i_stride[1];
                threadsArg[thNr].imgDst.i_stride[2] = hd3Data.buffor.img.i_stride[2];
                threadsArg[thNr].imgDst.i_stride[3] = hd3Data.buffor.img.i_stride[3];
                threadsArg[thNr].imgDst.i_plane = hd3Data.buffor.img.i_plane;
                threadsArg[thNr].method = hd3Data.boxSet->table.cells[hd3Data.boxSet->boxes[i].code].method;
                xReduction = hd3Data.boxSet->table.cells[hd3Data.boxSet->boxes[i].code].xReduction;
                yReduction = hd3Data.boxSet->table.cells[hd3Data.boxSet->boxes[i].code].yReduction;

                sbx = hd3Data.boxSet->boxes[i].aabb.begin.x;
                threadsArg[thNr].sbw = hd3Data.boxSet->boxes[i].aabb.end.x - sbx;
                sby = hd3Data.boxSet->boxes[i].aabb.begin.y;
                threadsArg[thNr].sbh = hd3Data.boxSet->boxes[i].aabb.end.y - sby;

                dbx = hd3Data.boxSet->boxes[i].dstAabb.begin.x;
                threadsArg[thNr].dbw = hd3Data.boxSet->boxes[i].dstAabb.end.x - dbx;
                dby = hd3Data.boxSet->boxes[i].dstAabb.begin.y;
                threadsArg[thNr].dbh = hd3Data.boxSet->boxes[i].dstAabb.end.y - dby;

                flags[dbx/32 + (dby/32)*hd3Data.width/32] = 1;
                threadsArg[thNr].sbw = threadsArg[thNr].sbw/xReduction;
                threadsArg[thNr].sbh = threadsArg[thNr].sbh/yReduction;
                if (threadsArg[thNr].dbw > 0 && threadsArg[thNr].dbh > 0)
                {
                    threadsArg[thNr].imgSrc.i_plane = pFrame->img.i_plane;
                    threadsArg[thNr].imgSrc.i_stride[0] = hd3Data.pInputs->linesize[0];
                    threadsArg[thNr].imgSrc.i_stride[1] = hd3Data.pInputs->linesize[1];
                    threadsArg[thNr].imgSrc.i_stride[2] = hd3Data.pInputs->linesize[2];
                    threadsArg[thNr].imgSrc.i_stride[3] = hd3Data.pInputs->linesize[3];
                    threadsArg[thNr].imgSrc.plane[0] = hd3Data.pInputs->data[0];
                    threadsArg[thNr].imgSrc.plane[1] = hd3Data.pInputs->data[1];
                    threadsArg[thNr].imgSrc.plane[2] = hd3Data.pInputs->data[2];

                    threadsArg[thNr].imgDst.plane[0] = hd3Data.buffor.img.plane[0];
                    threadsArg[thNr].imgDst.plane[1] = hd3Data.buffor.img.plane[1];
                    threadsArg[thNr].imgDst.plane[2] = hd3Data.buffor.img.plane[2];
                    if (pFrame->img.i_csp == X264_CSP_I420) {
                        threadsArg[thNr].imgDst.plane[0] += dbx   + threadsArg[thNr].imgDst.i_stride[0]*dby;
                        threadsArg[thNr].imgDst.plane[1] += dbx/2 + threadsArg[thNr].imgDst.i_stride[1]*(dby/2);
                        threadsArg[thNr].imgDst.plane[2] += dbx/2 + threadsArg[thNr].imgDst.i_stride[2]*(dby/2);

                        threadsArg[thNr].imgSrc.plane[0] +=  sbx + (threadsArg[thNr].sbw)*x    + threadsArg[thNr].imgSrc.i_stride[0]*( threadsArg[thNr].sbh*y + sby);
                        threadsArg[thNr].imgSrc.plane[1] += (sbx + (threadsArg[thNr].sbw)*x)/2 + threadsArg[thNr].imgSrc.i_stride[1]*((threadsArg[thNr].sbh*y + sby)/2);
                        threadsArg[thNr].imgSrc.plane[2] += (sbx + (threadsArg[thNr].sbw)*x)/2 + threadsArg[thNr].imgSrc.i_stride[2]*((threadsArg[thNr].sbh*y + sby)/2);
                    } else if (pFrame->img.i_csp == X264_CSP_RGB) {
                        threadsArg[thNr].imgDst.plane[0] +=  dbx*3 + threadsArg[thNr].imgDst.i_stride[0]*dby;
                        threadsArg[thNr].imgSrc.plane[0] += (sbx + (threadsArg[thNr].sbw)*x)*3 + threadsArg[thNr].imgSrc.i_stride[0]*(threadsArg[thNr].sbh*y + sby);
                    } else if (pFrame->img.i_csp == X264_CSP_GBRP) {
                        int dstOffset = dbx + threadsArg[thNr].imgDst.i_stride[0]*dby;
                        int srcOffset = sbx + (threadsArg[thNr].sbw)*x + threadsArg[thNr].imgSrc.i_stride[0]*(threadsArg[thNr].sbh*y + sby);
                        threadsArg[thNr].imgDst.plane[0] += dstOffset;
                        threadsArg[thNr].imgSrc.plane[0] += srcOffset;
                        threadsArg[thNr].imgDst.plane[1] += dstOffset;
                        threadsArg[thNr].imgSrc.plane[1] += srcOffset;
                        threadsArg[thNr].imgDst.plane[2] += dstOffset;
                        threadsArg[thNr].imgSrc.plane[2] += srcOffset;
                    }
                    threadsArg[thNr].format = hd3Data.pInputs->format;
                    //pthread_create(threads + thNr, NULL, threadScaling, threadsArg + thNr);
                    threadScaling(threadsArg + thNr);
                    if (sbx != dbx || sby != dby) { // kacziro - todo - do this once for hd3Data.buffor for dithering
                        ScalingArg flatScaling;
                        int sfx, sfy, dfx, dfy;
                        flatScaling.imgSrc.i_csp = flatScaling.imgDst.i_csp = hd3Data.buffor.img.i_csp;
                        flatScaling.imgDst.i_stride[0] = hd3Data.buffor.img.i_stride[0];
                        flatScaling.imgDst.i_stride[1] = hd3Data.buffor.img.i_stride[1];
                        flatScaling.imgDst.i_stride[2] = hd3Data.buffor.img.i_stride[2];
                        flatScaling.imgDst.i_stride[3] = hd3Data.buffor.img.i_stride[3];
                        flatScaling.imgDst.i_plane = hd3Data.buffor.img.i_plane;
                        flatScaling.method = hd3Data.boxSet->table.cells[hd3Data.boxSet->boxes[i].code].method;

                        sfx = hd3Data.boxSet->boxes[i].dstAabb.begin.x;
                        flatScaling.sbw = hd3Data.boxSet->boxes[i].dstAabb.end.x - sfx;//threadsArg[thNr].dbw;
                        sfy = hd3Data.boxSet->boxes[i].dstAabb.begin.y;
                        flatScaling.sbh = hd3Data.boxSet->boxes[i].dstAabb.end.y - sfy;//threadsArg[thNr].dbh;

                        dfx = hd3Data.boxSet->boxes[i].aabb.begin.x;
                        flatScaling.dbw = hd3Data.boxSet->boxes[i].aabb.end.x - dfx;
                        dfy = hd3Data.boxSet->boxes[i].aabb.begin.y;
                        flatScaling.dbh = hd3Data.boxSet->boxes[i].aabb.end.y - dfy;

                        flatScaling.imgSrc.i_plane = pFrame->img.i_plane;
                        flatScaling.imgSrc.i_stride[0] = hd3Data.pInputs->linesize[0];
                        flatScaling.imgSrc.i_stride[1] = hd3Data.pInputs->linesize[1];
                        flatScaling.imgSrc.i_stride[2] = hd3Data.pInputs->linesize[2];
                        flatScaling.imgSrc.i_stride[3] = hd3Data.pInputs->linesize[3];
                        flatScaling.imgSrc.plane[0] = hd3Data.pInputs->data[0];
                        flatScaling.imgSrc.plane[1] = hd3Data.pInputs->data[1];
                        flatScaling.imgSrc.plane[2] = hd3Data.pInputs->data[2];

                        flatScaling.imgDst.plane[0] = hd3Data.buffor.img.plane[0];
                        flatScaling.imgDst.plane[1] = hd3Data.buffor.img.plane[1];
                        flatScaling.imgDst.plane[2] = hd3Data.buffor.img.plane[2];

                       if (pFrame->img.i_csp == X264_CSP_I420) {
                            flatScaling.imgDst.plane[0] += dfx   + flatScaling.imgDst.i_stride[0]*dfy;
                            flatScaling.imgDst.plane[1] += dfx/2 + flatScaling.imgDst.i_stride[1]*(dfy/2);
                            flatScaling.imgDst.plane[2] += dfx/2 + flatScaling.imgDst.i_stride[2]*(dfy/2);

                            flatScaling.imgSrc.plane[0] += sfx + flatScaling.imgSrc.i_stride[0]*(sfy);
                            flatScaling.imgSrc.plane[1] += sfx/2 + flatScaling.imgSrc.i_stride[1]*(sfy/2);
                            flatScaling.imgSrc.plane[2] += sfx/2 + flatScaling.imgSrc.i_stride[2]*(sfy/2);
                        } else if (pFrame->img.i_csp == X264_CSP_RGB) {
                            flatScaling.imgDst.plane[0] += dfx*3 + flatScaling.imgDst.i_stride[0]*dby;
                            flatScaling.imgSrc.plane[0] += sfx*3 + flatScaling.imgSrc.i_stride[0]*(sfy);
                        } else if (pFrame->img.i_csp == X264_CSP_GBRP) {
                            int dstOffset = dfx + flatScaling.imgDst.i_stride[0]*dfy;
                            int srcOffset = sfx + flatScaling.imgSrc.i_stride[0]*sfy;
                            flatScaling.imgDst.plane[0] += dstOffset;
                            flatScaling.imgSrc.plane[0] += srcOffset;
                            flatScaling.imgDst.plane[1] += dstOffset;
                            flatScaling.imgSrc.plane[1] += srcOffset;
                            flatScaling.imgDst.plane[2] += dstOffset;
                            flatScaling.imgSrc.plane[2] += srcOffset;
                        }
                        flatScaling.format = hd3Data.pInputs->format;
                        //pthread_create(threads + thNr, NULL, threadScaling, &flatScaling);
                        //printf("(%d, %d -> %d, %d)\n", sfx, sfy, dfx, dfy);
                        //printf("[%d, %d -> %d, %d]\n", flatScaling.sbw, flatScaling.sbh, flatScaling.dbw, flatScaling.dbh);
                        threadScaling(&flatScaling);
                    }
                }
            }

            for (int thNr = 0; thNr < threadsNumber; thNr++)
            {
                if (threads[thNr])
                {
                    pthread_join(threads[thNr], NULL);
                }
            }
        }

        //border filter
        //if (hd3SourceType == HD3_SOURCE_TYPE_FILE)
        {
            if (X264_CSP_I420 == pFrame->img.i_csp)
            {
                for (int y = 0; y < hd3Data.height; y++)
                {
                    uint8_t *pD0 = hd3Data.buffor.img.plane[0] +     y*hd3Data.buffor.img.i_stride[0];
                    uint8_t *pD1 = hd3Data.buffor.img.plane[1] + (y/2)*hd3Data.buffor.img.i_stride[1];
                    uint8_t *pD2 = hd3Data.buffor.img.plane[2] + (y/2)*hd3Data.buffor.img.i_stride[2];
                    for (int x = 32; x < hd3Data.width; x += 32) {
                        if (flags[x/32 + (y/32)*hd3Data.width/32]) {
                            pD0[x - 1] = (pD0[x - 1]*1 + pD0[x - 2]*5 + pD0[x + 1]*4)/10;
                            pD0[x]     = (pD0[x]*1     + pD0[x - 2]*4 + pD0[x + 1]*5)/10;

                            pD1[x/2 - 1] = (pD1[x/2 - 1]*1 + pD1[x/2 - 2]*5 + pD1[x/2 + 1]*4)/10;
                            pD1[x/2]     = (pD1[x/2]*1     + pD1[x/2 - 2]*4 + pD1[x/2 + 1]*5)/10;

                            pD2[x/2 - 1] = (pD2[x/2 - 1]*1 + pD2[x/2 - 2]*5 + pD2[x/2 + 1]*4)/10;
                            pD2[x/2]     = (pD2[x/2]*1     + pD2[x/2 - 2]*4 + pD2[x/2 + 1]*5)/10;
                        }
                    }
                }
                uint8_t *pD0 = hd3Data.buffor.img.plane[0];
                uint8_t *pD1 = hd3Data.buffor.img.plane[1];
                uint8_t *pD2 = hd3Data.buffor.img.plane[2];
                int s0 = hd3Data.buffor.img.i_stride[0];
                int s1 = hd3Data.buffor.img.i_stride[1];
                int s2 = hd3Data.buffor.img.i_stride[2];
                for (int y = 32; y < hd3Data.height; y += 32) {
                    int yy = y >> 1;
                    int yym1 = (y - 1) >> 1;
                    int yym2 = (y - 2) >> 1;
                    int yyp1 = (y + 1) >> 1;
                    for (int x = 0; x < hd3Data.width; x++) {
                        if (flags[x/32 + (y/32)*hd3Data.width/32]) {
                            pD0[x + (y - 1)*s0] = (pD0[x + (y - 1)*s0]*1 + pD0[x + (y - 2)*s0]*5 + pD0[x + (y + 1)*s0]*4)/10;
                            pD0[x + (y)*s0]     = (pD0[x + (y)*s0]*1     + pD0[x + (y - 2)*s0]*4 + pD0[x + (y + 1)*s0]*5)/10;

                            pD1[x/2 + yym1*s1] = (pD1[x/2 + yym1*s1]*1 + pD1[x/2 + yym2*s1]*5 + pD1[x/2 + yyp1*s1]*4)/10;
                            pD1[x/2 + yy*s1]   = (pD1[x/2 + yy*s1]*1   + pD1[x/2 + yym2*s1]*4 + pD1[x/2 + yyp1*s1]*5)/10;

                            pD2[x/2 + yym1*s2] = (pD2[x/2 + yym1*s2]*1 + pD2[x/2 + yym2*s2]*5 + pD2[x/2 + yyp1*s2]*4)/10;
                            pD2[x/2 + yy*s2]   = (pD2[x/2 + yy*s2]*1   + pD2[x/2 + yym2*s2]*4 + pD2[x/2 + yyp1*s2]*5)/10;
                        }
                    }
                }
            } else if (pFrame->img.i_csp == X264_CSP_RGB || pFrame->img.i_csp == X264_CSP_BGR) {
                for (int y = 0; y < hd3Data.height; y++) {
                    uint8_t *pD0 = hd3Data.buffor.img.plane[0] + y*hd3Data.buffor.img.i_stride[0];
                    for (int x = 32; x < hd3Data.width; x += 32) {
                        if (flags[x/32 + (y/32)*hd3Data.width/32]) {
                            pD0[3*x - 3] = (pD0[3*x - 6]*5 + pD0[3*x - 3]*1 + pD0[3*x + 3]*4)/10;
                            pD0[3*x - 2] = (pD0[3*x - 5]*5 + pD0[3*x - 2]*1 + pD0[3*x + 4]*4)/10;
                            pD0[3*x - 1] = (pD0[3*x - 4]*5 + pD0[3*x - 1]*1 + pD0[3*x + 5]*4)/10;

                            pD0[3*x]     = (pD0[3*x - 6]*4 + pD0[3*x]*1     + pD0[3*x + 3]*5)/10;
                            pD0[3*x + 1] = (pD0[3*x - 5]*4 + pD0[3*x + 1]*1 + pD0[3*x + 4]*5)/10;
                            pD0[3*x + 2] = (pD0[3*x - 4]*4 + pD0[3*x + 2]*1 + pD0[3*x + 5]*5)/10;
                        }
                    }
                }
                uint8_t *pD0 = hd3Data.buffor.img.plane[0];
                int s0 = hd3Data.buffor.img.i_stride[0];
                for (int y = 32; y < hd3Data.height; y += 32) {
                    for (int x = 0; x < hd3Data.width; x++) {
                        if (flags[x/32 + (y/32)*hd3Data.width/32]) {
                            pD0[3*x + (y - 1)*s0]     = (pD0[3*x + (y - 1)*s0]*1 + pD0[3*x + (y - 2)*s0]*5 + pD0[3*x + (y + 1)*s0]*4)/10;
                            pD0[3*x + 1 + (y - 1)*s0] = (pD0[3*x + (y - 1)*s0]*1 + pD0[3*x + (y - 2)*s0]*5 + pD0[3*x + (y + 1)*s0]*4)/10;
                            pD0[3*x + 2 + (y - 1)*s0] = (pD0[3*x + (y - 1)*s0]*1 + pD0[3*x + (y - 2)*s0]*5 + pD0[3*x + (y + 1)*s0]*4)/10;

                            pD0[3*x + y*s0]           = (pD0[3*x + y*s0]*1       + pD0[3*x + (y - 2)*s0]*4     + pD0[3*x + (y + 1)*s0]*5)/10;
                            pD0[3*x + 1 + y*s0]       = (pD0[3*x + 1 + y*s0]*1   + pD0[3*x + 1 + (y - 2)*s0]*4 + pD0[3*x + 1 + (y + 1)*s0]*5)/10;
                            pD0[3*x + 2 + y*s0]       = (pD0[3*x + 2 + y*s0]*1   + pD0[3*x + 2 + (y - 2)*s0]*4 + pD0[3*x + 2 + (y + 1)*s0]*5)/10;
                        }
                    }
                }
            }
        }
        pFrame->i_type = hd3Data.buffor.i_type;
        pFrame->img.i_plane = hd3Data.buffor.img.i_plane;
        pFrame->img.i_csp = hd3Data.buffor.img.i_csp; //todo out diff pix_fmt then input
        int baseSize = hd3Data.width*hd3Data.height;
        if (X264_CSP_I420 == pFrame->img.i_csp)
        {
            memcpy(pFrame->img.plane[0], hd3Data.buffor.img.plane[0], baseSize);
            baseSize /= 4;
            memcpy(pFrame->img.plane[1], hd3Data.buffor.img.plane[1], baseSize);
            memcpy(pFrame->img.plane[2], hd3Data.buffor.img.plane[2], baseSize);
        }
        else if (pFrame->img.i_csp == X264_CSP_RGB || pFrame->img.i_csp == X264_CSP_BGR)
        {
            memcpy(pFrame->img.plane[0], hd3Data.buffor.img.plane[0], baseSize*3);
        }
        else if (pFrame->img.i_csp == X264_CSP_GBRP)
        {
            memcpy(pFrame->img.plane[0], hd3Data.buffor.img.plane[0], baseSize);
            memcpy(pFrame->img.plane[1], hd3Data.buffor.img.plane[1], baseSize);
            memcpy(pFrame->img.plane[2], hd3Data.buffor.img.plane[2], baseSize);
        }

        hd3Data.step++;
        hd3Data.buffor.i_type = X264_TYPE_AUTO;
        if (hd3Data.step >= hd3Data.globalXCompression*hd3Data.globalYCompression)
        {
            hd3Data.step = 0;
            if (0 || (0 == hd3Data.frameCountDown && 0 != hd3Data.gopSize)) //from hd3 alg.
            {
                hd3Data.buffor.i_type = X264_TYPE_IDR;
                hd3Data.frameCountDown = hd3Data.gopSize;
            }
            hd3Data.frameCountDown--;
            updateInput = 1;
            hd3Data.boxSet->number = 0;
        }
    }
    return updateInput;
}
#endif
