#ifndef HD3_FLAT_AREA_H
#define HD3_FLAT_AREA_H

#include "libavcodec/avcodec.h"
#include "hd3/movement.h"
#include "hd3/frame_copy.h"

static float linFun(float x, float x0, float y0, float x1, float y1) {
    float a = (y1 - y0) / (x1 - x0);
    float b = y0 - a*x0;
    return a*x + b;
}

static int findAreas_YUV420P(AABB *areas, FrameCopy *frame, FrameBoxes *movement, int blockSize, float darkThresold, float lightThreshold) {
    int nb = 0;
    for (int by = 0; by < frame->height - blockSize; by += blockSize) {
        for (int bx = 0; bx < frame->width - blockSize; bx += blockSize) {
            int sum = 0;
            uint8_t flag = 0;
            for (int i = 0; i < movement->number; i++) {
                if ((movement->boxes[i].aabb.begin.x - blockSize <= bx && bx <= movement->boxes[i].aabb.end.x + blockSize &&
                    movement->boxes[i].aabb.begin.y - blockSize <= by && by <= movement->boxes[i].aabb.end.y + blockSize) ||
                    (movement->boxes[i].aabb.begin.x - blockSize <= bx+blockSize && bx+blockSize <= movement->boxes[i].aabb.end.x + blockSize&&
                    movement->boxes[i].aabb.begin.y - blockSize <= by+blockSize && by+blockSize <= movement->boxes[i].aabb.end.y + blockSize)) {
                    flag = 1;
                    break;
                }
            }
            if (flag) {
                continue;
            }
            float avg = 0.0f;
            int maxDiff = 0;
            for (int y = by; y < by+blockSize; y++) {
                uint8_t *pY = frame->data[0] + y*frame->linesize[0];
                for (int x = bx+1; x < bx + blockSize-1; x++) {
                    int d = abs(pY[x-1] - pY[x+1]);
                    sum += d;
                    avg += pY[x];
                    maxDiff = MAX(d, maxDiff);
                }
            }
            float area = blockSize*(blockSize-2);
            avg /= area;
            //float threshold = linFun(avg, 173.99f, 1.95f, 16.226f, 0.5f);//173.99f, 3.337f, 16.226f, 0.116f);
            float threshold = linFun(avg, 175.0f, lightThreshold, 17.0f, darkThresold);

            if (maxDiff < 1.0f*lightThreshold + 2*darkThresold && (avg < 1 || sum/area < threshold)) {
                areas[nb].begin.x = bx;
                areas[nb].begin.y = by;
                areas[nb].end.x = bx + blockSize;
                areas[nb].end.y = by + blockSize;
                nb++;
            }
        }
    }
    return nb;
}
static int findAreas(AABB *areas, FrameCopy *frame, FrameBoxes *movement) {
    int blockSize = 64;
    if (AV_PIX_FMT_YUV420P == frame->format) {
        return findAreas_YUV420P(areas, frame, movement, blockSize, 12.0f, 0.28f);
    }
    return 0;
}
static void replaceWithFlatAreas(AABB *pFlatAreas, int areasNb, FrameBoxes *pMov, int xReduction, int yReduction) {
    int sortedMov[1000];
    sortedMov[0] = 0;
    for (int i = 1; i < pMov->number; i++) {
        int j;
        for (j = 0; j < i; j++) {
            int a0 = (pMov->boxes[i].aabb.end.x - pMov->boxes[i].aabb.begin.x)*(pMov->boxes[i].aabb.end.y - pMov->boxes[i].aabb.begin.y);
            int a1 = (pMov->boxes[j].aabb.end.x - pMov->boxes[j].aabb.begin.x)*(pMov->boxes[j].aabb.end.y - pMov->boxes[j].aabb.begin.y);
            if (a0 > a1){
                break;
            }
        }
        memcpy(sortedMov + j + 1, sortedMov + j, (i - j)*sizeof(int));
        sortedMov[j] = i;
    }

    for (int i = 0; i < pMov->number; i++) {
        int sw = (pMov->boxes[i].aabb.end.x - pMov->boxes[i].aabb.begin.x);
        int sh = (pMov->boxes[i].aabb.end.y - pMov->boxes[i].aabb.begin.y);
        int sa = sw*sh;
        if (sa == 0) {
            continue;
        }
        int minDiff = sw*sh*xReduction*yReduction;
        int bestIndex = -1;
        int dw, dh;
        for (int j = 0; j < areasNb; j++) {
            int w1 = (pFlatAreas[j].end.x - pFlatAreas[j].begin.x);
            int h1 = (pFlatAreas[j].end.y - pFlatAreas[j].begin.y);
            int a1 = w1*h1;
//            if (sa - a1 < minDiff &&
//                sw < w1 && sh < h1 &&
//                5*sw > w1 && 5*sh > h1) {
            if (sw < w1 && sh < h1 &&
                abs(sw*xReduction*sh*yReduction - a1) < minDiff) {
                minDiff = abs(sw*xReduction*sh*yReduction - a1) ;
                bestIndex = j;
                dw = w1;
                dh = h1;
            }
        }
        if (bestIndex > -1) {
            memcpy(&pMov->boxes[i].dstAabb, pFlatAreas + bestIndex, sizeof(AABB));
            int propW = dw/sw;
            int propH = dh/sh;
            int prop = MIN(propW, propH);
            dw = sw*prop;
            dh = sh*prop;
            pMov->boxes[i].dstAabb.end.x = pMov->boxes[i].dstAabb.begin.x + (int)dw;
            pMov->boxes[i].dstAabb.end.y = pMov->boxes[i].dstAabb.begin.y + (int)dh;
            memset(pFlatAreas + bestIndex, 0, sizeof(AABB));
        }
    }
}

#endif
