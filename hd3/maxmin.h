#ifndef HD3_MAXMIN_H
#define HD3_MAXMIN_H

static void updateMinMax(uint8_t* cvalue, uint8_t* mins, uint8_t* maxes, uint8_t* evalue, float* mean)
{
    uint64_t m = 0;
    for (; cvalue < evalue; mins++, maxes++, cvalue++)
    {
        m += *cvalue;
        *maxes = MAX(*maxes, *cvalue);
        *mins  = MIN(*mins, *cvalue);
    }
    *mean = m;
}
static void updateMaxMinDiff(uint8_t* cvalue, uint8_t* pvalue, uint8_t* diff, uint8_t* mins, uint8_t* maxes, uint8_t* ediff, float* mean)
{
    uint64_t m = 0;
    for (; diff < ediff; diff++, mins++, maxes++, cvalue++, pvalue++)
    {
        *diff = abs(*cvalue - *pvalue);
        m += *diff;
        *maxes = MAX(*maxes, *diff), *mins = MIN(*mins, *diff);
    }
    *mean = m;
}
static void updateMaxMinWithDiff(uint8_t* cvalue, uint8_t* pvalue, uint8_t* diff, uint8_t* mins, uint8_t* maxes, uint8_t* ediff, float* mean)
{
    uint64_t m = 0;
    int d = 0;
    for (; diff < ediff; diff++, mins++, maxes++, cvalue++, pvalue++)
    {
        *maxes = MAX(*maxes, *cvalue), *mins = MIN(*mins, *cvalue);
        //*diff  = MAX(*diff, MIN(255, abs(*maxes - *mins) + abs(*cvalue - *pvalue)));
        *diff  = MAX(*diff, MIN(255, *maxes - *mins + abs(*cvalue - *pvalue)));
        m += *diff;
    }

    *mean = m;
}
static void cumulateMaxMin(uint8_t* cvalue, uint8_t* diff, uint8_t* mins, uint8_t* maxes, uint8_t* ediff, float* mean)
{
    uint64_t m = 0;
    for (; diff < ediff; diff++, mins++, maxes++, cvalue++)
    {
        m += *cvalue;
        if (*maxes < *cvalue)
            *maxes = *cvalue;
        if (*mins > *cvalue)
            *mins = *cvalue;
        *diff = MIN(255, *diff + *maxes - *mins);
    }
    *mean = m;
}
static void cumulateMaxMinWithDiff(uint8_t* cvalue, uint8_t* pvalue, uint8_t* diff, uint8_t* mins, uint8_t* maxes, uint8_t* ediff, float* mean)
{
    uint64_t m = 0;
    for (; diff < ediff; diff++, mins++, maxes++, cvalue++, pvalue++)
    {
        *maxes = MAX(*maxes, *cvalue), *mins = MIN(*mins, *cvalue);
        //*diff = MIN(255, *diff + abs(*maxes - *mins) + abs(*cvalue - *pvalue));
        *diff = MIN(255, *diff + *maxes - *mins + abs(*cvalue - *pvalue));
        m += *diff;
    }

    *mean = m;
}

typedef struct MaxMinArgs
{
    uint8_t* cvalue;
    uint8_t* pvalue;
    int length;
    uint8_t* diff;
    uint8_t* mins;
    uint8_t* maxes;
    float* mean;
    uint8_t method;
} MaxMinArgs;
static void* threadMaxMin(void *arg)
{
    MaxMinArgs* a = arg;
    switch(a->method)
    {
        case 0:
            updateMaxMinDiff(a->cvalue, a->pvalue, a->diff, a->mins, a->maxes, a->diff + a->length, a->mean);
        break;
        case 1:
            updateMaxMinWithDiff(a->cvalue, a->pvalue, a->diff, a->mins, a->maxes, a->diff + a->length, a->mean);
        break;
        case 2:
            cumulateMaxMin(a->cvalue, a->diff, a->mins, a->maxes, a->diff + a->length, a->mean);
        break;
        case 3:
            cumulateMaxMinWithDiff(a->cvalue, a->pvalue, a->diff, a->mins, a->maxes, a->diff + a->length, a->mean);
        break;
        case 4:
            updateMinMax(a->cvalue, a->mins, a->maxes, a->cvalue + a->length, a->mean);
        break;
    };
    return NULL;
}


#endif
