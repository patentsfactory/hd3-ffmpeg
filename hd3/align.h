#ifndef HD3_ALIGN_H
#define HD3_ALIGN_H
#define MAX(a,b) ((a) < (b) ? (b) : (a))
#define MIN(a,b) ((a) > (b) ? (b) : (a))

static void align_aabb(AABB* box, int quantX, int quantY, int height)
{
    int bx = box->begin.x - box->begin.x%quantX;
    int bw = box->end.x;// + 1;
    int by = box->begin.y;
    int bh = box->end.y - box->begin.y;// + 1;
    if (0 != bw%quantX)
    {
        bw += quantX - bw%quantX;
    }
    bw = bw - bx;

    if (0 != bh%quantY)
    {
        bh += quantY - bh%quantY;
    }
    if (bh + by >= height)
    {
        bh -= bh + by - height;
    }

    box->begin.x  = bx;
    box->end.x    = bx + bw;
    box->begin.y  = by;
    box->end.y    = by + bh;
}

static void alignBoxes(FrameBoxes* inoutBoxes, int quantX, int quantY, int height)
{
    for (int i = 0; i < inoutBoxes->number; i++)
    {
        align_aabb(inoutBoxes->boxes + i, quantX, quantY, height);
//        int bx = inoutBoxes->boxes[i].aabb.begin.x - inoutBoxes->boxes[i].aabb.begin.x%quantX;
//        int bw = inoutBoxes->boxes[i].aabb.end.x;// + 1;
//        int by = inoutBoxes->boxes[i].aabb.begin.y;
//        int bh = inoutBoxes->boxes[i].aabb.end.y - inoutBoxes->boxes[i].aabb.begin.y;// + 1;
//        if (0 != bw%quantX)
//        {
//            bw += quantX - bw%quantX;
//        }
//        bw = bw - bx;
//
//        if (0 != bh%quantY)
//        {
//            bh += quantY - bh%quantY;
//        }
//        if (bh + by >= height)
//        {
//            bh -= bh + by - height;
//        }
//
//        inoutBoxes->boxes[i].aabb.begin.x  = bx;
//        inoutBoxes->boxes[i].aabb.end.x    = bx + bw;
//        inoutBoxes->boxes[i].aabb.begin.y  = by;
//        inoutBoxes->boxes[i].aabb.end.y    = by + bh;
    }
}

static void aggregation_closedMax(FrameBoxes* inoutBoxes, int marginX, int marginY)
{
    int newNumber = 0;
    marginX *= 2;
    marginY *= 2;
    for (int i = 0; i < inoutBoxes->number; i++)
    {
        if (inoutBoxes->boxes[i].aabb.begin.x != inoutBoxes->boxes[i].aabb.end.x)
        {
            for (int j = 0; j < inoutBoxes->number; j++)
            {
                if (j == i || inoutBoxes->boxes[j].aabb.begin.x == inoutBoxes->boxes[j].aabb.end.x)
                {
                    continue;
                }
                {
                    int beginx = MIN(inoutBoxes->boxes[j].aabb.begin.x, inoutBoxes->boxes[i].aabb.begin.x);
                    int endx   = MAX(inoutBoxes->boxes[j].aabb.end.x,   inoutBoxes->boxes[i].aabb.end.x);
                    int beginy = MIN(inoutBoxes->boxes[j].aabb.begin.y, inoutBoxes->boxes[i].aabb.begin.y);
                    int endy   = MAX(inoutBoxes->boxes[j].aabb.end.y,   inoutBoxes->boxes[i].aabb.end.y);
                    if ((endx - beginx - marginX <= inoutBoxes->boxes[j].aabb.end.x + inoutBoxes->boxes[i].aabb.end.x - inoutBoxes->boxes[j].aabb.begin.x - inoutBoxes->boxes[i].aabb.begin.x) &&
                        (endy - beginy - marginY <= inoutBoxes->boxes[j].aabb.end.y + inoutBoxes->boxes[i].aabb.end.y - inoutBoxes->boxes[j].aabb.begin.y - inoutBoxes->boxes[i].aabb.begin.y))
                    {
                        inoutBoxes->boxes[i].aabb.begin.x = beginx;
                        inoutBoxes->boxes[i].aabb.begin.y = beginy;
                        inoutBoxes->boxes[i].aabb.end.x   = endx;
                        inoutBoxes->boxes[i].aabb.end.y   = endy;

                        inoutBoxes->boxes[j].aabb.begin.x = inoutBoxes->boxes[j].aabb.end.x;
                    }
                }
            }
        }
    }
    for (int i = 0; i < inoutBoxes->number; i++)
    {
        if (inoutBoxes->boxes[i].aabb.begin.x != inoutBoxes->boxes[i].aabb.end.x)
        {
            inoutBoxes->boxes[newNumber] = inoutBoxes->boxes[i];
            newNumber++;
        }
    }
    inoutBoxes->number = newNumber;
}
static void aggregation_max(FrameBoxes* inoutBoxes)
{
    int newNumber = 0;
    for (int i = 0; i < inoutBoxes->number; i++)
    {
        if (inoutBoxes->boxes[i].aabb.begin.x != inoutBoxes->boxes[i].aabb.end.x) {
            for (int j = 0; j < inoutBoxes->number; j++)
            {
                if (j == i || inoutBoxes->boxes[i].aabb.begin.x == inoutBoxes->boxes[i].aabb.end.x)
                {
                    continue;
                }
                {
                    int beginx = MIN(inoutBoxes->boxes[j].aabb.begin.x, inoutBoxes->boxes[i].aabb.begin.x);
                    int endx = MAX(inoutBoxes->boxes[j].aabb.end.x, inoutBoxes->boxes[i].aabb.end.x);
                    int beginy = MIN(inoutBoxes->boxes[j].aabb.begin.y, inoutBoxes->boxes[i].aabb.begin.y);
                    int endy = MAX(inoutBoxes->boxes[j].aabb.end.y, inoutBoxes->boxes[i].aabb.end.y);
                    if ((endx - beginx < inoutBoxes->boxes[j].aabb.end.x + inoutBoxes->boxes[i].aabb.end.x - inoutBoxes->boxes[j].aabb.begin.x - inoutBoxes->boxes[i].aabb.begin.x) &&
                        (endy - beginy < inoutBoxes->boxes[j].aabb.end.y + inoutBoxes->boxes[i].aabb.end.y - inoutBoxes->boxes[j].aabb.begin.y - inoutBoxes->boxes[i].aabb.begin.y))
                    {
                        inoutBoxes->boxes[i].aabb.begin.x = beginx;
                        inoutBoxes->boxes[i].aabb.begin.y = beginy;
                        inoutBoxes->boxes[i].aabb.end.x = endx;
                        inoutBoxes->boxes[i].aabb.end.y = endy;

                        inoutBoxes->boxes[i].aabb.begin.x = inoutBoxes->boxes[i].aabb.end.x;
                    }
                }
            }
        }
    }
    for (int i = 0; i < inoutBoxes->number; i++)
    {
        if (inoutBoxes->boxes[i].aabb.begin.x != inoutBoxes->boxes[i].aabb.end.x)
        {
            inoutBoxes->boxes[newNumber] = inoutBoxes->boxes[i];
            newNumber++;
        }
    }
    inoutBoxes->number = newNumber;
}
// kacziro - todo - 'cross' case
static AABB aggregate_constArea(AABB* box0, AABB* box1, int quantX, int quantY) // kacziro - fixme - else if (.. && ..) {} -> if (..) {} else {}
{
    AABB outBB;
    memset(&outBB, 0, sizeof(AABB));
    uint8_t collisionBx = box1->begin.x <= box0->begin.x && box0->begin.x <= box1->end.x;
    uint8_t collisionEx = box1->begin.x <= box0->end.x   && box0->end.x   <= box1->end.x;
    uint8_t collisionBy = box1->begin.y <= box0->begin.y && box0->begin.y <= box1->end.y;
    uint8_t collisionEy = box1->begin.y <= box0->end.y   && box0->end.y   <= box1->end.y;
    uint8_t collisionBxRev = box0->begin.x <= box1->begin.x && box1->begin.x <= box0->end.x;
    uint8_t collisionExRev = box0->begin.x <= box1->end.x   && box1->end.x   <= box0->end.x;
    uint8_t collisionByRev = box0->begin.y <= box1->begin.y && box1->begin.y <= box0->end.y;
    uint8_t collisionEyRev = box0->begin.y <= box1->end.y   && box1->end.y   <= box0->end.y;

    if (collisionBx && collisionEx && collisionBy && collisionEy)// &&
        // !collisionBxRev && !collisionExRev && !collisionByRev && !collisionEyRev)
    {
        box0->begin.x = box0->end.x;
        box0->begin.y = box0->end.y; // practically removed
    }
    else if (collisionBxRev && collisionExRev && collisionByRev && collisionEyRev)
    {
        box1->begin.x = box1->end.x;
        box1->begin.y = box1->end.y; // practically removed
    }
    else if (collisionBx && collisionBy && collisionEy)
    {
        if (box0->begin.y == box1->begin.y && box0->end.y == box1->end.y) {
            box0->begin.x = box1->begin.x;
            box1->begin.x = box1->end.x;// practically removed
            box1->begin.y = box1->end.y;
        } else {
            box0->begin.x = box1->end.x;
            if (box0->end.x - box0->begin.x < quantX)
            {
                box0->end.x = box0->begin.x + quantX;
            }
        }
    }
    else if (collisionEx && collisionBy && collisionEy)
    {
        if (box0->begin.y == box1->begin.y && box0->end.y == box1->end.y) {
            box0->end.x = box1->end.x;
            box1->begin.x = box1->end.x;// practically removed
            box1->begin.y = box1->end.y;
        } else {
            box0->end.x = box1->begin.x;
            if (box0->end.x - box0->begin.x < quantX)
            {
                box0->begin.x = box0->end.x - quantX;
            }
        }
    }
    else if (collisionBy && collisionBx && collisionEx)
    {
        if (box0->begin.x == box1->begin.x && box0->end.x == box1->end.x) {
            box0->begin.y = box1->begin.y;
            box1->begin.x = box1->end.x;// practically removed
            box1->begin.y = box1->end.y;
        } else {
            box0->begin.y = box1->end.y;
        }
    }
    else if (collisionEy && collisionBx && collisionEx)
    {
        if (box0->begin.x == box1->begin.x && box0->end.x == box1->end.x) {
            box0->end.y = box1->end.y;
            box1->begin.x = box1->end.x;// practically removed
            box1->begin.y = box1->end.y;
        } else {
            box0->end.y = box1->begin.y;
        }
    }
    else if (collisionBxRev && collisionByRev && collisionEyRev)
    {
        box1->begin.x = box0->end.x;
        if (box1->end.x - box1->begin.x < quantX)
        {
            box1->end.x = box1->begin.x + quantX;
        }
    }
    else if (collisionExRev && collisionByRev && collisionEyRev)
    {
        box1->end.x = box0->begin.x;
        if (box1->end.x - box1->begin.x < quantX)
        {
            box1->begin.x = box1->end.x - quantX;
        }
    }
    else if (collisionByRev && collisionBxRev && collisionExRev)
    {
        box1->begin.y = box0->end.y; // kacziro - fixme - too small box for y changing (quantY)
    }
    else if (collisionEyRev && collisionBxRev && collisionExRev)
    {
        box1->end.y = box0->begin.y; // kacziro - fixme - too small box for y changing (quantY)
    }
    else if (collisionBx && collisionBy)
    {
        int w1 = box0->end.x - box1->end.x;
        int w2 = box1->end.x - box0->begin.x;
        int h1 = box1->end.y - box0->begin.y;
        int h2 = box0->end.y - box1->end.y;
        if (w1*h1 != 0 && w2*h2 != 0)
        {
            if (w1*h1 > w2*h2)
            {
                outBB.begin.x = box1->end.x;
                outBB.begin.y = box0->begin.y;
                outBB.end.x = box0->end.x;
                outBB.end.y = box1->end.y;
                box0->begin.y = box1->end.y;
            }
            else
            {
                outBB.begin.x = box0->begin.x;
                outBB.begin.y = box1->end.y;
                outBB.end.x = box1->end.x;
                outBB.end.y = box0->end.y;
                box0->begin.x = box1->end.x;
            }
        }
    }
    else if (collisionEx && collisionBy)
    {
        int w1 = box0->end.x - box1->begin.x;
        int w2 = box1->begin.x - box0->begin.x;
        int h1 = box0->end.y - box1->end.y;
        int h2 = box1->end.y - box0->begin.y;
        if (w1*h1 != 0 && w2*h2 != 0)
        {
            if (w1*h1 > w2*h2)
            {
                outBB.begin.x = box1->begin.x;
                outBB.begin.y = box1->end.y;
                outBB.end.x = box0->end.x;
                outBB.end.y = box0->end.y;
                box0->end.x = box1->begin.x;
            }
            else
            {
                outBB.begin.x = box0->begin.x;
                outBB.begin.y = box0->begin.y;
                outBB.end.x = box1->begin.x;
                outBB.end.y = box1->end.y;
                box0->begin.y = box1->end.y;
            }
        }
    }
    else if (collisionBx && collisionEy)
    {
        int w1 = box0->end.x - box1->end.x;
        int w2 = box1->end.x - box0->begin.x;
        int h1 = box0->end.y - box1->begin.y;
        int h2 = box1->begin.y - box0->begin.y;
        if (w1*h1 != 0 && w2*h2 != 0)
        {
            if (w1*h1 > w2*h2)
            {
                outBB.begin.x = box1->end.x;
                outBB.begin.y = box1->begin.y;
                outBB.end.x = box0->end.x;
                outBB.end.y = box0->end.y;
                box0->end.y = box1->begin.y;
            }
            else
            {
                outBB.begin.x = box0->begin.x;
                outBB.begin.y = box0->begin.y;
                outBB.end.x = box1->end.x;
                outBB.end.y = box1->begin.y;
                box0->begin.x = box1->end.x;
            }
        }
    }
    else if (collisionEx && collisionEy)
    {
        int w1 = box1->begin.x - box0->begin.x;
        int w2 = box0->end.x - box1->begin.x;
        int h1 = box0->end.y - box1->begin.y;
        int h2 = box1->begin.y - box0->begin.y;
        if (w1*h1 != 0 && w2*h2 != 0)
        {
            if (w1*h1 > w2*h2)
            {
                outBB.begin.x = box0->begin.x;
                outBB.begin.y = box1->begin.y;
                outBB.end.x = box1->begin.x;
                outBB.end.y = box0->end.y;
                box0->end.y = box1->begin.y;
            }
            else
            {
                outBB.begin.x = box1->begin.x;
                outBB.begin.y = box0->begin.y;
                outBB.end.x = box0->end.x;
                outBB.end.y = box1->begin.y;
                box0->end.x = box1->begin.x;
            }
        }
    }
    return outBB;
}

static int aggregation_aabb_constArea(AABB* inoutBoxes, int boxNb, int quantX, int quantY) {
    for (int i = 0; i < boxNb; i++)
    {
        for (int j = i+1; j < boxNb; j++)
        {
            AABB tmp0 = inoutBoxes[i];
            AABB tmp1 = inoutBoxes[j];
//            uint8_t collision = tmp0.begin.x <= tmp1.begin.x && tmp1.begin.x <= tmp0.end.x;
//            collision = collision + (tmp0.begin.x <= tmp1.end.x && tmp1.end.x   <= tmp0.end.x);
//            collision = collision + (tmp0.begin.y <= tmp1.begin.y && tmp1.begin.y <= tmp0.end.y);
//            collision = collision + (tmp0.begin.y <= tmp1.end.y   && tmp1.end.y   <= tmp0.end.y);
//            if (2 < collision)
//            printf("[%d, %d, %d, %d][%d, %d, %d, %d]->\n",
//                   tmp0.begin.x, tmp0.begin.y, tmp0.end.x, tmp0.end.y,
//                   tmp1.begin.x, tmp1.begin.y, tmp1.end.x, tmp1.end.y);
            AABB newBB = aggregate_constArea(inoutBoxes + i, inoutBoxes + j, quantX, quantY);
            //if (2 < collision)
//            printf("->[%d, %d, %d, %d][%d, %d, %d, %d]\n",
//                   inoutBoxes[i].begin.x, inoutBoxes[i].begin.y, inoutBoxes[i].end.x, inoutBoxes[i].end.y,
//                   inoutBoxes[j].begin.x, inoutBoxes[j].begin.y, inoutBoxes[j].end.x, inoutBoxes[j].end.y);
//            if (tmp0.begin.x != inoutBoxes[i].begin.x || inoutBoxes[j].begin.x != tmp1.begin.x ||
//                tmp0.end.x != inoutBoxes[i].end.x || inoutBoxes[j].end.x != tmp1.end.x ||
//                tmp0.end.y != inoutBoxes[i].end.y || inoutBoxes[j].end.y != tmp1.end.y ||
//                tmp0.begin.y != inoutBoxes[i].begin.y || inoutBoxes[j].begin.y != tmp1.begin.y)
//            {
//                printf("aggr\n");
//            }
            if (newBB.begin.x != newBB.end.x && newBB.begin.y != newBB.end.y) {
                inoutBoxes[boxNb] = newBB;
                boxNb++;
            }
        }
    }
    int newNb = 0;
    for (int i = 0; i < boxNb; i++)
    {
        if (inoutBoxes[i].begin.x != inoutBoxes[i].end.x)
        {
            inoutBoxes[newNb] = inoutBoxes[i];
            newNb++;
        }
    }
    return newNb;
}
static void aggregation_constArea(FrameBoxes* inoutBoxes, int quantX, int quantY)
{
    for (int i = 0; i < inoutBoxes->number; i++)
    {
        for (int j = i+1; j < inoutBoxes->number; j++)
        {
            AABB newBB = aggregate_constArea(&inoutBoxes->boxes[i].aabb, &inoutBoxes->boxes[j].aabb, quantX, quantY);
            if (newBB.begin.x != newBB.end.x && newBB.begin.y != newBB.end.y) {
                inoutBoxes->boxes[inoutBoxes->number].code = inoutBoxes->boxes[i].code;
                inoutBoxes->boxes[inoutBoxes->number].aabb = newBB;
                inoutBoxes->boxes[inoutBoxes->number].dstAabb = newBB;
                inoutBoxes->number++;
            }
        }
    }
}
static int aggregation_biggestRect(AABB* inoutBoxes, int boxNb) {
    int sorted[2000];
    uint8_t neighbors[2000];
    {
        memset(neighbors, 0, 2000);
        uint8_t neighborNb = 0;
        for (int j = 1; j < boxNb; j++) {
            uint8_t nx = inoutBoxes[0].begin.x == inoutBoxes[j].begin.x;
            nx += inoutBoxes[0].end.x   == inoutBoxes[j].end.x;
            nx += inoutBoxes[0].end.x   == inoutBoxes[j].begin.x;
            nx += inoutBoxes[0].begin.x == inoutBoxes[j].end.x;
            uint8_t ny = inoutBoxes[0].begin.y == inoutBoxes[j].begin.y;
            ny += inoutBoxes[0].end.y   == inoutBoxes[j].end.y;
            ny += inoutBoxes[0].end.y   == inoutBoxes[j].begin.y;
            ny += inoutBoxes[0].begin.y == inoutBoxes[j].end.y;
            if (ny > 0 && nx > 0) {
                neighborNb++;
            }
        }
        sorted[0] = 0;
        neighbors[0] = neighborNb;
        for (int i = 1; i < boxNb; i++) {
            int j;
            neighborNb = 0;
            for (j = 0; j < boxNb; j++) {
                uint8_t nx = inoutBoxes[i].begin.x == inoutBoxes[j].begin.x;
                nx += inoutBoxes[i].end.x   == inoutBoxes[j].end.x;
                nx += inoutBoxes[i].end.x   == inoutBoxes[j].begin.x;
                nx += inoutBoxes[i].begin.x == inoutBoxes[j].end.x;
                uint8_t ny = inoutBoxes[i].begin.y == inoutBoxes[j].begin.y;
                ny += inoutBoxes[i].end.y   == inoutBoxes[j].end.y;
                ny += inoutBoxes[i].end.y   == inoutBoxes[j].begin.y;
                ny += inoutBoxes[i].begin.y == inoutBoxes[j].end.y;
                if (ny > 0 && nx > 0) {
                    neighborNb++;
                }
            }
            for (j = 0; j < i; j++) {
                if (neighborNb > neighbors[j]){
                    break;
                }
            }
            memcpy(sorted + j + 1, sorted + j, (i - j)*sizeof(int));
            sorted[j] = i;
            memcpy(neighbors + j + 1, neighbors + j, (i - j)*sizeof(uint8_t));
            neighbors[j] = neighborNb;
        }
    }
    for (int si = 0; si < boxNb; si++) {
        int i = sorted[si];
        if (inoutBoxes[i].begin.x == inoutBoxes[i].end.x) {
            continue;
        }
        int aggr = 1;
        int aggrBoxes[100];
        int hSize = 1;
        int wSize = 1;
        while(aggr) {
            int boxCount = 0;
            int wFind = 0;
            int hFind = 0;
            aggr = 0;
            for (int j = 0; j < boxNb; j++) {
                if (i == j || inoutBoxes[j].begin.x == inoutBoxes[j].end.x) {
                    continue;
                }
                if (inoutBoxes[i].end.x == inoutBoxes[j].begin.x &&
                    inoutBoxes[i].begin.y <= inoutBoxes[j].begin.y &&
                    inoutBoxes[i].end.y >= inoutBoxes[j].end.y) {

                    aggrBoxes[boxCount] = j;
                    boxCount++;
                    hFind += (inoutBoxes[j].end.y - inoutBoxes[j].begin.y);
                    if (hFind == inoutBoxes[i].end.y - inoutBoxes[i].begin.y) {
                        inoutBoxes[i].end.x = inoutBoxes[j].end.x;
                        for (int k = 0; k < boxCount; k++){
                            inoutBoxes[aggrBoxes[k]].begin.x = inoutBoxes[aggrBoxes[k]].end.x;
                            inoutBoxes[aggrBoxes[k]].begin.y = inoutBoxes[aggrBoxes[k]].end.y;
                        }
                        aggr = 1;
                        wSize++;
                        break;
                    }
                }
            }
            boxCount = 0;
            for (int j = 0; j < boxNb; j++) {
                if (i == j || inoutBoxes[j].begin.x == inoutBoxes[j].end.x) {
                    continue;
                }
                if (inoutBoxes[i].end.y == inoutBoxes[j].begin.y &&
                    inoutBoxes[i].begin.x <= inoutBoxes[j].begin.x &&
                    inoutBoxes[i].end.x >= inoutBoxes[j].end.x) {

                    aggrBoxes[boxCount] = j;
                    wFind += (inoutBoxes[j].end.x - inoutBoxes[j].begin.x);
                    boxCount++;
                    if (wFind == inoutBoxes[i].end.x - inoutBoxes[i].begin.x) {
                        inoutBoxes[i].end.y = inoutBoxes[j].end.y;
                        for (int k = 0; k < boxCount; k++){
                            inoutBoxes[aggrBoxes[k]].begin.x = inoutBoxes[aggrBoxes[k]].end.x;
                            inoutBoxes[aggrBoxes[k]].begin.y = inoutBoxes[aggrBoxes[k]].end.y;
                        }
                        aggr = 1;
                        hSize++;
                        break;
                    }
                }
            }
        }
    }
    int newNb = 0;
    for (int i = 0; i < boxNb; i++)
    {
        if (inoutBoxes[i].begin.x != inoutBoxes[i].end.x)
        {
            inoutBoxes[newNb] = inoutBoxes[i];
            newNb++;
        }
    }
    return newNb;
}
static int aggregation_biggestArea(AABB* inoutBoxes, int boxNb) {
    uint8_t aggr = 1;
    uint8_t horizontal = 1;
//    int sorted[2000];
//    uint8_t neighbors[2000];
//    {
//        memset(neighbors, 0, 2000);
//        uint8_t neighborNb = 0;
//        for (int j = 1; j < boxNb; j++) {
//            uint8_t nx = inoutBoxes[0].begin.x == inoutBoxes[j].begin.x;
//            nx += inoutBoxes[0].end.x   == inoutBoxes[j].end.x;
//            nx += inoutBoxes[0].end.x   == inoutBoxes[j].begin.x;
//            nx += inoutBoxes[0].begin.x == inoutBoxes[j].end.x;
//            uint8_t ny = inoutBoxes[0].begin.y == inoutBoxes[j].begin.y;
//            ny += inoutBoxes[0].end.y   == inoutBoxes[j].end.y;
//            ny += inoutBoxes[0].end.y   == inoutBoxes[j].begin.y;
//            ny += inoutBoxes[0].begin.y == inoutBoxes[j].end.y;
//            if (ny > 0 && nx > 0) {
//                neighborNb++;
//            }
//        }
//        sorted[0] = 0;
//        neighbors[0] = neighborNb;
//        for (int i = 1; i < boxNb; i++) {
//            int j;
//            neighborNb = 0;
//            for (j = 0; j < boxNb; j++) {
//                uint8_t nx = inoutBoxes[i].begin.x == inoutBoxes[j].begin.x;
//                nx += inoutBoxes[i].end.x   == inoutBoxes[j].end.x;
//                nx += inoutBoxes[i].end.x   == inoutBoxes[j].begin.x;
//                nx += inoutBoxes[i].begin.x == inoutBoxes[j].end.x;
//                uint8_t ny = inoutBoxes[i].begin.y == inoutBoxes[j].begin.y;
//                ny += inoutBoxes[i].end.y   == inoutBoxes[j].end.y;
//                ny += inoutBoxes[i].end.y   == inoutBoxes[j].begin.y;
//                ny += inoutBoxes[i].begin.y == inoutBoxes[j].end.y;
//                if (ny > 0 && nx > 0) {
//                    neighborNb++;
//                }
//            }
//            for (j = 0; j < i; j++) {
//                if (neighborNb > neighbors[j]){
//                    break;
//                }
//            }
//            //printf("%d --%d-> %d (+%d)\n", j, i-j, j + 1, i);
//            memcpy(sorted + j + 1, sorted + j, (i - j)*sizeof(int));
//            sorted[j] = i;
//            memcpy(neighbors + j + 1, neighbors + j, (i - j)*sizeof(uint8_t));
//            neighbors[j] = neighborNb;
//            //if (i > 16 && sorted[16] > 2000) {
//            //    printf("usp\n");
//           // }
//        }
//    }
    //printf("s[40] %d\n", sorted[40]);
    while(aggr) {
        aggr--;
        //printf("--\n");
        for (int si = 0; si < boxNb; si++) {
            //printf("s: %d\n", sorted[si]);
            int i = si;//sorted[si];//si;//
            if (inoutBoxes[i].begin.x == inoutBoxes[i].end.x){
                continue;
            }
            for (int j = i + 1; j < boxNb; j++) {
                if (inoutBoxes[j].begin.x == inoutBoxes[j].end.x){ //i == j ||
                    continue;
                }
                if (horizontal == 1 &&
                    inoutBoxes[j].begin.x == inoutBoxes[i].end.x &&
                    inoutBoxes[j].begin.y == inoutBoxes[i].begin.y &&
                    inoutBoxes[j].end.y == inoutBoxes[i].end.y) {

                    inoutBoxes[i].end.x = inoutBoxes[j].end.x;

                    inoutBoxes[j].begin.x = inoutBoxes[j].end.x;
                    inoutBoxes[j].begin.y = inoutBoxes[j].end.y;
                    aggr = 2;
                    break;
                } else if (horizontal == 0 &&
                            inoutBoxes[j].begin.y == inoutBoxes[i].end.y &&
                            inoutBoxes[j].begin.x == inoutBoxes[i].begin.x &&
                            inoutBoxes[j].end.x == inoutBoxes[i].end.x) {

                    inoutBoxes[i].end.y = inoutBoxes[j].end.y;

                    inoutBoxes[j].begin.x = inoutBoxes[j].end.x;
                    inoutBoxes[j].begin.y = inoutBoxes[j].end.y;
                    aggr = 2;
                    break;
                }
            }
        }
        int newNb = 0;
        for (int i = 0; i < boxNb; i++)
        {
            if (inoutBoxes[i].begin.x != inoutBoxes[i].end.x)
            {
                inoutBoxes[newNb] = inoutBoxes[i];
                newNb++;
            }
        }
        boxNb = newNb;
        //memset(sorted, 0, 2000);
//        sorted[0] = 0;
//        for (int i = 1; i < boxNb; i++) {
//            int j;
//            for (j = 0; j < i; j++) {
//                int a0 = (inoutBoxes[i].end.x - inoutBoxes[i].begin.x)*(inoutBoxes[i].end.y - inoutBoxes[i].begin.y);
//                int a1 = (inoutBoxes[j].end.x - inoutBoxes[j].begin.x)*(inoutBoxes[j].end.y - inoutBoxes[j].begin.y);
//                if (a0 > a1){
//                    break;
//                }
//            }
//            memcpy(sorted + j + 1, sorted + j, (i - j)*sizeof(int));
//            sorted[j] = i;
//        }
        horizontal = !horizontal;
    }

    int newNb = 0;
    for (int i = 0; i < boxNb; i++)
    {
        if (inoutBoxes[i].begin.x != inoutBoxes[i].end.x)
        {
            inoutBoxes[newNb] = inoutBoxes[i];
            newNb++;
        }
    }
    return newNb;
}


#endif // HD3_ALIGN_H
