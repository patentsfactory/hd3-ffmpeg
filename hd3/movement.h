#ifndef HD3_MOVEMENT_H
#define HD3_MOVEMENT_H

#include <stdint.h>
#define MAX(a,b) ((a) < (b) ? (b) : (a))
#define MIN(a,b) ((a) > (b) ? (b) : (a))

typedef struct Point2u8
{
    uint8_t x;
    uint8_t y;
} Point2u8;

typedef struct Point2u16
{
    uint16_t x;
    uint16_t y;
} Point2u16;

typedef struct AABB
{
    Point2u16 begin;
    Point2u16 end;
} AABB;
typedef struct CodeCell
{
    uint8_t method;
    uint8_t xReduction;
    uint8_t yReduction;
} CodeCell;
typedef struct CodeTable
{
    int8_t number;
    int8_t blockXSize;
    int8_t blockYSize;
    int allocated;
    CodeCell* cells;
} CodeTable;
typedef struct MovementBox
{
    AABB aabb;
    AABB dstAabb; // different region if encoding / decoding
    uint8_t code;
} MovementBox;
typedef struct FrameBoxes
{
    int pts;
    int32_t number;
    MovementBox *boxes;
    CodeTable table;
} FrameBoxes;

typedef struct MovementMetadata
{
    int number;
    int maxNumber;
    MovementBox* pBoxes;
} MovementMetadata;

typedef struct Hd3Metadata
{
    uint8_t frameType;
    MovementMetadata* pMovements;
} Hd3Metadata;

typedef struct HD3SEIData {
    int maxNumber;
    FrameBoxes* pBoxes;
    int* allocatedBoxes;
} HD3SEIData;

//static AABB aabbIntersection(AABB *b0, AABB *b1) {
//    AABB ret;
//    ret.begin.x = MAX(b0->begin.x, b1->begin.x);
//    ret.begin.y = MAX(b0->begin.y, b1->begin.y);
//    ret.end.x = MIN(b0->end.x, b1->end.x);
//    ret.end.y = MIN(b0->end.y, b1->end.y);
//    if (ret.begin.x > ret.end.x || ret.begin.y > ret.end.y) {
//        memset(&ret, 0, sizeof(AABB));
//    }
//    return ret;
//}

#endif
