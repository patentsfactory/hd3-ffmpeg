#ifndef DUALRES_H
#define DUALRES_H

#include "hd3/frame_copy.h"
#include "hd3/pf_tools.h"

#include "libavutil/frame.h"

#define DR_NONE     0
#define DR_ENCODE   1
#define DR_DECODE   2

typedef struct DualResData
{
    int xRatio;
    int yRatio;
    int iFrameFreq;

    int frameStep;
    int seriesStep;

    FrameCopy buffer;
    int pts;
    int frameType;
    x264_picture_t rendered;

    AVCodecContext *ctx;
} DualResData;

static DualResData drData;
static uint8_t drMode;


static int dualResolution_init(AVCodecContext *ctx, int width, int height, int xRatio, int yRatio, int csp, int qp)
{
    int blockCount;
    drData.ctx = ctx;
    if (width % xRatio != 0)
    {
        av_log(drData.ctx, AV_LOG_WARNING, "width [%d] % xratio [%d] != 0\n", width, xRatio);
    }
    if (height % yRatio != 0)
    {
        av_log(drData.ctx, AV_LOG_WARNING, "height [%d] % yratio [%d] != 0\n", height, yRatio);
    }

    drData.xRatio = xRatio;
    drData.yRatio = yRatio;
    drData.iFrameFreq = 1;

    drData.frameStep = 0;
    drData.seriesStep = drData.iFrameFreq;
    initFrameCopy(drData.ctx, &drData.buffer, width, height, csp);
    x264_picture_alloc(&drData.rendered, csp, width, height);
    drData.pts = 0;
    drData.frameType = X264_TYPE_AUTO;

    pGopParam = (x264_param_t*)malloc(sizeof(x264_param_t));
    pGopParam->maxBufferedFrames = 1;

    blockCount = (width/16)*(height/16); // kacziro - fixme TODO magic number
    forcedQP = (int8_t**)malloc(sizeof(int8_t*));

    x264_param_default(pGopParam);
    pGopParam->i_csp = csp;
    pGopParam->i_width = width;
    pGopParam->i_height = height;

    *forcedQP = (int8_t*)malloc(sizeof(int8_t)*blockCount);
    memset(*forcedQP, -1, blockCount*sizeof(int8_t));
    pGopParam->forcedQP = forcedQP;
    pGopParam->maxBufferedFrames = 1;
    resetQP(pGopParam, qp, 0);

    return 1;
}
static void dualResolution_destroy()
{
    destroyFrameCopy(&drData.buffer);
    x264_picture_clean(&drData.rendered);
}

static int dualResolution_encode_update(AVFrame *pInFrame)
{
    if (NULL != pInFrame)
    {
        if (drData.seriesStep >= drData.iFrameFreq)
        {
            drData.seriesStep = 0;
            drData.frameType = X264_TYPE_IDR;
            switch(drData.buffer.format)
            {
                case X264_CSP_I420:
                {
                    memcpy(drData.buffer.data[0], pInFrame->data[0], drData.buffer.width*drData.buffer.height);
                    memcpy(drData.buffer.data[1], pInFrame->data[1], drData.buffer.width*drData.buffer.height/4);
                    memcpy(drData.buffer.data[2], pInFrame->data[2], drData.buffer.width*drData.buffer.height/4);
                }
                break;
                case X264_CSP_RGB:
                {
                    memcpy(drData.buffer.data[0], pInFrame->data[0], drData.buffer.width*drData.buffer.height*3);
                }
                break;
            }
        }
        else
        {
            int x, y, cw, ch, format;
            drData.frameType = X264_TYPE_AUTO;
            if (0 == drData.frameStep)
            {
                switch(drData.buffer.format)
                {
                    case X264_CSP_I420:
                    {
                        memset(drData.buffer.data[0], 0, drData.buffer.width*drData.buffer.height);
                        memset(drData.buffer.data[1], 0, drData.buffer.width*drData.buffer.height/4);
                        memset(drData.buffer.data[2], 0, drData.buffer.width*drData.buffer.height/4);
                    }
                    break;
                    case X264_CSP_RGB:
                    {
                        memset(drData.buffer.data[0], 0, drData.buffer.width*drData.buffer.height*3);
                    }
                    break;
                }
            }
            y = drData.frameStep/drData.xRatio;
            x = drData.frameStep - y*drData.xRatio;

            x264_image_t imgDst;
            x264_image_t imgSrc;
            imgSrc.i_csp = imgDst.i_csp = drData.buffer.format;

            cw = drData.buffer.width/drData.xRatio;
            ch = drData.buffer.height/drData.yRatio;
            imgSrc.i_stride[0] = pInFrame->linesize[0];
            imgSrc.i_stride[1] = pInFrame->linesize[1];
            imgSrc.i_stride[2] = pInFrame->linesize[2];
            imgSrc.i_stride[3] = pInFrame->linesize[3];
            imgSrc.plane[0] = pInFrame->data[0];
            imgSrc.plane[1] = pInFrame->data[1];
            imgSrc.plane[2] = pInFrame->data[2];

            imgDst.i_stride[0] = drData.buffer.linesize[0];
            imgDst.i_stride[1] = drData.buffer.linesize[1];
            imgDst.i_stride[2] = drData.buffer.linesize[2];
            imgDst.i_stride[3] = drData.buffer.linesize[3];
            imgDst.plane[0] = drData.buffer.data[0];
            imgDst.plane[1] = drData.buffer.data[1];
            imgDst.plane[2] = drData.buffer.data[2];
            format = AV_PIX_FMT_YUV420P;
            switch(drData.buffer.format)
            {
                case X264_CSP_I420:
                {
                    imgSrc.i_plane = imgDst.i_plane = 3;

                    imgDst.plane[0] += (cw)*x   + imgDst.i_stride[0]* (ch*y);
                    imgDst.plane[1] += (cw*x)/2 + imgDst.i_stride[1]*((ch*y)/2);
                    imgDst.plane[2] += (cw*x)/2 + imgDst.i_stride[2]*((ch*y)/2);
                    format = AV_PIX_FMT_YUV420P;
                }
                break;
                case X264_CSP_RGB:
                {
                    imgSrc.i_plane = imgDst.i_plane = 1;
                    imgDst.plane[0] += (cw)*x*3 + imgDst.i_stride[0]*(ch*y);
                    format = AV_PIX_FMT_RGB24;
                }
                break;
            }
            SwsContext *img_convert_ctx;
            img_convert_ctx = sws_getCachedContext(img_convert_ctx,
                                                   drData.buffer.width, drData.buffer.height, format,
                                                   cw, ch, format,
                                                   SWS_BICUBIC, NULL, NULL, NULL);
            if (img_convert_ctx != NULL)
            {
                sws_scale(img_convert_ctx, (const uint8_t * const *)imgSrc.plane, imgSrc.i_stride, 0, drData.buffer.height, imgDst.plane, imgDst.i_stride);
                sws_freeContext(img_convert_ctx);
            }
            drData.frameStep++;
            if (drData.frameStep >= drData.xRatio*drData.yRatio)
            {
                drData.frameStep = 0;
                drData.seriesStep++;
                return 1;
            }
            return 0;
        }
    }
    return 1; // if not input frames then render what is
}
static void dualResolution_encode_render(x264_picture_t* pOutFrame)
{
    if (NULL != pOutFrame)
    {
        drData.rendered.i_pts = pOutFrame->i_pts = drData.pts;
        drData.rendered.i_type = pOutFrame->i_type = drData.frameType;
        switch(drData.buffer.format)
        {
            case X264_CSP_I420:
            {
                memcpy(drData.rendered.img.plane[0], drData.buffer.data[0], drData.buffer.width*drData.buffer.height);
                memcpy(drData.rendered.img.plane[1], drData.buffer.data[1], drData.buffer.width*drData.buffer.height/4);
                memcpy(drData.rendered.img.plane[2], drData.buffer.data[2], drData.buffer.width*drData.buffer.height/4);
            }
            break;
            case X264_CSP_RGB:
            {
                memcpy(drData.rendered.img.plane[0], drData.buffer.data[0], drData.buffer.width*drData.buffer.height*3);
            }
            break;
        }
        pOutFrame->param = pGopParam;
        pOutFrame->img.i_plane = drData.rendered.img.i_plane;
        for (int i = 0; i < pOutFrame->img.i_plane; i++) {
            pOutFrame->img.plane[i]    = drData.rendered.img.plane[i];
            pOutFrame->img.i_stride[i] = drData.rendered.img.i_stride[i];
        }

        drData.pts++;
    }
}

static void dualResolution_decode_update(AVFrame *pInFrame)
{
    if (1 == pInFrame->pict_type)
    {
        drData.seriesStep = drData.iFrameFreq;
        drData.frameStep = 0;
    }
    if (NULL != pInFrame)
    {
        switch(drData.buffer.format)
        {
            case X264_CSP_I420:
            {
                memcpy(drData.buffer.data[0], pInFrame->data[0], drData.buffer.width*drData.buffer.height);
                memcpy(drData.buffer.data[1], pInFrame->data[1], drData.buffer.width*drData.buffer.height/4);
                memcpy(drData.buffer.data[2], pInFrame->data[2], drData.buffer.width*drData.buffer.height/4);
            }
            break;
            case X264_CSP_RGB:
            {
                memcpy(drData.buffer.data[0], pInFrame->data[0], drData.buffer.width*drData.buffer.height*3);
            }
            break;
        }
    }
}
static int dualResolution_decode_render(x264_picture_t* pOutFrame)
{
    int ret = 1;
    if (NULL != pOutFrame)
    {
        if (drData.seriesStep >= drData.iFrameFreq)
        {
            drData.seriesStep = 0;
            drData.frameType = X264_TYPE_AUTO;
            switch(drData.buffer.format)
            {
                case X264_CSP_I420:
                {
                    memcpy(drData.rendered.img.plane[0], drData.buffer.data[0], drData.buffer.width*drData.buffer.height);
                    memcpy(drData.rendered.img.plane[1], drData.buffer.data[1], drData.buffer.width*drData.buffer.height/4);
                    memcpy(drData.rendered.img.plane[2], drData.buffer.data[2], drData.buffer.width*drData.buffer.height/4);
                }
                break;
                case X264_CSP_RGB:
                {
                    memcpy(drData.rendered.img.plane[0], drData.buffer.data[0], drData.buffer.width*drData.buffer.height*3);
                }
                break;
            }
        }
        else
        {
            int startX, startY, cw, ch;
            const int dim = 4;
            drData.frameType = X264_TYPE_IDR;
            startY = drData.frameStep/drData.xRatio;
            startX = drData.frameStep - startY*drData.xRatio;

            cw = drData.buffer.width/drData.xRatio;
            ch = drData.buffer.height/drData.yRatio;
            ScaleArg args[16];
            pthread_t threads[16];
            for (int y = 0; y < dim; y++)
            {
                for (int x = 0; x < dim; x++)
                {
                    int i = x + y*dim;
                    args[i].imgSrc.i_csp = args[i].imgDst.i_csp = drData.buffer.format;
                    args[i].imgDst.i_stride[0] = drData.rendered.img.i_stride[0];
                    args[i].imgDst.i_stride[1] = drData.rendered.img.i_stride[1];
                    args[i].imgDst.i_stride[2] = drData.rendered.img.i_stride[2];
                    args[i].imgDst.i_stride[3] = drData.rendered.img.i_stride[3];
                    args[i].imgDst.plane[0] = drData.rendered.img.plane[0];
                    args[i].imgDst.plane[1] = drData.rendered.img.plane[1];
                    args[i].imgDst.plane[2] = drData.rendered.img.plane[2];

                    args[i].imgSrc.i_stride[0] = drData.buffer.linesize[0];
                    args[i].imgSrc.i_stride[1] = drData.buffer.linesize[1];
                    args[i].imgSrc.i_stride[2] = drData.buffer.linesize[2];
                    args[i].imgSrc.i_stride[3] = drData.buffer.linesize[3];
                    args[i].imgSrc.plane[0] = drData.buffer.data[0];
                    args[i].imgSrc.plane[1] = drData.buffer.data[1];
                    args[i].imgSrc.plane[2] = drData.buffer.data[2];
                    args[i].format = AV_PIX_FMT_YUV420P;
                    switch(drData.buffer.format)
                    {
                        case X264_CSP_I420:
                        {
                            args[i].imgSrc.i_plane = args[i].imgDst.i_plane = 3;

                            args[i].imgSrc.plane[0] +=  cw*startX + x*cw/dim    + args[i].imgSrc.i_stride[0]* (ch*startY + y*ch/dim);
                            args[i].imgSrc.plane[1] += (cw*startX + x*cw/dim)/2 + args[i].imgSrc.i_stride[1]*((ch*startY + y*ch/dim)/2);
                            args[i].imgSrc.plane[2] += (cw*startX + x*cw/dim)/2 + args[i].imgSrc.i_stride[2]*((ch*startY + y*ch/dim)/2);

                            args[i].imgDst.plane[0] += x*drData.buffer.width/dim + args[i].imgDst.i_stride[0]*(y*drData.buffer.height/dim);
                            args[i].imgDst.plane[1] += x*drData.buffer.width/(dim*2) + args[i].imgDst.i_stride[1]*(y*drData.buffer.height/(dim*2));
                            args[i].imgDst.plane[2] += x*drData.buffer.width/(dim*2) + args[i].imgDst.i_stride[2]*(y*drData.buffer.height/(dim*2));
                            args[i].format = AV_PIX_FMT_YUV420P;
                        }
                        break;
                        case X264_CSP_RGB:
                        {
                            args[i].imgSrc.i_plane = args[i].imgDst.i_plane = 1;
                            args[i].imgSrc.plane[0] += 3*(cw*startX + x*cw/dim)    + args[i].imgSrc.i_stride[0]*(ch*startY + y*ch/dim);

                            args[i].imgDst.plane[0] += 3*x*drData.buffer.width/dim + args[i].imgDst.i_stride[0]*(y*drData.buffer.height/dim);
                            args[i].format = AV_PIX_FMT_RGB24;
                        }
                        break;
                    }
                    args[i].srcW = cw/dim;
                    args[i].srcH = ch/dim;
                    args[i].dstW = drData.buffer.width/dim;
                    args[i].dstH = drData.buffer.height/dim;

                    pthread_create(threads + i, NULL, scaleThread, args + i);
                }
            }
            for (int i = 0; i < dim*dim; i++)
            {
                pthread_join(threads[i], NULL);
            }

            drData.frameStep++;
            ret = 0;
            if (drData.frameStep >= drData.xRatio*drData.yRatio)
            {
                drData.frameStep = 0;
                drData.seriesStep++;
                ret = 1;
            }
        }

        pOutFrame->img.i_plane = drData.rendered.img.i_plane;
        for (int i = 0; i < pOutFrame->img.i_plane; i++) {
            pOutFrame->img.plane[i]    = drData.rendered.img.plane[i];
            pOutFrame->img.i_stride[i] = drData.rendered.img.i_stride[i];
        }
        drData.rendered.i_pts = pOutFrame->i_pts = drData.pts;
        drData.pts++;
    }
    return ret;
}


#endif
