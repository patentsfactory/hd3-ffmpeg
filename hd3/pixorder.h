#ifndef HD3_PIXORDER_H
#define HD3_PIXORDER_H

#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <hd3/movement.h>
#include <math.h>

#define PIX_ORDER_EVEN          0
#define PIX_ORDER_ODD           1
#define PIX_ORDER_ZIG_H         2
#define PIX_ORDER_ZIG_ZAG       3
#define PIX_ORDER_ZIG_CHAOS     4
#define PIX_ORDER_AWAY          5
#define PIX_ORDER_MAGIC         6

static void shuffleOrder(Point2u8* pData, uint8_t w, uint8_t h, uint8_t xFirst, uint8_t xSecond, uint8_t yFirst, uint8_t ySecond)
{
    int index = 0;
    for (int x = xFirst; x < w; x+=2)
    {
        for (int y = yFirst; y < h; y+=2)
        {
            pData[index].x = x;
            pData[index].y = y;
            index++;
        }
        for (int y = ySecond; y < h; y+=2)
        {
            pData[index].x = x;
            pData[index].y = y;
            index++;
        }
    }
    for (int x = xSecond; x < w; x+=2)
    {
        for (int y = yFirst; y < h; y+=2)
        {
            pData[index].x = x;
            pData[index].y = y;
            index++;
        }
        for (int y = ySecond; y < h; y+=2)
        {
            pData[index].x = x;
            pData[index].y = y;
            index++;
        }
    }
}
static void evenPixOrder(Point2u8* pData, uint8_t w, uint8_t h)
{
    shuffleOrder(pData, w, h, 0, 1, 0, 1);
}
static void oddPixOrder(Point2u8* pData, uint8_t w, uint8_t h)
{
    shuffleOrder(pData, w, h, 1, 0, 1, 0);
}
static void zigHPixOrder(Point2u8* pData, uint8_t w, uint8_t h)
{
    pData[0].x = pData[0].y = 0;
    int index = 1;
    int y = 0;
    for (int p = 1; p < w*h; p++)
    {
        for (int x = p; x >= 0; x--)
        {
            y = p - x;
            if (x < w && y < h)
            {
                pData[index].x = x;
                pData[index].y = y;
                index++;
            }
        }
    }
}
static void zigzagPixOrder(Point2u8* pData, uint8_t w, uint8_t h)
{
    pData[0].x = pData[0].y = 0;
    int index, y, x;
    index = 1;
    for (int p = 1; p < w*h; p++)
    {
        if (p%2)
        {
            for (y = p; y >= 0; y--)
            {
                x = p - y;
                if (x < w && y < h)
                {
                    pData[index].x = x;
                    pData[index].y = y;
                    index++;
                }
            }
        }
        else
        {
            for (y = 0; y < p; y++)
            {
                x = p - y;
                if (x < w && y < h)
                {
                    pData[index].x = x;
                    pData[index].y = y;
                    index++;
                }
            }
        }
    }
}
static void zigChaosPixOrder(Point2u8* pData, uint8_t w, uint8_t h)
{
    zigzagPixOrder(pData, w, h);
    Point2u8* p1 = (Point2u8*)malloc(sizeof(Point2u8)*w*h/2);
    Point2u8* p2 = (Point2u8*)malloc(sizeof(Point2u8)*w*h/2);

    for (int i = 0; i < w*h; i+=2)
    {
        p1[i/2] = pData[i];
        p2[i/2] = pData[i + 1];
    }
    memcpy(pData, p1, sizeof(Point2u8)*w*h/2);
    for (int i = w*h/2 - 1; i >= 0; i--)
    {
        pData[w*h - i] = p2[i];
    }

    free(p1);
    free(p2);
}
static void awayPixOrder(Point2u8* pData, uint8_t w, uint8_t h)
{
    int index, rc, max;
    index = 0;
    pData[index].x = w/2;
    pData[index].y = h/2;
    index++;
    uint8_t *pT, *pZ;
    pT = (uint8_t*)malloc(w*h);
    pZ = (uint8_t*)malloc(w*h);
    for (; index < w*h; index++)
    {
        memset(pZ, 255, w*h);
        for (int x = 0; x < w; x++)
        {
            for (int y = 0; y < h; y++)
            {
                for (int i = 0; i < index; i++)
                {
                    int d = abs(x - pData[i].x) + abs(y - pData[i].y);
                    if (pZ[x+y*w] > d)
                    {
                        pZ[x+y*w] = d;
                    }
                }
            }
        }
        rc = 0;
        max = 0;
        for (int z = 0; z < w*h; z++)
        {
            if (max < pZ[z])
            {
                max = pZ[z];
                rc = z;
                if (1 == index%2)
                {
                    break;
                }
            }
        }
        pT[rc] = index;
        pData[index].y = rc/w;
        pData[index].x = rc - pData[index].y*w;
    }
    free(pT);
    free(pZ);
}
static void magicPixOrder(Point2u8* pData, uint8_t w, uint8_t h)
{
    int n, p, i;
    n = w > h? w: h;
    int* pBase = (int*)malloc(sizeof(int)*n*n);
    if (0 == n%2)
    {
        if (0 == n%4)
        {
            i = 0;
            for (int y = 0; y < n; y++)
            {
                for (int x = 0; x < n; x++)
                {
                    pBase[i] = i;
                    if ((x%4)/2 == (y%4)/2)
                    {
                        pBase[i] = n*n - pBase[i];
                    }
                    i++;
                }
            }
        }
        else
        {
            p = n/2;
            i = 0;
            for (int y = 0; y < p; y++)
            {
                for (int x = 0; x < p; x++)
                {
                    pBase[i] = (y+x+(n-3)/2)%n;
                    pBase[i] *= n;
                    pBase[i] += (y+2*x-2)%n;
                    i++;
                }
                for (int x = p; x < n; x++)
                {
                    pBase[i] = pBase[i - p] + 2*p*p;
                    i++;
                }
            }
            for (int y = 0; y < p; y++) //sic!
            {
                for (int x = 0; x < p; x++)
                {
                    pBase[i] = pBase[y*n + x]+ 3*p*p;
                    i++;
                }
                for (int x = p; x < n; x++)
                {
                    pBase[i] = pBase[i - p] - 2*p*p;
                    i++;
                }
            }
            int k = (n-2)/4;
            for (int y = 0; y < p; y++)
            {
                for (int x = 0; x < k; x++)
                {
                    int b = pBase[x + y*n];
                    pBase[x + y*n] = pBase[x + (y+p)*n];
                    pBase[x + (y+p)*n] = b;
                }
                for (int x = n-k+1; x < n; x++)
                {
                    int b = pBase[x + y*n];
                    pBase[x + y*n] = pBase[x + (y+p)*n];
                    pBase[x + (y+p)*n] = b;
                }
            }
            int y = k+1;
            for (int x = 0; x < 1; x++)
            {
                int b = pBase[x + y*n];
                pBase[x + y*n] = pBase[x + (y+p)*n];
                pBase[x + (y+p)*n] = b;
            }
            for (int x = 0; x < y; x++)
            {
                int b = pBase[x + y*n];
                pBase[x + y*n] = pBase[x + (y+p)*n];
                pBase[x + (y+p)*n] = b;
            }
        }
    }
    else
    {
        i = 0;
        for (int y = 0; y < n; y++)
        {
            for (int x = 0; x < n; x++)
            {
                pBase[i] = (y+x+(n-3)/2)%n;
                pBase[i] *= n;
                pBase[i] += (y+2*x-2)%n;
                i++;
            }
        }
    }
    //trim to w, h
    for (int p = 0; p < w*h; p++)
    {
        i = 0;
        int min = n*n;
        for (int y = 0; y < h; y++)
        {
            for (int x = 0; x < w; x++)
            {
                if (pBase[i + x] < min)
                {
                    min = pBase[i + x];
                    pData[p].x = x;
                    pData[p].y = y;
                }
            }
            i += n;
        }
        pBase[pData[p].x + pData[p].y*n] = n*n;
    }

    free(pBase);
}
static void generatePixOrder(int type, Point2u8* pData, uint8_t w, uint8_t h)
{
    switch(type)
    {
        case PIX_ORDER_EVEN:
        {
            evenPixOrder(pData, w, h);
        }
        break;
        case PIX_ORDER_ODD:
        {
            oddPixOrder(pData, w, h);
        }
        break;
        case PIX_ORDER_ZIG_H:
        {
            zigHPixOrder(pData, w, h);
        }
        break;
        case PIX_ORDER_ZIG_ZAG:
        {
            zigzagPixOrder(pData, w, h);
        }
        break;
        case PIX_ORDER_ZIG_CHAOS:
        {
            zigChaosPixOrder(pData, w, h);
        }
        break;
        case PIX_ORDER_AWAY:
        {
            awayPixOrder(pData, w, h);
        }
        break;
        case PIX_ORDER_MAGIC:
        {
            magicPixOrder(pData, w, h);
        }
        break;
    }
}

#endif
