#ifndef HD3_FSP_H
#define HD3_FSP_H

#include "config.h"

#include "libavutil/avassert.h"
#include "libavutil/avstring.h"
#include "libavutil/opt.h"
#include "libavutil/time.h"
#include <pthread.h>

#include "libavformat/avformat.h"
#include "libavformat/http.h"
#include "libavformat/httpauth.h"
#include "libavformat/internal.h"
#include "libavformat/network.h"
#include "libavformat/os_support.h"
#include "libavformat/url.h"

#include "libavformat/http_global.h"

#define FSP_SHORT_STR 32
#define FSP_MEDIUM_STR 64
#define FSP_LONG_STR 128

#define FSP_URL_STREAM_REQUEST "stream_request/"
#define FSP_URL_GET_FILE "get_file/"
#define FSP_URL_END_SESSION "client_session_end/"

typedef struct UrlCtxWrapper {
    URLContext *ctx;
    int64_t dataLeft;
    int8_t ready;
} UrlCtxWrapper;

typedef struct HD3FSPContext {
    const AVClass *class;
    UrlCtxWrapper current;
    UrlCtxWrapper next;
    char serviceUrl[FSP_MEDIUM_STR];
    int serviceUrlLen;
    char currentFileLink[FSP_LONG_STR];
    int flags;
    int32_t token;
    pthread_t readThread;
    AVDictionary *options;
} HD3FSPContext;

static int hd3fsp_handshake(URLContext *c)//
{
//    int ret, err, new_location;
//    HTTPContext *ch = c->priv_data;
//    URLContext *cl = ch->hd;
    return 0;//AVERROR(EINVAL);
}

static int hd3fsp_listen(URLContext *h, const char *uri, int flags,//
                         AVDictionary **options) {
//    HD3FSPContext *s = h->priv_data;
//    int ret;
    return 0;
}
#define FSP_RESPONSE_STATUS "status"
#define FSP_RESPONSE_URL "url"

#define FSP_STATUS_UNKNOWN 255

typedef struct HD3FSPResponse
{
    uint8_t status;
    char url[FSP_LONG_STR];
    int32_t token;
} HD3FSPResponse;

static void hd3fsp_parseResponse(char* message, HD3FSPResponse *res)
{
    res->status = FSP_STATUS_UNKNOWN;
    memset(res->url, 0, FSP_LONG_STR);

    //printf("message: %s\n", message);
    if (message[0] == '{')
    {
        uint8_t statement = 0;
        uint8_t stringState = 0;
        char key[FSP_MEDIUM_STR];
        char value[FSP_LONG_STR];
        int kIndex = 0;
        int vIndex = 0;
        memset(key, 0, FSP_LONG_STR);
        memset(value, 0, FSP_LONG_STR);
        message++;

        while(*message) {
            switch(*message) {
                case ' ':
                case '}':
                {
                    stringState = 2;
                }
                break;
                case '\"':
                {
                    if (stringState == 1)
                        stringState = 0;
                }
                break;
                case ',':
                {
                    stringState = 2;
                    statement = 0;
                    memset(key, 0, FSP_LONG_STR);
                    memset(value, 0, FSP_LONG_STR);
                    kIndex = 0;
                    vIndex = 0;
                }
                break;
                case ':':
                {
                    if (1 != stringState) {
                        stringState = 2;
                        statement = 1;
                        break;
                    }
                }
                default:
                {
                    stringState = 1; // pending
                    if (!statement) {
                        key[kIndex++] = *message;
                        //printf("key: %s\n", key);
                    } else {
                        value[vIndex++] = *message;
                        //printf("value: %s\n", value);
                    }
                }
                break;
            };
            if (!stringState && value[0] && key[0]) {
                if (key[0] == 's' &&
                    key[1] == 't' &&
                    key[2] == 'a' &&
                    key[3] == 't' &&
                    key[4] == 'u' &&
                    key[5] == 's' &&
                    key[6] == 0)
                {
                    res->status = atoi(value);
                } else if (key[0] == 'u' &&
                           key[1] == 'r' &&
                           key[2] == 'l' &&
                           key[3] == 0) {
                    memcpy(res->url, value, FSP_LONG_STR);
                } else if (key[0] == 't' &&
                           key[1] == 'o' &&
                           key[2] == 'k' &&
                           key[3] == 'e' &&
                           key[4] == 'n' &&
                           key[5] == 0) {
                    res->token = atoi(value);
                }
            }
            message++;
        }
    }
}
static int appendToStr(char* first, const char* second)
{
    int i = 0;
    for (int j = 0; second[j]; j++)
    {
        first[i++] = second[j];
    }
    return i;
}

static uint8_t popUrlCtx(HD3FSPContext *s)
{
    if (s->next.ready == 1) {
        if (s->current.ctx) {
            ffurl_closep(&s->current.ctx);
        }
        s->current.ctx = s->next.ctx;
        s->current.dataLeft = s->next.dataLeft;
        s->current.ready = 1;

        s->next.ctx = NULL;
        s->next.dataLeft = 0;
        s->next.ready = 0;
        return 1;
    }
    return 0;
}

static int hd3fsp_connect(URLContext *h)
{
    HD3FSPContext *s = h->priv_data;
    char requestUrl[FSP_LONG_STR];
    char message[FSP_LONG_STR];
    int timeout = 60;
    int8_t fileReady = 0;
    int off = 0;
    int ret = 0;
    URLContext *tmp;
    HD3FSPResponse response;

    memset(requestUrl, 0, FSP_LONG_STR);
    memset(message, 0, FSP_LONG_STR);
    s->next.ready = -1;

    //wait for file

    memset(requestUrl, 0, FSP_LONG_STR);
    memcpy(requestUrl, s->serviceUrl, s->serviceUrlLen);
    off = appendToStr(requestUrl + s->serviceUrlLen, FSP_URL_GET_FILE);
    sprintf(requestUrl + s->serviceUrlLen + off, "%d/", s->token);

    ret = ffurl_alloc(&tmp, requestUrl, s->flags, NULL);

    while(timeout && 0 == fileReady)
    {
        memset(message, 0, FSP_LONG_STR);
        http_open(tmp, requestUrl, s->flags, NULL);
        http_read(tmp, message, FSP_LONG_STR);
        hd3fsp_parseResponse(message, &response);
        if (!response.status && response.url[0] != 0) {
            fileReady = 1;
        }
        timeout--;
        sleep(1);
    }
    ffurl_closep(&tmp);
    ret = (timeout == 0 && 0 == fileReady);
    if (!ret) {
        //stream file
        ret = ffurl_alloc(&s->next.ctx, response.url, s->flags, &h->interrupt_callback);
        if (!ret) {
            printf("open %s\n", response.url);
            ret = http_open(s->next.ctx, response.url, s->flags, &s->options);
        }
        if (!ret) {
            HTTPContext* ctx = s->next.ctx->priv_data;
            s->next.dataLeft = ctx->filesize;
        }
    }
    s->next.ready = ret == 0;
    printf("opened: %d\n", s->next.ready);
    return -abs(ret);
}

static void connectThread(URLContext *h) {
    HD3FSPContext *s = h->priv_data;
    while(1) {
        printf("try connect: %d\n", s->next.ready);
        if (0 == s->next.ready) {
            hd3fsp_connect(h);
        } else {
            sleep(1);
        }
    }
}

static int hd3fsp_open(URLContext *h, const char *uri, int flags,
                     AVDictionary **options)//
{
    HD3FSPContext *s = h->priv_data;
    char requestUrl[FSP_LONG_STR];
    char message[FSP_LONG_STR];
    int ret = 0;
    int i = 0;
    URLContext *tmp;
    HD3FSPResponse response;
    memset(s->serviceUrl, 0, FSP_MEDIUM_STR);
    memset(s->currentFileLink, 0, FSP_LONG_STR);

    //http://kaczirosan.vipserv.org/filestream/
    //hd3fsp://kaczirosan.vipserv.org/filestream/

    //parse link
    memset(requestUrl, 0, FSP_LONG_STR);

    s->serviceUrl[i++] = 'h';
    s->serviceUrl[i++] = 't';
    s->serviceUrl[i++] = 't';
    s->serviceUrl[i++] = 'p';
    s->serviceUrl[i++] = ':';
    s->serviceUrl[i++] = '/';
    s->serviceUrl[i++] = '/';

    for (int j = 9; h->filename[j]; j++)
    {
        s->serviceUrl[i++] = h->filename[j];
    }
    if (s->serviceUrl[i - 1] != '/') {
        s->serviceUrl[i++] = '/';
    }
    memcpy(requestUrl, s->serviceUrl, i);
    s->serviceUrlLen = i;
    s->flags = flags;
    appendToStr(requestUrl + i, FSP_URL_STREAM_REQUEST);

    ret |= ffurl_alloc(&tmp, requestUrl, flags, NULL);

    //send request
    memset(message, 0, FSP_LONG_STR);
    http_open(tmp, requestUrl, flags, NULL);
    http_read(tmp, message, FSP_LONG_STR);
    //check status is ok
    hd3fsp_parseResponse(message, &response);

    s->token = -1;

    av_dict_copy(&s->options, *options, 0);
    av_dict_set(&s->options, "protocol_whitelist", "http,tcp", 0);

    ret = response.status;
    if (!ret) {
        s->token = response.token;
        ret = hd3fsp_connect(h);
        if (!ret) {
            popUrlCtx(s);
            pthread_create(s->readThread, NULL, connectThread, h);
        }
    }
    if (ret != 0) {
        printf("error %d\n", ret);
    }
    return -abs(ret);
}

static int hd3fsp_read(URLContext *h, uint8_t *buf, int size) // kacziro - important - http read data
{
    HD3FSPContext *s = h->priv_data;
    int readed = 0;
    if (s->current.ready)
    {
        HTTPContext* ctx = s->current.ctx->priv_data;
        readed = http_read(s->current.ctx, buf, size);//size;
        if (readed > 0) {
    //        if (s->next.ready == 0)
    ////        if (s->current.dataLeft > ctx->filesize/2 &&
    ////            s->current.dataLeft - readed < ctx->filesize/2)
    //        {
    //            s->next.ready = -1;
    //            printf("hd3fsp_read %d -> %d [%d .. %d]\n", size, readed, ctx->filesize, s->current.dataLeft);
    //            pthread_t thread;
    //            pthread_create(&thread, NULL, hd3fsp_connect, h);
    //        }
            s->current.dataLeft -= readed;
        }
    }
    return readed;
}

static int hd3fsp_write(URLContext *h, const uint8_t *buf, int size) //
{
    HD3FSPContext *s = h->priv_data;
    return http_write(s->current.ctx, buf, size);//size;
}

static int hd3fsp_shutdown(URLContext *h, int flags) //
{
    HD3FSPContext *s = h->priv_data;
    return http_shutdown(s->current.ctx, flags);
}

static int hd3fsp_close(URLContext *h) //
{
    HD3FSPContext *s = h->priv_data;
    char requestUrl[FSP_LONG_STR];
    char message[FSP_LONG_STR];
    int off = 0;
    URLContext *tmp;
    HD3FSPResponse response;

    memset(requestUrl, 0, FSP_LONG_STR);
    memset(message, 0, FSP_LONG_STR);

    memcpy(requestUrl, s->serviceUrl, s->serviceUrlLen);
    off = appendToStr(requestUrl + s->serviceUrlLen, FSP_URL_END_SESSION);
    sprintf(requestUrl + s->serviceUrlLen + off, "%d/", s->token);
    ffurl_alloc(&tmp, requestUrl, s->flags, NULL);

    http_open(tmp, requestUrl, s->flags, NULL);
    http_read(tmp, message, FSP_LONG_STR);

    hd3fsp_parseResponse(message, &response);

    return http_close(s->current.ctx);
}

static int64_t hd3fsp_seek(URLContext *h, int64_t off, int whence) //
{
    HD3FSPContext *s = h->priv_data;
    int64_t size = 0;
    if (off == 0 && whence == 0)
    {
        int ret = popUrlCtx(s);
        if (!ret) {
            printf("not yet\n");
            //return -1;
        }
    }
    printf("seek %d, %d\n", off, whence);
    size = http_seek(s->current.ctx, off, whence);
    printf("hd3fsp_seek %I64d, %d; size: %I64d | %s\n", off, whence, size, s->current.ctx->filename);
    return size;
}

static int hd3fsp_get_file_handle(URLContext *h) //
{
    HD3FSPContext *s = h->priv_data;
    return http_get_file_handle(s->current.ctx);
}

static const AVClass hd3fsp_context_class = {
    .class_name = "hd3fsp",
    .item_name  = av_default_item_name,
    .option     = NULL,//options,
    .version    = LIBAVUTIL_VERSION_INT,
};

const URLProtocol ff_hd3fsp_protocol = {
    .name                = "hd3fsp",
    .url_open2           = hd3fsp_open,
    .url_accept          = NULL,
    .url_handshake       = hd3fsp_handshake,
    .url_read            = hd3fsp_read,
    .url_write           = hd3fsp_write,
    .url_seek            = hd3fsp_seek,
    .url_close           = hd3fsp_close,
    .url_get_file_handle = hd3fsp_get_file_handle,
    .url_shutdown        = hd3fsp_shutdown,
    .priv_data_size      = sizeof(HD3FSPContext),
    .priv_data_class     = &hd3fsp_context_class,
    .flags               = URL_PROTOCOL_FLAG_NETWORK,
    .default_whitelist   = "hd3fsp"
};

#endif
