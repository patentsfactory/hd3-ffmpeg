#ifndef PF_BRIDGE_H
#define PF_BRIDGE_H

#include <windows.h>
#include "hd3/movement.h"

/////////////////////
static int fillFrameHeader(uint8_t *pBuff, AVFrame *frame) {
    pBuff[0] = 0; // type
    *(uint16_t*)(pBuff + 1) = (uint16_t)frame->width;
    *(uint16_t*)(pBuff + 3) = (uint16_t)frame->height;
    pBuff[5] = 1; // RGB - format
    return 6;
}
static int fillMovementHeader (uint8_t *pBuff, FrameBoxes* boxes) {
    int size = 0;
    if (boxes != NULL) {
        //boxes->pts
        pBuff[0] = 1; // type
        *(int32_t*)(pBuff + 1) = boxes->number*sizeof(uint16_t)*4;
        pBuff[5] = 0;
        size = 6;
    }
    return size;
}

static int convertToRGBPkg(uint8_t *pBuff, AVFrame *frame, FrameBoxes *boxes) {
    uint8_t *pDst = pBuff;
    pDst += fillFrameHeader(pBuff, frame);
    if (AV_PIX_FMT_RGB24 == frame->format) {
        for (int y = 0; y < frame->height; y++) {
            memcpy(pDst + y*frame->width*3, frame->data[0] + frame->linesize[0]*y, frame->width*3);
        }
    } else if (AV_PIX_FMT_YUV420P == frame->format) {
        SwsContext* ctx = sws_getCachedContext(NULL,
                                            frame->width, frame->height, frame->format,
                                            frame->width, frame->height, AV_PIX_FMT_RGB24,
                                            SWS_POINT, NULL, NULL, NULL);
        if (ctx != NULL) {
            uint8_t *dstChannels[AV_NUM_DATA_POINTERS];
            memset(dstChannels, 0, sizeof(dstChannels));
            dstChannels[0] = pDst;

            int dstLinesize[AV_NUM_DATA_POINTERS];
            memset(dstLinesize, 0, sizeof(dstLinesize));
            dstLinesize[0] = frame->width*3;

            sws_scale(ctx, frame->data, frame->linesize, 0, frame->height, (const uint8_t * const *)dstChannels, dstLinesize);
            sws_freeContext(ctx);
        }
    }
    pDst += frame->width*frame->height*3;
    if (boxes) {
        pDst = pDst + fillMovementHeader(pDst, boxes);
        {
            uint16_t* pD = (uint16_t*)pDst;
            for (int i = 0; i < boxes->number; i++) {
                pD[i*4]     = boxes->boxes[i].aabb.begin.x;
                pD[i*4 + 1] = boxes->boxes[i].aabb.begin.y;
                pD[i*4 + 2] = boxes->boxes[i].aabb.end.x;
                pD[i*4 + 3] = boxes->boxes[i].aabb.end.y;
            }
            //printf("nr: %d, %d+%d\n", boxes->number, pDst - pBuff, boxes->number*sizeof(uint16_t)*4);
            pDst += boxes->number*sizeof(uint16_t)*4;
        }
    }
    return pDst - pBuff;
}

/////////////////////
typedef struct SoketBridgeCtx {
    WSADATA wsaData;

    int32_t mainPort;
    SOCKET mainSock;
    SOCKADDR_IN saddr;
    fd_set rs;
    fd_set ws;

    SOCKET clientSocket;
    uint8_t isClient;

    uint8_t *pBuff;
} SoketBridgeCtx;

////////////////////

static int soketBridgeInit(SoketBridgeCtx* ctx) {
    int optval = 1;
    ctx->mainPort = 5151;
    WSAStartup(MAKEWORD(2,2), &ctx->wsaData);

    ctx->mainSock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    memset((void*)&ctx->saddr, 0, sizeof(ctx->saddr));
    ctx->saddr.sin_family = AF_INET;
    ctx->saddr.sin_port = htons(ctx->mainPort);
    ctx->saddr.sin_addr.s_addr = htonl(INADDR_ANY);

    ctx->pBuff = NULL;

    if (setsockopt(ctx->mainSock, SOL_SOCKET, SO_REUSEADDR, (char*)&optval, sizeof(optval)) == SOCKET_ERROR) {
        printf("[BRIDGE] Main socket error - set OPT: %lu", GetLastError());
        return -1;
    }
    if (bind(ctx->mainSock, (struct sockaddr*)&ctx->saddr, sizeof(ctx->saddr)) == SOCKET_ERROR) {
        printf("[BRIDGE] Main socket error - bind: %lu", GetLastError());
        return -1;
    }
    if (listen(ctx->mainSock, 1) == SOCKET_ERROR) {
        printf("[BRIDGE] Main socket error - listen: %lu", GetLastError());
        return -1;
    }
    return 0;
}

static void soketBridgeUpdate(SoketBridgeCtx* ctx, AVFrame *frame) {
    struct timeval t;
    int e = SOCKET_ERROR;
    FD_ZERO(&ctx->rs);
    FD_ZERO(&ctx->ws);

    if (ctx->isClient) {
        FD_SET(ctx->clientSocket, &ctx->rs);
        FD_SET(ctx->clientSocket, &ctx->ws);
    }
    if (ctx->pBuff == NULL) {
        ctx->pBuff = malloc(frame->width*frame->height*3*2);
    }
    FD_SET(ctx->mainSock, &ctx->rs);
    t.tv_sec = 0;
    t.tv_usec = 100;

    e = select(0, &ctx->rs, &ctx->ws, NULL, &t);
    if (e != SOCKET_ERROR) {
        if (FD_ISSET(ctx->clientSocket, &ctx->ws)) {
            uint8_t *pDst = ctx->pBuff + 5;
            int size = convertToRGBPkg(ctx->pBuff, frame, NULL);
            int chunkSize = size/10;
            e = send(ctx->clientSocket, ctx->pBuff, 5, 0); // header
            for (int i = 0; i < 10; i++) {
                e = send(ctx->clientSocket, pDst + i*chunkSize, chunkSize, 0); // kacziro - tmp
            }
            if (e == SOCKET_ERROR) {
                ctx->isClient = 0;
                closesocket(ctx->clientSocket);
                printf("[error][send]\n");
            }
        }
        if (FD_ISSET(ctx->mainSock, &ctx->rs))
        {
            SOCKET sock = accept(ctx->mainSock, NULL, NULL);
            closesocket(ctx->clientSocket);
            ctx->clientSocket = sock;
            ctx->isClient = 1;
        }
    } else {
        printf("[error][select]\n");
        ctx->isClient = 0;
        closesocket(ctx->clientSocket);
    }
}

static void soketBridgeDestroy(SoketBridgeCtx* ctx) {
    free(ctx->pBuff);
    closesocket(ctx->mainSock);
    WSACleanup();
}

////////////////////

typedef struct FileMappingBridgeCtx {
   HANDLE hMapFile;
   LPCTSTR fileBuff;

   uint8_t *pBuff;
} FileMappingBridgeCtx;

static int fileMappingBridgeInit(FileMappingBridgeCtx *ctx, const char *name) {
    uint32_t maxPkgSize = 180000801;//12000*5000*3;
    ctx->hMapFile = CreateFileMappingA(
                        INVALID_HANDLE_VALUE,
                        NULL,
                        PAGE_READWRITE,
                        0,
                        maxPkgSize,
                        name);//"PF_BRIDGE");

    if (ctx->hMapFile == NULL)
    {
        DWORD e = GetLastError();
        printf("Could not create file mapping object (%x).\n", e);
        return -1;
    }
    ctx->fileBuff = (LPTSTR) MapViewOfFile(ctx->hMapFile,   // handle to map object
                                            FILE_MAP_ALL_ACCESS, // read/write permission
                                            0,
                                            0,
                                            maxPkgSize);
    return 0;
}

static void fileMappingBridgeUpdate(FileMappingBridgeCtx *ctx, AVFrame *frame, HD3SEIData* pSei) {
    FrameBoxes *boxes = NULL;
    if (ctx->pBuff == NULL) {
        ctx->pBuff = malloc(frame->width*frame->height*3*2);
    }
    //check modulo
    if (pSei) {
        for (int i = 0; i < pSei->maxNumber; i++) {
            if (pSei->pBoxes[i].pts == frame->pkt_pts) {
                boxes = pSei->pBoxes + i;
                break;
            }
        }
    }
    {
        int size = convertToRGBPkg(ctx->pBuff, frame, boxes);
//        printf("pts: %d\n", frame->pts%256);//size);
        CopyMemory((PVOID)ctx->fileBuff, ctx->pBuff, size);
    }
}

static void fileMappingBridgeDstroy(FileMappingBridgeCtx *ctx) {
   UnmapViewOfFile(ctx->fileBuff);
   CloseHandle(ctx->hMapFile);
}

#endif
