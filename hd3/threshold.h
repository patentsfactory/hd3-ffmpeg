#ifndef HD3_THRESHOLD_BB_H
#define HD3_THRESHOLD_BB_H

#include <stdint.h>

static int compareFloatsAsc(const void* a, const void* b )
{
    float fa, fb;
    fa = * ( (float*) a );
    fb = * ( (float*) b );

    if ( fa == fb ) return 0;
    else if ( fa < fb ) return -1;
    else return 1;
}
static float calculateThreashold(float *pMean, float *pStd, int gridSize, float mod, int minLen, int maxLen, uint8_t mix, float lastThreshold)
{
    qsort(pStd, gridSize, sizeof(float), compareFloatsAsc);
    qsort(pMean, gridSize, sizeof(float), compareFloatsAsc);
    float mmin, smin, mmax;
    mmin = mmax = smin = 0.0f;

    int counterM, counterStd;
    counterStd = counterM = 0;
    for (int i = 0; i < gridSize && counterM < minLen && counterStd < minLen; i++)
    {
        if (pMean[i] > 0 && counterM < minLen)
        {
            mmin += pMean[i];
            counterM++;
        }
        if (pStd[i] > 0 && counterStd < minLen)
        {
            smin += sqrt(pStd[i]*mod); // sqrt only for selected
            counterStd++;
        }
    }
    if (counterStd > 0)
    {
        smin /= counterStd;
    }
    if (counterM > 0)
    {
        mmin /= counterM;
    }
    float threshold = mmin + mix*smin;
    pMean += gridSize;
    for (int i = 0; i < maxLen; i++)
    {
        mmax += pMean[- 1 - i];
    }
    if (threshold >= mmax)
    {
        threshold = lastThreshold;
    }
    return threshold;
}
static float calculateThreasholdForCumulation(float *pMean, float *pStd, int gridSize, float mod,
                                              int minLen, int maxLen, float stdglobal, float meanglobal)
{
    qsort(pStd, gridSize, sizeof(float), compareFloatsAsc);
    qsort(pMean, gridSize, sizeof(float), compareFloatsAsc);
    float mmin, smin, smax, mmax;
    mmin = mmax = smin = smax = 0.0f;

    int counterM, counterStdMin, counterStdMax;
    counterStdMin = counterStdMax = counterM = 0;
    for (int i = 0; i < gridSize && (counterM < minLen || counterStdMin < minLen || counterStdMax < maxLen); i++)
    {
        if (pMean[i] > 0 && counterM < minLen)
        {
            mmin += pMean[i];
            counterM++;
        }
        if (pStd[i] > 0 && counterStdMin < minLen)
        {
            smin += sqrt(pStd[i]); // sqrt only for selected
            counterStdMin++;
        }
        if (counterStdMax < maxLen)
        {
            smax += sqrt(pStd[gridSize - i - 1]); // sqrt only for selected
            counterStdMax++;
        }
    }
    if (counterStdMin > 0)
    {
        smin /= counterStdMin;
    }
    if (counterM > 0)
    {
        mmin /= counterM;
    }
    if (counterM > 0)
    {
        smax /= counterStdMax;
    }
    float maxThreshold = 25.0f; //kacziro - question - what has greater frequency: 3, 25, stdglobal or (stdglobal)*(meanglobal+3*stdmin) ???
    float mix = smax/(2*stdglobal) + (mmax * stdglobal)/(meanglobal * smax);
    float threshold = MAX(3, (meanglobal + 3*smin)*mix);
    threshold = (1 + ((maxThreshold - threshold)/maxThreshold))*threshold;

    return threshold;
}
static float calculateThreasholdGray(float *pMean, float *pStd, int gridSize, float mod,
                                     int minLen, int maxLen, float stdglobal, float meanglobal, float lastThreshold)
{
    qsort(pStd, gridSize, sizeof(float), compareFloatsAsc);
    qsort(pMean, gridSize, sizeof(float), compareFloatsAsc);
    float mmin, smin, mmax;
    mmin = mmax = smin = 0.0f;

    int counterM, counterStd;
    counterStd = counterM = 0;
    for (int i = 0; i < gridSize && counterM < minLen && counterStd < minLen; i++)
    {
        if (pMean[i] > 0 && counterM < minLen)
        {
            mmin += pMean[i];
            counterM++;
        }
        if (pStd[i] > 0 && counterStd < minLen)
        {
            smin += sqrt(pStd[i]); // sqrt only for selected
            counterStd++;
        }
    }
    if (counterStd > 0)
    {
        smin /= counterStd;
    }
    if (counterM > 0)
    {
        mmin /= counterM;
    }
    float maxThreshold = 25.0f;
    //kacziro - question - what has greater frequency: 3, 25, stdglobal or (stdglobal)*(meanglobal+3*stdmin) ???
    float threshold = MAX(3, MIN(maxThreshold, MIN(stdglobal, (stdglobal)*(meanglobal+3*smin))));
    threshold = (1+((maxThreshold-threshold)/maxThreshold))*threshold;
    pMean += gridSize;
    for (int i = 0; i < maxLen; i++) // kacziro - fixme - change to JM alg.
    {
        mmax += pMean[- 1 - i];
    }
    if (threshold >= mmax)
    {
        threshold = lastThreshold;
    }
    return threshold;
}

#endif
