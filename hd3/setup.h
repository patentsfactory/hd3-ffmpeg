#ifndef HD3_SETUP_H
#define HD3_SETUP_H

#define MOVEMENT_BOX_DIM 32
#include <time.h>
#include <stdint.h>

typedef struct AvgData
{
    float sum;
    uint32_t number;
} AvgData;
typedef struct PerformanceData
{
    AvgData update;
    AvgData mean;
    AvgData threshold;
    AvgData scaling;
    AvgData box;
    AvgData render;
    AvgData cna;
    AvgData gen;
    AvgData std;
    AvgData edge;
    AvgData subEdge;

} PerformanceData;
static PerformanceData perfData;

#endif
