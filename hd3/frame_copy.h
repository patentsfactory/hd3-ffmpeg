#ifndef FRAMECOPY_H
#define FRAMECOPY_H

#include "libavcodec/avcodec.h"

typedef struct FrameCopy
{
    uint8_t *data[AV_NUM_DATA_POINTERS];
    int linesize[AV_NUM_DATA_POINTERS];
    int width, height;
    int64_t pts;
    int format;
} FrameCopy;

static void initFrameCopy(AVCodecContext *ctx, FrameCopy *pDst, int width, int height, int format)
{
    //memset(pDst, 0, sizeof(FrameCopy));
    int baseSize;
    pDst->pts = 0;
    pDst->width = width;
    pDst->height = height;
    pDst->format = format;
    if (width % 64 != 0) {
        width += 64 - (width % 64);
    }
    baseSize = width*height;
    switch(format) {
        case AV_PIX_FMT_YUV420P:
        {
            pDst->data[0] = (uint8_t*)malloc(baseSize);
            baseSize /= 4;
            pDst->data[1] = (uint8_t*)malloc(baseSize);
            pDst->data[2] = (uint8_t*)malloc(baseSize);

            pDst->linesize[0] = width;
            pDst->linesize[1] = width/2;
            pDst->linesize[2] = pDst->linesize[1];
            for (int i = 3; i < AV_NUM_DATA_POINTERS; i++) {
                pDst->data[i] = NULL;
                pDst->linesize[i] = 0;
            }
        } break;
        case AV_PIX_FMT_YUV422P:
        {
            pDst->data[0] = (uint8_t*)malloc(baseSize);
            baseSize /= 2;
            pDst->data[1] = (uint8_t*)malloc(baseSize);
            pDst->data[2] = (uint8_t*)malloc(baseSize);

            pDst->linesize[0] = width;
            pDst->linesize[1] = width/2;
            pDst->linesize[2] = pDst->linesize[1];
            for (int i = 3; i < AV_NUM_DATA_POINTERS; i++) {
                pDst->data[i] = NULL;
                pDst->linesize[i] = 0;
            }
        } break;
        case AV_PIX_FMT_YUV444P:
        {
            pDst->data[0] = (uint8_t*)malloc(baseSize);
            pDst->data[1] = (uint8_t*)malloc(baseSize);
            pDst->data[2] = (uint8_t*)malloc(baseSize);

            pDst->linesize[0] = width;
            pDst->linesize[1] = pDst->linesize[0];
            pDst->linesize[2] = pDst->linesize[1];
            for (int i = 3; i < AV_NUM_DATA_POINTERS; i++) {
                pDst->data[i] = NULL;
                pDst->linesize[i] = 0;
            }
        } break;
        case AV_PIX_FMT_RGB24:
        {
            pDst->data[0] = (uint8_t*)malloc(3*baseSize);
            pDst->linesize[0] = width*3;

            for (int i = 1; i < AV_NUM_DATA_POINTERS; i++) {
                pDst->data[i] = NULL;
                pDst->linesize[i] = 0;
            }
        } break;
        case AV_PIX_FMT_BGRA:
        {
            pDst->data[0] = (uint8_t*)malloc(4*baseSize);
            pDst->linesize[0] = width*4;
            for (int i = 1; i < AV_NUM_DATA_POINTERS; i++) {
                pDst->data[i] = NULL;
                pDst->linesize[i] = 0;
            }
        } break;
        //case AV_PIX_FMT_GBR24P:
        case AV_PIX_FMT_GBRP:
        {
            pDst->data[0] = (uint8_t*)malloc(baseSize);
            pDst->data[1] = (uint8_t*)malloc(baseSize);
            pDst->data[2] = (uint8_t*)malloc(baseSize);
            pDst->linesize[0] = width;
            pDst->linesize[1] = width;
            pDst->linesize[2] = width;
            for (int i = 3; i < AV_NUM_DATA_POINTERS; i++) {
                pDst->data[i] = NULL;
                pDst->linesize[i] = 0;
            }
        } break;
        case AV_PIX_FMT_GBRAP:
        {
            pDst->data[0] = (uint8_t*)malloc(baseSize);
            pDst->data[1] = (uint8_t*)malloc(baseSize);
            pDst->data[2] = (uint8_t*)malloc(baseSize);
            pDst->data[3] = (uint8_t*)malloc(baseSize);
            pDst->linesize[0] = width;
            pDst->linesize[1] = width;
            pDst->linesize[2] = width;
            pDst->linesize[3] = width;
            for (int i = 4; i < AV_NUM_DATA_POINTERS; i++) {
                pDst->data[i] = NULL;
                pDst->linesize[i] = 0;
            }
        } break;
        case AV_PIX_FMT_GRAY16BE:
        {
            pDst->data[0] = (uint8_t*)malloc(sizeof(uint16_t)*baseSize);
            pDst->linesize[0] = width*sizeof(uint16_t);

            for (int i = 1; i < AV_NUM_DATA_POINTERS; i++) {
                pDst->data[i] = NULL;
                pDst->linesize[i] = 0;
            }
        } break;
        case AV_PIX_FMT_BAYER_GBRG8:
        case AV_PIX_FMT_GRAY8:
        {
            pDst->data[0] = (uint8_t*)malloc(baseSize);
            pDst->linesize[0] = width*sizeof(uint8_t);

            for (int i = 1; i < AV_NUM_DATA_POINTERS; i++) {
                pDst->data[i] = NULL;
                pDst->linesize[i] = 0;
            }
        } break;
        default:
        {
            av_log(ctx, AV_LOG_ERROR, "Error: not implemented pixel format %d\n", format);
        } break;
    }
}
static uint8_t isInitializedFrameCopy(FrameCopy* pFrame) {
    return pFrame->data[0] != NULL;
}
static void destroyFrameCopy(FrameCopy* pFrame)
{
    for (int i = 0; i < AV_NUM_DATA_POINTERS; i++) {
        //x264_free(pFrame->data[i]);
        free(pFrame->data[i]);
    }
    memset(pFrame, 0, sizeof(FrameCopy));
}
#endif
