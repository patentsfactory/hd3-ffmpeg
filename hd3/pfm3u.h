#ifndef HD3_PFM3U_H
#define HD3_PFM3U_H

#include "config.h"

#include "libavutil/avassert.h"
#include "libavutil/avstring.h"
#include "libavutil/opt.h"
#include "libavutil/time.h"

#include "libavformat/avformat.h"
#include "libavformat/http.h"
#include "libavformat/httpauth.h"
#include "libavformat/internal.h"
#include "libavformat/network.h"
#include "libavformat/os_support.h"
#include "libavformat/url.h"

#include "libavformat/http_global.h"

#define PFM3U_SHORT_STR 32
#define PFM3U_MEDIUM_STR 64
#define PFM3U_LONG_STR 128

#define PFM3U_URL_STREAM_REQUEST "stream_request/"
#define PFM3U_URL_GET_PLAYLIST "get_playlist/"
#define PFM3U_URL_END_SESSION "client_session_end/"

typedef struct PFM3UContext {
    const AVClass *class;
    URLContext *ctx;
    int64_t dataLeft;
    int8_t ready;
    char serviceUrl[FSP_MEDIUM_STR];
    int serviceUrlLen;
    char currentFileLink[FSP_LONG_STR];
    int flags;
    int32_t token;
    AVDictionary *options;
} PFM3UContext;

static int pfm3u_handshake(URLContext *c)//
{
//    int ret, err, new_location;
//    HTTPContext *ch = c->priv_data;
//    URLContext *cl = ch->hd;
    return 0;//AVERROR(EINVAL);
}

#define PFM3U_RESPONSE_STATUS "status"
#define PFM3U_RESPONSE_URL "url"

#define PFM3U_STATUS_UNKNOWN 255

typedef struct PFM3UResponse
{
    uint8_t status;
    char url[FSP_LONG_STR];
    int32_t token;
} PFM3UResponse;

static void pfm3u_parseResponse(char* message, PFM3UResponse *res)
{
    res->status = FSP_STATUS_UNKNOWN;
    memset(res->url, 0, FSP_LONG_STR);

    if (message[0] == '{')
    {
        uint8_t statement = 0;
        uint8_t stringState = 0;
        char key[FSP_MEDIUM_STR];
        char value[FSP_LONG_STR];
        int kIndex = 0;
        int vIndex = 0;
        message++;
        memset(key, 0, FSP_LONG_STR);
        memset(value, 0, FSP_LONG_STR);

        while(*message) {
            switch(*message) {
                case ' ':
                case '}':
                {
                    stringState = 2;
                }
                break;
                case '\"':
                {
                    if (stringState == 1)
                        stringState = 0;
                }
                break;
                case ',':
                {
                    stringState = 2;
                    statement = 0;
                    memset(key, 0, FSP_LONG_STR);
                    memset(value, 0, FSP_LONG_STR);
                    kIndex = 0;
                    vIndex = 0;
                }
                break;
                case ':':
                {
                    if (1 != stringState) {
                        stringState = 2;
                        statement = 1;
                        break;
                    }
                }
                default:
                {
                    stringState = 1; // pending
                    if (!statement) {
                        key[kIndex++] = *message;
                        //printf("key: %s\n", key);
                    } else {
                        value[vIndex++] = *message;
                        //printf("value: %s\n", value);
                    }
                }
                break;
            };
            if (!stringState && value[0] && key[0]) {
                if (key[0] == 's' &&
                    key[1] == 't' &&
                    key[2] == 'a' &&
                    key[3] == 't' &&
                    key[4] == 'u' &&
                    key[5] == 's' &&
                    key[6] == 0)
                {
                    res->status = atoi(value);
                } else if (key[0] == 'u' &&
                           key[1] == 'r' &&
                           key[2] == 'l' &&
                           key[3] == 0) {
                    memcpy(res->url, value, FSP_LONG_STR);
                } else if (key[0] == 't' &&
                           key[1] == 'o' &&
                           key[2] == 'k' &&
                           key[3] == 'e' &&
                           key[4] == 'n' &&
                           key[5] == 0) {
                    res->token = atoi(value);
                }
            }
            message++;
        }
    }
}
static int pfm3u_appendToStr(char* first, const char* second)
{
    int i = 0;
    for (int j = 0; second[j]; j++)
    {
        first[i++] = second[j];
    }
    return i;
}

static int pfm3u_connect(URLContext *h)
{
    char requestUrl[FSP_LONG_STR];
    char message[FSP_LONG_STR];
    //wait for file
    int timeout = 60;
    int8_t fileReady = 0;
    //int off = 0;
    int ret = 0;
    PFM3UResponse response;
    URLContext *tmp;

    PFM3UContext *s = h->priv_data;
    memset(requestUrl, 0, FSP_LONG_STR);
    memset(message, 0, FSP_LONG_STR);
    s->ready = -1;

    memset(requestUrl, 0, FSP_LONG_STR);
    memcpy(requestUrl, s->serviceUrl, s->serviceUrlLen);
    pfm3u_appendToStr(requestUrl + s->serviceUrlLen, PFM3U_URL_GET_PLAYLIST);
    //off = pfm3u_appendToStr(requestUrl + s->serviceUrlLen, PFM3U_URL_GET_PLAYLIST);
    //sprintf(requestUrl + s->serviceUrlLen + off, "%d/", s->token);

    ret = ffurl_alloc(&tmp, requestUrl, s->flags, NULL);

    while(timeout && 0 == fileReady)
    {
        memset(message, 0, FSP_LONG_STR);
        http_open(tmp, requestUrl, s->flags, NULL);
        http_read(tmp, message, FSP_LONG_STR);
        hd3fsp_parseResponse(message, &response);
        if (!response.status && response.url[0] != 0) {
            fileReady = 1;
        }
        timeout--;
        sleep(1);
    }
    ffurl_closep(&tmp);
    ret = (timeout == 0 && 0 == fileReady);
    if (!ret) {
        //stream file
        ret = ffurl_alloc(&s->ctx, response.url, s->flags, &h->interrupt_callback);
        if (!ret) {
            printf("open %s\n", response.url);
            ret = http_open(s->ctx, response.url, s->flags, &s->options);
        }
        if (!ret) {
            HTTPContext* ctx = s->ctx->priv_data;
            s->dataLeft = ctx->filesize;
            // kacziro - warning - assumption: h->filename is ptr
            memset(h->filename, 0, strlen(response.url)+1);
            memcpy(h->filename, response.url, strlen(response.url));
            av_free(h->protocol_whitelist);
            h->protocol_whitelist = av_strdup("pfm3u,http,tcp");
            h->is_streamed = 1;
        }
    }
    s->ready = ret == 0;
    printf("opened: %d\n", s->ready);
    return -abs(ret);
}

static int pfm3u_open(URLContext *h, const char *uri, int flags,
                     AVDictionary **options)//
{
    int ret = 0;
    char requestUrl[FSP_LONG_STR];
    char message[FSP_LONG_STR];
    int i = 0;
    URLContext *tmp;

    PFM3UContext *s = h->priv_data;
    memset(s->serviceUrl, 0, FSP_MEDIUM_STR);
    memset(s->currentFileLink, 0, FSP_LONG_STR);

    //http://kaczirosan.vipserv.org/filestream/
    //pfm3u://kaczirosan.vipserv.org/filestream/

    //parse link
    memset(requestUrl, 0, FSP_LONG_STR);

    s->serviceUrl[i++] = 'h';
    s->serviceUrl[i++] = 't';
    s->serviceUrl[i++] = 't';
    s->serviceUrl[i++] = 'p';
    s->serviceUrl[i++] = ':';
    s->serviceUrl[i++] = '/';
    s->serviceUrl[i++] = '/';

    for (int j = 8; h->filename[j]; j++)
    {
        s->serviceUrl[i++] = h->filename[j];
    }
    if (s->serviceUrl[i - 1] != '/') {
        s->serviceUrl[i++] = '/';
    }
    memcpy(requestUrl, s->serviceUrl, i);
    s->serviceUrlLen = i;
    s->flags = flags;
    pfm3u_appendToStr(requestUrl + i, FSP_URL_STREAM_REQUEST);

    ret |= ffurl_alloc(&tmp, requestUrl, flags, NULL);

    //send request
    memset(message, 0, FSP_LONG_STR);
    http_open(tmp, requestUrl, flags, NULL);
    http_read(tmp, message, FSP_LONG_STR);
    //check status is ok
    PFM3UResponse response;
    pfm3u_parseResponse(message, &response);

    s->token = -1;

    av_dict_copy(&s->options, *options, 0);
    av_dict_set(&s->options, "protocol_whitelist", "http,tcp", 0);

    ret = response.status;
    if (!ret) {
        s->token = response.token;
        ret = pfm3u_connect(h);
    }
    if (ret != 0) {
        printf("error %d\n", ret);
    }
    return -abs(ret);
}

static int pfm3u_read(URLContext *h, uint8_t *buf, int size) // kacziro - important - http read data
{
    int readed = 0;
    PFM3UContext *s = h->priv_data;
    if (s->ready)
    {
        HTTPContext* ctx = s->ctx->priv_data;
        readed = http_read(s->ctx, buf, size);//size;
        if (readed > 0) {
            s->dataLeft -= readed;
        }
    }
    return readed;
}

static int pfm3u_write(URLContext *h, const uint8_t *buf, int size) //
{
    //printf("pfm3u_write\n");
    PFM3UContext *s = h->priv_data;
    return http_write(s->ctx, buf, size);//size;
}

static int pfm3u_shutdown(URLContext *h, int flags) //
{
    PFM3UContext *s = h->priv_data;
    printf("pfm3u_shutdown\n");
    return http_shutdown(s->ctx, flags);
}

static int pfm3u_close(URLContext *h) //
{
    char requestUrl[FSP_LONG_STR];
    char message[FSP_LONG_STR];
    int off = 0;
    URLContext *tmp;
    HD3FSPResponse response;
    PFM3UContext *s = h->priv_data;

    memset(requestUrl, 0, FSP_LONG_STR);
    memset(message, 0, FSP_LONG_STR);

    memcpy(requestUrl, s->serviceUrl, s->serviceUrlLen);
    off = pfm3u_appendToStr(requestUrl + s->serviceUrlLen, FSP_URL_END_SESSION);
    sprintf(requestUrl + s->serviceUrlLen + off, "%d/", s->token);
    ffurl_alloc(&tmp, requestUrl, s->flags, NULL);

    http_open(tmp, requestUrl, s->flags, NULL);
    http_read(tmp, message, FSP_LONG_STR);

    hd3fsp_parseResponse(message, &response);

    return http_close(s->ctx);
}

static int64_t pfm3u_seek(URLContext *h, int64_t off, int whence) //
{
    int64_t size = 0;
    PFM3UContext *s = h->priv_data;
    if (off == 0 && whence == 0)
    {
        int ret = popUrlCtx(s);
        if (!ret) {
            printf("not yet\n");
            //return -1;
        }
    }
    //printf("seek %d, %d\n", off, whence);
    size = http_seek(s->ctx, off, whence);
    if (size < 0){
        printf("error\n");
    }
    printf("pfm3u_seek %I64d, %d; size: %I64d | %s\n", off, whence, size, s->ctx->filename);
    return size;
}

static int pfm3u_get_file_handle(URLContext *h) //
{
    //printf("hd3fsp_get_file_handle\n");
    PFM3UContext *s = h->priv_data;
    return http_get_file_handle(s->ctx);
}

static const AVClass pfm3u_context_class = {
    .class_name = "pfm3u",
    .item_name  = av_default_item_name,
    .option     = NULL,//options,
    .version    = LIBAVUTIL_VERSION_INT,
};

const URLProtocol ff_pfm3u_protocol = {
    .name                = "pfm3u",
    .url_open2           = pfm3u_open,
    .url_accept          = NULL,
    .url_handshake       = pfm3u_handshake,
    .url_read            = pfm3u_read,
    .url_write           = pfm3u_write,
    .url_seek            = pfm3u_seek,
    .url_close           = pfm3u_close,
    .url_get_file_handle = pfm3u_get_file_handle,
    .url_shutdown        = pfm3u_shutdown,
    .priv_data_size      = sizeof(PFM3UContext),
    .priv_data_class     = &pfm3u_context_class,
    .flags               = URL_PROTOCOL_FLAG_NETWORK,
    .default_whitelist   = "pfm3u"
};

#endif
