#ifndef HD3_ENCODE_JP_H
#define HD3_ENCODE_JP_H

#include "hd3/setup.h"
#include "hd3/hd3.h"
#include "hd3/maxmin.h"
#include "hd3/detectBB.h"
#include "hd3/threshold.h"

static void update_JP(AVFrame *frame, float *pGlobalMean)
{
    int mode = 0;
    int frameSize = frame->width*frame->height;
    pthread_t diffThread1, diffThread2, diffThread3;
    if (AV_PIX_FMT_YUV420P == frame->format) {
        if (hd3Data.step > 0) {
            float mean1;
            //FrameCopy *pPrev = pCurr - 1;
            FrameCopy *pPrev = &hd3Data.prevFrame;
            int hsize = (frame->height >> 1)*(frame->width >> 1);

            MaxMinArgs arg1, arg2;
            arg1.cvalue = frame->data[0];
            arg1.pvalue = pPrev->data[0];
            arg1.length = frameSize;
            arg1.diff   = hd3Data.buff.data[0];
            arg1.mins   = hd3Data.mins.data[0];
            arg1.maxes  = hd3Data.maxes.data[0];
            arg1.mean   = &mean1;
            arg1.method = mode;
            pthread_create(&diffThread1, NULL, threadMaxMin, &arg1);

            updateMaxMinDiff(frame->data[1], pPrev->data[1], hd3Data.buff.data[1], hd3Data.mins.data[1], hd3Data.maxes.data[1], hd3Data.buff.data[1] + hsize, pGlobalMean);
            updateMaxMinDiff(frame->data[2], pPrev->data[2], hd3Data.buff.data[2], hd3Data.mins.data[2], hd3Data.maxes.data[2], hd3Data.buff.data[2] + hsize, pGlobalMean);
        }

        hd3Data.prevFrame.format = frame->format;
        hd3Data.prevFrame.pts    = frame->pts;
        hd3Data.prevFrame.width  = frame->width;
        hd3Data.prevFrame.height = frame->height;
        hd3Data.prevFrame.linesize[0] = frame->linesize[0];
        hd3Data.prevFrame.linesize[1] = frame->linesize[1];
        hd3Data.prevFrame.linesize[2] = frame->linesize[2];
        hd3Data.prevFrame.linesize[3] = frame->linesize[3];
        memcpy(hd3Data.prevFrame.data[0], frame->data[0], frameSize);
        memcpy(hd3Data.prevFrame.data[1], frame->data[1], frameSize/4);
        memcpy(hd3Data.prevFrame.data[2], frame->data[2], frameSize/4);

        pthread_join(diffThread1, NULL);
    } else if (AV_PIX_FMT_RGB24 == frame->format ||
             AV_PIX_FMT_BGR24 == frame->format) {
        float mean1, mean2;
        if (hd3Data.step > 0)
        {
            //FrameCopy *pPrev = pCurr - 1;
            FrameCopy *pPrev = &hd3Data.prevFrame;

            MaxMinArgs arg1, arg2, arg3;
            arg1.cvalue = frame->data[0];
            arg1.pvalue = pPrev->data[0];
            arg1.length = frameSize; // 1/3 frame values
            arg1.diff   = hd3Data.buff.data[0];
            arg1.mins   = hd3Data.mins.data[0];
            arg1.maxes  = hd3Data.maxes.data[0];
            arg1.mean   = &mean1;
            arg1.method = mode;
            pthread_create(&diffThread1, NULL, threadMaxMin, &arg1);
            //threadMaxMin(&arg1);
            arg2.cvalue = frame->data[0] + arg1.length;
            arg2.pvalue = pPrev->data[0] + arg1.length;
            arg2.length = frameSize; // 1/3 frame values
            arg2.diff   = hd3Data.buff.data[0] + arg1.length;
            arg2.mins   = hd3Data.mins.data[0] + arg1.length;
            arg2.maxes  = hd3Data.maxes.data[0] + arg1.length;
            arg2.mean   = &mean2;
            arg2.method = mode;
            pthread_create(&diffThread2, NULL, threadMaxMin, &arg2);
            //threadMaxMin(&arg2);
            arg3.cvalue = frame->data[0] + arg1.length + arg2.length;
            arg3.pvalue = pPrev->data[0] + arg1.length + arg2.length;
            arg3.length = frameSize; // 1/3 frame values
            arg3.diff   = hd3Data.buff.data[0] + arg1.length + arg2.length;
            arg3.mins   = hd3Data.mins.data[0] + arg1.length + arg2.length;
            arg3.maxes  = hd3Data.maxes.data[0] + arg1.length  + arg2.length;
            arg3.mean   = pGlobalMean;
            arg3.method = mode;
            pthread_create(&diffThread3, NULL, threadMaxMin, &arg3);
            //threadMaxMin(&arg3);
        }
        hd3Data.prevFrame.format = frame->format;
        hd3Data.prevFrame.pts    = frame->pts;
        hd3Data.prevFrame.width  = frame->width;
        hd3Data.prevFrame.height = frame->height;
        hd3Data.prevFrame.linesize[0] = frame->linesize[0];
        hd3Data.prevFrame.linesize[1] = frame->linesize[1];
        hd3Data.prevFrame.linesize[2] = frame->linesize[2];
        hd3Data.prevFrame.linesize[3] = frame->linesize[3];
        memcpy(hd3Data.prevFrame.data[0], frame->data[0], 3*frameSize);

        pthread_join(diffThread1, NULL);
        pthread_join(diffThread2, NULL);
        pthread_join(diffThread3, NULL);
    } else {
        av_log(hd3Data.ctx, AV_LOG_ERROR, "Error: not implemented pixel format %d\n", hd3Data.buffor.img.i_csp);
    }
}

// kacziro - important - assumption: blockSize > 5
static void diffXY2(float* pBuf1, uint8_t* pIn1, float* pBuf2, uint8_t* pIn2, int chNr, int w, int blockSize)
{
    float diff1, diff2;
    int bx, by, ey, ex, blockSize_2;
    blockSize_2 = blockSize - 2;
    for (int iy = 0; iy < blockSize; iy++)
    {
        by = MAX(2, iy - 2)*blockSize;
        ey = MIN(blockSize_2, iy + 3)*blockSize;
        const uint8_t *pI1 = pIn1 + iy*w*chNr;
        const uint8_t *pI2 = pIn2 + iy*w*chNr;
        for (int ix = 0; ix < blockSize; ix++)
        {
            diff1 = (abs(pI1[chNr*(ix - (iy > 0)*w)] - pI1[chNr*(ix + (iy < blockSize-1)*w)]) + abs(pI1[chNr*(ix - (ix > 0))] - pI1[chNr*(ix + (ix < blockSize-1))]));
            diff2 = (abs(pI2[chNr*(ix - (iy > 0)*w)] - pI2[chNr*(ix + (iy < blockSize-1)*w)]) + abs(pI2[chNr*(ix - (ix > 0))] - pI2[chNr*(ix + (ix < blockSize-1))]));
            bx = MAX(2, ix - 2);
            ex = MIN(blockSize_2, ix + 3);
            for (float *pCurr1 = pBuf1 + by, *pCurr2 = pBuf2 + by; pCurr1 < pBuf1 + ey; pCurr1 += blockSize, pCurr2 += blockSize)
            {
                for (int ixx = bx; ixx < ex; ixx++)
                {
                    pCurr1[ixx] += diff1;
                    pCurr2[ixx] += diff2;
                }
            }
        }
    }
}
static void planeImdensity(int* s1, int* s2, uint8_t* pIn1, uint8_t* pIn2, int w,
                           float* pBuf1, float* pBuf2, int channelNr, int blockSize, float th)
{
    int blockSizePow;
    blockSizePow = blockSize*blockSize;

    float mean1, mean2, std1, std2;
    mean1 = 0.0f;
    mean2 = 0.0f;
    std1 = 0.0f;
    std2 = 0.0f;
    diffXY2(pBuf1, pIn1, pBuf2, pIn2, channelNr, w, blockSize);

    float* pB1;
    float* pB2;
    float* pE1;
    float out[4];
    __m128 tmp;

    pE1 = pBuf1 + blockSizePow;
    {
        __m128 vMod;
        float mulNum = 0.04f*0.5f;//  1/25
        vMod = _mm_load1_ps(&mulNum);
        for (pB1 = pBuf1, pB2 = pBuf2; pB1 < pE1; pB1+=2, pB2+=2)
        {
            _mm_store_ps(out, _mm_mul_ps(_mm_setr_ps(pB1[0], pB1[1], pB2[0], pB2[1]), vMod));

            pB1[0] = out[0];
            pB1[1] = out[1];
            pB2[0] = out[2];
            pB2[1] = out[3];

            mean1 += pB1[0] + pB1[1];
            mean2 += pB2[0] + pB2[1];
        }
    }
    mean1 = mean1/blockSizePow;
    mean2 = mean2/blockSizePow;

    {
        __m128 _mean, _out;
        _mean = _mm_setr_ps(mean1, mean1, mean2, mean2);
        _out = _mm_setr_ps(0.0f, 0.0f, 0.0f, 0.0f);
        for (pB1 = pBuf1, pB2 = pBuf2; pB1 < pE1; pB1+=2, pB2+=2)
        {
            tmp = _mm_sub_ps(_mean, _mm_setr_ps(pB1[0], pB1[1], pB2[0], pB2[1]));
            _out = _mm_add_ps(_out, _mm_mul_ps(tmp, tmp));
        }
        _mm_store_ps(out, _out);
        std1 = out[0] + out[1];
        std2 = out[2] + out[3];
    }
    std1 = sqrt(MAX(std1, std2)/blockSizePow);

    if (fabs(std1/MIN(mean1, mean2)-1) < 0.2f)
    {
        float mod = mean2/mean1;
        __m128 _mod = _mm_load1_ps(&mod);
        pB1 = pBuf1;
        for (pB1 = pBuf1; pB1 < pE1; pB1+=4)
        {
            _mm_store_ps(pB1, _mm_mul_ps(_mod, _mm_load_ps(pB1)));
        }
        for (int i = 0; i < blockSizePow%4; i++)
        {
            pBuf1[blockSizePow - i - 1] *= mod;
        }
    }
    //
    pE1 = pBuf1 + 3*blockSize - 2;
    for (pB1 = pBuf1, pB2 = pBuf2; pB1 < pE1; pB1++, pB2++)
    {
        (*s1) += (fabs(*pB1 - *pB2) > th)*3;
        (*s2) += ((*pB1 + *pB2) > 2*th)*3;
    }
    pB1 += 4;
    pB2 += 4;
    for (int row = 4; row < blockSize-2; row++)
    {
        (*s1) += (fabs(*pB1 - *pB2) > th)*3;
        (*s2) += ((*pB1 + *pB2) > 2*th)*3;
        pB1++;
        pB2++;
        pE1 = pBuf1 + blockSize*row - 3;
        for (; pB1 < pE1; pB1++, pB2++)
        {
            (*s1) += (fabs(*pB1 - *pB2) > th);
            (*s2) += ((*pB1 + *pB2) > 2*th);
        }
        (*s1) += (fabs(*pB1 - *pB2) > th)*3;
        (*s2) += ((*pB1 + *pB2) > 2*th)*3;
        pB1 += 5;
        pB2 += 5;
    }
    pE1 = pBuf1 + blockSizePow - 2;
    for (; pB1 < pE1; pB1++, pB2++)
    {
        (*s1) += (fabs(*pB1 - *pB2) > th)*3;
        (*s2) += ((*pB1 + *pB2) > 2*th)*3;
    }
}
typedef struct ImdensityArg
{
    int s1;
    int s2;
    uint8_t* pIn1;
    uint8_t* pIn2;
    int linesize1;
    int linesize2;
    float* pBuf1;
    float* pBuf2;
    int blockSize;
    float th;

} ImdensityArg;

static void convertRgb2Yuv(float* y, float* u, float* v, float r, float g, float b)
{
#define RGB2YUV_SHIFT 15
#define BY ( (0.114 * 219 / 255 * (1 << RGB2YUV_SHIFT) + 0.5f))
#define BV (-(0.081 * 224 / 255 * (1 << RGB2YUV_SHIFT) + 0.5f))
#define BU ( (0.500 * 224 / 255 * (1 << RGB2YUV_SHIFT) + 0.5f))
#define GY ( (0.587 * 219 / 255 * (1 << RGB2YUV_SHIFT) + 0.5f))
#define GV (-(0.419 * 224 / 255 * (1 << RGB2YUV_SHIFT) + 0.5f))
#define GU (-(0.331 * 224 / 255 * (1 << RGB2YUV_SHIFT) + 0.5f))
#define RY ( (0.299 * 219 / 255 * (1 << RGB2YUV_SHIFT) + 0.5f))
#define RV ( (0.500 * 224 / 255 * (1 << RGB2YUV_SHIFT) + 0.5f))
#define RU (-(0.169 * 224 / 255 * (1 << RGB2YUV_SHIFT) + 0.5f))

    *y = (RY * r + GY * g + BY * b + ( 33 << (RGB2YUV_SHIFT - 1))) / (1 << RGB2YUV_SHIFT);
    *u = (RU * r + GU * g + BU * b + (257 << (RGB2YUV_SHIFT - 1))) / (1 << RGB2YUV_SHIFT);
    *v = (RV * r + GV * g + BV * b + (257 << (RGB2YUV_SHIFT - 1))) / (1 << RGB2YUV_SHIFT);
}
static float imdensity(FrameCopy* pF1, float* pBuf1, FrameCopy* pF2, float* pBuf2, int x, int y, int blockSize, float th)
{
    uint8_t *pIn1, *pIn2;
    int s1, s2, div;
    s2 = s1 = 0;
    div = 1;

    if (pF1->format == AV_PIX_FMT_YUV420P)
    {
        float threshold[3];
        pIn1 = pF1->data[0] + y*pF1->linesize[0] + x;
        pIn2 = pF2->data[0] + y*pF2->linesize[0] + x;
        convertRgb2Yuv(threshold, threshold+1, threshold+2, th, th, th);
        threshold[1] -= 128;
        threshold[2] -= 128;
        div = pF2->linesize[0]/pF2->linesize[1];
        planeImdensity(&s1, &s2, pIn1, pIn2, pF2->linesize[0], pBuf1, pBuf2, 1, blockSize, threshold[0]);
        pIn1 = pF1->data[1] + (y/div)*pF1->linesize[1] + x/div;
        pIn2 = pF2->data[1] + (y/div)*pF2->linesize[1] + x/div;
        planeImdensity(&s1, &s2, pIn1, pIn2, pF2->linesize[1], pBuf1, pBuf2, 1, blockSize/div, threshold[1]);
        pIn1 = pF1->data[2] + (y/div)*pF1->linesize[2] + x/div;
        pIn2 = pF2->data[2] + (y/div)*pF2->linesize[2] + x/div;
        planeImdensity(&s1, &s2, pIn1, pIn2, pF2->linesize[2], pBuf1, pBuf2, 1, blockSize/div, threshold[2]);
    }
    else
    {
        pIn1 = pF1->data[0] + y*pF1->linesize[0] + x*3;
        pIn2 = pF2->data[0] + y*pF2->linesize[0] + x*3;
        planeImdensity(&s1, &s2, pIn1, pIn2, pF1->width, pBuf1, pBuf2, 3, blockSize, th);
        pIn1 = pF1->data[0] + y*pF1->linesize[0] + x*3 + 1;
        pIn2 = pF2->data[0] + y*pF2->linesize[0] + x*3 + 1;
        planeImdensity(&s1, &s2, pIn1, pIn2, pF1->width, pBuf1, pBuf2, 3, blockSize, th);
        pIn1 = pF1->data[0] + y*pF1->linesize[0] + x*3 + 2;
        pIn2 = pF2->data[0] + y*pF2->linesize[0] + x*3 + 2;
        planeImdensity(&s1, &s2, pIn1, pIn2, pF1->width, pBuf1, pBuf2, 3, blockSize, th);
    }

    if (s2 != 0)
    {
        return s1/(float)s2;
    }
    return s1;
}

static void edgeFilter(AABB* pAabb, FrameCopy* pF1, FrameCopy* pF2, int blockSize, float th)
{
    float mm = imdensity(pF1,
                          hd3Data.stats.pDxBuff,
                          pF2,
                          hd3Data.stats.pDyBuff,
                          pAabb->begin.x,
                          pAabb->begin.y,
                          blockSize,
                          th);

    if (mm < hd3Data.stats.edgeLvl) {
        pAabb->begin.x = pAabb->end.x;
    }
}
typedef struct EdgeFilterArg
{
    AABB* pAabb;
    FrameCopy* pF1;
    FrameCopy* pF2;
    int blockSize;
    float th;
} EdgeFilterArg;
static void* threadEdgeFilter(void* arg)
{
    EdgeFilterArg* a = arg;
    edgeFilter(a->pAabb, a->pF1, a->pF2, a->blockSize, a->th);
    return NULL;
}
// kacziro - assumption: width % 4 = 0
static void calcuateMean(float *pMean, FrameCopy *pBuff, FrameCopy *pMin, FrameCopy *pMax,
                         int blockWidth, int blockHeight, int gridWidth, int gridHeight)
{
    int hw;
    hw = pBuff->width >> 1;

    for (int iy = 0; iy < pBuff->height; iy += 2) {
        float *pM = pMean + (iy/blockHeight)*gridWidth;
        const uint8_t *pMin0 = (const uint8_t *)pMin->data[0] + iy*pBuff->width;
        const uint8_t *pMax0 = (const uint8_t *)pMax->data[0] + iy*pBuff->width;
        const uint8_t *pMin1 = (const uint8_t *)pMin->data[1] + (iy >> 1)*hw;
        const uint8_t *pMax1 = (const uint8_t *)pMax->data[1] + (iy >> 1)*hw;
        const uint8_t *pMin2 = (const uint8_t *)pMin->data[2] + (iy >> 1)*hw;
        const uint8_t *pMax2 = (const uint8_t *)pMax->data[2] + (iy >> 1)*hw;
        uint8_t *pOut0 = pBuff->data[0] + iy*pBuff->width;
        uint8_t *pOut1 = pBuff->data[1] + (iy >> 1)*hw;
        uint8_t *pOut2 = pBuff->data[2] + (iy >> 1)*hw;
        for (int ix = 0; ix < pBuff->width; ix+=2) {
            pOut0[ix]      = pMax0[ix]     - pMin0[ix];
            pOut0[ix + 1]  = pMax0[ix + 1] - pMin0[ix + 1];
            pOut1[ix >> 1] = pMax1[ix >> 1] - pMin1[ix >> 1];//
            pOut2[ix >> 1] = pMax2[ix >> 1] - pMin2[ix >> 1];//
            pM[ix/blockWidth] += pOut2[ix >> 1] + pOut1[ix >> 2] + pOut0[ix] + pOut0[ix + 1];
        }
        pM = pMean + ((iy+1)/blockHeight)*gridWidth;
        pMin0 = (const uint8_t *)pMin->data[0] + (iy + 1)*pBuff->width;
        pMax0 = (const uint8_t *)pMax->data[0] + (iy + 1)*pBuff->width;
        pOut0 = pBuff->data[0] + (iy + 1)*pBuff->width;
        for (int ix = 0; ix < pBuff->width; ix+=2) {
            pOut0[ix] = pMax0[ix] - pMin0[ix];
            pOut0[ix + 1] = pMax0[ix + 1] - pMin0[ix + 1];
            pM[ix/blockWidth] += pOut0[ix] + pOut0[ix + 1];
        }
    }
    {
        float* pEnd = pMean + gridWidth*gridHeight;
        //hd3Data.stats.globalMean = 0; // not used
        float mod = 1.0f/(blockWidth*blockHeight*1.5f);
        __m128 _mod = _mm_load1_ps(&mod);
        for (; pMean < pEnd; pMean += 4) {
            _mm_store_ps(pMean, _mm_mul_ps(_mod, _mm_load_ps(pMean)));
            //hd3Data.stats.globalMean += *pMean; // not used
        }
        for (int i = 0; i < (gridWidth*gridHeight)%4; i++) {
            pEnd[-1 - i] *= mod;
        }
        //hd3Data.stats.globalMean /= gridSize; // not
    }
}
static void calcuateMeanRGB(float *pMean, FrameCopy *pBuff, FrameCopy *pMin, FrameCopy *pMax,
                            int blockWidth, int blockHeight, int gridWidth, int gridHeight)
{
    for (int iy = 0; iy < pBuff->height; iy++)
    {
        float *pM = pMean + (iy/blockHeight)*gridWidth;
        const uint8_t *pMin0 = (const uint8_t *)pMin->data[0] + iy*pBuff->width*3;
        const uint8_t *pMax0 = (const uint8_t *)pMax->data[0] + iy*pBuff->width*3;
        uint8_t *pOut0 = pBuff->data[0] + iy*pBuff->width*3;
        for (int ix = 0; ix < pBuff->width*3; ix+=3) {
            pOut0[ix] = pMax0[ix] - pMin0[ix];
            pOut0[ix + 1] = pMax0[ix + 1] - pMin0[ix + 1];
            pOut0[ix + 2] = pMax0[ix + 2] - pMin0[ix + 2];
            pM[(ix/3)/blockWidth] += pOut0[ix] + pOut0[ix + 1] + pOut0[ix + 2];
        }
    }
    {
        float* pEnd = pMean + gridWidth*gridHeight;
        float mod = 1.0f/(blockWidth*blockHeight*3);
        __m128 _mod = _mm_load1_ps(&mod);
        for (; pMean < pEnd; pMean += 4) {
            _mm_store_ps(pMean, _mm_mul_ps(_mod, _mm_load_ps(pMean)));
        }
        for (int i = 0; i < (gridWidth*gridHeight)%4; i++) {
            pEnd[-1 - i] *= mod;
        }
    }
}
static void calculateStd(float *pStd, float *pMean, FrameCopy* pDiff,
                         int blockWidth, int blockHeight, int gridWidth, int gridHeight)
{
    float tmp1, tmp2;
    int hw;
    hw = pDiff->width >> 1;
    for (int iy = 0; iy < pDiff->height; iy+=2) {
        const uint8_t *pBuff0 = (const uint8_t *)(pDiff->data[0] + iy*pDiff->width);
        const uint8_t *pBuff1 = (const uint8_t *)(pDiff->data[1] + (iy >> 1)*hw);
        const uint8_t *pBuff2 = (const uint8_t *)(pDiff->data[2] + (iy >> 1)*hw);
        const float *pM = pMean + (iy/blockHeight)*gridWidth;
              float *pS = pStd + (iy/blockHeight)*gridWidth;
        for (int ix = 0; ix < pDiff->width; ix+=2) {
            tmp1 = pBuff0[ix] - pM[ix/blockWidth];
            tmp2 = pBuff0[ix + 1] - pM[ix/blockWidth];
            pS[ix/blockWidth] += tmp1*tmp1 + tmp2*tmp2;
            tmp1 = pBuff1[ix >> 1] - pM[ix/blockWidth];
            tmp2 = pBuff2[ix >> 1] - pM[ix/blockWidth];
            pS[ix/blockWidth] += tmp1*tmp1 + tmp2*tmp2;
        }
        pBuff0 = (const uint8_t *)(pDiff->data[0] + (iy + 1)*pDiff->width);
        pM = pMean + ((iy + 1)/blockHeight)*gridWidth;
        pS = pStd  + ((iy + 1)/blockHeight)*gridWidth;
        for (int ix = 0; ix < pDiff->width; ix+=2) {
            tmp1 = pBuff0[ix]     - pM[ix/blockWidth];
            tmp2 = pBuff0[ix + 1] - pM[ix/blockWidth];
            pS[ix/blockWidth] += tmp1*tmp1 + tmp2*tmp2;
        }
    }
    // std/(blockWidth*blockHeight*1.5f) and sqrt after sort - only for selected
}
static void calculateStdRGB(float *pStd, float *pMean, FrameCopy* pDiff,
                            int blockWidth, int blockHeight, int gridWidth, int gridHeight)
{
    float tmp0, tmp1, tmp2;
    for (int iy = 0; iy < pDiff->height; iy++) {
        const uint8_t *pBuff0 = (const uint8_t *)(pDiff->data[0] + iy*pDiff->width*3);
        const float *pM = pMean + (iy/blockHeight)*gridWidth;
              float *pS = pStd + (iy/blockHeight)*gridWidth;
        for (int ix = 0; ix < pDiff->width; ix++) {
            tmp0 = pBuff0[3*ix] - pM[ix/blockWidth];
            tmp1 = pBuff0[3*ix + 1] - pM[ix/blockWidth];
            tmp2 = pBuff0[3*ix + 2] - pM[ix/blockWidth];
            pS[ix/blockWidth] += tmp0*tmp0 + tmp1*tmp1 + tmp2*tmp2;
        }
    }
    // std/(blockWidth*blockHeight*3.0f) and sqrt after sort - only for selected
}

static void generateBoxes_JP(FrameBoxes *pCurrentSet, x264_picture_t *pFrame)
{
    clock_t genBegin = clock();
    int baseSize, minMovementBox, gridSize, statBlockWidth, statBlockHeight;
    baseSize = hd3Data.width*hd3Data.height;
    gridSize = hd3Data.stats.gridWidth*hd3Data.stats.gridHeight;
    minMovementBox = MOVEMENT_BOX_DIM; //kacziro - important - denominator width and height - start here !

    pCurrentSet->table.number = 2; // kacziro - fixme - generate this
    pCurrentSet->table.cells[0].method = SWS_BICUBIC;
    pCurrentSet->table.cells[0].xReduction = hd3Data.globalXCompression;
    pCurrentSet->table.cells[0].yReduction = hd3Data.globalYCompression;
    pCurrentSet->table.cells[1].method = SWS_POINT;
    pCurrentSet->table.cells[1].xReduction = hd3Data.globalXCompression;
    pCurrentSet->table.cells[1].yReduction = hd3Data.globalYCompression;

    statBlockWidth = hd3Data.width/hd3Data.stats.gridWidth;
    statBlockHeight = hd3Data.height/hd3Data.stats.gridHeight;

    memset(hd3Data.stats.blockMean, 0, sizeof(float)*gridSize);
    memset(hd3Data.stats.blockStd, 0, sizeof(float)*gridSize);

    if (X264_CSP_I420 == pFrame->img.i_csp)
    {
        calcuateMean(hd3Data.stats.blockMean, &hd3Data.buff, &hd3Data.mins, &hd3Data.maxes,
                     statBlockWidth, statBlockHeight, hd3Data.stats.gridWidth, hd3Data.stats.gridHeight);

    } else if (X264_CSP_RGB == pFrame->img.i_csp) {
        calcuateMeanRGB(hd3Data.stats.blockMean, &hd3Data.buff, &hd3Data.mins, &hd3Data.maxes,
                        statBlockWidth, statBlockHeight, hd3Data.stats.gridWidth, hd3Data.stats.gridHeight);
    }

    clock_t meanBegin = clock();
    perfData.mean.sum += (float)(meanBegin - genBegin);
    perfData.mean.number++;

    float itemsMod = 1.0f;
    if (X264_CSP_I420 == pFrame->img.i_csp)
    {
        calculateStd(hd3Data.stats.blockStd, hd3Data.stats.blockMean, &hd3Data.buff,
                     statBlockWidth, statBlockHeight, hd3Data.stats.gridWidth, hd3Data.stats.gridHeight);

        itemsMod = 1.0f/(statBlockWidth*statBlockHeight*1.5f);

        memset(pFrame->img.plane[0], 0, baseSize);
        memset(pFrame->img.plane[1], 0, (hd3Data.width/minMovementBox)*(hd3Data.height/minMovementBox));//baseSize/4);
        memset(pFrame->img.plane[2], 0, baseSize/4);
    } else if (X264_CSP_RGB == pFrame->img.i_csp) {
        calculateStdRGB(hd3Data.stats.blockStd, hd3Data.stats.blockMean, &hd3Data.buff,
                        statBlockWidth, statBlockHeight, hd3Data.stats.gridWidth, hd3Data.stats.gridHeight);

        itemsMod = 1.0f/(statBlockWidth*statBlockHeight*3);

        memset(pFrame->img.plane[0], 0, baseSize*3);
    }
    hd3Data.stats.threshold = calculateThreashold(hd3Data.stats.blockMean, hd3Data.stats.blockStd, gridSize, itemsMod,
                                                  hd3Data.stats.minGroup, hd3Data.stats.maxGroup, hd3Data.stats.mix, hd3Data.stats.threshold);

    clock_t boxBegin = clock();
    perfData.std.sum += (float)(boxBegin - meanBegin);
    perfData.std.number++;
    perfData.threshold.sum += (float)(boxBegin - genBegin);
    perfData.threshold.number++;

    if (X264_CSP_I420 == pFrame->img.i_csp)
    {
        detectBBYUV(pCurrentSet, &hd3Data.buff, &pFrame->img, minMovementBox, hd3Data.stats.threshold, hd3Data.stats.minObjSize);
    } else if (X264_CSP_RGB == pFrame->img.i_csp) {
        detectBBRGB(pCurrentSet, &hd3Data.buff, &pFrame->img, minMovementBox, hd3Data.stats.threshold, hd3Data.stats.minObjSize);
    }
    //remove edges
    clock_t edgeBegin = clock();
    perfData.box.sum += (float)(edgeBegin - boxBegin);
    perfData.box.number++;
    {
        int lastIndex;
        lastIndex = hd3Data.globalXCompression*hd3Data.globalYCompression - 1;
        EdgeFilterArg threadsArg;

        MovementBox *pEnd;
        pEnd = pCurrentSet->boxes + pCurrentSet->number;
        for (MovementBox *pBox = pCurrentSet->boxes; pBox < pEnd; pBox++) {
            threadsArg.blockSize = minMovementBox;
            threadsArg.pAabb = &pBox->aabb;
            threadsArg.pF1 = hd3Data.pInputs;
            threadsArg.pF2 = hd3Data.pInputs + lastIndex;
            threadsArg.th = hd3Data.stats.threshold;
            threadEdgeFilter(&threadsArg);

            perfData.subEdge.sum++;
        }
    }
    perfData.subEdge.number++;
    {
        int newNum = 0;
        MovementBox *pCurr, *pEnd;
        pEnd = pCurrentSet->boxes + pCurrentSet->number;
        for (pCurr = pCurrentSet->boxes; pCurr < pEnd; pCurr++) {
            if (pCurr->aabb.begin.x != pCurr->aabb.end.x) {
//                            if (X264_CSP_I420 == pFrame->img.i_csp)
//                            {
//                                for (int iyy = pCurr->aabb.begin.y/2; iyy < pCurr->aabb.end.y/2; iyy++)
//                                {
//                                    memset(pFrame->img.plane[2] + iyy*(hd3Data.width/2) + pCurr->aabb.begin.x/2,
//                                        255,
//                                        minMovementBox/2);
//                                }
//                            }
//                            else if (X264_CSP_RGB == pFrame->img.i_csp)
//                            {
//                                for (int iyy = pCurr->aabb.begin.y; iyy < pCurr->aabb.end.y; iyy++)
//                                {
//                                    memset(pFrame->img.plane[0] + iyy*(hd3Data.width)*3 + pCurr->aabb.begin.x*3,
//                                           255,
//                                           minMovementBox*3);
//                                }
//                            }
                pCurrentSet->boxes[newNum] = *pCurr;
                newNum++;
            }
        }
        pCurrentSet->number = newNum;
        aggregation_closedMax(pCurrentSet, hd3Data.stats.bbMarginX, hd3Data.stats.bbMarginY);
    }

    clock_t genEnd = clock();
    perfData.edge.sum += (float)(genEnd - edgeBegin);
    perfData.edge.number++;
    perfData.gen.sum += (float)(genEnd - genBegin);
    perfData.gen.number++;
}

#endif
