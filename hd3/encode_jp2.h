#ifndef HD3_ENCODE_JP2_H
#define HD3_ENCODE_JP2_H

#include "hd3/setup.h"
#include "hd3/hd3.h"
#include "hd3/maxmin.h"
#include "hd3/detectBB.h"
#include "hd3/threshold.h"
#include "hd3/flat_area.h"

typedef struct OpArgs {
    FrameCopy *pInOut0;
    FrameCopy *pInOut1;
    FrameCopy *pInOut2;
    FrameCopy *pOut0;
    int starty;
    int endy;
} OpArgs;

static void doParallel(OpArgs *args, void*(*func)(void*), int nr)
{
    pthread_t threads[24];
    OpArgs threadArgs[24];
    memset(threads, 0, sizeof(pthread_t)*24);
    memset(threadArgs, 0, sizeof(OpArgs)*24);
    nr = MAX(1, MIN(24, nr));
    int i = 0;
    for (; i < nr - 1; i++)
    {
        threadArgs[i].pInOut0 = args->pInOut0;
        threadArgs[i].pInOut1 = args->pInOut1;
        threadArgs[i].pInOut2 = args->pInOut2;
        threadArgs[i].pOut0   = args->pOut0;
        threadArgs[i].starty  = i*args->pInOut2->height/nr;
        threadArgs[i].endy = (i + 1)*args->pInOut2->height/nr;
        //func(threadArgs + i);
        pthread_create(threads + i, NULL, func, threadArgs + i);
    }
    threadArgs[i].pInOut0 = args->pInOut0;
    threadArgs[i].pInOut1 = args->pInOut1;
    threadArgs[i].pInOut2 = args->pInOut2;
    threadArgs[i].pOut0   = args->pOut0;
    threadArgs[i].starty  = i*args->pInOut2->height/nr;
    threadArgs[i].endy = (i + 1)*args->pInOut2->height/nr;
    func(threadArgs + i);
    //pthread_create(threads + i, NULL, func, threadArgs + i);
    for (int i = 0; i < nr; i++)
    {
        pthread_join(threads[i], NULL);
    }
}

static void updateMinMax_gray16(OpArgs *args)
{
    FrameCopy *maxes = args->pInOut0;
    FrameCopy *mins = args->pInOut1;
    FrameCopy *frame = args->pInOut2;
    int starty = args->starty;
    int endy = args->endy;
    for (int iy = starty; iy < endy; iy++) {
        uint16_t *pMin  = (uint16_t*)(mins->data[0]  + iy*mins->linesize[0]);
        uint16_t *pMax  = (uint16_t*)(maxes->data[0] + iy*maxes->linesize[0]);
        uint16_t *pData = (uint16_t*)(frame->data[0] + iy*frame->linesize[0]);
        for (int ix = 0; ix < frame->width; ix++) {
            pMin[ix] = pMin[ix] < pData[ix]? pMin[ix]: pData[ix];
            pMax[ix] = pMax[ix] > pData[ix]? pMax[ix]: pData[ix];
        }
    }
}
static void updateMinMaxAndDiff_gray16(OpArgs *args)
{
    FrameCopy *maxes = args->pInOut0;
    FrameCopy *mins = args->pInOut1;
    FrameCopy *frame = args->pInOut2;
    FrameCopy *diff = args->pOut0;
    int starty = args->starty;
    int endy = args->endy;
    for (int iy = starty; iy < endy; iy++) {
        uint16_t *pMin  = (uint16_t*)(mins->data[0]  + iy*mins->linesize[0]);
        uint16_t *pMax  = (uint16_t*)(maxes->data[0] + iy*maxes->linesize[0]);
        uint16_t *pData = (uint16_t*)(frame->data[0] + iy*frame->linesize[0]);
        uint16_t *pDiff = (uint16_t*)(diff->data[0] + iy*diff->linesize[0]);
        for (int ix = 0; ix < frame->width; ix++) {
            pMin[ix] = pMin[ix] < pData[ix]? pMin[ix]: pData[ix];
            pMax[ix] = pMax[ix] > pData[ix]? pMax[ix]: pData[ix];
            pDiff[ix] = pMax[ix] - pMin[ix];
        }
    }
}
static void update_JP2(AVFrame *frame, float *pGlobalMean)
{
    int mode = 0;
    int frameSize = frame->width*frame->height;
    pthread_t diffThread1, diffThread2, diffThread3;
    if (AV_PIX_FMT_YUV420P == frame->format)
    {
        if (hd3Data.step > 0)
        {
            float mean1;
            //FrameCopy *pPrev = pCurr - 1;
            FrameCopy *pPrev = &hd3Data.prevFrame;
            int hsize = (frame->height >> 1)*(frame->width >> 1);

            MaxMinArgs arg1, arg2;
            arg1.cvalue = frame->data[0];
            arg1.pvalue = pPrev->data[0];
            arg1.length = frameSize;
            arg1.diff = hd3Data.buff.data[0];
            arg1.mins = hd3Data.mins.data[0];
            arg1.maxes = hd3Data.maxes.data[0];
            arg1.mean = &mean1;
            arg1.method = mode;
            pthread_create(&diffThread1, NULL, threadMaxMin, &arg1);

            updateMaxMinDiff(frame->data[1], pPrev->data[1], hd3Data.buff.data[1], hd3Data.mins.data[1], hd3Data.maxes.data[1], hd3Data.buff.data[1] + hsize, pGlobalMean);
            updateMaxMinDiff(frame->data[2], pPrev->data[2], hd3Data.buff.data[2], hd3Data.mins.data[2], hd3Data.maxes.data[2], hd3Data.buff.data[2] + hsize, pGlobalMean);
        }

        hd3Data.prevFrame.format      = frame->format;
        hd3Data.prevFrame.pts         = frame->pts;
        hd3Data.prevFrame.width       = frame->width;
        hd3Data.prevFrame.height      = frame->height;
        hd3Data.prevFrame.linesize[0] = frame->linesize[0];
        hd3Data.prevFrame.linesize[1] = frame->linesize[1];
        hd3Data.prevFrame.linesize[2] = frame->linesize[2];
        hd3Data.prevFrame.linesize[3] = frame->linesize[3];
        memcpy(hd3Data.prevFrame.data[0], frame->data[0], frameSize);
        memcpy(hd3Data.prevFrame.data[1], frame->data[1], frameSize/4);
        memcpy(hd3Data.prevFrame.data[2], frame->data[2], frameSize/4);

        pthread_join(diffThread1, NULL);
    } else if (AV_PIX_FMT_RGB24 == frame->format ||
               AV_PIX_FMT_BGR24 == frame->format) {
        if (hd3Data.step == 0) {
            for (int iy = 0; iy < frame->height; iy++) {
                memcpy(hd3Data.mins.data[0]  + iy*hd3Data.mins.linesize[0],  frame->data[0] + iy*frame->linesize[0], frame->linesize[0]);
                memcpy(hd3Data.maxes.data[0] + iy*hd3Data.maxes.linesize[0], frame->data[0] + iy*frame->linesize[0], frame->linesize[0]);
            }
        } else {
            for (int iy = 0; iy < frame->height; iy++) {
                uint8_t *pMin  = (uint8_t*)(hd3Data.mins.data[0]  + iy*hd3Data.mins.linesize[0]);
                uint8_t *pMax  = (uint8_t*)(hd3Data.maxes.data[0] + iy*hd3Data.maxes.linesize[0]);
                uint8_t *pData = (uint8_t*)(frame->data[0]        + iy*frame->linesize[0]);
                for (int ix = 0; ix < frame->width*3; ix+=3) {
                    pMin[ix] = pMin[ix] < pData[ix]? pMin[ix]: pData[ix];
                    pMax[ix] = pMax[ix] > pData[ix]? pMax[ix]: pData[ix];
                    pMin[ix+1] = pMin[ix+1] < pData[ix+1]? pMin[ix+1]: pData[ix+1];
                    pMax[ix+1] = pMax[ix+1] > pData[ix+1]? pMax[ix+1]: pData[ix+1];
                    pMin[ix+2] = pMin[ix+2] < pData[ix+2]? pMin[ix+2]: pData[ix+2];
                    pMax[ix+2] = pMax[ix+2] > pData[ix+2]? pMax[ix+2]: pData[ix+2];
                }
            }
        }
    } else if (AV_PIX_FMT_GRAY16BE == frame->format) {
        if (hd3Data.step == 0) {
            // copy frame to min and max
            for (int iy = 0; iy < frame->height; iy++) {
                memcpy(hd3Data.mins.data[0]  + iy*hd3Data.mins.linesize[0],  frame->data[0] + iy*frame->linesize[0], frame->linesize[0]);
                memcpy(hd3Data.maxes.data[0] + iy*hd3Data.maxes.linesize[0], frame->data[0] + iy*frame->linesize[0], frame->linesize[0]);
            }
        } else {
            FrameCopy frameReference;
            memcpy(frameReference.data, frame->data, sizeof(frameReference.data));
            memcpy(frameReference.linesize, frame->linesize, sizeof(frameReference.linesize));
            frameReference.format = frame->format;
            frameReference.width = frame->width;
            frameReference.height = frame->height;
            frameReference.pts = frame->pts;
            OpArgs args;
            args.pInOut0 = &hd3Data.maxes;
            args.pInOut1 = &hd3Data.mins;
            args.pInOut2 = &frameReference;
            args.pOut0   = &hd3Data.buff;
            if (hd3Data.step != hd3Data.globalXCompression*hd3Data.globalYCompression - 1) {
                doParallel(&args, updateMinMax_gray16, 24);
            } else {
                doParallel(&args, updateMinMaxAndDiff_gray16, 24);
            }
        }
        hd3Data.prevFrame.format      = frame->format;
        hd3Data.prevFrame.pts         = frame->pts;
        hd3Data.prevFrame.width       = frame->width;
        hd3Data.prevFrame.height      = frame->height;
        hd3Data.prevFrame.linesize[0] = frame->linesize[0];
        hd3Data.prevFrame.linesize[1] = frame->linesize[1];
        hd3Data.prevFrame.linesize[2] = frame->linesize[2];
        hd3Data.prevFrame.linesize[3] = frame->linesize[3];
        memcpy(hd3Data.prevFrame.data[0], frame->data[0], frame->height*frame->linesize[0]);
    } else {
        av_log(hd3Data.ctx, AV_LOG_ERROR, "Error: not implemented pixel format %d\n", hd3Data.buffor.img.i_csp);
    }
}
static void diff_gray16(OpArgs *args)
{
    FrameCopy *pMaxFrame = args->pInOut0;
    FrameCopy *pMinFrame = args->pInOut1;
    FrameCopy *pOut = args->pInOut2;
    int starty = args->starty;
    int endy = args->endy;
    for (int iy = starty; iy < endy; iy++) {
        const uint16_t *pMin = (const uint16_t *)(pMinFrame->data[0] + iy*pMinFrame->linesize[0]);
        const uint16_t *pMax = (const uint16_t *)(pMaxFrame->data[0] + iy*pMaxFrame->linesize[0]);
        uint16_t *pGray      = (uint16_t*)(pOut->data[0] + iy*pOut->linesize[0]);
        for (int ix = 0; ix < pOut->width; ix++) {
            pGray[ix]     = pMax[ix]     - pMin[ix];
        }
    }
}
static int diff(FrameCopy *pMinFrame, FrameCopy *pMaxFrame, FrameCopy *pOut)
{
    int ret = -1;
    if (AV_PIX_FMT_YUV420P == pOut->format) {
        ret = 0;
        for (int iy = 0; iy < pOut->height; iy += 2) {
            const uint8_t *pMinY = (const uint8_t *)pMinFrame->data[0] + iy*pMinFrame->linesize[0];
            const uint8_t *pMaxY = (const uint8_t *)pMaxFrame->data[0] + iy*pMaxFrame->linesize[0];
            const uint8_t *pMinU = (const uint8_t *)pMinFrame->data[1] + (iy >> 1)*pMinFrame->linesize[1];
            const uint8_t *pMaxU = (const uint8_t *)pMaxFrame->data[1] + (iy >> 1)*pMaxFrame->linesize[1];
            const uint8_t *pMinV = (const uint8_t *)pMinFrame->data[2] + (iy >> 1)*pMinFrame->linesize[2];
            const uint8_t *pMaxV = (const uint8_t *)pMaxFrame->data[2] + (iy >> 1)*pMaxFrame->linesize[2];
            uint8_t *pOutY = pOut->data[0] +  iy*pOut->linesize[0];
            uint8_t *pOutU = pOut->data[1] + (iy >> 1)*pOut->linesize[1];
            uint8_t *pOutV = pOut->data[2] + (iy >> 1)*pOut->linesize[2];
            for (int ix = 0; ix < pOut->width; ix += 2) {
                pOutY[ix]      = pMaxY[ix]      - pMinY[ix];
                pOutY[ix + 1]  = pMaxY[ix + 1]  - pMinY[ix + 1];
                pOutU[ix >> 1] = pMaxU[ix >> 1] - pMinU[ix >> 1];
                pOutV[ix >> 1] = pMaxV[ix >> 1] - pMinV[ix >> 1];
            }
            pMinY = (const uint8_t *)pMinFrame->data[0] + (iy + 1)*pMinFrame->linesize[0];
            pMaxY = (const uint8_t *)pMaxFrame->data[0] + (iy + 1)*pMaxFrame->linesize[0];
            pOutY = pOut->data[0] + (iy + 1)*pOut->linesize[0];
            for (int ix = 0; ix < pOut->width; ix+=2) {
                pOutY[ix]     = pMaxY[ix]     - pMinY[ix];
                pOutY[ix + 1] = pMaxY[ix + 1] - pMinY[ix + 1];
            }
        }
    } else if (AV_PIX_FMT_RGB24 == pOut->format ||
               AV_PIX_FMT_BGR24 == pOut->format) {
        ret = 0;
        for (int iy = 0; iy < pOut->height; iy++) {
            const uint8_t *pMin = (const uint8_t *)pMinFrame->data[0] + iy*pMinFrame->linesize[0];
            const uint8_t *pMax = (const uint8_t *)pMaxFrame->data[0] + iy*pMaxFrame->linesize[0];
            uint8_t *pRGB = pOut->data[0] + iy*pOut->linesize[0];
            for (int ix = 0; ix < pOut->linesize[0]; ix += 3) {
                pRGB[ix]     = pMax[ix]     - pMin[ix];
                pRGB[ix + 1] = pMax[ix + 1] - pMin[ix + 1];
                pRGB[ix + 2] = pMax[ix + 2] - pMin[ix + 2];
            }
        }
    } else if (AV_PIX_FMT_GRAY16BE == pOut->format) {
        ret = 0;
        OpArgs args;
        args.pInOut0 = pMaxFrame;
        args.pInOut1 = pMinFrame;
        args.pInOut2 = pOut;
        doParallel(&args, diff_gray16, 16);
    }
    return ret;
}
typedef struct StatsArgs {
    AABB box;
    FrameCopy *pFrame;
    float meanSum;
    float stdSum;
} StatsArgs;
static void stats16(StatsArgs* args) {
    for (int iy = args->box.begin.y; iy < args->box.end.y; iy ++) {
        uint16_t *pGray = (uint16_t*)(args->pFrame->data[0] + iy*args->pFrame->linesize[0]);
        for (uint16_t *pG = pGray + args->box.begin.x; pG < pGray + args->box.end.x; pG++) {
            args->meanSum += *pG;
            args->stdSum += (*pG)*(*pG);
        }
    }
}
static int calculateMeanAndStd(float *pMean, float *pStd, AABB sampleBox, FrameCopy *pFrame)
{
    int ret = -1;
    if (AV_PIX_FMT_YUV420P == pFrame->format) {
        int counter = 0;
        for (int iy = sampleBox.begin.y; iy < sampleBox.end.y; iy += 2) {
            uint8_t *pDY = pFrame->data[0] +  iy*pFrame->linesize[0];
            uint8_t *pDU = pFrame->data[1] + (iy >> 1)*pFrame->linesize[1];
            uint8_t *pDV = pFrame->data[2] + (iy >> 1)*pFrame->linesize[2];
            for (int ix = sampleBox.begin.x; ix < sampleBox.end.x; ix += 2) {
                *pMean += pDY[ix] + pDY[ix + 1] + pDU[ix >> 1] + pDV[ix >> 1];
                *pStd  += pDY[ix]*pDY[ix] +
                         pDY[ix + 1] *pDY[ix + 1]  +
                         pDU[ix >> 1]*pDU[ix >> 1] +
                         pDV[ix >> 1]*pDV[ix >> 1];
                counter += 4;
            }
            // only 2 Y channels - not 4 - it is a estimation
        }
        ret = -2;
        if (counter) {
            ret = 0;
            *pMean /= counter;
            *pStd = sqrt(*pStd/counter - (*pMean) * (*pMean));
        }
    } else if (AV_PIX_FMT_RGB24 == pFrame->format ||
               AV_PIX_FMT_BGR24 == pFrame->format) {
        int counter = 0;
        for (int iy = sampleBox.begin.y; iy < sampleBox.end.y; iy ++) {
            uint8_t *pDRgb = pFrame->data[0] +  iy*pFrame->linesize[0];
            for (int ix = sampleBox.begin.x; ix < sampleBox.end.x; ix++) {
                *pMean += pDRgb[ix] + pDRgb[ix + 1] + pDRgb[ix + 2];
                *pStd  += pDRgb[ix]*pDRgb[ix] +
                         pDRgb[ix + 1]*pDRgb[ix + 1] +
                         pDRgb[ix + 2]*pDRgb[ix + 2];
                counter += 3;
            }
        }
        ret = -2;
        if (counter) {
            ret = 0;
            *pMean /= counter;
            *pStd = sqrt(*pStd/counter - (*pMean) * (*pMean));
        }
    } else if (AV_PIX_FMT_GRAY16BE == pFrame->format) {
        int counter = 0;
        float m = 0.0f;
        float s = 0.0f;
        StatsArgs args[12];
        pthread_t threads[12];
        memset(args, 0, 12*sizeof(StatsArgs));
        memset(threads, 0, 12*sizeof(pthread_t));
        int w = (sampleBox.end.y - sampleBox.begin.y)/5;
        int i, nb;
        nb = 5;
        for (i = 0; i < nb-1; i++)
        {
            args[i].box.begin.x = sampleBox.begin.x;
            args[i].box.begin.y = sampleBox.begin.y + i*w;
            args[i].box.end.x = sampleBox.end.x;
            args[i].box.end.y = sampleBox.begin.y + (i+1)*w;
            args[i].meanSum = 0.0f;
            args[i].stdSum = 0.0f;
            args[i].pFrame = pFrame;
            pthread_create(threads + i, NULL, stats16, args + i);
        }
        args[i].box.begin.x = sampleBox.begin.x;
        args[i].box.begin.y = sampleBox.begin.y + i*w;
        args[i].box.end.x = sampleBox.end.x;
        args[i].box.end.y = sampleBox.end.y;
        args[i].meanSum = 0.0f;
        args[i].stdSum = 0.0f;
        args[i].pFrame = pFrame;
        stats16(args + i);
        for (i = 0; i < nb; i++)
        {
            pthread_join(threads[i], NULL);
            m += args[i].meanSum;
            s += args[i].stdSum;
        }
        counter = (sampleBox.end.x - sampleBox.begin.x) * (sampleBox.end.y - sampleBox.begin.y);
        ret = -2;
        if (counter) {
            ret = 0;
            *pMean = m/counter;
            *pStd = sqrt(s/counter - (*pMean) * (*pMean));
        }
    }
    return ret;
}
static void generateBoxes_JP2(FrameBoxes *pCurrentSet, x264_picture_t *pFrame)
{
    clock_t genBegin = clock();
    float globalStd, globalMean;
    int minMovementBox;
    minMovementBox = MOVEMENT_BOX_DIM; //kacziro - start here - important - denominator width and height!

    pCurrentSet->table.number = 2; // kacziro - fixme - generate this
    pCurrentSet->table.cells[0].method = SWS_BICUBIC;
    pCurrentSet->table.cells[0].xReduction = hd3Data.globalXCompression;
    pCurrentSet->table.cells[0].yReduction = hd3Data.globalYCompression;
    pCurrentSet->table.cells[1].method = SWS_POINT;
    pCurrentSet->table.cells[1].xReduction = hd3Data.globalXCompression;
    pCurrentSet->table.cells[1].yReduction = hd3Data.globalYCompression;

    if (AV_PIX_FMT_GRAY16BE != hd3Data.buff.format) {
        diff(&hd3Data.mins, &hd3Data.maxes, &hd3Data.buff);
    }

    clock_t meanBegin = clock();
    perfData.mean.sum += (float)(meanBegin - genBegin);
    perfData.mean.number++;

    AABB statSample;
    statSample.begin.x = 0;
    statSample.begin.y = 0;
    statSample.end.x = hd3Data.buff.width;
    statSample.end.y = hd3Data.buff.height/3;
    calculateMeanAndStd(&globalMean, &globalStd, statSample, &hd3Data.buff);

    //km = 2; ks = 5; ks2 = 3; || th = km*mA+ks2*sA;
    //hd3Data.stats.threshold = 2*globalMean + 3*globalStd;// + globalStd/2;
    hd3Data.stats.threshold = hd3Data.stats.km*globalMean + hd3Data.stats.ks*globalStd;// + globalStd/2;

    clock_t boxBegin = clock();
    perfData.std.sum += (float)(boxBegin - meanBegin);
    perfData.std.number++;
    perfData.threshold.sum += (float)(boxBegin - genBegin);
    perfData.threshold.number++;

    if (AV_PIX_FMT_YUV420P == hd3Data.buff.format) {
        detectBBYUV(pCurrentSet, &hd3Data.buff, &pFrame->img, minMovementBox, hd3Data.stats.threshold, hd3Data.stats.minObjSize);
    } else if (AV_PIX_FMT_RGB24 == hd3Data.buff.format) {
        detectBBRGB(pCurrentSet, &hd3Data.buff, &pFrame->img, minMovementBox, hd3Data.stats.threshold, hd3Data.stats.minObjSize);
    } else if (AV_PIX_FMT_GRAY16BE == hd3Data.buff.format) {
        //detectBBGray16(pCurrentSet, &hd3Data.buff, &pFrame->img, minMovementBox, hd3Data.stats.threshold, hd3Data.stats.minObjSize);
        detectBB_gray16_parallel(pCurrentSet, &hd3Data.buff, &pFrame->img, minMovementBox, hd3Data.stats.threshold, hd3Data.stats.minObjSize, 24);
    }

    //remove edges
    clock_t edgeBegin = clock();
    perfData.box.sum += (float)(edgeBegin - boxBegin);
    perfData.box.number++;

    perfData.subEdge.number++;
    {
        int newNum = 0;
        MovementBox *pCurr, *pEnd;
        pEnd = pCurrentSet->boxes + pCurrentSet->number;
        for (pCurr = pCurrentSet->boxes; pCurr < pEnd; pCurr++) {
            if (pCurr->aabb.begin.x != pCurr->aabb.end.x) {
//                            if (X264_CSP_I420 == pFrame->img.i_csp)
//                            {
//                                for (int iyy = pCurr->aabb.begin.y/2; iyy < pCurr->aabb.end.y/2; iyy++)
//                                {
//                                    memset(pFrame->img.plane[2] + iyy*(hd3Data.width/2) + pCurr->aabb.begin.x/2,
//                                        255,
//                                        minMovementBox/2);
//                                }
//                            }
//                            else if (X264_CSP_RGB == pFrame->img.i_csp)
//                            {
//                                for (int iyy = pCurr->aabb.begin.y; iyy < pCurr->aabb.end.y; iyy++)
//                                {
//                                    memset(pFrame->img.plane[0] + iyy*(hd3Data.width)*3 + pCurr->aabb.begin.x*3,
//                                           255,
//                                           minMovementBox*3);
//                                }
//                            }
                pCurrentSet->boxes[newNum] = *pCurr;
                newNum++;
            }
        }
        pCurrentSet->number = newNum;
        aggregation_closedMax(pCurrentSet, hd3Data.stats.bbMarginX, hd3Data.stats.bbMarginY);
    }

    clock_t genEnd = clock();
    perfData.edge.sum += (float)(genEnd - edgeBegin);
    perfData.edge.number++;
    perfData.gen.sum += (float)(genEnd - genBegin);
    perfData.gen.number++;
}

#endif
