#ifndef HD3_DETECT_BB_H
#define HD3_DETECT_BB_H

#include "hd3/hd3.h"
#include <stdint.h>

static void detectBBYUV(FrameBoxes* pSet, FrameCopy *pDiff, x264_image_t *pFrame,
                        int minMovementBox, float threshold, int minObjSize)
{
    int indexMask, hw;
    hw = pDiff->width >> 1;
    MovementBox* pEnd;
    pEnd = pSet->boxes;
    for (int iy = 0; iy < pDiff->height; iy++)
    {
        const uint8_t *pY = pDiff->data[0] + iy*pDiff->width;
        const uint8_t *pU = pDiff->data[1] + (iy >> 1)*hw;
        const uint8_t *pV = pDiff->data[2] + (iy >> 1)*hw;
        uint8_t *pBoxMask = pFrame->plane[1] + (iy/minMovementBox)*hw;
        uint8_t *pMask = pFrame->plane[0] + iy*pDiff->width;
        for (int ix = 0; ix < pDiff->width; ix++)
        {
            pMask[ix] = (pV[ix/2] > threshold ||
                         pU[ix/2] > threshold ||
                         pY[ix]   > threshold); // kacziro - fixme - why the same threshold for each channel
            //pFrame->plane[0][index] *= 32;
            if(pMask[ix] > 0)
            {
                indexMask = ix/minMovementBox;
                pBoxMask[indexMask]++;
                if (pBoxMask[indexMask] == minObjSize)
                {
                    pEnd->aabb.begin.x = indexMask*minMovementBox;
                    pEnd->aabb.end.x =   pEnd->aabb.begin.x + minMovementBox;
                    pEnd->aabb.begin.y = (iy/minMovementBox)*minMovementBox;
                    pEnd->aabb.end.y =   pEnd->aabb.begin.y + minMovementBox;

                    pEnd->dstAabb = pEnd->aabb;

                    pEnd++;
                }
            }
        }
    }
    pSet->number = pEnd - pSet->boxes;
}
static void detectBBRGB(FrameBoxes* pSet, FrameCopy *pDiff, x264_image_t *pFrame,
                        int minMovementBox, float threshold, int minObjSize)
{
    int indexMask;
    MovementBox* pEnd;
    pEnd = pSet->boxes;
    for (int iy = 0; iy < pDiff->height; iy++) {
        const uint8_t *pRGB = pDiff->data[0] + iy*pDiff->linesize[0];
        uint8_t *pBoxMask = pFrame->plane[0] + (iy/minMovementBox)*pFrame->i_stride[0] + 1;
        uint8_t *pMask = pFrame->plane[0] + iy*pDiff->linesize[0];
        for (int ix = 0; ix < pDiff->linesize[0]; ix+=3) {
            pMask[ix] = (pRGB[ix]     > threshold ||
                         pRGB[ix + 1] > threshold ||
                         pRGB[ix + 2] > threshold); // kacziro - fixme - why the same threshold for each channel
            pMask[ix] *= 48;
            if(pMask[ix] > 0) {
                indexMask = ((ix/3)/minMovementBox);
                pBoxMask[indexMask]++;
                if (pBoxMask[indexMask] == minObjSize) {
                    pEnd->aabb.begin.x = indexMask*minMovementBox;
                    pEnd->aabb.end.x   = pEnd->aabb.begin.x + minMovementBox;
                    pEnd->aabb.begin.y = (iy/minMovementBox)*minMovementBox;
                    pEnd->aabb.end.y   = pEnd->aabb.begin.y + minMovementBox;

                    pEnd->dstAabb = pEnd->aabb;

                    pEnd++;
                }
            }
        }
    }
    pSet->number = pEnd - pSet->boxes;
}

static void detectBBGray8(FrameBoxes* pSet, FrameCopy *pDiff, x264_image_t *pFrame,
                        int minMovementBox, float threshold, int minObjSize)
{
    MovementBox* pEnd;
    pEnd = pSet->boxes;
    int xDiv = 3*minMovementBox;
    int w = pDiff->width*3;
    for (int iy = 0; iy < pDiff->height; iy++)
    {
        const uint8_t *pD = pDiff->data[0] + iy*w;
        uint8_t *pBoxMask = pFrame->plane[0] + (iy/minMovementBox)*w + 1;
        uint8_t *pMask = pFrame->plane[0] + iy*w;
        for (int ix = 0; ix < w; ix += 3)
        {
            pMask[ix] = (pD[ix] > threshold)*128;
            if(pMask[ix] > 0)
            {
                int indexMask = (ix/xDiv);
                pBoxMask[3*indexMask]++;
                if (pBoxMask[3*indexMask] == minObjSize)
                {
                    pEnd->aabb.begin.x = indexMask*minMovementBox;
                    pEnd->aabb.end.x =   pEnd->aabb.begin.x + minMovementBox;
                    pEnd->aabb.begin.y = (iy/minMovementBox)*minMovementBox;
                    pEnd->aabb.end.y =   pEnd->aabb.begin.y + minMovementBox;

                    pEnd->dstAabb = pEnd->aabb;

                    pEnd++;
                }
            }
        }
    }
    pSet->number = pEnd - pSet->boxes;
}
typedef struct DetectArgs {
    MovementBox *pStart;
    FrameCopy *pDiff;
    x264_image_t *pFrame;
    int minObjSize;
    int minMovementBox;
    float threshold;
    int starty;
    int endy;
} DetectArgs;
static void detect_gray16(DetectArgs *args)
{
    MovementBox* pEnd;
    pEnd = args->pStart;
    for (int iy = args->starty; iy < args->endy; iy++) {
        const uint16_t *pD = (const uint16_t*)(args->pDiff->data[0] + iy*args->pDiff->linesize[0]);
        uint8_t *pBoxMask = args->pFrame->plane[0] + (iy/args->minMovementBox)*args->pFrame->i_stride[0] + 1; // +1 -> different channel in rgb
        uint8_t *pMask    = args->pFrame->plane[0] + iy*args->pFrame->i_stride[0];
        for (int ix = 0; ix < args->pDiff->width; ix++) {
            pMask[3*ix] = (pD[ix] > args->threshold)*128;
            if (pMask[3*ix] > 0) {
                int indexMask = (ix/args->minMovementBox);
                pBoxMask[indexMask*3]++;
                if (pBoxMask[indexMask*3] == args->minObjSize) {
                    pEnd->aabb.begin.x = indexMask*args->minMovementBox;
                    pEnd->aabb.end.x   = pEnd->aabb.begin.x + args->minMovementBox;
                    pEnd->aabb.begin.y = (iy/args->minMovementBox)*args->minMovementBox;
                    pEnd->aabb.end.y   = pEnd->aabb.begin.y + args->minMovementBox;

                    pEnd->dstAabb = pEnd->aabb;

                    pEnd++;
                }
            }
        }
    }
}
static void detectBB_gray16_parallel(FrameBoxes* pSet, FrameCopy *pDiff, x264_image_t *pFrame,
                                     int minMovementBox, float threshold, int minObjSize, int nr)
{
    DetectArgs args[24];
    pthread_t threads[24];
    nr = MAX(1, MIN(24, nr));
    memset(threads, 0, sizeof(pthread_t)*24);
    memset(args, 0, sizeof(DetectArgs)*24);

    int delta = ((pDiff->height / nr) / minMovementBox)*minMovementBox; // int - multiplication of minMovementBox
    if (delta == 0)
    {
        delta = minMovementBox;
        nr = pDiff->height / delta;
        if (pDiff->height % delta) {
            nr++;
        }
    }
    int maxBoxPerThread = (pDiff->width/minMovementBox + 1)*(delta/minMovementBox);
    memset(pSet->boxes, 0, sizeof(MovementBox)*maxBoxPerThread*nr);

    int i;
    for (i = 0; i < nr - 1; i++)
    {
        args[i].pStart = pSet->boxes + i*maxBoxPerThread;
        args[i].pDiff = pDiff;
        args[i].pFrame = pFrame;
        args[i].minObjSize = minObjSize;
        args[i].minMovementBox = minMovementBox;
        args[i].threshold = threshold;

        args[i].starty = i*delta;
        args[i].endy = (i + 1)*delta;

        pthread_create(threads + i, NULL, detect_gray16, args + i);
        //detect_gray16(args + i);
    }
    args[i].pStart = pSet->boxes + i*maxBoxPerThread;
    args[i].pDiff = pDiff;
    args[i].pFrame = pFrame;
    args[i].minObjSize = minObjSize;
    args[i].minMovementBox = minMovementBox;
    args[i].threshold = threshold;

    args[i].starty = i*delta;
    args[i].endy = pDiff->height;
    //pthread_create(threads + i, NULL, detect_gray16, args + i);
    detect_gray16(args + i);

    for (int i = 0; i < nr; i++)
    {
        pthread_join(threads[i], NULL);
    }

    pSet->number = maxBoxPerThread*nr;
}
static void detectBBGray16(FrameBoxes* pSet, FrameCopy *pDiff, x264_image_t *pFrame,
                           int minMovementBox, float threshold, int minObjSize)
{
    MovementBox* pEnd;
    pEnd = pSet->boxes;
    int xDiv = minMovementBox;
    for (int iy = 0; iy < pDiff->height; iy++) {
        const uint16_t *pD = (const uint16_t*)(pDiff->data[0] + iy*pDiff->linesize[0]);
        uint8_t *pBoxMask = pFrame->plane[0] + (iy/minMovementBox)*pFrame->i_stride[0] + 1; // +1 -> different channel in rgb
        uint8_t *pMask    = pFrame->plane[0] + iy*pFrame->i_stride[0];
        for (int ix = 0; ix < pDiff->width; ix++) {
            pMask[3*ix] = (pD[ix] > threshold)*128;
            if(pMask[3*ix] > 0) {
                int indexMask = (ix/xDiv);
                pBoxMask[indexMask*3]++;
                if (pBoxMask[indexMask*3] == minObjSize) {
                    pEnd->aabb.begin.x = indexMask*minMovementBox;
                    pEnd->aabb.end.x   = pEnd->aabb.begin.x + minMovementBox;
                    pEnd->aabb.begin.y = (iy/minMovementBox)*minMovementBox;
                    pEnd->aabb.end.y   = pEnd->aabb.begin.y + minMovementBox;

                    pEnd->dstAabb = pEnd->aabb;

                    pEnd++;
                }
            }
        }
    }
    pSet->number = pEnd - pSet->boxes;
}
#endif
