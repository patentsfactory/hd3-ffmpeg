#ifndef HD3_TOOLS_H
#define HD3_TOOLS_H
#define MAX(a,b) ((a) < (b) ? (b) : (a))
#define MIN(a,b) ((a) > (b) ? (b) : (a))

#include <pthread.h>
#include "libswscale/swscale_internal.h"
#include "libswscale/swscale.h"
#include <stdint.h>
#include <x264.h>
#include <common/common.h> //in x264
#include "frame_copy.h"

static void scale(x264_image_t *src, int srcW, int srcH, x264_image_t* dst, int dstW, int dstH, int format, unsigned method)
{
    SwsContext *img_convert_ctx = NULL;
    img_convert_ctx = sws_getCachedContext(img_convert_ctx, srcW, srcH, format, dstW, dstH, format, method, NULL, NULL, NULL);
    if (img_convert_ctx != NULL)
    {
        sws_scale(img_convert_ctx, (const uint8_t * const *)src->plane, src->i_stride, 0, srcH, dst->plane, dst->i_stride);
        sws_freeContext(img_convert_ctx);
    }
}

typedef struct ScaleArg
{
    int srcW;
    int srcH;
    x264_image_t imgSrc;
    int dstW;
    int dstH;
    x264_image_t imgDst;
    int format;
} ScaleArg;

static void* scaleThread(ScaleArg *a)
{
    SwsContext *img_convert_ctx = NULL;
    img_convert_ctx = sws_getCachedContext(img_convert_ctx,
                                           a->srcW, a->srcH, a->format,
                                           a->dstW, a->dstH, a->format,
                                           SWS_BICUBIC, NULL, NULL, NULL);
    if (img_convert_ctx != NULL)
    {
        sws_scale(img_convert_ctx, (const uint8_t * const *)a->imgSrc.plane, a->imgSrc.i_stride, 0, a->srcH, a->imgDst.plane, a->imgDst.i_stride);
        sws_freeContext(img_convert_ctx);
    }
    return NULL;
}
typedef struct BayerThreadArgs
{
    AVFrame *pIn;
    FrameCopy *pOut;
    uint32_t start;
    uint32_t end;
} BayerThreadArgs;

static void doBayerParallel(AVFrame *pIn, FrameCopy *pOut, int starty, int endy, int delta, void*(*func)(BayerThreadArgs*), int nr)
{
    int i = 0;
    pthread_t threads[24];
    BayerThreadArgs threadArgs[24];
    memset(threads, 0, sizeof(pthread_t)*24);
    memset(threadArgs, 0, sizeof(BayerThreadArgs)*24);
    nr = MAX(1, MIN(24, nr));
    for (; i < nr - 1; i++)
    {
        threadArgs[i].pIn = pIn;
        threadArgs[i].pOut = pOut;
        threadArgs[i].start = i*delta + starty;
        threadArgs[i].end = (i + 1)*delta + starty;
        pthread_create(threads + i, NULL, func, (void*)(threadArgs + i));
        //func(threadArgs + i);
    }
    threadArgs[i].pIn = pIn;
    threadArgs[i].pOut = pOut;
    threadArgs[i].start = i*delta + starty;
    threadArgs[i].end = endy;
    func(threadArgs + i);
    //pthread_create(threads + i, NULL, func, (void*)(threadArgs + i));
    for (int i = 0; i < nr; i++)
    {
        pthread_join(threads[i], NULL);
    }
}

static void preBayerProcessing(uint16_t *pD, uint8_t *pS, int start, int end)
{
    for (int i = start; i < end; i++) { //pSrc->width*pSrc->height
        pD[i] = ((pS[2*i] << 8) + pS[2*i + 1]) >> 4;
        //if (pD[i] & 0xff00)
        if (pD[i] > 255)
            pD[i] = 255;
    }
}
static void preBayerProcessingInverse8(uint8_t *pD, uint8_t *pS, int start, int end)
{
    for (int i = start; i < end; i++) { //pSrc->width*pSrc->height
        uint16_t v = ((pS[2*i + 1] << 8) + pS[2*i]) >> 4;
        //if (pD[i] & 0xff00)
        pD[i] = v;
        if (v > 255)
            pD[i] = 255;
    }
}
static void preBayerProcessingInverse16(uint16_t *pD, uint8_t *pS, int start, int end)
{
    for (int i = start; i < end; i++) { //pSrc->width*pSrc->height
        pD[i] = ((pS[2*i + 1] << 8) + pS[2*i]) >> 4;
        //if (pD[i] & 0xff00)
        if (pD[i] > 255)
            pD[i] = 255;
    }
}
static void preBayerProcessingThread(BayerThreadArgs *args)
{
    preBayerProcessing(args->pIn->data[0], args->pIn->data[0], args->start, args->end);
}
static void preBayerProcessingInverseThread(BayerThreadArgs *args)
{
    preBayerProcessingInverse16(args->pIn->data[0], args->pIn->data[0], args->start, args->end);
}
static void bayerToRgb_edgeDirected(FrameCopy *pDst, AVFrame *pSrc, int startY, int endY)
{
    int rrow = 0;
    int gcolumn = !rrow;
    for (int y = startY; y < endY; y++) {
        uint8_t *pRGB = pDst->data[0] + y*pDst->linesize[0];
        uint16_t *pD = pSrc->data[0] + y*pSrc->linesize[0];
        if (gcolumn) {
            for (int x = 1; x < pSrc->width - 1; x += 2) {
                uint8_t g = (pD[x] + pD[x + 2] + pD[x + 1 - pSrc->width] + pD[x + 1 + pSrc->width]) >> 2;
                pRGB[3 * x] = (pD[x - 1] + pD[x + 1]) >> 1;
                pRGB[3 * x + 1] =  pD[x];
                pRGB[3 * x + 2] = (pD[x - pSrc->width] + pD[x + pSrc->width]) >> 1;
                pRGB[3 * x + 3] = pD[x + 1];
                if (abs(pD[x] - pD[x + 2]) > abs(pD[x + 1 - pSrc->width] - pD[x + 1 + pSrc->width])) {
                    g = (pD[x + 1 - pSrc->width] + pD[x + 1 + pSrc->width]) >> 1;
                } else if (abs(pD[x] - pD[x + 2]) < abs(pD[x + 1 - pSrc->width] - pD[x + 1 + pSrc->width])) {
                    g = (pD[x] + pD[x + 2]) >> 1;
                }
                pRGB[3 * x + 4] = g;//(pD[x] + pD[x + 2] + pD[x + 1 - pSrc->width] + pD[x + 1 + pSrc->width]) >> 2;
                pRGB[3 * x + 5] = (pD[x - pSrc->width] + pD[x + 2 + pSrc->width] + pD[x + 2 - pSrc->width] + pD[x + pSrc->width]) >> 2;
            }
        } else {
            for (int x = 1; x < pSrc->width - 1; x += 2) {
                uint8_t g = (pD[x - 1] + pD[x + 1] + pD[x - pSrc->width] + pD[x + pSrc->width]) >> 2;
                pRGB[3 * x + 3] = (pD[x + 1 - pSrc->width] + pD[x + 1 + pSrc->width]) >> 1;
                pRGB[3 * x + 4] = pD[x + 1];
                pRGB[3 * x + 5] = (pD[x] + pD[x + 2]) >> 1;
                pRGB[3 * x] = (pD[x - 1 - pSrc->width] + pD[x + 1 + pSrc->width] + pD[x + 1 - pSrc->width] + pD[x - 1 + pSrc->width]) >> 2;
                if (abs(pD[x - 1] - pD[x + 1]) > abs(pD[x - pSrc->width] - pD[x + pSrc->width])) {
                    g = (pD[x - pSrc->width] + pD[x + pSrc->width]) >> 1;
                } else if (abs(pD[x] - pD[x + 2]) < abs(pD[x + 1 - pSrc->width] - pD[x + 1 + pSrc->width])) {
                    g = (pD[x - 1] + pD[x + 1]) >> 1;
                }
                pRGB[3 * x + 1] = g;//(pD[x - 1] + pD[x + 1] + pD[x - pSrc->width] + pD[x + pSrc->width]) >> 2;
                pRGB[3 * x + 2] = pD[x];
            }
        }
        rrow = !rrow;
        gcolumn = !gcolumn;
    }
}static void bayerToRgb8_simple_internal(uint8_t *pDst, int32_t destLinesize, uint8_t *pSrc, int32_t srcLinesize, int32_t width, int32_t startY, int32_t endY)
{
    int rrow = 0;
    int gcolumn = !rrow;
    for (int y = startY; y < endY; y++) {
        uint8_t *pRGB = pDst + y*destLinesize;
        uint8_t *pD = pSrc + y*srcLinesize;
        if (gcolumn) {
            for (int x = 1; x < width - 1; x += 2) {
                pRGB[3 * x] = (pD[x - 1] + pD[x + 1]) >> 1;
                pRGB[3 * x + 1] =  pD[x];
                pRGB[3 * x + 2] = (pD[x - width] + pD[x + width]) >> 1;
                pRGB[3 * x + 3] =  pD[x + 1];
                pRGB[3 * x + 4] = (pD[x] + pD[x + 2] + pD[x + 1 - width] + pD[x + 1 + width]) >> 2;
                pRGB[3 * x + 5] = (pD[x - width] + pD[x + 2 + width] + pD[x + 2 - width] + pD[x + width]) >> 2;
            }
        } else {
            for (int x = 1; x < width - 1; x += 2) {
                pRGB[3 * x] = (pD[x - 1 - width] + pD[x + 1 + width] + pD[x + 1 - width] + pD[x - 1 + width]) >> 2;
                pRGB[3 * x + 1] = (pD[x - 1] + pD[x + 1] + pD[x - width] + pD[x + width]) >> 2;
                pRGB[3 * x + 2] =  pD[x];
                pRGB[3 * x + 3] = (pD[x + 1 - width] + pD[x + 1 + width]) >> 1;
                pRGB[3 * x + 4] =  pD[x + 1];
                pRGB[3 * x + 5] = (pD[x] + pD[x + 2]) >> 1;
            }
        }
        gcolumn = !gcolumn;
    }
}
static void bayerToRgb8_simple(BayerThreadArgs *args)
{
    bayerToRgb8_simple_internal(args->pOut->data[0], args->pOut->linesize[0], args->pIn->data[0], args->pIn->linesize[0], args->pIn->width, args->start, args->end);
}
static void bayerToRgb16_simple_internal(uint8_t *pDst, int32_t destLinesize, uint8_t *pSrc, int32_t srcLinesize, int32_t width, int32_t startY, int32_t endY)
{
    int rrow = 0;
    int gcolumn = !rrow;
    for (int y = startY; y < endY; y++) {
        uint8_t *pRGB = pDst + y*destLinesize;
        uint16_t *pD = (uint16_t*)(pSrc + y*srcLinesize);
        if (gcolumn) {
            for (int x = 1; x < width - 1; x += 2) {
                pRGB[3 * x] = (pD[x - 1] + pD[x + 1]) >> 1;
                pRGB[3 * x + 1] =  pD[x];
                pRGB[3 * x + 2] = (pD[x - width] + pD[x + width]) >> 1;
                pRGB[3 * x + 3] =  pD[x + 1];
                pRGB[3 * x + 4] = (pD[x] + pD[x + 2] + pD[x + 1 - width] + pD[x + 1 + width]) >> 2;
                pRGB[3 * x + 5] = (pD[x - width] + pD[x + 2 + width] + pD[x + 2 - width] + pD[x + width]) >> 2;
            }
        } else {
            for (int x = 1; x < width - 1; x += 2) {
                pRGB[3 * x] = (pD[x - 1 - width] + pD[x + 1 + width] + pD[x + 1 - width] + pD[x - 1 + width]) >> 2;
                pRGB[3 * x + 1] = (pD[x - 1] + pD[x + 1] + pD[x - width] + pD[x + width]) >> 2;
                pRGB[3 * x + 2] =  pD[x];
                pRGB[3 * x + 3] = (pD[x + 1 - width] + pD[x + 1 + width]) >> 1;
                pRGB[3 * x + 4] =  pD[x + 1];
                pRGB[3 * x + 5] = (pD[x] + pD[x + 2]) >> 1;
            }
        }
        gcolumn = !gcolumn;
    }
}
static void bayerToRgb16_simple(BayerThreadArgs *args)
{
    bayerToRgb16_simple_internal(args->pOut->data[0], args->pOut->linesize[0], args->pIn->data[0], args->pIn->linesize[0], args->pIn->width, args->start, args->end);
}
static void edgeDirectedBayerThread(BayerThreadArgs *args)
{
    bayerToRgb_edgeDirected(args->pOut, args->pIn, args->start, args->end);
}
static void bayerToYuv_simple_internal(uint8_t *pYDst, uint8_t *pUDst, uint8_t *pVDst,
                                       int32_t destYLinesize, int32_t destULinesize, int32_t destVLinesize,
                                       uint8_t *pSrc, int32_t srcLinesize, int32_t width, int32_t startY, int32_t endY)
{
    int rrow = 0;
    int gcolumn = !rrow;
    for (int y = startY; y < endY; y++) {
        uint8_t *pY = pYDst + y*destYLinesize;
        uint8_t *pU = pUDst + (y/2)*destULinesize;
        uint8_t *pV = pVDst + (y/2)*destVLinesize;
        uint16_t *pD = (uint16_t*)(pSrc + y*srcLinesize);
        if (gcolumn) {
            for (int x = 1; x < width - 1; x += 2) {
                uint8_t r0 = (pD[x - 1] + pD[x + 1]) >> 1;
                uint8_t g0 =  pD[x];
                uint8_t b0 = (pD[x - width] + pD[x + width]) >> 1;

                uint8_t r1 =  pD[x + 1];
                uint8_t g1 = (pD[x] + pD[x + 2] + pD[x + 1 - width] + pD[x + 1 + width]) >> 2;
                uint8_t b1 = (pD[x - width] + pD[x + 2 + width] + pD[x + 2 - width] + pD[x + width]) >> 2;

                pY[x] =     ((66*r0 + 129*g0 + 25*b0) >> 8) + 16;
                pY[x + 1] = ((66*r1 + 129*g1 + 25*b1) >> 8) + 16;

                pU[x >> 1] = ((-38*(r0 + r1) + -74*(g0 + g1) + 112*(b0 + b1)) >> 8) + 128;
                pV[x >> 1] = ((112*(r0 + r1) + -94*(g0 + g1) + -18*(b0 + b1)) >> 8) + 128;
            }
        } else {
            for (int x = 1; x < width - 1; x += 2) {
                uint8_t r = (pD[x - 1 - width] + pD[x + 1 + width] + pD[x + 1 - width] + pD[x - 1 + width]) >> 2;
                uint8_t g = (pD[x - 1] + pD[x + 1] + pD[x - width] + pD[x + width]) >> 2;
                uint8_t b =  pD[x];
                pY[x] = ((66*r + 129*g + 25*b) >> 8) + 16;
                r = (pD[x + 1 - width] + pD[x + 1 + width]) >> 1;
                g =  pD[x + 1];
                b = (pD[x] + pD[x + 2]) >> 1;
                pY[x + 1] = ((66*r + 129*g + 25*b) >> 8) + 16;
            }
        }
        gcolumn = !gcolumn;
    }
}
static void bayerToYuv_simple(BayerThreadArgs *args)
{
    bayerToYuv_simple_internal(args->pOut->data[0], args->pOut->data[1], args->pOut->data[2],
                               args->pOut->linesize[0], args->pOut->linesize[1], args->pOut->linesize[2],
                               args->pIn->data[0], args->pIn->linesize[0], args->pIn->width,
                               args->start, args->end);
}

#endif /* HD3_TOOLS_H */
