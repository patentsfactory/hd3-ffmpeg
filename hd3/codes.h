#ifndef HD3_CODES_H
#define HD3_CODES_H

#include "hd3/movement.h"

static uint8_t calculateBitsPerCode(int codeNumber)
{
    int i;
//    if (codeNumber%2 == 1)
//    {
//        codeNumber++;
//    }
//    codeNumber /= 2;
//    while(codeNumber > 0)
//    {
//        codeNumber /= 2;
//        i++;
//    }
    if (codeNumber <= 2)
    {
        i = 1;
    }
    else if (codeNumber <= 4) // kacziro - fixme - i=3,5,6,7
    {
        i = 2;
    }
    else if (codeNumber <= 16)
    {
        i = 4;
    }
    else
    {
        i = 8;
    }
    return i;
}
static void codeMatrixSize(int* outX, int* outY, int width, int height, uint8_t xSize, uint8_t ySize)
{
    *outX = width/xSize;
    //printf("x: %d (%d/%d) -> ", *outX, width, xSize);
    if (width%xSize)
    {
        (*outX)++;
    }
    //printf("%d\n", *outX);
    *outY = height/ySize;
    if (height%ySize)
    {
        (*outY)++;
    }
}
static void codesSize(int* outX, int* outY, int* outSize, AABB* aabb, uint8_t xSize, uint8_t ySize, uint8_t bitsPerCode)
{
    codeMatrixSize(outX, outY, aabb->end.x - aabb->begin.x, aabb->end.y - aabb->begin.y, xSize, ySize);

    *outSize = (*outX)*(*outY)*bitsPerCode;
    if (0 != (*outSize)%8)
    {
       (*outSize) += 8 - (*outSize)%8;
    }
    (*outSize) = (*outSize) / 8;
}
#endif
