#ifndef HD3_H
#define HD3_H

#include <stdint.h>

#include <x264.h>
#include <common/common.h> //in x264

#include "hd3/frame_copy.h"
#include "hd3/movement.h"
#include "hd3/pixorder.h"
#include "hd3/pf_tools.h"
#include "hd3/align.h"

#include "libavutil/frame.h"
#include "libavcodec/h264_sei.h"
#include "libavcodec/avcodec.h"

#include <Winsock2.h>

#define HD3_MIN_MOVEMENT_BOX_SIZE 8
#define HD3_MOVEMENT_MARGIN 1
#define HD3_MIN_MOVEMENT ((HD3_MIN_MOVEMENT_BOX_SIZE+HD3_MOVEMENT_MARGIN*2)*(HD3_MIN_MOVEMENT_BOX_SIZE+HD3_MOVEMENT_MARGIN*2)*2)

#define HD3_AGGREGATION_MAX             1
#define HD3_AGGREGATION_CONST_AREA      2

typedef struct PluginData {
    uint8_t enabled;

} PluginData;
typedef struct BoxReaderData
{
    FILE* pBoxSource;
    MovementBox nextBox;
    int nextPts;
    int nextSceneCut;
    int align;
} BoxReaderData;
typedef struct Stats
{
    uint8_t mix;
    uint8_t minObjSize;
    uint8_t gridWidth;
    uint8_t gridHeight;
    uint8_t minGroup;
    uint8_t maxGroup;
    uint8_t bbMarginX;
    uint8_t bbMarginY;
    float edgeLvl;

    float *blockMean;
    float *blockStd;

    float maxStd;
    float minStd;
    float maxMean;
    float minMean;

    float globalMean;

    float threshold;
    float ks;
    float km;

    float *pDxBuff;
    float *pDyBuff;
} Stats;
typedef struct Hd3Data
{
    int gopSize;
    int frameCountDown;

    int default_lookahead;
    int default_sync_lookahead;
    int default_keyint_max;
    int default_scenecut_threshold;

    float surfaceThreshold;
    Stats stats;
    FrameCopy mins;
    FrameCopy maxes;
    FrameCopy buff;
    FrameCopy integral;

    CodeTable allCodes;
    FrameCopy* pScaledDown;

    uint8_t globalCopressionDefault;
    int globalXCompression;
    int globalYCompression;
    int step;
    Point2u8* pPixOrder;

    int width;
    int height;

    int maxDelayedFrames;
    x264_nal_t metaNal;

    x264_picture_t buffor;
    x264_picture_t outFrame;
    x264_picture_t outPluginFrame;
    FrameCopy* pInputs;
    FrameCopy  prevFrame;

    BoxReaderData readerData;
    FrameBoxes *boxSet;
    FrameBoxes *prevBoxSet;

    int qpForMovements;
    int qpGlobal;
    int staticFlush;

    HD3SEIData *pSei;

    AVCodecContext *ctx;

    int aggregationMethod;
    uint8_t bayerInverse;

    PluginData plugin;
} Hd3Data;

typedef struct QPData
{
    BoxReaderData readerData;
    FrameBoxes boxSet;

    int maxDelayedFrames;

    Hd3Metadata metadata;
    x264_nal_t metaNal;

    int xCompression;
    int yCompression;
} QPData;

static int hd3Encode;
static char *hd3Source; // kacziro - mod
static int hd3SourceType;
static char *pfQP; // kacziro - mod
static QPData qpData;
static Hd3Data hd3Data;

static x264_param_t* pGopParam;
static uint8_t** forcedQP;
static int maxParamIndex;
static int gopParamIndex;

static void initFrameBoxes(FrameBoxes* pBoxSet, int boxNumber)
{
    pBoxSet->boxes = (MovementBox*)malloc(boxNumber*sizeof(MovementBox));
    memset(pBoxSet->boxes, 0, boxNumber*sizeof(MovementBox));
    pBoxSet->number = 0;
    pBoxSet->pts = -1;
    pBoxSet->table.blockXSize = hd3Data.allCodes.blockXSize;
    pBoxSet->table.blockYSize = hd3Data.allCodes.blockYSize;
    if (0 != hd3Data.allCodes.blockXSize && 0 != hd3Data.allCodes.blockYSize) // kacziro - fixme - not here
    {
        pBoxSet->table.allocated = (hd3Data.width*hd3Data.height)/(hd3Data.allCodes.blockXSize*hd3Data.allCodes.blockYSize);
        pBoxSet->table.cells = (CodeCell*)malloc(pBoxSet->table.allocated*sizeof(CodeCell));
    }
    pBoxSet->table.number = 0;
}
static void destroyFrameBoxes(FrameBoxes* pBoxSet)
{
    free(pBoxSet->boxes);
    if (0 != hd3Data.allCodes.blockXSize && 0 != hd3Data.allCodes.blockYSize) // kacziro - fixme - not here
    {
        free(pBoxSet->table.cells); // kacziro - fixme - crash
    }
    pBoxSet->table.number = pBoxSet->table.allocated = 0;
    pBoxSet->number = 0;
    pBoxSet->pts = -1;
}

static int readAABB(MovementBox* pBox, int* pts, int* pIFrameRequest, FILE* pBoxSource)
{
    char ch = EOF;
    *pts = -1;
    if (pBoxSource) {
        char number[8];
        int8_t index = 0;
        int8_t numberCount = 0;
        int state = 0;
        memset(number, 0, sizeof(number));
        while (1) {
            ch = getc(pBoxSource);
            switch(ch) {
                case '=':
                {
                    if (state == 0) {
                        state = 1;
                        index = 0;
                        memset(number, 0, sizeof(number));
                    }
                } break;
                case '0': case '1': case '2':
                case '3': case '4': case '5':
                case '6': case '7': case '8':
                case '9':
                {
                    if (state == 1) {
                        number[index] = ch;
                        index++;
                    }
                } break;
                case ' ':
                {
                    if (index == 0 || state != 1) {
                        //ignore
                        break;
                    }
                }
                default:
                {
                    if (state == 1) {
                        int value = atoi(number);
                        switch(numberCount) {
                            case 0: {
                                *pts = value - 1;
                            } break;
                            case 1: { } break;//who cares..
                            case 2: { pBox->aabb.begin.y = value - 1; } break;
                            case 3: { pBox->aabb.begin.x = value - 1; } break;
                            case 4: { pBox->aabb.end.y = value - 1; } break;
                            case 5: { pBox->aabb.end.x = value - 1; } break;
                            case 6: { } break; //delta y
                            case 7: { } break; //delta x
                            case 8: { } break; // dframe
                            case 9: { //scene cut
                                *pIFrameRequest = value;
                            } break;
                        }
                        numberCount++;
                        state = 0;
                        if (numberCount == 10) {
                            state = 2;
                        }
                    }
                    if (state == 0 && '%' == ch) {
                        state = 4;
                    }
                }
            }
            if('\n' == ch) {
                numberCount = 0;
                if (state != 0 && state != 4) {
                    state = 0;
                    break;
                }
                state = 0;
            } else if ((int)EOF == (int)ch) {
                break;
            }
        }
    }
    return EOF != ch;
}

///////////////////////////////////////////
static int readAABB2(MovementBox* pBox, int* pts, int* pIFrameRequest, FILE* pBoxSource, int boxSize)
{
    char ch = EOF;
    *pts = -1;
    if (pBoxSource) {
        char number[8];
        int8_t index = 0;
        int8_t numberCount = 0;
        int state = 0;
        memset(number, 0, sizeof(number));
        while (1) {
            ch = getc(pBoxSource);
            switch(ch) {
                case '=': {
                    if (state == 0) {
                        state = 1;
                        index = 0;
                        memset(number, 0, sizeof(number));
                    }
                } break;
                case '0': case '1': case '2':
                case '3': case '4': case '5':
                case '6': case '7': case '8':
                case '9': {
                    if (state == 1) {
                        number[index] = ch;
                        index++;
                    }
                } break;
                case ' ': {
                    if (index == 0 || state != 1) { //ignore
                        break;
                    }
                }
                default: {
                    if (state == 1) {
                        int value = atoi(number);
                        switch(numberCount) {
                            case 0: { *pts = value - 1; } break;
                            case 1: {
                                pBox->aabb.begin.x = (value - 1)*boxSize;
                                pBox->aabb.end.x = pBox->aabb.begin.x + boxSize - 1;
                            } break;
                            case 2: {
                                pBox->aabb.begin.y = (value - 1)*boxSize;
                                pBox->aabb.end.y = pBox->aabb.begin.y + boxSize - 1;
                            } break;
                        }
                        numberCount++;
                        state = 0;
                        if (numberCount == 3) {
                            state = 2;
                        }
                    }
                    if (state == 0 && '%' == ch) {
                        state = 4;
                    }
                }
            }
            if('\n' == ch) {
                numberCount = 0;
                if (state != 0 && state != 4) {
                    state = 0;
                    break;
                }
                state = 0;
            } else if ((int)EOF == (int)ch) {
                break;
            }
        }
    }
    //printf("%d [%d, %d; %d, %d]\n", *pts, pBox->aabb.begin.x, pBox->aabb.begin.y, pBox->aabb.end.x, pBox->aabb.end.y);
    return EOF != ch;
}
////////////////////////////////
#define READ_BOXES_METHOD_BEGIN_END 0
#define READ_BOXES_METHOD_GRID 1

static int readBoxes(FrameBoxes* pBoxSet, BoxReaderData* pReader, int* iFrameRequest, int method)
{
    int isMore = 1;
    pBoxSet->number = 0;
    pBoxSet->table.number = 2;
    pBoxSet->table.cells[0].method = SWS_BICUBIC;
    pBoxSet->table.cells[0].xReduction = hd3Data.globalXCompression;
    pBoxSet->table.cells[0].yReduction = hd3Data.globalYCompression;
    pBoxSet->table.cells[1].method = SWS_POINT;
    pBoxSet->table.cells[1].xReduction = hd3Data.globalXCompression;
    pBoxSet->table.cells[1].yReduction = hd3Data.globalYCompression;

    if (pBoxSet->pts > pReader->nextPts)
    {
        if (READ_BOXES_METHOD_BEGIN_END == method) {
            isMore = readAABB(&pReader->nextBox, &pReader->nextPts, &pReader->nextSceneCut, pReader->pBoxSource);
        } else if (READ_BOXES_METHOD_GRID == method) {
            isMore = readAABB2(&pReader->nextBox, &pReader->nextPts, &pReader->nextSceneCut, pReader->pBoxSource, 32);
        }
        pReader->nextBox.code = 0;
    }
    if (pReader->nextPts == pBoxSet->pts)
    {
        *iFrameRequest = pReader->nextSceneCut;
        if(pReader->nextBox.aabb.begin.x != pReader->nextBox.aabb.end.x && pReader->nextBox.aabb.begin.y != pReader->nextBox.aabb.end.y)
        {
            pBoxSet->boxes[pBoxSet->number] = pReader->nextBox;
            pBoxSet->boxes[pBoxSet->number].aabb.end.x++;
            pBoxSet->boxes[pBoxSet->number].aabb.end.y++;
            pBoxSet->number++;
            while(isMore && (pReader->nextPts == pBoxSet->pts))
            {
                if (0 == method) {
                    isMore = readAABB(&pReader->nextBox, &pReader->nextPts, &pReader->nextSceneCut, pReader->pBoxSource);
                } else if (1 == method) {
                    isMore = readAABB2(&pReader->nextBox, &pReader->nextPts, &pReader->nextSceneCut, pReader->pBoxSource, 32);
                }
                pReader->nextBox.code = 0;

                if (pReader->nextPts == pBoxSet->pts)
                {
                    *iFrameRequest = pReader->nextSceneCut;
                    pBoxSet->boxes[pBoxSet->number] = pReader->nextBox;
                    pBoxSet->boxes[pBoxSet->number].aabb.end.x++;
                    pBoxSet->boxes[pBoxSet->number].aabb.end.y++;
                    pBoxSet->number++;
                }
            }
        }
    }
    return isMore;
}
static int dynamicQP_init(AVCodecContext *ctx, x264_param_t* pParams, int maxDelayedFrames, int xCompression, int yCompression)
{
    FILE* pF = fopen(pfQP, "r");
    if (NULL != pF || HD3_SOURCE_TYPE_FILE != hd3SourceType)
    {
        int add = 0;
        int maxBoxes = (pParams->i_width*pParams->i_height)/1000;
        int blockCount = (pParams->i_width/16)*(add + pParams->i_height/16); // kacziro - fixme TODO magic number
        qpData.xCompression = xCompression;
        qpData.yCompression = yCompression;
        qpData.maxDelayedFrames = maxDelayedFrames;
        qpData.metadata.pMovements = (MovementMetadata*)malloc(maxDelayedFrames*sizeof(MovementMetadata));
        memset(qpData.metadata.pMovements, 0, maxDelayedFrames*sizeof(MovementMetadata));

        qpData.metaNal.p_payload = (uint8_t*)malloc(sizeof(MovementBox)*maxBoxes);
        memset(qpData.metaNal.p_payload, 0, sizeof(MovementBox)*maxBoxes);
        qpData.metaNal.i_payload = sizeof(MovementBox)*maxBoxes;
        qpData.metaNal.i_padding = 0;
        for (int i = 0; i < maxDelayedFrames; i++)
        {
            qpData.metadata.pMovements[i].maxNumber = maxBoxes;
            qpData.metadata.pMovements[i].pBoxes = (MovementBox*)malloc(maxBoxes*sizeof(MovementBox));
        }
        initFrameBoxes(&qpData.boxSet, pParams->i_width*pParams->i_height);
        qpData.readerData.nextPts = -1;
        qpData.readerData.pBoxSource = pF;
        gopParamIndex = 0;
        pGopParam = (x264_param_t*)malloc(maxDelayedFrames*sizeof(x264_param_t));

        if (pParams->i_height % 16 != 0) {
            add = 1;
        }

        forcedQP = (int8_t**)malloc(sizeof(int8_t*)*maxDelayedFrames);
        for (int i = 0; i < maxDelayedFrames; i++)
        {
            x264_param_default(pGopParam + i);
            pGopParam[i].i_csp = pParams->i_csp;
            pGopParam[i].i_width = pParams->i_width;
            pGopParam[i].i_height = pParams->i_height;
            pGopParam[i].rc.i_lookahead = pParams->rc.i_lookahead;
            pGopParam[i].i_sync_lookahead = pParams->i_sync_lookahead;
            pGopParam[i].i_keyint_max = pParams->i_keyint_max;
            pGopParam[i].i_scenecut_threshold = pParams->i_scenecut_threshold;

            forcedQP[i] = (int8_t*)malloc(sizeof(int8_t)*blockCount);
            memset(forcedQP[i], -1, blockCount*sizeof(int8_t));
            pGopParam[i].forcedQP = forcedQP;
            pGopParam[i].maxBufferedFrames = maxDelayedFrames;
        }
        return 0;
    }
    av_log(ctx, AV_LOG_ERROR, "Error: cannot open file %s\n", pfQP);
    return 1;
}
static void dynamicQP_destroy(void)
{
    destroyFrameBoxes(&qpData.boxSet);
    for (int i = 0; i < maxParamIndex; i++)
    {
        free(forcedQP[i]);
        qpData.metadata.pMovements[i].number = 0;
        qpData.metadata.pMovements[i].maxNumber = 0;
        free(qpData.metadata.pMovements[i].pBoxes);
    }
    free(forcedQP);
    free(pGopParam);
    free(qpData.metaNal.p_payload);
}
static void resetQP(x264_param_t* pParam, int defaultQP, int pts)
{
    //memset(pParam->forcedQP[pts%qpData.maxDelayedFrames], defaultQP, (pParam->i_width/16)*(pParam->i_height/16)); // kacziro - fixme
    memset(pParam->forcedQP[pts%pParam->maxBufferedFrames], defaultQP, (pParam->i_width/16)*(pParam->i_height/16));
}
static void setQPZone(x264_param_t* pParam, int beginX, int beginY, int endX, int endY, int movementQP, int pts)
{
    int yMin, yMax, xMin, xMax, w;
    if (NULL == pParam)
    {
        av_log(hd3Data.ctx, AV_LOG_WARNING, "Warning: param is NULL\n");
        return;
    }
    w = pParam->i_width/16;
    yMin = MAX(0, beginY/16 - 1)*w;
    yMax = MIN(pParam->i_height/16 - 1, endY/16 + 1)*w;
    xMin = MAX(0, beginX/16 - 1);
    xMax = MIN(w - 1, endX/16 + 1);
    {
        int8_t* pStart = pParam->forcedQP[pts%qpData.maxDelayedFrames] + xMin;
        for (int y = yMin; y < yMax; y+=w)
        {
            memset(pStart + y,
                   movementQP,
                   xMax - xMin);
        }
    }
}
static void setQP(FrameBoxes* pBoxSet, x264_param_t* pParam, int defaultQP, int movementQP, int pts)
{
    resetQP(pParam, defaultQP, pts);

    for (int i = 0; i < pBoxSet->number; i++)
    {
        setQPZone(pParam,
                  pBoxSet->boxes[i].aabb.begin.x,
                  pBoxSet->boxes[i].aabb.begin.y,
                  pBoxSet->boxes[i].aabb.end.x,
                  pBoxSet->boxes[i].aabb.end.y,
                  movementQP,
                  pts);
    }
}
static int hd3_init(AVCodecContext *ctx,
                    x264_param_t* pParams,
                    int maxDelayedFrames,
                    int xCompression,
                    int yCompression)
{
    int w = pParams->i_width;
    int maxMovementBox = (pParams->i_width*pParams->i_height)/HD3_MIN_MOVEMENT;
    hd3Data.staticFlush = 0;
    hd3Data.step = 0;

    hd3Data.globalXCompression = xCompression;
    hd3Data.globalYCompression = yCompression;
    hd3Data.width = pParams->i_width;
    hd3Data.height = pParams->i_height;

    hd3Data.default_lookahead = pParams->rc.i_lookahead = 0; // this is fix - frame assert (when flush)
    hd3Data.default_sync_lookahead = pParams->i_sync_lookahead = 0;
    hd3Data.default_keyint_max = pParams->i_keyint_max = INT_MAX;
    hd3Data.default_scenecut_threshold = pParams->i_scenecut_threshold = 0;
    hd3Data.buffor.param = NULL; //pParams;
    hd3Data.maxDelayedFrames = maxDelayedFrames;

    hd3Data.pPixOrder = (Point2u8*)malloc(hd3Data.globalXCompression*hd3Data.globalYCompression*sizeof(Point2u8));
    generatePixOrder(PIX_ORDER_AWAY, hd3Data.pPixOrder, hd3Data.globalXCompression, hd3Data.globalYCompression);

    hd3Data.boxSet = (FrameBoxes*)malloc(hd3Data.maxDelayedFrames*sizeof(FrameBoxes));
    printf("allocated boxes: %d\n", maxMovementBox);
    for (int i = 0; i < hd3Data.maxDelayedFrames; i++)
    {
        initFrameBoxes(&hd3Data.boxSet[i], maxMovementBox);
    }
    hd3Data.metaNal.p_payload = (uint8_t*)malloc(sizeof(MovementBox)*maxMovementBox + (hd3Data.width*hd3Data.height)/16); // kacziro - fixme - magic number
    memset(hd3Data.metaNal.p_payload, 0, sizeof(MovementBox)*maxMovementBox + (hd3Data.width*hd3Data.height)/16);
    hd3Data.metaNal.i_payload = sizeof(MovementBox)*maxMovementBox;
    hd3Data.metaNal.i_padding = 0;

    if (w % 64 != 0)
    {
        w += 64 - (w % 64);
    }
    x264_picture_alloc(&hd3Data.buffor,   pParams->i_csp, w, hd3Data.height);
    x264_picture_alloc(&hd3Data.outFrame, pParams->i_csp, w, hd3Data.height);
    x264_picture_alloc(&hd3Data.outPluginFrame, pParams->i_csp, w, hd3Data.height);

    hd3Data.readerData.nextPts = -1;
    hd3Data.readerData.pBoxSource = NULL;
    if (HD3_SOURCE_TYPE_FILE == hd3SourceType)
    {
        hd3Data.readerData.pBoxSource = fopen(hd3Source, "r");
        if (hd3Data.readerData.pBoxSource == NULL)
        {
            av_log(ctx, AV_LOG_ERROR, "Error: cannot open file %s\n", hd3Source);
            return 1;
        }
    }
    hd3Data.ctx = ctx;
    return 0;
}
static void hd3_destroy()
{
    for (int i = 0; i < hd3Data.maxDelayedFrames; i++)
    {
        destroyFrameBoxes(&hd3Data.boxSet[i]);
    }
    free(hd3Data.boxSet);
    free(hd3Data.pPixOrder);
    free(hd3Data.metaNal.p_payload);

    x264_picture_clean(&hd3Data.buffor);
    x264_picture_clean(&hd3Data.outFrame);
    x264_picture_clean(&hd3Data.outPluginFrame);

    if (hd3Data.readerData.pBoxSource)
    {
        fclose(hd3Data.readerData.pBoxSource);
        hd3Data.readerData.pBoxSource = NULL;
    }
}

#endif
