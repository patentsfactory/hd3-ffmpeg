#ifndef HD3_ENCODE_H
#define HD3_ENCODE_H

#include "hd3/hd3.h"
#include "hd3/pix_format_converter.h"
#include "hd3/encode_jp.h"
#include "hd3/encode_jp2.h"

#include <xmmintrin.h>
    int w = 27;
    char pIF[27];// = {0, 5, 10, 15, 20, };
    uint8_t values[14];
static AABB flatBoxes[9000];
static int absM(int i, int j)
{
    int r = rand();
    int tmp = abs(i - j - r);
    printf("%d\n", tmp);
    return tmp;
}
void tmpasm()
{
    memset(values, 0, 14);
    for (int y = 0; y < 3; y++) {
        for (int x = 0; x < 9; x++) {
            pIF[x + y*9] = y + x*5;
        }
    }
    pIF[0] = 100;
    pIF[1] = 100;
    pIF[2] = 100;

    uint8_t f1, f2;
    f2 = f1 = pIF[3];
    f2 = 10;
    printf("%d..", sizeof((uint8_t*)values));

    f1 = absM(f2, f1);
    f1 *= 2;
    f1 -= 3;
    __asm__(
        "push %%rax\n"
        "push %%rbx\n"
        "push %%rcx\n"
        "push %%rdx\n"
        "push %%rsi\n"
        "push %%rdi\n"

        "mov    %0, %%rsi\n"
        "mov    %1, %%rdx\n"
        "mov    %2, %%rdi\n"

        "sub    $3, %%rdi\n"
        "mov    $3, %%rcx\n"

    "pix:\n"
        "xor    %%ax, %%ax\n"
        "xor    %%bx, %%bx\n"
        "movb   (%%rdx), %%al\n"
        "movb   (%%rdi), %%bl\n"
        "sub    %%ax, %%bx\n"
        "mov    %%bx, %%ax\n"
        "neg    %%bx\n"
        "cmovg  %%bx,  %%ax\n"
        "movb    %%al, (%%rsi)\n"

        "inc    %%rdx\n"
        "inc    %%rdi\n"
        "inc    %%rsi\n"

        "xor    %%ax, %%ax\n"
        "xor    %%bx, %%bx\n"
        "movb   (%%rdx), %%al\n"
        "movb   (%%rdi), %%bl\n"
        "sub    %%ax, %%bx\n"
        "mov    %%bx, %%ax\n"
        "neg    %%bx\n"
        "cmovg  %%bx,  %%ax\n"  // kacziro - warning - if ax is gt 255? 0 to ah?
        "movb    %%al, (%%rsi)\n"

        "inc    %%rdx\n"
        "inc    %%rdi\n"
        "inc    %%rsi\n"

        "xor    %%ax, %%ax\n"
        "xor    %%bx, %%bx\n"
        "movb   (%%rdx), %%al\n"
        "movb   (%%rdi), %%bl\n"
        "sub    %%ax, %%bx\n"
        "mov    %%bx, %%ax\n"
        "neg    %%bx\n"
        "cmovg  %%bx,  %%ax\n"
        "movb   %%al, (%%rsi)\n"

        "inc   %%rdx\n"
        "sub   $5, %%rdi\n"
        "inc   %%rsi\n"

    "loop pix\n"

        "pop %%rdi\n"
        "pop %%rsi\n"
        "pop %%rdx\n"
        "pop %%rcx\n"
        "pop %%rbx\n"
        "pop %%rax\n"

        : //"=r" (27)//, "=r" (f2)
        : "r" ((uint8_t*)values), "r" (pIF), "r" ((uint8_t*)(pIF + 27)) // val, p - w, p + w
    );

    printf("-> %d, %d\n", values[0], f1);
}
static int hd3_encode_init(AVCodecContext *ctx,
                           x264_param_t* pParams,
                           int maxDelayedFrames,
                           int xCompression, int yCompression,
                           int xBlock, int yBlock,
                           int qp, int qpGlobal,
                           int aggregationMethod,
                           int inFormat, int outFormat)
{
    memset(&perfData, 0, sizeof(PerformanceData));
    hd3Data.stats.blockStd = malloc(sizeof(float)*hd3Data.stats.gridWidth*hd3Data.stats.gridHeight);
    hd3Data.stats.blockMean = malloc(sizeof(float)*hd3Data.stats.gridWidth*hd3Data.stats.gridHeight);

    hd3Data.stats.pDxBuff = malloc(sizeof(float)*MOVEMENT_BOX_DIM*MOVEMENT_BOX_DIM);
    hd3Data.stats.pDyBuff = malloc(sizeof(float)*MOVEMENT_BOX_DIM*MOVEMENT_BOX_DIM);

    hd3Data.allCodes.blockXSize = xBlock;
    hd3Data.allCodes.blockYSize = yBlock;

    hd3Data.allCodes.allocated = xCompression*yCompression*2; //kacziro - fixme - this is estimation... *xCompression*yCompression
    hd3Data.allCodes.cells = (CodeCell*)malloc(sizeof(CodeCell)*hd3Data.allCodes.allocated);
    hd3Data.allCodes.number = 0;
    for (int i = 1; i < xCompression*yCompression+1; i++) {
        for (int j = 1; j < xCompression*yCompression+1; j++) {
            if (i*j == xCompression*yCompression) { //kacziro - todo - if (i*j <= xCompression*yCompression) for interpolation
                hd3Data.allCodes.cells[hd3Data.allCodes.number].method = SWS_BICUBIC;
                hd3Data.allCodes.cells[hd3Data.allCodes.number].xReduction = i;
                hd3Data.allCodes.cells[hd3Data.allCodes.number].yReduction = j;
                hd3Data.allCodes.number++;
                hd3Data.allCodes.cells[hd3Data.allCodes.number].method = SWS_POINT;
                hd3Data.allCodes.cells[hd3Data.allCodes.number].xReduction = i;
                hd3Data.allCodes.cells[hd3Data.allCodes.number].yReduction = j;
                hd3Data.allCodes.number++;
            }
        }
    }
    pParams->i_csp = convert_pix_fmt(outFormat);
    int ret = hd3_init(ctx, pParams, maxDelayedFrames, xCompression, yCompression);
    hd3Data.pInputs = (FrameCopy*)malloc(xCompression*yCompression*sizeof(FrameCopy));
    hd3Data.aggregationMethod = aggregationMethod;
    for (int i = 0; i < hd3Data.globalXCompression*hd3Data.globalYCompression; i++) {
        initFrameCopy(hd3Data.ctx, &hd3Data.pInputs[i], hd3Data.width, hd3Data.height, outFormat);
    }
    hd3Data.pScaledDown = (FrameCopy*)malloc(hd3Data.allCodes.number*sizeof(FrameCopy));
    for (int i = 0; i < hd3Data.allCodes.number; i++) {
        initFrameCopy(hd3Data.ctx, &hd3Data.pScaledDown[i], hd3Data.width/hd3Data.allCodes.cells[i].xReduction, hd3Data.height/hd3Data.allCodes.cells[i].yReduction, outFormat);
    }
    initFrameCopy(hd3Data.ctx, &hd3Data.mins,       hd3Data.width, hd3Data.height, inFormat);
    initFrameCopy(hd3Data.ctx, &hd3Data.maxes,      hd3Data.width, hd3Data.height, inFormat);
    initFrameCopy(hd3Data.ctx, &hd3Data.buff,       hd3Data.width, hd3Data.height, inFormat);
    initFrameCopy(hd3Data.ctx, &hd3Data.prevFrame,  hd3Data.width, hd3Data.height, inFormat);

    initFrameCopy(hd3Data.ctx, &hd3Data.integral, hd3Data.width*8, hd3Data.height, inFormat); // for uint64_t
    hd3Data.integral.width = hd3Data.width;
    hd3Data.integral.format = X264_CSP_MAX;

    int baseSize = hd3Data.width*hd3Data.height;
    switch(inFormat) {
        case AV_PIX_FMT_YUV420P:
        {
            memset(hd3Data.mins.data[0], 255, baseSize);
            memset(hd3Data.mins.data[1], 255, baseSize/4);
            memset(hd3Data.mins.data[2], 255, baseSize/4);
            memset(hd3Data.maxes.data[0], 0, baseSize);
            memset(hd3Data.maxes.data[1], 0, baseSize/4);
            memset(hd3Data.maxes.data[2], 0, baseSize/4);
        } break;
        case AV_PIX_FMT_RGB24:
        {
            memset(hd3Data.mins.data[0], 255, baseSize*3);
            memset(hd3Data.maxes.data[0], 0, baseSize*3);
        } break;
    }
    hd3Data.qpForMovements = qp;
    hd3Data.qpGlobal = qpGlobal;
    pfQP = hd3Source;
    ret = ret || dynamicQP_init(ctx, pParams, maxDelayedFrames, xCompression, yCompression);
    return ret;
}
static void hd3_encode_destroy()
{
    av_log(hd3Data.ctx, AV_LOG_INFO, "avg update: %f\n", perfData.update.number > 0 ? perfData.update.sum/perfData.update.number : -1.0f);
    av_log(hd3Data.ctx, AV_LOG_INFO, "      avg mean: %f\n", perfData.mean.sum/perfData.mean.number);
    av_log(hd3Data.ctx, AV_LOG_INFO, "      avg std: %f\n", perfData.std.sum/perfData.std.number);
    av_log(hd3Data.ctx, AV_LOG_INFO, "    avg threshold: %f\n", perfData.threshold.sum/perfData.threshold.number);
    av_log(hd3Data.ctx, AV_LOG_INFO, "    avg box: %f\n", perfData.box.sum/perfData.box.number);
    av_log(hd3Data.ctx, AV_LOG_INFO, "      avg subedge: %f\n", perfData.subEdge.sum/perfData.subEdge.number);
    av_log(hd3Data.ctx, AV_LOG_INFO, "    avg edge: %f\n", perfData.edge.sum/perfData.edge.number);
    av_log(hd3Data.ctx, AV_LOG_INFO, "  avg gen: %f\n", perfData.gen.sum/perfData.gen.number);
    av_log(hd3Data.ctx, AV_LOG_INFO, "  avg align: %f\n", perfData.cna.sum/perfData.cna.number);
    av_log(hd3Data.ctx, AV_LOG_INFO, "  avg scaling: %f\n", perfData.scaling.sum/perfData.scaling.number);
    av_log(hd3Data.ctx, AV_LOG_INFO, "avg render: %f\n", perfData.render.sum/perfData.render.number);
    av_log(hd3Data.ctx, AV_LOG_INFO, "----------------------\n");
    av_log(hd3Data.ctx, AV_LOG_INFO, "avg all: %f\n", (perfData.render.sum/perfData.render.number) + hd3Data.globalXCompression*hd3Data.globalYCompression*(perfData.update.sum/perfData.update.number));
    for (int i = 0; i < hd3Data.globalXCompression*hd3Data.globalYCompression; i++) {
        destroyFrameCopy(&hd3Data.pInputs[i]);
    }
    for (int i = 0; i < hd3Data.allCodes.number; i++) {
        destroyFrameCopy(&hd3Data.pScaledDown[i]);
    }
    destroyFrameCopy(&hd3Data.mins);
    destroyFrameCopy(&hd3Data.maxes);
    destroyFrameCopy(&hd3Data.buff);
    destroyFrameCopy(&hd3Data.integral);
    destroyFrameCopy(&hd3Data.prevFrame);
    free(hd3Data.stats.blockMean);
    free(hd3Data.stats.blockStd);
    free(hd3Data.stats.pDxBuff);
    free(hd3Data.stats.pDyBuff);
    free(hd3Data.allCodes.cells);
    hd3Data.allCodes.allocated = hd3Data.allCodes.number = 0;
    free(hd3Data.pInputs);
    hd3_destroy();
    pfQP = NULL;
    dynamicQP_destroy();
}
static void updateStaticYUV(uint8_t* pYOut, uint8_t* pUOut, uint8_t* pVOut, uint8_t* pYIn, uint8_t* pUIn, uint8_t* pVIn,
                            int startX, int startY, int width, int height, int xStep, int yStep)
{
    int halfW;
    halfW = width >> 1;
    for (int by = startY; by < height; by+=yStep) {
        const uint8_t *pYI, *pUI, *pVI;
        pYI = pYIn + by*width;
        pUI = pUIn + (by >> 1)*halfW;
        pVI = pVIn + (by >> 1)*halfW;
        uint8_t *pYO, *pUO, *pVO;
        pYO = pYOut + by*width;
        pUO = pUOut + (by >> 1)*halfW;
        pVO = pVOut + (by >> 1)*halfW;
        for (int bx = startX; bx < width; bx+=xStep) {
            pYO[bx] = pYI[bx];
            pUO[bx >> 1] = pUI[bx >> 1];
            pVO[bx >> 1] = pVI[bx >> 1];
        }
    }
}
static void updateStaticRGB(uint8_t* pRGBOut, uint8_t* pRGBIn,
                            int startX, int startY, int width, int height, int xStep, int yStep)
{
    width *= 3;
    startX *= 3;
    xStep *= 3;

    height *= width;
    startY *= width;
    yStep *= width;
    uint8_t *pRGBO;
    const uint8_t *pRGBI;
    for (pRGBI = pRGBIn + startY, pRGBO = pRGBOut + startY; pRGBI < pRGBIn + height; pRGBI+=yStep, pRGBO+=yStep) {
        for (int bx = startX; bx < width; bx+=xStep) {
            memcpy(pRGBO + bx, pRGBI + bx, 3);
        }
    }
}
typedef struct UpdateStaticArgs
{
    uint8_t* pCh0Out;
    uint8_t* pCh1Out;
    uint8_t* pCh2Out;
    uint8_t* pCh0In;
    uint8_t* pCh1In;
    uint8_t* pCh2In;
    int startX;
    int startY;
    int width;
    int height;
    int xStep;
    int yStep;
    int csp;
} UpdateStaticArgs;
static void* threadUpdateStatic(void *arg) {
    UpdateStaticArgs* a = arg;
    switch(a->csp) {
        case X264_CSP_I420:
        {
            updateStaticYUV(a->pCh0Out, a->pCh1Out, a->pCh2Out, a->pCh0In, a->pCh1In, a->pCh2In, a->startX, a->startY, a->width, a->height, a->xStep, a->yStep);
        } break;
        case X264_CSP_BGR:
        case X264_CSP_RGB:
        {
            updateStaticRGB(a->pCh0Out, a->pCh0In, a->startX, a->startY, a->width, a->height, a->xStep, a->yStep);
        } break;
        default:
        {
            av_log(hd3Data.ctx, AV_LOG_ERROR, "Error: not implemented pixel format %d\n", a->csp);
        } break;
    }
    return NULL;
}
static void update_JM(AVFrame *frame, float *pGlobalMean, int mode)
{
    int frameSize = frame->width*frame->height;
    #define LOCAL_JM_THREADS_NB 15
    pthread_t threads[LOCAL_JM_THREADS_NB];
    MaxMinArgs args[LOCAL_JM_THREADS_NB];
    float means[LOCAL_JM_THREADS_NB];
    memset(threads, 0, sizeof(pthread_t)*LOCAL_JM_THREADS_NB);

    if (X264_CSP_RGB == hd3Data.buffor.img.i_csp ||
        X264_CSP_BGR == hd3Data.buffor.img.i_csp) {
        int byteSize = frameSize*3;
        //FrameCopy *pPrev = hd3Data.pInputs;//pCurr - 1;
        FrameCopy *pPrev = &hd3Data.prevFrame;

        for (int i = 0; i < LOCAL_JM_THREADS_NB; i++) {
            args[i].cvalue = frame->data[0] + i*byteSize/LOCAL_JM_THREADS_NB;
            args[i].pvalue = pPrev->data[0] + i*byteSize/LOCAL_JM_THREADS_NB;
            args[i].length = byteSize/LOCAL_JM_THREADS_NB; // one part of bits
            args[i].diff   = hd3Data.buff.data[0]  + i*byteSize/LOCAL_JM_THREADS_NB;
            args[i].mins   = hd3Data.mins.data[0]  + i*byteSize/LOCAL_JM_THREADS_NB;
            args[i].maxes  = hd3Data.maxes.data[0] + i*byteSize/LOCAL_JM_THREADS_NB;
            args[i].mean   = means + i;
            args[i].method = mode;
            pthread_create(threads + i, NULL, threadMaxMin, args + i);
        }

        if (hd3Data.step == 0) {
            hd3Data.prevFrame.format      = frame->format;
            hd3Data.prevFrame.pts         = frame->pts;
            hd3Data.prevFrame.width       = frame->width;
            hd3Data.prevFrame.height      = frame->height;
            hd3Data.prevFrame.linesize[0] = frame->linesize[0];
            hd3Data.prevFrame.linesize[1] = frame->linesize[1];
            hd3Data.prevFrame.linesize[2] = frame->linesize[2];
            hd3Data.prevFrame.linesize[3] = frame->linesize[3];
            memcpy(hd3Data.prevFrame.data[0], frame->data[0], byteSize);
        }
        for (int i = 0; i < LOCAL_JM_THREADS_NB; i++) {
            pthread_join(threads[i], NULL);
        }
    } else {
        av_log(hd3Data.ctx, AV_LOG_ERROR, "Error: not implemented pixel format %d\n", hd3Data.buffor.img.i_csp);
    }
}
static void saveFrame(FrameCopy *pDst, AVFrame *pFrame)
{
    int frameSize = pFrame->width*pFrame->height;
    if (AV_PIX_FMT_YUV420P == pDst->format) {
        if (AV_PIX_FMT_YUV420P == pFrame->format) {
             pDst->pts = pFrame->pts;
             pDst->width = pFrame->width;
             pDst->height = pFrame->height;
             pDst->linesize[0] = pFrame->linesize[0];
             pDst->linesize[1] = pFrame->linesize[1];
             pDst->linesize[2] = pFrame->linesize[2];
             pDst->linesize[3] = pFrame->linesize[3];
             memcpy(pDst->data[0], pFrame->data[0], frameSize);
             memcpy(pDst->data[1], pFrame->data[1], frameSize/4);
             memcpy(pDst->data[2], pFrame->data[2], frameSize/4); // kacziro - todo - foreach y
        } else if (AV_PIX_FMT_GRAY16BE == pFrame->format) {
            int threadsNb = 16;
            pDst->pts = pFrame->pts;
            if (hd3Data.bayerInverse) {
                doBayerParallel(pFrame, NULL,
                                0, pFrame->width*pFrame->height,
                                pFrame->width*(pFrame->height/threadsNb),
                                preBayerProcessingInverseThread, threadsNb);
            } else {
                doBayerParallel(pFrame, NULL,
                                0, pFrame->width*pFrame->height,
                                pFrame->width*(pFrame->height/threadsNb),
                                preBayerProcessingThread, threadsNb);
            }
            doBayerParallel(pFrame, pDst, 1, pFrame->height-1, pFrame->height/threadsNb, bayerToYuv_simple, threadsNb);
        }
    } else if (AV_PIX_FMT_RGB24 == pDst->format) {
        if (AV_PIX_FMT_RGB24 == pFrame->format) {
            pDst->pts = pFrame->pts;
            for (int iy = 0; iy < pFrame->height; iy++) {
                memcpy(pDst->data[0] + iy*pDst->linesize[0],
                       pFrame->data[0] + iy*pFrame->linesize[0],
                       pFrame->linesize[0]);
            }
        } else if (AV_PIX_FMT_GRAY16BE == pFrame->format) {
            int threadsNb = 16;
            pDst->pts = pFrame->pts;
            if (hd3Data.bayerInverse) {
                doBayerParallel(pFrame, NULL,
                                0, pFrame->width*pFrame->height,
                                pFrame->width*(pFrame->height/threadsNb),
                                preBayerProcessingInverseThread, threadsNb);
            } else {
                doBayerParallel(pFrame, NULL,
                                0, pFrame->width*pFrame->height,
                                pFrame->width*(pFrame->height/threadsNb),
                                preBayerProcessingThread, threadsNb);
            }
            doBayerParallel(pFrame, pDst, 1, pFrame->height-1, pFrame->height/threadsNb, bayerToRgb16_simple, threadsNb);
        }
    }
}
typedef struct HD3UpdateArgs {
    int sourceType;
    AVFrame *frame;
    float globalMean;
} HD3UpdateArgs;

static void updateHD3(HD3UpdateArgs *args)
{
    if (HD3_SOURCE_TYPE_GENERATE_JP == args->sourceType) {
        update_JP(args->frame, &args->globalMean);
    } else if (HD3_SOURCE_TYPE_GENERATE_JP2 == args->sourceType) {
        update_JP2(args->frame, &args->globalMean);
    } else if (HD3_SOURCE_TYPE_GENERATE_JM == args->sourceType) {
        update_JM(args->frame, &args->globalMean, 2);
    } else if (HD3_SOURCE_TYPE_GENERATE_JM2 == args->sourceType) {
        update_JM(args->frame, &args->globalMean, 2);
    }
}
static int hd3_encode_update(AVFrame *frame)
{
    clock_t updateBegin = clock();
    if (NULL != frame) {
        float globalMean, mean1, mean2;
        int frameSize = frame->width*frame->height;
        int threadsNb = 24; // kacziro - todo - to hd3 params

        FrameCopy *pCurr = hd3Data.pInputs + hd3Data.step;
        ////
        saveFrame(pCurr, frame);
        ////

        globalMean = mean1 = mean2 = 0.0f;

        HD3UpdateArgs updateArgs;
        updateArgs.globalMean = 0.0f;
        updateArgs.frame = frame;
        updateArgs.sourceType = hd3SourceType;
        pthread_t updateThread;
        pthread_create(&updateThread, NULL, updateHD3, &updateArgs);

        #define LOCAL_MAX_UPDATE_THREADS 24
        pthread_t threads[LOCAL_MAX_UPDATE_THREADS];
        UpdateStaticArgs args[LOCAL_MAX_UPDATE_THREADS];
        threadsNb = MIN(LOCAL_MAX_UPDATE_THREADS, MAX(1, threadsNb));
        memset(threads, 0, sizeof(pthread_t)* threadsNb);
        memset(args, 0, sizeof(UpdateStaticArgs)* threadsNb);
        int delta = ((pCurr->height / threadsNb) / hd3Data.globalYCompression)*hd3Data.globalYCompression; // delta % globalYCompression = 0

        for (int i = 0; i < threadsNb; i++) {
            args[i].pCh0Out = hd3Data.buffor.img.plane[0];
            args[i].pCh1Out = hd3Data.buffor.img.plane[1];
            args[i].pCh2Out = hd3Data.buffor.img.plane[2];
            args[i].pCh0In  = pCurr->data[0];
            args[i].pCh1In  = pCurr->data[1];
            args[i].pCh2In  = pCurr->data[2];
            args[i].width   = pCurr->width;
            args[i].height  = delta + hd3Data.globalYCompression*2; // for < condition
            if (i == threadsNb - 1) {
                args[i].height = pCurr->height - delta*i;
            }
            args[i].startX = hd3Data.pPixOrder[hd3Data.step].x;
            args[i].startY = hd3Data.pPixOrder[hd3Data.step].y;
            args[i].xStep = hd3Data.globalXCompression;
            args[i].yStep = hd3Data.globalYCompression;
            args[i].csp = hd3Data.buffor.img.i_csp;
            switch(args[i].csp) {
                case X264_CSP_I420:
                {
                    args[i].pCh0Out += (i*delta)    *hd3Data.buffor.img.i_stride[0];
                    args[i].pCh1Out += (i*(delta/2))*hd3Data.buffor.img.i_stride[1];
                    args[i].pCh2Out += (i*(delta/2))*hd3Data.buffor.img.i_stride[2];

                    args[i].pCh0In += (i*delta)    *pCurr->linesize[0];
                    args[i].pCh1In += (i*(delta/2))*pCurr->linesize[1];
                    args[i].pCh2In += (i*(delta/2))*pCurr->linesize[2];
                } break;
                case X264_CSP_BGR:
                case X264_CSP_RGB:
                {
                    args[i].pCh0Out += (i*delta)*hd3Data.buffor.img.i_stride[0];
                    args[i].pCh0In  += (i*delta)*pCurr->linesize[0];
                } break;
            }
            //pthread_create(threads + i, NULL, threadUpdateStatic, args + i);
            threadUpdateStatic(args + i);
        }
        for (int i = 0; i < threadsNb; i++) {
            pthread_join(threads[i], NULL);
        }
        //pthread_join(USThread, NULL);

//            if (HD3_SOURCE_TYPE_GENERATE_JP == hd3SourceType) // kacziro - fixme - scene change
//            {
//                if (hd3Data.step)
//                {
//                    globalMean += mean1 + mean2;
//
//                    globalMean /= 1.5*frame->height*frame->width;
//                    //printf("%f\n", mean);
//                    if (40 < globalMean)
//                    {
//                        // scene changed
//                    }
//                }
//            }
        if (HD3_SOURCE_TYPE_GENERATE_JP == hd3SourceType) {
            update_JP(frame, &globalMean);
        } else if (HD3_SOURCE_TYPE_GENERATE_JP2 == hd3SourceType) {
            update_JP2(frame, &globalMean);
        } else if (HD3_SOURCE_TYPE_GENERATE_JM == hd3SourceType) {
            update_JM(frame, &globalMean, 2);
        } else if (HD3_SOURCE_TYPE_GENERATE_JM2 == hd3SourceType) {
            update_JM(frame, &globalMean, 2);
        }
        pthread_join(updateThread, NULL);
        //step
        hd3Data.step++;
        if (hd3Data.step >= hd3Data.globalXCompression*hd3Data.globalYCompression) {
            hd3Data.step = 0;
            //if (0 || (0 == hd3Data.frameCountDown && 0 != hd3Data.gopSize)) { //from hd3 alg.
            if (0 == hd3Data.frameCountDown && 0 != hd3Data.gopSize) { //from hd3 alg.
                hd3Data.buffor.i_type = X264_TYPE_IDR;
                hd3Data.frameCountDown = hd3Data.gopSize;
            }
            hd3Data.frameCountDown--;

            clock_t updateEnd = clock();
            perfData.update.sum += (float)(updateEnd - updateBegin);
            perfData.update.number++;
            return 1;
        }
    }
    clock_t updateEnd = clock();
    perfData.update.sum += (float)(updateEnd - updateBegin);
    perfData.update.number++;
    return 0;
}

typedef struct BestScalingArg
{
    FrameBoxes *pSet;
    FrameCopy *pBuf;
    FrameCopy *pOrigin;
} BestScalingArg;
static void determineBestScaling(BestScalingArg *pArg)
{
    FrameCopy *pBuf = pArg->pBuf;
    FrameCopy *pOrigin = pArg->pOrigin;
    int *pBestError = (int*)malloc(pArg->pSet->number*sizeof(int));
    uint8_t *pBestCodes = (uint8_t*)malloc(pArg->pSet->number*sizeof(uint8_t));

    x264_image_t imgDst;
    x264_image_t imgSrc;

    imgDst.i_stride[0] = pBuf->linesize[0];
    imgDst.i_stride[1] = pBuf->linesize[1];
    imgDst.i_stride[2] = pBuf->linesize[2];
    imgDst.i_stride[3] = pBuf->linesize[3];
    imgDst.plane[0] = pBuf->data[0];
    imgDst.plane[1] = pBuf->data[1];
    imgDst.plane[2] = pBuf->data[2];
    imgDst.plane[3] = pBuf->data[3];

    //imgSrc.i_csp = imgDst.i_csp = pArg->pFrame->img.i_csp;
    imgSrc.i_plane = imgDst.i_plane = 3; // kacziro - fixme - implement for yuv too
    int format = hd3Data.pInputs[0].format;

    imgSrc.i_stride[0] = hd3Data.pScaledDown[0].linesize[0];
    imgSrc.i_stride[1] = hd3Data.pScaledDown[0].linesize[1];
    imgSrc.i_stride[2] = hd3Data.pScaledDown[0].linesize[2];
    imgSrc.i_stride[3] = hd3Data.pScaledDown[0].linesize[3];
    imgSrc.plane[0] = hd3Data.pScaledDown[0].data[0];
    imgSrc.plane[1] = hd3Data.pScaledDown[0].data[1];
    imgSrc.plane[2] = hd3Data.pScaledDown[0].data[2];
    imgSrc.plane[3] = hd3Data.pScaledDown[0].data[3];

    //scaling up
    scale(&imgSrc, hd3Data.pScaledDown[0].width, hd3Data.pScaledDown[0].height,
          &imgDst, pBuf->width, pBuf->height,
          format, hd3Data.allCodes.cells[0].method);

    for (int i = 0; i < pArg->pSet->number; i++) {
        pBestError[i] = 0;
        pBestCodes[i] = 0;
        for (int y = pArg->pSet->boxes[i].aabb.begin.y; y < pArg->pSet->boxes[i].aabb.end.y; y++) {
            const uint8_t *pData, *pOr;
            pData = pBuf->data[0]    + y*pBuf->linesize[0];
            pOr   = pOrigin->data[0] + y*pOrigin->linesize[0];
            for (int x = pArg->pSet->boxes[i].aabb.begin.x; x < pArg->pSet->boxes[i].aabb.end.x*3; x+=3) {
                // try max, avg, qunat etc.
                pBestError[i] += abs(pData[x] - pOr[x]) + abs(pData[x + 1] - pOr[x + 1]) + abs(pData[x + 2] - pOr[x + 2]);
            }
        }
    }

    for (int code = 1; code < hd3Data.allCodes.number; code++) {
        imgSrc.i_stride[0] = hd3Data.pScaledDown[code].linesize[0];
        imgSrc.i_stride[1] = hd3Data.pScaledDown[code].linesize[1];
        imgSrc.i_stride[2] = hd3Data.pScaledDown[code].linesize[2];
        imgSrc.i_stride[3] = hd3Data.pScaledDown[code].linesize[3];
        imgSrc.plane[0] = hd3Data.pScaledDown[code].data[0];
        imgSrc.plane[1] = hd3Data.pScaledDown[code].data[1];
        imgSrc.plane[2] = hd3Data.pScaledDown[code].data[2];
        imgSrc.plane[3] = hd3Data.pScaledDown[code].data[3];

        scale(&imgSrc, hd3Data.pScaledDown[code].width, hd3Data.pScaledDown[code].height,
              &imgDst, pBuf->width, pBuf->height,
              format, hd3Data.allCodes.cells[code].method);

        for (int i = 0; i < pArg->pSet->number; i++) {
            int error = 0;
            for (int y = pArg->pSet->boxes[i].aabb.begin.y; y < pArg->pSet->boxes[i].aabb.end.y; y++) {
                const uint8_t *pData, *pOr;
                pData = pBuf->data[0]    + y*pBuf->linesize[0];
                pOr   = pOrigin->data[0] + y*pOrigin->linesize[0];
                for (int x = pArg->pSet->boxes[i].aabb.begin.x; x < pArg->pSet->boxes[i].aabb.end.x; x++) {
                    // try max, avg, qunat etc.
                    pBestError[i] += abs(pData[x] - pOr[x]) + abs(pData[x + 1] - pOr[x + 1]) + abs(pData[x + 2] - pOr[x + 2]);
                }
            }
            if (error < pBestError[i]) {
                pBestError[i] = error;
                pBestCodes[i] = code;
            }
        }
    }
    free(pBestError);
    free(pBestCodes);
}
static void copyTo(x264_picture_t* pFrame,
                   int sbx, int sby, int sbw, int sbh,
                   int dbx, int dby, int dbw, int dbh) {
    x264_image_t imgDst;
    x264_image_t imgSrc;

    imgSrc.i_plane     = imgDst.i_plane     = pFrame->img.i_plane;
    imgSrc.plane[0] = imgDst.plane[0] = pFrame->img.plane[0];
    imgSrc.plane[1] = imgDst.plane[1] = pFrame->img.plane[1];
    imgSrc.plane[2] = imgDst.plane[2] = pFrame->img.plane[2];
    imgSrc.i_stride[0] = imgDst.i_stride[0] = pFrame->img.i_stride[0];
    imgSrc.i_stride[1] = imgDst.i_stride[1] = pFrame->img.i_stride[1];
    imgSrc.i_stride[2] = imgDst.i_stride[2] = pFrame->img.i_stride[2];
    if (pFrame->img.i_csp == X264_CSP_I420)
    {
        imgSrc.plane[0] += sby*imgSrc.i_stride[0] + sbx;
        imgSrc.plane[1] += (sby >> 1)*imgSrc.i_stride[1] + (sbx >> 1);
        imgSrc.plane[2] += (sby >> 1)*imgSrc.i_stride[2] + (sbx >> 1);

        imgDst.plane[0] += dby*imgDst.i_stride[0] + dbx;
        imgDst.plane[1] += (dby >> 1)*imgDst.i_stride[1] + (dbx >> 1);
        imgDst.plane[2] += (dby >> 1)*imgDst.i_stride[2] + (dbx >> 1);

        scale(&imgSrc, sbw, sbh,
                &imgDst, dbw, dbh,
                AV_PIX_FMT_YUV420P, SWS_BICUBIC);
    }
}
static void bbEncoding(x264_picture_t* pFrame,
                       uint8_t xReduction, uint8_t yReduction, uint8_t method,
                       int sbx, int sby, int sbw, int sbh,
                       int dbx, int dby, int dbw, int dbh)
{
    x264_image_t imgDst;
    x264_image_t imgSrc;
    imgSrc.i_csp = imgDst.i_csp = pFrame->img.i_csp;
//                for (int code = 0; code < hd3Data.allCodes.number; code++)
//                {
//                    imgSrc.i_plane = pFrame->img.i_plane;
//                    imgSrc.i_stride[0] = hd3Data.pInputs[0].linesize[0]; // kacziro - fixme - tmp 0
//                    imgSrc.i_stride[1] = hd3Data.pInputs[0].linesize[1];
//                    imgSrc.i_stride[2] = hd3Data.pInputs[0].linesize[2];
//                    imgSrc.i_stride[3] = hd3Data.pInputs[0].linesize[3];
//                    imgSrc.plane[0] = hd3Data.pInputs[0].data[0];
//                    imgSrc.plane[1] = hd3Data.pInputs[0].data[1];
//                    imgSrc.plane[2] = hd3Data.pInputs[0].data[2];
//                    imgSrc.plane[3] = hd3Data.pInputs[0].data[3];
//                    imgDst.i_plane = pFrame->img.i_plane;
//                    imgDst.i_stride[0] = hd3Data.pScaledDown[code].linesize[0];
//                    imgDst.i_stride[1] = hd3Data.pScaledDown[code].linesize[1];
//                    imgDst.i_stride[2] = hd3Data.pScaledDown[code].linesize[2];
//                    imgDst.i_stride[3] = hd3Data.pScaledDown[code].linesize[3];
//                    imgDst.plane[0] = hd3Data.pScaledDown[code].data[0];
//                    imgDst.plane[1] = hd3Data.pScaledDown[code].data[1];
//                    imgDst.plane[2] = hd3Data.pScaledDown[code].data[2];
//                    imgDst.plane[3] = hd3Data.pScaledDown[code].data[3];
//                    int cw2 = bw / hd3Data.allCodes.cells[code].xReduction;
//                    int ch2 = bh / hd3Data.allCodes.cells[code].yReduction;
//                    printf("%d, %d -> %d, %d\n", bw, bh, cw2, ch2);
//                    if (cw2 > 0 && ch2 > 0)
//                    {
//                        scale(&imgSrc, bw, bh,
//                              &imgDst, cw2, ch2, hd3Data.pInputs[0].format, hd3Data.allCodes.cells[code].method);
//                    }
//                }

    copyTo(pFrame, dbx, dby, dbw*xReduction, dbh*yReduction, sbx, sby, sbw, sbh);

    setQPZone(pFrame->param, dbx, dby, dbx+dbw, dby+dbh, hd3Data.qpForMovements, pFrame->i_pts);

    imgDst.i_plane = pFrame->img.i_plane;
    imgDst.i_stride[0] = pFrame->img.i_stride[0];
    imgDst.i_stride[1] = pFrame->img.i_stride[1];
    imgDst.i_stride[2] = pFrame->img.i_stride[2];
    int srcPlane0Off, srcPlane1Off, srcPlane2Off,
        dstPlane0Off, dstPlane1Off, dstPlane2Off;
    FrameCopy* pCurr = hd3Data.pInputs;
    if (pFrame->img.i_csp == X264_CSP_I420)
    {
        srcPlane0Off = sbx + pCurr->linesize[0]*sby; // kacziro - warning - assumption: all hd3Data.pInputs frames has the same format
        srcPlane1Off = (sbx >> 1) + pCurr->linesize[1]*(sby >> 1);
        srcPlane2Off = (sbx >> 1) + pCurr->linesize[2]*(sby >> 1);
        for (int y = dby; y < dbh*yReduction + dby; y+=dbh)
        {
            dstPlane0Off = imgDst.i_stride[0]*y;
            dstPlane1Off = imgDst.i_stride[1]*(y >> 1);
            dstPlane2Off = imgDst.i_stride[2]*(y >> 1);
            for (int x = dbx; x < dbw*xReduction + dbx; x+=dbw)
            {
                imgSrc.i_plane = pFrame->img.i_plane;
                imgSrc.i_stride[0] = pCurr->linesize[0];
                imgSrc.i_stride[1] = pCurr->linesize[1];
                imgSrc.i_stride[2] = pCurr->linesize[2];
                imgSrc.i_stride[3] = pCurr->linesize[3];
                imgSrc.plane[0] = pCurr->data[0] + srcPlane0Off;
                imgSrc.plane[1] = pCurr->data[1] + srcPlane1Off;
                imgSrc.plane[2] = pCurr->data[2] + srcPlane2Off;

                imgDst.plane[0] = pFrame->img.plane[0] + x + dstPlane0Off;
                imgDst.plane[1] = pFrame->img.plane[1] + (x >> 1) + dstPlane1Off;
                imgDst.plane[2] = pFrame->img.plane[2] + (x >> 1) + dstPlane2Off;

                scale(&imgSrc, sbw, sbh,
                        &imgDst, dbw, dbh,
                        pCurr->format, method);
                pCurr++;
            }
        }
    } else {
        if (X264_CSP_RGB == pFrame->img.i_csp || X264_CSP_BGR == pFrame->img.i_csp)
        {
            srcPlane0Off = sbx*3 + pCurr->linesize[0]*sby; // kacziro - warning - assumption: all hd3Data.pInputs frames has the same format
            for (int y = sby; y < dbh*yReduction + sby; y+=dbh)
            {
                dstPlane0Off = imgDst.i_stride[0]*y;
                for (int x = sbx; x < dbw*xReduction + sbx; x+=dbw)
                {
                    imgSrc.i_plane = pFrame->img.i_plane;
                    imgSrc.i_stride[0] = pCurr->linesize[0];
                    imgSrc.plane[0] = pCurr->data[0] + srcPlane0Off;
                    imgDst.plane[0] = pFrame->img.plane[0] + x*3 + dstPlane0Off;

                    scale(&imgSrc, sbw, sbh,
                          &imgDst, dbw, dbh,
                          pCurr->format, method);
                    pCurr++;
                }
            }
        }
        else
        {
            av_log(hd3Data.ctx, AV_LOG_ERROR, "Error: not implemented pixel format %d\n", pFrame->img.i_csp);
        }
    }
}
typedef struct BBEncodingArg
{
    x264_picture_t* pFrame;
    uint8_t xReduction;
    uint8_t yReduction;
    uint8_t method;

    int sbx;
    int sby;
    int sbw;
    int sbh;
    int dbx;
    int dby;
    int dbw;
    int dbh;
} BBEncodingArg;
static void* threadBBEncoding(void* arg)
{
    BBEncodingArg* a = arg;
    bbEncoding(a->pFrame, a->xReduction, a->yReduction, a->method,
               a->sbx, a->sby, a->sbw, a->sbh,
               a->dbx, a->dby, a->dbw, a->dbh);
    return NULL;
}

static int update_movement(AVFrame *frame)
{
    if (frame != NULL) {
        int frameSize = frame->width*frame->height;
        pthread_t diffThread1, diffThread2, diffThread3;
        float globalMean = 0.0f;
        if (AV_PIX_FMT_YUV420P == frame->format)
        {
            if (hd3Data.step > 0)
            {
                float mean1;
                int hsize = (frame->height >> 1)*(frame->width >> 1);

                MaxMinArgs arg1, arg2;
                arg1.cvalue = frame->data[0];
                arg1.pvalue = NULL;
                arg1.length = frameSize;
                arg1.diff = hd3Data.buff.data[0];
                arg1.mins = hd3Data.mins.data[0];
                arg1.maxes = hd3Data.maxes.data[0];
                arg1.mean = &mean1;
                arg1.method = 2;
                pthread_create(&diffThread1, NULL, threadMaxMin, &arg1);

                cumulateMaxMin(frame->data[1], hd3Data.buff.data[1], hd3Data.mins.data[1], hd3Data.maxes.data[1], hd3Data.buff.data[1] + hsize, &globalMean);
                cumulateMaxMin(frame->data[2], hd3Data.buff.data[2], hd3Data.mins.data[2], hd3Data.maxes.data[2], hd3Data.buff.data[2] + hsize, &globalMean);
            }
            pthread_join(diffThread1, NULL);
        } else {
            av_log(hd3Data.ctx, AV_LOG_ERROR, "Error: not implemented pixel format %d\n", hd3Data.buffor.img.i_csp);
        }
        hd3Data.step++;
        if (hd3Data.step >= hd3Data.globalXCompression*hd3Data.globalYCompression) {
            hd3Data.step = 0;
            //if (0 || (0 == hd3Data.frameCountDown && 0 != hd3Data.gopSize)) { //from hd3 alg.
            if (0 == hd3Data.frameCountDown && 0 != hd3Data.gopSize) { //from hd3 alg.
                hd3Data.buffor.i_type = X264_TYPE_IDR;
                hd3Data.frameCountDown = hd3Data.gopSize;
            }
            hd3Data.frameCountDown--;
            return 1;
        }
    }
    return 0;
}
typedef struct DiffRGBArgs
{
    uint8_t *pDiff;
    uint8_t *pMin;
    uint8_t *pMax;
    uint8_t *pFirst;
    uint8_t *pLast;
    float sum;
    float sumSq;
    float *pSum;
    float *pSumSq;
    int startY;
    int endY;
    int width;
    int blockWidth;
    int blockHeight;
    int gridWidth;
    int gridHeight;
} DiffRGBArgs;
static void generateDiffRGB4(DiffRGBArgs* args)
{
    int w = args->width * 3;
    int blockWidth = args->blockWidth;
    uint8_t values[24];
    for (int y = args->startY; y < args->endY; y++)
    {
        const uint8_t *pIF, *pIL, *pIMax, *pIMin;
        pIF = args->pFirst + y*w;
        pIL = args->pLast + y*w;
        pIMax = args->pMax + y*w;
        pIMin = args->pMin + y*w;
        uint8_t *pOut = args->pDiff + y*w;
        float *pM = args->pSum + (y/args->blockHeight)*args->gridWidth;
        float *pS = args->pSumSq  + (y/args->blockHeight)*args->gridWidth;
        memset(pOut, 0, 3);
        memset(pOut + w-3, 0, 3);
        for (int x = 3; x < w-3; x += 3)
        {
            uint8_t f1, f2;
            values[0] = abs(pIF[x]      - pIF[x - w - 3]);
            values[1] = abs(pIF[x + 1]  - pIF[x - w - 2]);
            values[2] = abs(pIF[x + 2]  - pIF[x - w - 1]);
            values[3] = abs(pIF[x]      - pIF[x - w]);
            values[4] = abs(pIF[x + 1]  - pIF[x - w + 1]);
            values[5] = abs(pIF[x + 2]  - pIF[x - w + 2]);
            values[6] = abs(pIF[x]      - pIF[x - w + 3]);
            values[7] = abs(pIF[x + 1]  - pIF[x - w + 4]);
            values[8] = abs(pIF[x + 2]  - pIF[x - w + 5]);

            values[9] = abs(pIF[x]      - pIF[x - 3]);
            values[10] = abs(pIF[x + 1] - pIF[x - 3 + 1]);
            values[11] = abs(pIF[x + 2] - pIF[x - 3 + 2]);
            values[12] = abs(pIF[x]     - pIF[x + 3]);
            values[13] = abs(pIF[x + 1] - pIF[x + 3 + 1]);
            values[14] = abs(pIF[x + 2] - pIF[x + 3 + 2]);

            values[15] = abs(pIF[x]     - pIF[x + w - 3]);
            values[16] = abs(pIF[x + 1] - pIF[x + w - 2]);
            values[17] = abs(pIF[x + 2] - pIF[x + w - 1]);
            values[18] = abs(pIF[x]     - pIF[x + w]);
            values[19] = abs(pIF[x + 1] - pIF[x + w + 1]);
            values[20] = abs(pIF[x + 2] - pIF[x + w + 2]);
            values[21] = abs(pIF[x]     - pIF[x + w + 3]);
            values[22] = abs(pIF[x + 1] - pIF[x + w + 4]);
            values[23] = abs(pIF[x + 2] - pIF[x + w + 5]);
            f1 = values[0];
            for (int i = 1; i < 24; i++)
            {
                if(f1 < values[i])
                {
                    f1 = values[i];
                }
            }
            values[0] = abs(pIL[x]      - pIL[x - w - 3]);
            values[1] = abs(pIL[x + 1]  - pIL[x - w - 2]);
            values[2] = abs(pIL[x + 2]  - pIL[x - w - 1]);
            values[3] = abs(pIL[x]      - pIL[x - w]);
            values[4] = abs(pIL[x + 1]  - pIL[x - w + 1]);
            values[5] = abs(pIL[x + 2]  - pIL[x - w + 2]);
            values[6] = abs(pIL[x]      - pIL[x - w + 3]);
            values[7] = abs(pIL[x + 1]  - pIL[x - w + 4]);
            values[8] = abs(pIL[x + 2]  - pIL[x - w + 5]);

            values[9] = abs(pIF[x]      - pIL[x - 3]);
            values[10] = abs(pIF[x + 1] - pIL[x - 3 + 1]);
            values[11] = abs(pIF[x + 2] - pIL[x - 3 + 2]);
            values[12] = abs(pIF[x]     - pIL[x + 3]);
            values[13] = abs(pIF[x + 1] - pIL[x + 3 + 1]);
            values[14] = abs(pIF[x + 2] - pIL[x + 3 + 2]);

            values[15] = abs(pIL[x]     - pIL[x + w - 3]);
            values[16] = abs(pIL[x + 1] - pIL[x + w - 2]);
            values[17] = abs(pIL[x + 2] - pIL[x + w - 1]);
            values[18] = abs(pIL[x]     - pIL[x + w]);
            values[19] = abs(pIL[x + 1] - pIL[x + w + 1]);
            values[20] = abs(pIL[x + 2] - pIL[x + w + 2]);
            values[21] = abs(pIL[x]     - pIL[x + w + 3]);
            values[22] = abs(pIL[x + 1] - pIL[x + w + 4]);
            values[23] = abs(pIL[x + 2] - pIL[x + w + 5]);
            f2 = values[0];
            for (int i = 1; i < 24; i++)
            {
                if(f2 < values[i])
                {
                    f2 = values[i];
                }
            }
            f1 = (f1 + f2)/2;

            pOut[x] = MIN(MAX(MAX(MAX(pIMax[x] - pIMin[x], pIMax[x + 1] - pIMin[x + 1]), pIMax[x + 2] - pIMin[x + 2]) - f1, 0), 255);
            args->sum += pOut[x];
            pM[(x/3)/blockWidth] += pOut[x];
            args->sumSq += pOut[x]*pOut[x];
            pS[(x/3)/blockWidth] += pOut[x]*pOut[x];
        }
    }
}
static void generateDiffRGB8(DiffRGBArgs* args)
{
    int w = args->width * 3;
    int blockWidth = args->blockWidth;
    uint8_t values[12];
    for (int y = args->startY; y < args->endY; y++)
    {
        const uint8_t *pIF, *pIL;
        uint8_t *pOut = args->pDiff + y*w;
        float *pM = args->pSum + (y/args->blockHeight)*args->gridWidth;
        float *pS = args->pSumSq  + (y/args->blockHeight)*args->gridWidth;
        pIF = args->pFirst + y*w;
        pIL = args->pLast + y*w;
        memset(pOut, 0, 3);
        memset(pOut + w-3, 0, 3);
        for (int x = 3; x < w-3; x += 3)
        {
            uint8_t f1, f2;
            values[0] = abs(pIF[x + w - 3] - pIF[x - w - 3]);
            values[1] = abs(pIF[x + w - 2] - pIF[x - w - 2]);
            values[2] = abs(pIF[x + w - 1] - pIF[x - w - 1]);
            values[3] = abs(pIF[x + w]     - pIF[x - w]);
            values[4] = abs(pIF[x + w + 1] - pIF[x - w + 1]);
            values[5] = abs(pIF[x + w + 2] - pIF[x - w + 2]);
            values[6] = abs(pIF[x + w + 3] - pIF[x - w + 3]);
            values[7] = abs(pIF[x + w + 4] - pIF[x - w + 4]);
            values[8] = abs(pIF[x + w + 5] - pIF[x - w + 5]);
            values[9]  = abs(pIF[x + 3]     - pIF[x - 3]);
            values[10] = abs(pIF[x + 3 + 1] - pIF[x - 3 + 1]);
            values[11] = abs(pIF[x + 3 + 2] - pIF[x - 3 + 2]);

            f1 = values[0];
            for (int i = 1; i < 12; i++)
            {
                if(f1 < values[i])
                {
                    f1 = values[i];
                }
            }
            values[0] = abs(pIL[x + w - 3] - pIL[x - w - 3]);
            values[1] = abs(pIL[x + w - 2] - pIL[x - w - 2]);
            values[2] = abs(pIL[x + w - 1] - pIL[x - w - 1]);
            values[3] = abs(pIL[x + w]     - pIL[x - w]);
            values[4] = abs(pIL[x + w + 1] - pIL[x - w + 1]);
            values[5] = abs(pIL[x + w + 2] - pIL[x - w + 2]);
            values[6] = abs(pIL[x + w + 3] - pIL[x - w + 3]);
            values[7] = abs(pIL[x + w + 4] - pIL[x - w + 4]);
            values[8] = abs(pIL[x + w + 5] - pIL[x - w + 5]);
            values[9]  = abs(pIL[x + 3]     - pIL[x - 3]);
            values[10] = abs(pIL[x + 3 + 1] - pIL[x - 3 + 1]);
            values[11] = abs(pIL[x + 3 + 2] - pIL[x - 3 + 2]);
            f2 = values[0];
            for (int i = 1; i < 12; i++)
            {
                if(f2 < values[i])
                {
                    f2 = values[i];
                }
            }
            f1 = (f1 + f2)/2;

            pOut[x] = pOut[x + 1] = pOut[x + 2] = MIN(MAX(MAX(MAX(pOut[x], pOut[x + 1]), pOut[x + 2]) - f1, 0), 255);
            args->sum += pOut[x];
            pM[(x/3)/blockWidth] += pOut[x];
            args->sumSq += pOut[x]*pOut[x];
            pS[(x/3)/blockWidth] += pOut[x]*pOut[x];
        }
    }
}
static void generateDiffRGB8_maxmin(DiffRGBArgs* args)
{
    int w = args->width * 3;
    int blockWidth = args->blockWidth;
    uint8_t values[12];
    for (int y = args->startY; y < args->endY; y++)
    {
        const uint8_t *pIF, *pIL, *pIMax, *pIMin;
        uint8_t *pOut = args->pDiff + y*w;
        float *pM = args->pSum + (y/args->blockHeight)*args->gridWidth;
        float *pS = args->pSumSq  + (y/args->blockHeight)*args->gridWidth;
        pIF = args->pFirst + y*w;
        pIL = args->pLast + y*w;
        pIMax = args->pMax + y*w;
        pIMin = args->pMin + y*w;
        memset(pOut, 0, 3);
        memset(pOut + w-3, 0, 3);
        for (int x = 3; x < w-3; x += 3)
        {
            uint8_t f1, f2;
            values[0] = abs(pIF[x + w - 3] - pIF[x - w - 3]);
            values[1] = abs(pIF[x + w - 2] - pIF[x - w - 2]);
            values[2] = abs(pIF[x + w - 1] - pIF[x - w - 1]);
            values[3] = abs(pIF[x + w]     - pIF[x - w]);
            values[4] = abs(pIF[x + w + 1] - pIF[x - w + 1]);
            values[5] = abs(pIF[x + w + 2] - pIF[x - w + 2]);
            values[6] = abs(pIF[x + w + 3] - pIF[x - w + 3]);
            values[7] = abs(pIF[x + w + 4] - pIF[x - w + 4]);
            values[8] = abs(pIF[x + w + 5] - pIF[x - w + 5]);
            values[9]  = abs(pIF[x + 3]     - pIF[x - 3]);
            values[10] = abs(pIF[x + 3 + 1] - pIF[x - 3 + 1]);
            values[11] = abs(pIF[x + 3 + 2] - pIF[x - 3 + 2]);

            f1 = values[0];
            for (int i = 1; i < 12; i++)
            {
                if(f1 < values[i])
                {
                    f1 = values[i];
                }
            }
            values[0] = abs(pIL[x + w - 3] - pIL[x - w - 3]);
            values[1] = abs(pIL[x + w - 2] - pIL[x - w - 2]);
            values[2] = abs(pIL[x + w - 1] - pIL[x - w - 1]);
            values[3] = abs(pIL[x + w]     - pIL[x - w]);
            values[4] = abs(pIL[x + w + 1] - pIL[x - w + 1]);
            values[5] = abs(pIL[x + w + 2] - pIL[x - w + 2]);
            values[6] = abs(pIL[x + w + 3] - pIL[x - w + 3]);
            values[7] = abs(pIL[x + w + 4] - pIL[x - w + 4]);
            values[8] = abs(pIL[x + w + 5] - pIL[x - w + 5]);
            values[9]  = abs(pIL[x + 3]     - pIL[x - 3]);
            values[10] = abs(pIL[x + 3 + 1] - pIL[x - 3 + 1]);
            values[11] = abs(pIL[x + 3 + 2] - pIL[x - 3 + 2]);
            f2 = values[0];
            for (int i = 1; i < 12; i++)
            {
                if(f2 < values[i])
                {
                    f2 = values[i];
                }
            }
            f1 = (f1 + f2)/2;

            pOut[x] = MIN(MAX(MAX(MAX(pIMax[x] - pIMin[x], pIMax[x + 1] - pIMin[x + 1]), pIMax[x + 2] - pIMin[x + 2]) - f1, 0), 255);
            args->sum += pOut[x];
            pM[(x/3)/blockWidth] += pOut[x];
            args->sumSq += pOut[x]*pOut[x];
            pS[(x/3)/blockWidth] += pOut[x]*pOut[x];
        }
    }
}
static void generateDiffRGB8_withoutStats(DiffRGBArgs* args)
{
    #define LOCAL_VALUES_SIZE 11
    int w = args->width * 3;
    uint8_t values[LOCAL_VALUES_SIZE];
    uint8_t* pEndValues = values + LOCAL_VALUES_SIZE;

    uint8_t *pIF, *pIL, *pEnd, *pOut;
    pIF = args->pFirst + args->startY*w;
    pIL = args->pLast + args->startY*w;
    pOut = args->pDiff + args->startY*w;
    pEnd = args->pLast + args->endY*w;
    for (; pIL < pEnd; pIF += w, pIL += w, pOut += w)
    {
        memset(pOut, 0, 3);
        memset(pOut + w - 3, 0, 3);
        for (int x = 0; x < w - 6; x += 3)
        {
            int xpw, xmw;
            uint8_t f1, f2;
            xpw = x + w;
            xmw = x - w + 6;

            f1         = abs(pIF[xpw++] - pIF[xmw++]);
            values[0]  = abs(pIF[xpw++] - pIF[xmw++]);
            values[1]  = abs(pIF[xpw++] - pIF[xmw]);
            xmw -= 5;
            values[2]  = abs(pIF[xpw++] - pIF[xmw++]);
            values[3]  = abs(pIF[xpw++] - pIF[xmw++]);
            values[4]  = abs(pIF[xpw++] - pIF[xmw]);
            xmw -= 5;
            values[5]  = abs(pIF[xpw++] - pIF[xmw++]);
            values[6]  = abs(pIF[xpw++] - pIF[xmw++]);
            values[7]  = abs(pIF[xpw]   - pIF[xmw]);

//            __asm__( // kacziro - start here - todo: test (values2 to comp.), max, pIL..
//                "push %%rax\n"
//                "push %%rbx\n"
//                "push %%rcx\n"
//                "push %%rdx\n"
//                "push %%rsi\n"
//                "push %%rdi\n"
//
//                "mov    %0, %%rsi\n"
//                "mov    %1, %%rdx\n"
//                "mov    %2, %%rdi\n"
//                "sub    $3, %%rdi\n"
//                "mov    $3, %%rcx\n"
//
//            "pixIF:\n"
//                "xor    %%ax, %%ax\n"
//                "xor    %%bx, %%bx\n"
//                "movb   (%%rdx), %%al\n"
//                "movb   (%%rdi), %%bl\n"
//                "sub    %%ax, %%bx\n"
//                "mov    %%bx, %%ax\n"
//                "neg    %%bx\n"
//                "cmovg  %%bx,  %%ax\n"
//                "movb    %%al, (%%rsi)\n"
//
//                "inc    %%rdx\n"
//                "inc    %%rdi\n"
//                "inc    %%rsi\n"
//
//                "xor    %%ax, %%ax\n"
//                "xor    %%bx, %%bx\n"
//                "movb   (%%rdx), %%al\n"
//                "movb   (%%rdi), %%bl\n"
//                "sub    %%ax, %%bx\n"
//                "mov    %%bx, %%ax\n"
//                "neg    %%bx\n"
//                "cmovg  %%bx,  %%ax\n"  // kacziro - warning - if ax is gt 255? 0 to ah?
//                "movb    %%al, (%%rsi)\n"
//
//                "inc    %%rdx\n"
//                "inc    %%rdi\n"
//                "inc    %%rsi\n"
//
//                "xor    %%ax, %%ax\n"
//                "xor    %%bx, %%bx\n"
//                "movb   (%%rdx), %%al\n"
//                "movb   (%%rdi), %%bl\n"
//                "sub    %%ax, %%bx\n"
//                "mov    %%bx, %%ax\n"
//                "neg    %%bx\n"
//                "cmovg  %%bx,  %%ax\n"
//                "movb   %%al, (%%rsi)\n"
//
//                "inc   %%rdx\n"
//                "sub   $5, %%rdi\n"
//                "inc   %%rsi\n"
//
//                "loop pixIF\n"
//
//                "pop %%rdi\n"
//                "pop %%rsi\n"
//                "pop %%rdx\n"
//                "pop %%rcx\n"
//                "pop %%rbx\n"
//                "pop %%rax\n"
//
//                : //"=r" (27)//, "=r" (f2)
//                : "r" ((uint8_t*)values), "r" (pIF - w + x), "r" ((uint8_t*)(pIF + w + x + 9)) // val, p - w, p + w
//            );
            values[8]  = abs(pIF[x + 6] - pIF[x]);
            values[9]  = abs(pIF[x + 7] - pIF[x + 1]);
            values[10] = abs(pIF[x + 8] - pIF[x + 2]);
            for (uint8_t i = 0; i < LOCAL_VALUES_SIZE; i++)
            {
                if(f1 < values[i])
                {
                    f1 = values[i];
                }
            }
            xpw -= 2;
            f2         = abs(pIL[xpw++] - pIL[xmw++]);
            values[0]  = abs(pIL[xpw++] - pIL[xmw++]);
            values[1]  = abs(pIL[xpw]   - pIL[xmw++]);
            xpw -= 5;
            values[2]  = abs(pIL[xpw++] - pIL[xmw++]);
            values[3]  = abs(pIL[xpw++] - pIL[xmw++]);
            values[4]  = abs(pIL[xpw]   - pIL[xmw++]);
            xpw -= 5;
            values[5]  = abs(pIL[xpw++] - pIL[xmw++]);
            values[6]  = abs(pIL[xpw++] - pIL[xmw++]);
            values[7]  = abs(pIL[xpw]   - pIL[xmw]);
            values[8]  = abs(pIL[x + 6] - pIL[x]);
            values[9]  = abs(pIL[x + 7] - pIL[x + 1]);
            values[10] = abs(pIL[x + 8] - pIL[x + 2]);

            for (uint8_t i = 0; i < LOCAL_VALUES_SIZE; i++)
            {
                if(f1 < values[i])
                {
                    f1 = values[i];
                }
            }
            f1 = (f1 + f2)/2;
            memset(pOut + x + 3, MIN(MAX(MAX(MAX(pOut[x + 3], pOut[x + 4]), pOut[x + 5]) - f1, 0), 255), 3);
        }
    }
}
static void generateDiffRGB8_withoutStats_b(DiffRGBArgs* args)
{
    #define LOCAL_VALUES_SIZE 11
    int w = args->width * 3;
    uint8_t values[LOCAL_VALUES_SIZE];
    uint8_t* pEndValues = values + LOCAL_VALUES_SIZE;

    uint8_t *pIF, *pIL, *pEnd, *pOut;
    pIF = args->pFirst + args->startY*w;
    pIL = args->pLast + args->startY*w;
    pOut = args->pDiff + args->startY*w;
    pEnd = args->pLast + args->endY*w;
    for (; pIL < pEnd; pIF += w, pIL += w, pOut += w)
    {
        memset(pOut, 0, 3);
        memset(pOut + w-3, 0, 3);
        for (int x = 0; x < w-6; x += 3)
        {
            int xpw, xmw;
            uint8_t f1, f2;
            xpw = x + w;
            xmw = x - w;

            f1         = abs(pIF[xpw++] - pIF[xmw++]);
            values[0]  = abs(pIF[xpw++] - pIF[xmw++]);
            values[1]  = abs(pIF[xpw++] - pIF[xmw++]);
            values[2]  = abs(pIF[xpw++] - pIF[xmw++]);
            values[3]  = abs(pIF[xpw++] - pIF[xmw++]);
            values[4]  = abs(pIF[xpw++] - pIF[xmw++]);
            values[5]  = abs(pIF[xpw++] - pIF[xmw++]);
            values[6]  = abs(pIF[xpw++] - pIF[xmw++]);
            values[7]  = abs(pIF[xpw]   - pIF[xmw]);
            values[8]  = abs(pIF[x + 6] - pIF[x]);
            values[9]  = abs(pIF[x + 7] - pIF[x + 1]);
            values[10] = abs(pIF[x + 8] - pIF[x + 2]);

            for (uint8_t i = 0; i < LOCAL_VALUES_SIZE; i++)
            {
                if(f1 < values[i])
                {
                    f1 = values[i];
                }
            }
            f2         = abs(pIL[xpw--] - pIL[xmw--]);
            values[0]  = abs(pIL[xpw--] - pIL[xmw--]);
            values[1]  = abs(pIL[xpw--] - pIL[xmw--]);
            values[2]  = abs(pIL[xpw--] - pIL[xmw--]);
            values[3]  = abs(pIL[xpw--] - pIL[xmw--]);
            values[4]  = abs(pIL[xpw--] - pIL[xmw--]);
            values[5]  = abs(pIL[xpw--] - pIL[xmw--]);
            values[6]  = abs(pIL[xpw--] - pIL[xmw--]);
            values[7]  = abs(pIL[xpw]   - pIL[xmw]);
            values[8]  = abs(pIL[x + 6] - pIL[x]);
            values[9]  = abs(pIL[x + 7] - pIL[x + 1]);
            values[10] = abs(pIL[x + 8] - pIL[x + 2]);

            for (uint8_t i = 0; i < LOCAL_VALUES_SIZE; i++)
            {
                if(f1 < values[i])
                {
                    f1 = values[i];
                }
            }
            f1 = (f1 + f2)/2;
            memset(pOut + x + 3, MIN(MAX(MAX(MAX(pOut[x + 3], pOut[x + 4]), pOut[x + 5]) - f1, 0), 255), 3);
        }
    }
}

static void removeEdgesRGB(DiffRGBArgs* args)
{
    int w = args->width * 3;
    for (int y = args->startY; y < args->endY; y++)
    {
        const uint8_t *pIF, *pIL;
        uint8_t *pOut = args->pDiff + y*w;
        float *pM = args->pSum + (y/args->blockHeight)*args->gridWidth;
        float *pS = args->pSumSq  + (y/args->blockHeight)*args->gridWidth;
        pIF = args->pFirst + y*w;
        pIL = args->pLast + y*w;
        memset(pOut, 0, 3);
        memset(pOut + w-3, 0, 3);
        for (int x = 3; x < w-3; x += 3)
        {
            uint8_t a1 = MAX(MAX(abs(pIF[x] - pIF[x - w]), abs(pIF[x + 1] - pIF[x - w + 1])), abs(pIF[x + 2] - pIF[x - w + 2]));
            uint8_t a2 = MAX(MAX(abs(pIF[x] - pIF[x + w]), abs(pIF[x + 1] - pIF[x + w + 1])), abs(pIF[x + 2] - pIF[x + w + 2]));
            uint8_t a3 = MAX(MAX(abs(pIF[x] - pIF[x - 3]), abs(pIF[x + 1] - pIF[x - 3 + 1])), abs(pIF[x + 2] - pIF[x - 3 + 2]));
            uint8_t a4 = MAX(MAX(abs(pIF[x] - pIF[x + 3]), abs(pIF[x + 1] - pIF[x + 3 + 1])), abs(pIF[x + 2] - pIF[x + 3 + 2]));
            int f = MAX(MAX(a1, a2), MAX(a3, a4));
            a1 = MAX(MAX(abs(pIL[x] - pIL[x - w]), abs(pIL[x + 3] - pIL[x - w + 1])), abs(pIL[x + 2] - pIL[x - w + 2]));
            a2 = MAX(MAX(abs(pIL[x] - pIL[x + w]), abs(pIL[x + 3] - pIL[x + w + 1])), abs(pIL[x + 2] - pIL[x + w + 2]));
            a3 = MAX(MAX(abs(pIL[x] - pIL[x - 3]), abs(pIL[x + 3] - pIL[x - 3 + 1])), abs(pIL[x + 2] - pIL[x - 3 + 2]));
            a4 = MAX(MAX(abs(pIL[x] - pIL[x + 3]), abs(pIL[x + 3] - pIL[x + 3 + 1])), abs(pIL[x + 2] - pIL[x + 3 + 2]));
            f = (f + MAX(MAX(a1, a2), MAX(a3, a4)))/2;

            pOut[x] = MIN(MAX(MAX(MAX(pOut[x], pOut[x + 1]), pOut[x + 2]) - f, 0), 255);
            pOut[x + 1] = 0;
            pOut[x + 2] = 0;// only for debugging
            args->sum += pOut[x];
            pM[(x/3)/args->blockWidth] += pOut[x];
            args->sumSq += pOut[x]*pOut[x];
            pS[(x/3)/args->blockWidth] += pOut[x]*pOut[x];
        }
    }
}
static void generateMask(FrameCopy *pOutMask, FrameCopy *pDiff,
                            FrameCopy *pInMin, FrameCopy* pInMax,
                            FrameCopy *pInFirst, FrameCopy *pInLast,
                            float *pGlobalMean, float *pMean,
                            float *pGlobalStd, float *pStd,
                            int blockWidth, int blockHeight, int gridWidth, int gridHeight,
                            int method)
{
    int w = pDiff->width*3;
    memset(pDiff->data[0], 0, w);
    memset(pDiff->data[0] + w*(pDiff->height-1), 0, w);

    #define LOCAL_EDGE_THREADS 15
    pthread_t threads[LOCAL_EDGE_THREADS]; // kacziro - fixme - magic number gridHeight % ex. 4 == 0
    DiffRGBArgs args[LOCAL_EDGE_THREADS];

    for (int i = 0; i < LOCAL_EDGE_THREADS; i++)
    {
        args[i].pDiff = pDiff->data[0];
        args[i].pMin = pInMin->data[0];
        args[i].pMax = pInMax->data[0];
        args[i].pFirst = pInFirst->data[0];
        args[i].pLast = pInLast->data[0];
        args[i].sum = 0.0f;
        args[i].sumSq = 0.0f;
        args[i].pSum = pMean;
        args[i].pSumSq = pStd;
        args[i].startY = MAX(1, i*pDiff->height/LOCAL_EDGE_THREADS);
        args[i].endY = MIN(pDiff->height - 1, (i+1)*pDiff->height/LOCAL_EDGE_THREADS);
        args[i].width = pDiff->width;
        args[i].blockWidth = blockWidth;
        args[i].blockHeight = blockHeight;
        args[i].gridWidth = gridWidth;
        args[i].gridHeight = gridHeight;
        if (method)
            pthread_create(threads + i, NULL, generateDiffRGB8, args + i);
        else
            pthread_create(threads + i, NULL, removeEdgesRGB, args + i);
    }
    for (int i = 0; i < LOCAL_EDGE_THREADS; i++)
    {
        pthread_join(threads[i], NULL);
        *pGlobalMean += args[i].sum;
        *pGlobalStd += args[i].sumSq;
    }
    *pGlobalMean /= (pDiff->width-2)*(pDiff->height-2);
    *pGlobalStd = sqrt(*pGlobalStd/((pDiff->width-2)*(pDiff->height-2)) - (*pGlobalMean) * (*pGlobalMean));
    {
        float* pMEnd = pMean + gridWidth*gridHeight;
        float* pSEnd = pStd + gridWidth*gridHeight;
        float mod = 1.0f/(blockWidth*blockHeight);
        __m128 _mod = _mm_load1_ps(&mod);
        for (; pMean < pMEnd; pMean += 4, pStd += 4)
        {
            _mm_store_ps(pMean, _mm_mul_ps(_mod, _mm_load_ps(pMean)));
            _mm_store_ps(pStd , _mm_mul_ps(_mod, _mm_load_ps(pStd)));

            pStd[0] = pStd[0] - pMean[0]*pMean[0];
            pStd[1] = pStd[1] - pMean[1]*pMean[1];
            pStd[2] = pStd[2] - pMean[2]*pMean[2];
            pStd[3] = pStd[3] - pMean[3]*pMean[3];
        }
        for (int i = 0; i < (gridWidth*gridHeight)%4; i++)
        {
            pMEnd[-1 - i] *= mod;
            pSEnd[-1 - i] = pSEnd[-1 - i] - pMEnd[-1 - i]*pMEnd[-1 - i];
        }
    }
}
static void generateMaskRGB_bicubic(FrameCopy *pDiff,
                                  FrameCopy *pInMin, FrameCopy* pInMax,
                                  FrameCopy *pInFirst, FrameCopy *pInLast,
                                  float *pGlobalMean, float *pMean,
                                  float *pGlobalStd, float *pStd,
                                  int blockWidth, int blockHeight, int gridWidth, int gridHeight)
{
    int w = pDiff->width*3;
    memset(pDiff->data[0], 0, w);
    memset(pDiff->data[0] + w*(pDiff->height-1), 0, w);

    #define LOCAL_EDGE_THREADS 15
    pthread_t threads[LOCAL_EDGE_THREADS]; // kacziro - fixme - magic number gridHeight % ex. 4 == 0
    DiffRGBArgs args[LOCAL_EDGE_THREADS];
    *pGlobalMean = 0;
    *pGlobalStd = 0;
    for (int i = 0; i < LOCAL_EDGE_THREADS; i++)
    {
        args[i].pDiff = pDiff->data[0];
        args[i].pMin = pInMin->data[0];
        args[i].pMax = pInMax->data[0];
        args[i].pFirst = pInFirst->data[0];
        args[i].pLast = pInLast->data[0];
        args[i].sum = 0.0f;
        args[i].sumSq = 0.0f;
        args[i].pSum = pMean;
        args[i].pSumSq = pStd;
        args[i].startY = MAX(1, i*pDiff->height/LOCAL_EDGE_THREADS);
        args[i].endY = MIN(pDiff->height - 1, (i+1)*pDiff->height/LOCAL_EDGE_THREADS);
        args[i].width = pDiff->width;
        args[i].blockWidth = blockWidth;
        args[i].blockHeight = blockHeight;
        args[i].gridWidth = gridWidth;
        args[i].gridHeight = gridHeight;
        pthread_create(threads + i, NULL, generateDiffRGB8_withoutStats, args + i);
    }
    for (int i = 0; i < LOCAL_EDGE_THREADS; i++)
    {
        pthread_join(threads[i], NULL);
    }
    {
        x264_image_t src, dst;
        int cw, ch, dv;
        memset(&src, 0, sizeof(x264_image_t));
        memset(&dst, 0, sizeof(x264_image_t));
        src.i_csp = dst.i_csp = pDiff->format;
        src.i_plane = dst.i_plane = 1;
        src.i_stride[0] = pDiff->linesize[0];
        src.plane[0] = pDiff->data[0];
        dst.i_stride[0] = hd3Data.pScaledDown[4].linesize[0];
        dst.plane[0] = hd3Data.pScaledDown[4].data[0];
        cw = hd3Data.pScaledDown[4].width;
        ch = hd3Data.pScaledDown[4].height;
        scale(&src, pDiff->width, pDiff->height,
              &dst, cw, ch, hd3Data.pInputs->format, SWS_BICUBIC);

        ScaleArg a;
        a.dstH = pDiff->height;
        a.dstW = pDiff->width;
        a.format = hd3Data.pInputs->format;
        a.imgDst = src;
        a.imgSrc = dst;
        a.srcH = ch;
        a.srcW = cw;
        pthread_t upScaleing;
        pthread_create(&upScaleing, NULL, scaleThread, &a);

        blockWidth  = cw / gridWidth;
        blockHeight = ch / gridHeight;
        dv = blockWidth*3;
        for (int y = 0; y < ch; y++)
        {
            uint8_t *pData = dst.plane[0] + y*dst.i_stride[0];
            float *pM = pMean + (y/blockHeight)*gridWidth;
            float *pS = pStd + (y/blockHeight)*gridWidth;
            for (int x = 0; x < cw*3; x+=3)
            {
                float d = pData[x];
                *pGlobalMean += d;
                pM[x/dv] += d;
                d *= d;
                *pGlobalStd += d;
                pS[x/dv] += d;
            }
        }
        *pGlobalMean /= cw*ch;
        *pGlobalStd = sqrt(*pGlobalStd/(cw*ch) - (*pGlobalMean) * (*pGlobalMean));

        {
            float* pMEnd = pMean + gridWidth*gridHeight;
            float* pSEnd = pStd + gridWidth*gridHeight;
            float mod = 1.0f/(blockWidth*blockHeight);
            __m128 _mod = _mm_load1_ps(&mod);
            for (; pMean < pMEnd; pMean += 4, pStd += 4)
            {
                _mm_store_ps(pMean, _mm_mul_ps(_mod, _mm_load_ps(pMean)));
                _mm_store_ps(pStd , _mm_mul_ps(_mod, _mm_load_ps(pStd)));

                pStd[0] = pStd[0] - pMean[0]*pMean[0];
                pStd[1] = pStd[1] - pMean[1]*pMean[1];
                pStd[2] = pStd[2] - pMean[2]*pMean[2];
                pStd[3] = pStd[3] - pMean[3]*pMean[3];
            }
            for (int i = 0; i < (gridWidth*gridHeight)%4; i++)
            {
                pMEnd[-1 - i] *= mod;
                pSEnd[-1 - i] = pSEnd[-1 - i] - pMEnd[-1 - i]*pMEnd[-1 - i];
            }
        }
        pthread_join(upScaleing, NULL);
    }
}

static void generateBoxes_JM2(FrameBoxes *pCurrentSet, x264_picture_t *pFrame)
{
    clock_t edgeBegin, boxBegin, meanBegin, genEnd;
    clock_t genBegin = clock();
    float globalStd, globalMean;
    int baseSize, minMovementBox, gridSize, statBlockWidth, statBlockHeight;
    baseSize = hd3Data.width*hd3Data.height;
    gridSize = hd3Data.stats.gridWidth*hd3Data.stats.gridHeight;
    minMovementBox = MOVEMENT_BOX_DIM; //kacziro - important - denominator width and height - start here !

    statBlockWidth = hd3Data.width/hd3Data.stats.gridWidth;
    statBlockHeight = hd3Data.height/hd3Data.stats.gridHeight;

    memset(hd3Data.stats.blockMean, 0, sizeof(float)*gridSize);
    memset(hd3Data.stats.blockStd, 0, sizeof(float)*gridSize);

    pCurrentSet->table.number = 2; // kacziro - fixme - generate this
    pCurrentSet->table.cells[0].method = SWS_BICUBIC;
    pCurrentSet->table.cells[0].xReduction = hd3Data.globalXCompression;
    pCurrentSet->table.cells[0].yReduction = hd3Data.globalYCompression;
    pCurrentSet->table.cells[1].method = SWS_POINT;
    pCurrentSet->table.cells[1].xReduction = hd3Data.globalXCompression;
    pCurrentSet->table.cells[1].yReduction = hd3Data.globalYCompression;

    generateMask(&hd3Data.buff, &hd3Data.buff, &hd3Data.mins, &hd3Data.maxes,
                hd3Data.pInputs, hd3Data.pInputs + hd3Data.globalXCompression*hd3Data.globalYCompression - 1,
                &globalMean, hd3Data.stats.blockMean,
                &globalStd, hd3Data.stats.blockStd,
                statBlockWidth, statBlockHeight, hd3Data.stats.gridWidth, hd3Data.stats.gridHeight, 0);

    {
        meanBegin = clock();
        perfData.mean.sum += (float)(meanBegin - genBegin);
        perfData.mean.number++;
    }

    hd3Data.stats.threshold = calculateThreasholdGray(hd3Data.stats.blockMean, hd3Data.stats.blockStd, gridSize, 1.0f/(statBlockWidth*statBlockHeight),
                                                      hd3Data.stats.minGroup, hd3Data.stats.maxGroup, globalStd, globalMean, hd3Data.stats.threshold);


    {
        boxBegin = clock();
        perfData.std.sum += (float)(boxBegin - meanBegin);
        perfData.std.number++;
        perfData.threshold.sum += (float)(boxBegin - genBegin);
        perfData.threshold.number++;
    }

    detectBBGray8(pCurrentSet, &hd3Data.buff, &pFrame->img, minMovementBox, hd3Data.stats.threshold, hd3Data.stats.minObjSize);
    memset(hd3Data.buff.data[0], 0, baseSize*3);

    //remove edges
    {
        edgeBegin = clock();
        perfData.box.sum += (float)(edgeBegin - boxBegin);
        perfData.box.number++;
    }
    {
        int newNum = 0;
        MovementBox *pCurr, *pEnd;
        pEnd = pCurrentSet->boxes + pCurrentSet->number;
        for (pCurr = pCurrentSet->boxes; pCurr < pEnd; pCurr++)
        {
            if (pCurr->aabb.begin.x != pCurr->aabb.end.x)
            {
                pCurrentSet->boxes[newNum] = *pCurr;
                newNum++;
            }
        }
        pCurrentSet->number = newNum;
        aggregation_closedMax(pCurrentSet, hd3Data.stats.bbMarginX, hd3Data.stats.bbMarginY);
    }

    genEnd = clock();
    perfData.edge.sum += (float)(genEnd - edgeBegin);
    perfData.edge.number++;
    perfData.gen.sum += (float)(genEnd - genBegin);
    perfData.gen.number++;
}

static void generateBoxes_JM(FrameBoxes *pCurrentSet, x264_picture_t *pFrame)
{
    clock_t edgeBegin, boxBegin, meanBegin, genEnd;
    clock_t genBegin = clock();
    float globalStd, globalMean;
    int baseSize, minMovementBox, gridSize, statBlockWidth, statBlockHeight;
    baseSize = hd3Data.width*hd3Data.height;
    gridSize = hd3Data.stats.gridWidth*hd3Data.stats.gridHeight;
    minMovementBox = MOVEMENT_BOX_DIM; //kacziro - important - denominator width and height - start here !

    statBlockWidth = hd3Data.width/hd3Data.stats.gridWidth;
    statBlockHeight = hd3Data.height/hd3Data.stats.gridHeight;

    memset(hd3Data.stats.blockMean, 0, sizeof(float)*gridSize);
    memset(hd3Data.stats.blockStd,  0, sizeof(float)*gridSize);

    pCurrentSet->table.number = 2; // kacziro - fixme - generate this
    pCurrentSet->table.cells[0].method = SWS_BICUBIC;
    pCurrentSet->table.cells[0].xReduction = hd3Data.globalXCompression;
    pCurrentSet->table.cells[0].yReduction = hd3Data.globalYCompression;
    pCurrentSet->table.cells[1].method = SWS_POINT;
    pCurrentSet->table.cells[1].xReduction = hd3Data.globalXCompression;
    pCurrentSet->table.cells[1].yReduction = hd3Data.globalYCompression;

    generateMaskRGB_bicubic(&hd3Data.buff, &hd3Data.mins, &hd3Data.maxes,
                              hd3Data.pInputs, hd3Data.pInputs + hd3Data.globalXCompression*hd3Data.globalYCompression - 1,
                              &globalMean, hd3Data.stats.blockMean,
                              &globalStd, hd3Data.stats.blockStd,
                              statBlockWidth, statBlockHeight, hd3Data.stats.gridWidth, hd3Data.stats.gridHeight);

    {
        meanBegin = clock();
        perfData.mean.sum += (float)(meanBegin - genBegin);
        perfData.mean.number++;
    }

    hd3Data.stats.threshold = calculateThreasholdForCumulation(hd3Data.stats.blockMean, hd3Data.stats.blockStd, gridSize, 1.0f/(statBlockWidth*statBlockHeight),
                                                               hd3Data.stats.minGroup, hd3Data.stats.maxGroup, globalStd, globalMean);

    {
        boxBegin = clock();
        perfData.std.sum += (float)(boxBegin - meanBegin);
        perfData.std.number++;
        perfData.threshold.sum += (float)(boxBegin - genBegin);
        perfData.threshold.number++;
    }

    detectBBGray8(pCurrentSet, &hd3Data.buff, &pFrame->img, minMovementBox, hd3Data.stats.threshold, hd3Data.stats.minObjSize);
    memset(hd3Data.buff.data[0], 0, 3*baseSize);
    //remove edges
    {
        edgeBegin = clock();
        perfData.box.sum += (float)(edgeBegin - boxBegin);
        perfData.box.number++;
    }
    {
        aggregation_closedMax(pCurrentSet, hd3Data.stats.bbMarginX, hd3Data.stats.bbMarginY);
    }

    {
        genEnd = clock();
        perfData.edge.sum += (float)(genEnd - edgeBegin);
        perfData.edge.number++;
        perfData.gen.sum += (float)(genEnd - genBegin);
        perfData.gen.number++;
    }
}
typedef struct DitheringFlushArgs {
    x264_picture_t *pFrame;
    x264_picture_t *pBuffor;
    FrameCopy *mins;
    FrameCopy *maxes;
} DitheringFlushArgs;
static void ditheringFlush(DitheringFlushArgs *args) {
    int baseSize = args->mins->width*args->mins->height;
    x264_picture_t *pFrame = args->pFrame;
    x264_picture_t *pBuffor = args->pBuffor;
    if (X264_CSP_I420 == pFrame->img.i_csp) {
        for (int iy = 0; iy < hd3Data.height; iy+=2) {
            memcpy(pFrame->img.plane[0] +        iy*pFrame->img.i_stride[0], pBuffor->img.plane[0] +        iy*pBuffor->img.i_stride[0], pBuffor->img.i_stride[0]);
            memcpy(pFrame->img.plane[0] + (iy + 1) *pFrame->img.i_stride[0], pBuffor->img.plane[0] + (iy + 1) *pBuffor->img.i_stride[0], pBuffor->img.i_stride[0]);
            memcpy(pFrame->img.plane[1] + (iy >> 1)*pFrame->img.i_stride[1], pBuffor->img.plane[1] + (iy >> 1)*pBuffor->img.i_stride[1], pBuffor->img.i_stride[1]);
            memcpy(pFrame->img.plane[2] + (iy >> 1)*pFrame->img.i_stride[2], pBuffor->img.plane[2] + (iy >> 1)*pBuffor->img.i_stride[2], pBuffor->img.i_stride[2]);
        }
    } else if (X264_CSP_RGB == pFrame->img.i_csp || X264_CSP_BGR == pFrame->img.i_csp) {
        for (int iy = 0; iy < hd3Data.height; iy++) {
            memcpy(pFrame->img.plane[0] + iy*pFrame->img.i_stride[0],
                   pBuffor->img.plane[0] + iy*pBuffor->img.i_stride[0],
                   pBuffor->img.i_stride[0]);

        }
    } else {
        av_log(hd3Data.ctx, AV_LOG_ERROR, "Error: not implemented pixel format %d\n", pBuffor->img.i_csp);
    }
    switch(args->mins->format) {
        case AV_PIX_FMT_YUV420P:
        {
            memset(args->mins->data[0], 255, baseSize);
            memset(args->mins->data[1], 255, baseSize/4);
            memset(args->mins->data[2], 255, baseSize/4);
            memset(args->maxes->data[0], 0, baseSize);
            memset(args->maxes->data[1], 0, baseSize/4);
            memset(args->maxes->data[2], 0, baseSize/4);
        } break;
        case AV_PIX_FMT_RGB24:
        {
            memset(args->mins->data[0], 255, baseSize*3);
            memset(args->maxes->data[0], 0, baseSize*3);
        } break;
    }
}
static int hd3_encode_render(x264_picture_t *pFrame)
{
    clock_t renderBegin = clock();
    if (NULL != pFrame)
    {
        int baseSize, setIndex;
        baseSize = hd3Data.width*hd3Data.height;

        pFrame->i_pts = hd3Data.buffor.i_pts;
        pFrame->i_type = hd3Data.buffor.i_type;
        pFrame->img.i_csp = hd3Data.buffor.img.i_csp;
        setIndex = pFrame->i_pts%hd3Data.maxDelayedFrames;
        pFrame->img = hd3Data.outFrame.img;
        if (hd3Data.plugin.enabled) {
            if (X264_CSP_I420 == hd3Data.outPluginFrame.img.i_csp){
                memset(hd3Data.outPluginFrame.img.plane[0], 0, baseSize);
                memset(hd3Data.outPluginFrame.img.plane[1], 0, baseSize/4);
                memset(hd3Data.outPluginFrame.img.plane[2], 0, baseSize/4);
            } else if (X264_CSP_RGB == hd3Data.outPluginFrame.img.i_csp || X264_CSP_BGR == hd3Data.outPluginFrame.img.i_csp) {
                memset(hd3Data.outPluginFrame.img.plane[0], 0, baseSize*3);
            }
            hd3Data.outPluginFrame.i_pts = pFrame->i_pts;
            hd3Data.outPluginFrame.i_type = pFrame->i_type;
            hd3Data.outPluginFrame.img.i_csp = pFrame->img.i_csp;
        }
        FrameBoxes* pCurrentSet = hd3Data.boxSet + setIndex;

        pCurrentSet->pts = pFrame->i_pts;
        pCurrentSet->number = 0;

        switch(hd3SourceType)
        {
            case HD3_SOURCE_TYPE_FILE:
            {
                int iFrame = 0;
                hd3Data.boxSet[setIndex].pts = pFrame->i_pts;
                hd3Data.boxSet[setIndex].number = 0;
                if (0 == readBoxes(&hd3Data.boxSet[setIndex], &hd3Data.readerData, &iFrame, READ_BOXES_METHOD_BEGIN_END))
                //if (0 == readBoxes(&hd3Data.boxSet[setIndex], &hd3Data.readerData, &iFrame, READ_BOXES_METHOD_GRID))
                {
                    fclose(hd3Data.readerData.pBoxSource);
                    hd3Data.readerData.pBoxSource = NULL;
                }
                if (iFrame) {
                    pFrame->i_type = X264_TYPE_IDR;
                }
            }
            break;
            case HD3_SOURCE_TYPE_GENERATE_JM:
            {
                generateBoxes_JM(pCurrentSet, pFrame);
            }
            break;
            case HD3_SOURCE_TYPE_GENERATE_JM2:
            {
                if (X264_CSP_I420 == pFrame->img.i_csp){
                    memset(pFrame->img.plane[0], 0, baseSize);
                    memset(pFrame->img.plane[1], 0, baseSize/4);
                    memset(pFrame->img.plane[2], 0, baseSize/4);
                } else if (X264_CSP_RGB == pFrame->img.i_csp || X264_CSP_BGR == pFrame->img.i_csp) {
                    memset(pFrame->img.plane[0], 0, baseSize*3);
                }
                generateBoxes_JM2(pCurrentSet, pFrame);
            } break;
            case HD3_SOURCE_TYPE_GENERATE_JP:
            {
                generateBoxes_JP(pCurrentSet, pFrame);
            } break;
            case HD3_SOURCE_TYPE_GENERATE_JP2:
            {
                generateBoxes_JP2(pCurrentSet, pFrame);
            } break;
            default:
            {
                av_log(hd3Data.ctx, AV_LOG_WARNING, "Warning: encoding based something diff than file is not implemented\n");
            } break;
        }
        clock_t caBegin = clock();

        DitheringFlushArgs args;
        pthread_t flushThread;

        args.pBuffor = &hd3Data.buffor;
        args.pFrame = pFrame;
        args.maxes = &hd3Data.maxes;
        args.mins = &hd3Data.mins;
        pthread_create(&flushThread, NULL, ditheringFlush, &args);

        alignBoxes(pCurrentSet, 32*hd3Data.globalXCompression, hd3Data.globalYCompression, hd3Data.height);
        //hd3Data.aggregationMethod = 3; // kacziro - tmp
        if (HD3_AGGREGATION_CONST_AREA == hd3Data.aggregationMethod) {
            aggregation_constArea(pCurrentSet, 32*hd3Data.globalXCompression, hd3Data.globalYCompression);
        } else if (HD3_AGGREGATION_MAX == hd3Data.aggregationMethod) {
            aggregation_max(pCurrentSet);
        }
        for (int i = 0; i < pCurrentSet->number; i++) {
            pCurrentSet->boxes[i].dstAabb = pCurrentSet->boxes[i].aabb;
        }
        if (maxParamIndex <= gopParamIndex)
        {
            gopParamIndex = 0;
        }
        pFrame->param = pGopParam + gopParamIndex;

        gopParamIndex++;
        resetQP(pFrame->param, hd3Data.qpGlobal, pFrame->i_pts);
        pthread_join(flushThread, NULL);
        int flatNb = 0;
        if (1)
        {
            FrameCopy buffRef;
            buffRef.data[0] = hd3Data.buffor.img.plane[0];
            buffRef.data[1] = hd3Data.buffor.img.plane[1];
            buffRef.data[2] = hd3Data.buffor.img.plane[2];
            buffRef.linesize[0] = hd3Data.buffor.img.i_stride[0];
            buffRef.linesize[1] = hd3Data.buffor.img.i_stride[1];
            buffRef.linesize[2] = hd3Data.buffor.img.i_stride[2];
            buffRef.width = hd3Data.width;
            buffRef.height = hd3Data.height;
            buffRef.format = hd3Data.pInputs->format;
            flatNb = findAreas(flatBoxes, &buffRef, pCurrentSet);
//            for (int i = 0; i < flatNb; i++) {
//                align_aabb(flatBoxes + i, 32*hd3Data.globalXCompression, hd3Data.globalYCompression, hd3Data.height);
//            }
            flatNb = aggregation_biggestRect(flatBoxes, flatNb);//aggregation_biggestArea(flatBoxes, flatNb);//
            flatNb = aggregation_aabb_constArea(flatBoxes, flatNb, 64, 64);
            replaceWithFlatAreas(flatBoxes, flatNb, pCurrentSet, hd3Data.globalXCompression, hd3Data.globalYCompression);
        }
        //resetQP(pFrame->param, -1, pFrame->i_pts);

        uint8_t xReduction, yReduction, method;
        int sbx, sby, sbw, sbh, dbx, dby, dbw, dbh;

        clock_t scalingBegin = clock();
        perfData.cna.sum += (float)(scalingBegin - caBegin);
        perfData.cna.number++;

        #define LOCAL_SCALING_THREADS 196 // SIC!! - temporary
        int threadsNumber = LOCAL_SCALING_THREADS;// av_cpu_count();
        pthread_t threads[LOCAL_SCALING_THREADS];
        BBEncodingArg threadsArg[LOCAL_SCALING_THREADS];

        MovementBox *pEnd = pCurrentSet->boxes + pCurrentSet->number;
        for (MovementBox *pBox = pCurrentSet->boxes; pBox < pEnd;) // kacziro - todo - split big box to many smaller - thread perf.
        {
            memset(threads, 0, sizeof(pthread_t)*threadsNumber);
            for (int thNr = 0; thNr < threadsNumber && pBox < pEnd; thNr++, pBox++) {
                method =     pCurrentSet->table.cells[pBox->code].method;
                xReduction = pCurrentSet->table.cells[pBox->code].xReduction;
                yReduction = pCurrentSet->table.cells[pBox->code].yReduction;

                if (pBox->aabb.end.x > hd3Data.width)
                {
                    pBox->aabb.end.x = hd3Data.width;
                }

                sbx = pBox->aabb.begin.x;
                sbw = pBox->aabb.end.x - pBox->aabb.begin.x;
                sby = pBox->aabb.begin.y;
                sbh = pBox->aabb.end.y - pBox->aabb.begin.y;
                dbx = pBox->dstAabb.begin.x;
                dbw = pBox->dstAabb.end.x - pBox->dstAabb.begin.x;
                dby = pBox->dstAabb.begin.y;
                dbh = pBox->dstAabb.end.y - pBox->dstAabb.begin.y;
                dbw = dbw/xReduction;
                dbh = dbh/yReduction;
                if (dbw > 0 && dbh > 0 && sbw > 0 && sbh > 0) {
                    hd3Data.buffor.param = pFrame->param;
                    // kacziro - start here - pFrame if format != gmax ??
                    threadsArg[thNr].pFrame = pFrame;//&hd3Data.buffor;//
                    threadsArg[thNr].xReduction = xReduction;
                    threadsArg[thNr].yReduction = yReduction;
                    threadsArg[thNr].method = method;
                    threadsArg[thNr].sbx = sbx;
                    threadsArg[thNr].sby = sby;
                    threadsArg[thNr].sbw = sbw;
                    threadsArg[thNr].sbh = sbh;
                    threadsArg[thNr].dbx = dbx;
                    threadsArg[thNr].dby = dby;
                    threadsArg[thNr].dbw = dbw;
                    threadsArg[thNr].dbh = dbh;
                    //pthread_create(threads + thNr, NULL, threadBBEncoding, threadsArg + thNr);
                    //printf("%d, %d, %d, %d || %d, %d, %d, %d\n", sbx, sby, sbw, sbh, dbx, dby, dbw*xReduction, dbh*yReduction);
                    threadBBEncoding(threadsArg + thNr);
                    if (hd3Data.plugin.enabled) {
                        if (X264_CSP_I420 == hd3Data.outPluginFrame.img.i_csp) {
                            for (int y = sby; y < sby+sbh; y++) {
                                memcpy(hd3Data.outPluginFrame.img.plane[0] + sbx + y*hd3Data.outPluginFrame.img.i_stride[0],
                                       hd3Data.pInputs->data[0] + sbx + y*hd3Data.pInputs->linesize[0],
                                       sbw);
                            }
                            for (int y = sby/2; y < (sby+sbh)/2; y++) {
                                memcpy(hd3Data.outPluginFrame.img.plane[1] + sbx/2 + y*hd3Data.outPluginFrame.img.i_stride[1],
                                       hd3Data.pInputs->data[1] + sbx/2 + y*hd3Data.pInputs->linesize[1],
                                       sbw/2);
                                memcpy(hd3Data.outPluginFrame.img.plane[2] + sbx/2 + y*hd3Data.outPluginFrame.img.i_stride[2],
                                       hd3Data.pInputs->data[2] + sbx/2 + y*hd3Data.pInputs->linesize[2],
                                       sbw/2);
                            }
                        } else if (X264_CSP_RGB == hd3Data.outPluginFrame.img.i_csp || X264_CSP_BGR == hd3Data.outPluginFrame.img.i_csp) {
                            for (int y = sby; y < sby+sbh; y++) {
                                memcpy(hd3Data.outPluginFrame.img.plane[0] + sbx*3 + y*hd3Data.outPluginFrame.img.i_stride[0],
                                       hd3Data.pInputs->data[0] + sbx*3 + y*hd3Data.pInputs->linesize[0],
                                       sbw*3);
                            }
                        }
                    }
                }
            }
            for (int thNr = 0; thNr < threadsNumber; thNr++) {
                if (threads[thNr]) {
                    pthread_join(threads[thNr], NULL);
                }
            }
        }
        clock_t scalingEnd = clock();
        perfData.scaling.sum += (float)(scalingEnd - scalingBegin);
        perfData.scaling.number++;
    }
    clock_t renderEnd = clock();
    perfData.render.sum += (float)(renderEnd - renderBegin);
    perfData.render.number++;

    hd3Data.buffor.i_type = X264_TYPE_AUTO;
    hd3Data.buffor.i_pts++;
    return 1; //always update input
}
static int hd3_movement_render(x264_picture_t *pFrame)
{
    if (NULL != pFrame)
    {
        {
            pFrame->img = hd3Data.outFrame.img;
        }
        pFrame->i_pts = hd3Data.buffor.i_pts;
        pFrame->i_type = hd3Data.buffor.i_type;
        pFrame->img.i_csp = hd3Data.buffor.img.i_csp; //todo out diff pix_fmt then input
        int baseSize, setIndex;
        baseSize = hd3Data.width*hd3Data.height;

        {
            if (AV_PIX_FMT_YUV420P == hd3Data.buff.format) {
                diff(&hd3Data.mins, &hd3Data.maxes, &hd3Data.buff);
                {
                    uint8_t *pDY = hd3Data.buff.data[0];
                    int th = 20;
                    for (int i = 0; i < hd3Data.buff.height*hd3Data.buff.linesize[0]; i++) {
                        pDY[i] = pDY[i] > th ? pDY[i]: 0;
                    }
                    uint8_t *pDU = hd3Data.buff.data[1];
                    uint8_t *pDV = hd3Data.buff.data[2];
                    for (int i = 0; i < (hd3Data.buff.height/2)*hd3Data.buff.linesize[0]; i++) {
                        pDU[i] = pDU[i] > th ? pDU[i]: 0;
                        pDV[i] = pDV[i] > th ? pDV[i]: 0;
                    }
                }
                for (int y = 0; y < hd3Data.buff.height; y++) {
                   // memset(pFrame->img.plane[0] + y*pFrame->img.i_stride[0], 0, pFrame->img.i_stride[0]);
                    memcpy(pFrame->img.plane[0] + y*pFrame->img.i_stride[0],
                           hd3Data.buff.data[0] + y*hd3Data.buff.linesize[0],
                           pFrame->img.i_stride[0]);
                    memset(hd3Data.buff.data[0] + y*hd3Data.buff.linesize[0], 0, hd3Data.buff.linesize[0]);
                }
                for (int y = 0; y < hd3Data.buff.height/2; y++) {
                    //memset(pFrame->img.plane[1] + y*pFrame->img.i_stride[1], 128, pFrame->img.i_stride[1]);
                    memcpy(pFrame->img.plane[1] + y*pFrame->img.i_stride[1],
                           hd3Data.buff.data[1] + y*hd3Data.buff.linesize[1],
                           pFrame->img.i_stride[1]);
                    //memset(hd3Data.buff.data[1] + y*hd3Data.buff.linesize[1], 0, hd3Data.buff.linesize[1]);
                    //memset(pFrame->img.plane[2] + y*pFrame->img.i_stride[2], 128, pFrame->img.i_stride[2]);
                    memcpy(pFrame->img.plane[2] + y*pFrame->img.i_stride[2],
                           hd3Data.buff.data[2] + y*hd3Data.buff.linesize[2],
                           pFrame->img.i_stride[2]);
                    //memset(hd3Data.buff.data[2] + y*hd3Data.buff.linesize[2], 0, hd3Data.buff.linesize[2]);
                }
                memset(hd3Data.mins.data[0], 255, baseSize);
                memset(hd3Data.mins.data[1], 255, baseSize/4);
                memset(hd3Data.mins.data[2], 255, baseSize/4);
                memset(hd3Data.maxes.data[0], 0, baseSize);
                memset(hd3Data.maxes.data[1], 0, baseSize/4);
                memset(hd3Data.maxes.data[2], 0, baseSize/4);
            }
        }
    }

    hd3Data.buffor.i_type = X264_TYPE_AUTO;
    hd3Data.buffor.i_pts++;
    return 1;//always update input
}
#endif
