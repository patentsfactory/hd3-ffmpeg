import threading, msvcrt
import sys, os, time, json
from shutil import copyfile

def readInput(caption, default, timeout = 1):
    class KeyboardThread(threading.Thread):
        def run(self):
            self.timedout = False
            self.input = ''
            while True:
                if msvcrt.kbhit():
                    chr = msvcrt.getche()
                    if ord(chr) >= 32:
                        self.input += str(chr)
                    print('')
                break
                if len(self.input) == 0 and self.timedout:
                    break
    result = default
    it = KeyboardThread()
    it.start()
    it.join(timeout)
    it.timedout = True
    if len(it.input) > 0:
        it.join()
        result = str(it.input)
    return result

def checkRun():
    a = readInput(None, None, 5)
    if a == str(b'q'):
        return False
    return True
    
def listFiles(path, ext):
    all = os.listdir(path)
    files = []
    for item in all:
        if os.path.isfile(os.path.join(path, item)) and item[-len(ext):] == ext:
            files.append(item)
    return files

begin = "#EXTM3U\n#EXT-X-VERSION:3\n#EXT-X-MEDIA-SEQUENCE:%d\n#EXT-X-ALLOW-CACHE:YES\n#EXT-X-TARGETDURATION:7\n#EXT-X-PLAYLIST-TYPE:EVENT\n"
end = ""#"#EXT-X-ENDLIST"
times = [
    '#EXTINF:5.101800,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:5.090900,',
    '#EXTINF:2.772722,',
]

if __name__ == "__main__":
    streamDir = "./streams/"
    allFilesDir = streamDir + "all_parts/"
    files = listFiles(allFilesDir, '.ts')
    files.sort()
    print("press [q] to quit")
    run = True
    index = 0
    counter = 0
    
    playlist = []
    
    while run:
        #copyfile(allFilesDir + files[index], streamDir + files[index])
        filename = "chunk%04d.ts" % counter
        copyfile(allFilesDir + files[index], streamDir + filename)
        playlist.append({'time': times[index], 'filename': filename})
        f = open(os.path.join(streamDir, 'playlist'), 'w')
        currentLen = len(listFiles(streamDir, '.ts'))
        while len(playlist) > currentLen + 5:
            playlist.pop(0)
        f.write(begin % counter)
        for p in playlist:
            f.write(p['time']+'\n'+p['filename']+'\n')
        f.write(end)
        f.close()
        index = (index + 1) % len(files)
        counter = counter + 1
        print(index, files[index], filename)
        time.sleep(2 + currentLen)
        run = checkRun()
        
