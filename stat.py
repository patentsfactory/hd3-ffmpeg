
import sys
if __name__ == "__main__":
    f = open(sys.argv[1])
    lines = f.readlines()

    sumI = 0
    numI = 0
    sumB = 0
    numB = 0
    sumP = 0
    numP = 0

    lastSize = 0
    for indexl in range(len(lines)):
        if lines[indexl] == 'pict_type=I\n':
            sumI += lastSize
            numI = numI + 1
        elif lines[indexl] == 'pict_type=B\n':
            sumB += lastSize
            numB = numB + 1
        elif lines[indexl] == 'pict_type=P\n':
            sumP += lastSize
            numP = numP + 1
        #else:
        #    try:
        #        lastSize = int(lines[indexl][9:])
        #    except:
        #        print('Error', indexl, lines[indexl][9:])
        #        exit()
    if numI > 0:
        print('I:', numI, sumI, sumI/float(numI))
    if numP > 0:
        print('P:', numP, sumP, sumP/float(numP))
    if numB > 0:
        print('B:', numB, sumB, sumB/float(numB))
    print('Sum:', numI+numB+numP, sumI+sumB+sumP)