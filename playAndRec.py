import sys, os

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Usage:")
        print("  ", sys.argv[0], "input", "output_file");
    else:
        from subprocess import Popen, call
        cmd = ['start', '/wait', '.\\ffplay.exe', '-i', 'tcp://127.0.0.1:321?listen', '-pf_show_mini' ,'-hd3_decode', 'SEI', '-hd3_size', '4x4', '-r', '4.4']
        print(cmd)
        p0 = Popen(cmd, shell=True)
        cmd = ['.\\ffmpeg.exe', '-i', sys.argv[1], '-c:v', 'copy', sys.argv[2], '-c:v', 'copy', '-f', 'mpegts', 'tcp://127.0.0.1:321', '-y']
        print(cmd)
        call(cmd)