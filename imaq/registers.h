#ifndef GMAX_REGISTERS_H
#define GMAX_REGISTERS_H

#include <stdint.h>
#include "gmax_tools.h"

#define REGIESTER_BANK_FPGA 0
#define REGIESTER_BANK_SENSOR 1
#define REGIESTER_BANK_PARAMETER 2

static uint8_t *registers;

static void setValue(uint8_t index, uint8_t bank, uint8_t address, uint8_t value)
{
	registers[3*index + 0] = ((address << 4) & 0xf0) | ((bank << 2) & 0x0c) | 1;
	registers[3*index + 1] = ((value << 6) & 0xc0) | (address >> 2) & 0x3c | 3;
	registers[3*index + 2] = (value & 0xfc) | 3;
}
static void getValue(uint8_t index, uint8_t *bank, uint8_t *address, uint8_t *value)
{
    *bank = (registers[3 * index + 0] & 0x0c ) >> 2;
    *address = ((registers[3 * index + 1] & 0x3c) << 2) | ((registers[3 * index + 0] & 0xf0) >> 4);
    *value = ((registers[3 * index + 1] & 0xc0) >> 6) | (registers[3 * index + 2] & 0xfc);
}
static void getRegister(uint8_t index, uint8_t sizeOf, uint8_t *bank, uint8_t *address, uint32_t *value)
{
    *value = 0;
    for (int i = 0; i < sizeOf; i++)
    {
        uint8_t val = 0;
        getValue(index + i, bank, address, &val);
        *value = *value | (val << (8 * i));
    }
    *address -= sizeOf - 1;
}
static void setRegister(uint8_t index, uint8_t sizeOf, uint8_t bank, uint8_t address, uint32_t value, uint32_t min, uint32_t max)
{
    if (value < min) {
        value = min;
    }
    else if (value > max) {
        value = max;
    }
    {
        uint8_t b, a;
        uint32_t val;
        getRegister(index, sizeOf, &b, &a, &val);
        fprintf(LOG_STREAM, "Reg 0x%x: 0x%x -> 0x%x\n", address, val, value);
    }
    for (int i = 0; i < sizeOf; i++)
    {
        setValue(index+i, bank, address+i, (uint8_t)(value >> (8*i)));
    }
}
// 24bit - min 1, max 16777215
static void setIntergrationTime(uint32_t value)
{
    setRegister(0, 3, REGIESTER_BANK_PARAMETER, 0, value, 1, 16777215);
}
static uint32_t getIntegrationTime()
{
    uint8_t b, a;
    uint32_t v;
    getRegister(0, 3, &b, &a, &v);
    return v;
}
static void setBinningMode(uint32_t value)
{
    setRegister(3, 1, REGIESTER_BANK_PARAMETER, 3, value, 0, 3);
}
static void setWindowRowStart(uint32_t value)
{
    setRegister(4, 2, REGIESTER_BANK_PARAMETER, 11, value, 0, 5004);
}
static void setWindowRowLenght(uint32_t value)
{
    setRegister(6, 2, REGIESTER_BANK_PARAMETER, 13, value, 1, 5005);
}
static void selectWindowColumn(uint32_t value)
{
    setRegister(28, 1, REGIESTER_BANK_FPGA, 21, value, 0, 2);
}
static void setIColPc(uint32_t value)
{
    if (value > 15) value = 15;
    uint8_t b, a;
    uint32_t val;
    getRegister(8, 1, &b, &a, &val);
    value = (value << 4) | (val & 0x0f);
    setRegister(8, 1, REGIESTER_BANK_SENSOR, 31, value, 0, 255);
}
static void setIColLoad(uint32_t value)
{
    if (value > 15) value = 15;
    uint8_t b, a;
    uint32_t val;
    getRegister(8, 1, &b, &a, &val);
    value = (val & 0xf0) | (value & 0x0f);
    setRegister(8, 1, REGIESTER_BANK_SENSOR, 31, value, 0, 255);
}
static void setIInternalDriv(uint32_t value)
{
    if (value > 15) value = 15;
    uint8_t b, a;
    uint32_t val;
    getRegister(9, 1, &b, &a, &val);
    value = (value << 4) | (val & 0x0f);
    setRegister(9, 1, REGIESTER_BANK_SENSOR, 30, value, 0, 255);
}
static void setICompInv(uint32_t value)
{
    if (value > 15) value = 15;
    uint8_t b, a;
    uint32_t val;
    getRegister(9, 1, &b, &a, &val);
    value = (val & 0xf0) | (value & 0x0f);
    setRegister(9, 1, REGIESTER_BANK_SENSOR, 30, value, 0, 255);
}
static void setIComp(uint32_t value)
{
    if (value > 15) value = 15;
    uint8_t b, a;
    uint32_t val;
    getRegister(10, 1, &b, &a, &val);
    value = (value << 4) | (val & 0x0f);
    setRegister(10, 1, REGIESTER_BANK_SENSOR, 29, value, 0, 255);
}
static void setIPGA(uint32_t value)
{
    if (value > 15) value = 15;
    uint8_t b, a;
    uint32_t val;
    getRegister(10, 1, &b, &a, &val);
    value = (val & 0xf0) | (value & 0x0f);
    setRegister(10, 1, REGIESTER_BANK_SENSOR, 29, value, 0, 255);
}
static void setLvdsLcEnabled(uint32_t value)
{
    if (value > 1) value = 1;
    uint8_t b, a;
    uint32_t val;
    getRegister(25, 1, &b, &a, &val);
    value = ((value << 7) & 0x80) | (val & 0x7f);
    setRegister(25, 1, REGIESTER_BANK_SENSOR, 22, value, 0, 255);
}
static void setLvdsDrivIbias(uint32_t value)
{
    if (value > 15) value = 15;
    uint8_t b, a;
    uint32_t val;
    getRegister(26, 1, &b, &a, &val);
    value = (value << 4) | (val & 0x0f);
    setRegister(26, 1, REGIESTER_BANK_SENSOR, 16, value, 0, 255);
}
static void setLvdsRecEn(uint32_t value)
{
    if (value > 1) value = 1;
    uint8_t b, a;
    uint32_t val;
    getRegister(24, 1, &b, &a, &val);
    value = (value << 7) | (val & 0x7f);
    setRegister(24, 1, REGIESTER_BANK_SENSOR, 24, value, 0, 255);
}
static void setLvdsRecIbias(uint32_t value)
{
    if (value > 15) value = 15;
    uint8_t b, a;
    uint32_t val;
    getRegister(24, 1, &b, &a, &val);
    value = ((value << 3) & 0x78) | (val & 0x87);
    setRegister(24, 1, REGIESTER_BANK_SENSOR, 24, value, 0, 255);
}
static void setPixHgEnabled(uint32_t value)
{
    if (value > 1) value = 1;
    uint8_t b, a;
    uint32_t val;
    getRegister(24, 1, &b, &a, &val);
    value = ((value << 2) & 0x04) | (val & 0xfb);
    setRegister(24, 1, REGIESTER_BANK_SENSOR, 24, value, 0, 255);
}
static void setPixBinEnabled(uint32_t value)
{
    if (value > 1) value = 1;
    uint8_t b, a;
    uint32_t val;
    getRegister(24, 1, &b, &a, &val);
    value = ((value << 1) & 0x02) | (val & 0xfd);
    setRegister(24, 1, REGIESTER_BANK_SENSOR, 24, value, 0, 255);
}
static void setPixDirEnabled(uint32_t value)
{
    if (value > 1) value = 1;
    uint8_t b, a;
    uint32_t val;
    getRegister(24, 1, &b, &a, &val);
    value = (value & 0x01) | (val & 0xfe);
    setRegister(24, 1, REGIESTER_BANK_SENSOR, 24, value, 0, 255);
}
static void setControlTana(uint32_t value)
{
    if (value > 15) value = 15;
    uint8_t b, a;
    uint32_t val;
    getRegister(26, 1, &b, &a, &val);
    value = (val & 0xf0) | (value & 0x0f);
    setRegister(26, 1, REGIESTER_BANK_SENSOR, 16, value, 0, 255);
}
static void setPllDiv(uint32_t value)
{
    if (value > 15) value = 15;
    uint8_t b, a;
    uint32_t val;
    getRegister(23, 1, &b, &a, &val);
    value = (val & 0xf0) | (value & 0x0f);
    setRegister(23, 1, REGIESTER_BANK_SENSOR, 25, value, 0, 255);
}
static void setPllCpc(uint32_t value)
{
    if (value > 1) value = 1;
    uint8_t b, a;
    uint32_t val;
    getRegister(23, 1, &b, &a, &val);
    value = (val & 0xef) | ((value << 4) & 0x10);
    setRegister(23, 1, REGIESTER_BANK_SENSOR, 25, value, 0, 255);
}
static void setPllCtrl(uint32_t value)
{
    if (value > 15) value = 15;
    uint8_t b, a;
    uint32_t val;
    getRegister(23, 1, &b, &a, &val);
    value = (val & 0x9f) | ((value << 5) & 0x60);
    setRegister(23, 1, REGIESTER_BANK_SENSOR, 25, value, 0, 255);
}
static void setFlip(uint32_t value)
{
    if (value > 1) value = 1;
    uint8_t b, a;
    uint32_t val;
    getRegister(23, 1, &b, &a, &val);
    value = (val & 0xfd) | ((value << 1) & 0x02);
    setRegister(23, 1, REGIESTER_BANK_SENSOR, 25, value, 0, 255);
}
static void setFullAdderEnable(uint32_t value)
{
    if (value > 1) value = 1;
    uint8_t b, a;
    uint32_t val;
    getRegister(20, 1, &b, &a, &val);
    value = (val & 0xfe) | (value & 0x01);
    setRegister(20, 1, REGIESTER_BANK_SENSOR, 23, value, 0, 255);
}
static void setClipEnable(uint32_t value)
{
    if (value > 1) value = 1;
    uint8_t b, a;
    uint32_t val;
    getRegister(20, 1, &b, &a, &val);
    value = (val & 0xfd) | ((value << 1) & 0x02);
    setRegister(20, 1, REGIESTER_BANK_SENSOR, 23, value, 0, 255);
}
static void setLvdsDrivEnable(uint32_t value)
{
    if (value > 1) value = 1;
    uint8_t b, a;
    uint32_t val;
    getRegister(20, 1, &b, &a, &val);
    value = (val & 0xfb) | ((value << 2) & 0x04);
    setRegister(20, 1, REGIESTER_BANK_SENSOR, 23, value, 0, 255);
}

static void setDelSampleEnable(uint32_t value)
{
    if (value > 1) value = 1;
    uint8_t b, a;
    uint32_t val;
    getRegister(20, 1, &b, &a, &val);
    value = (val & 0xf7) | ((value << 3) & 0x08);
    setRegister(20, 1, REGIESTER_BANK_SENSOR, 23, value, 0, 255);
}
static void setDelLoadEnable(uint32_t value)
{
    if (value > 1) value = 1;
    uint8_t b, a;
    uint32_t val;
    getRegister(20, 1, &b, &a, &val);
    value = (val & 0xef) | ((value << 4) & 0x10);
    setRegister(20, 1, REGIESTER_BANK_SENSOR, 23, value, 0, 255);
}
static void setAdc13BitEnable(uint32_t value)
{
    if (value > 1) value = 1;
    uint8_t b, a;
    uint32_t val;
    getRegister(20, 1, &b, &a, &val);
    value = (val & 0xdf) | ((value << 5) & 0x20);
    setRegister(20, 1, REGIESTER_BANK_SENSOR, 23, value, 0, 255);
}
static void setAdcClkGateEnable(uint32_t value)
{
    if (value > 1) value = 1;
    uint8_t b, a;
    uint32_t val;
    getRegister(20, 1, &b, &a, &val);
    value = (val & 0xbf) | ((value << 6) & 0x40);
    setRegister(20, 1, REGIESTER_BANK_SENSOR, 23, value, 0, 255);
}
static void setClkOutEnable(uint32_t value)
{
    if (value > 1) value = 1;
    uint8_t b, a;
    uint32_t val;
    getRegister(20, 1, &b, &a, &val);
    value = (val & 0x7f) | ((value << 7) & 0x80);
    setRegister(20, 1, REGIESTER_BANK_SENSOR, 23, value, 0, 255);
}
static void setITestDriv(uint32_t value)
{
    if (value > 15) value = 15;
    uint8_t b, a;
    uint32_t val;
    getRegister(11, 1, &b, &a, &val);
    value = (val & 0xc3) | ((value << 2) & 0x3c);
    setRegister(11, 1, REGIESTER_BANK_SENSOR, 19, value, 0, 255);
}
static void setAdcCntInt(uint32_t value)
{
    if (value > 8191) value = 8191;
    uint8_t b, a;
    uint32_t val;
    getRegister(11, 1, &b, &a, &val);
    val = (val & 0x3f) | ((value << 6) & 0xc0);
    setRegister(11, 1, REGIESTER_BANK_SENSOR, 19, val, 0, 255);
    val = (value >> 2) & 0xff;
    setRegister(21, 1, REGIESTER_BANK_SENSOR, 20, val, 0, 255);
    getRegister(22, 1, &b, &a, &val);
    value = (val & 0xf8) | ((value >> 10) & 0x07);
    setRegister(22, 1, REGIESTER_BANK_SENSOR, 21, value, 0, 255);
}
static void setTDig(uint32_t value)
{
    if (value > 15) value = 15;
    uint8_t b, a;
    uint32_t val;
    getRegister(22, 1, &b, &a, &val);
    val = (value << 3) & 0x78 | (val & 0x87);
    setRegister(22, 1, REGIESTER_BANK_SENSOR, 21, val, 0, 255);
}
//void setPGAGain(uint32_t value)
//{
//    if (value > 15) value = 15;?
//    uint8_t b, a;?
//    uint32_t val;?
//    getRegister(22, 1, &b, &a, &val);?
//    val = (value << 3) & 0x78 | (val & 0x87);?
//    setRegister(22, 1, REGIESTER_BANK_SENSOR, 21, val, 0, 255);?
//}
static void selectMainClock(uint32_t value)
{
    setRegister(29, 1, REGIESTER_BANK_FPGA, 24, value, 0, 7);
}
static void setFramesNb(uint32_t value)
{
    setRegister(30, 1, REGIESTER_BANK_FPGA, 25, value, 1, 255);
}
static void setTestImageEnabled (uint32_t value)
{
    setRegister(31, 1, REGIESTER_BANK_FPGA, 27, value, 0, 1);
}
static void setLEDEnabled(uint32_t value)
{
    setRegister(32, 1, REGIESTER_BANK_FPGA, 40, value, 0, 1);
}
static void setTrainingWord(uint32_t value)
{
    setRegister(33, 1, REGIESTER_BANK_FPGA, 38, value, 0, 255);
}
static void setG1205Enabled(uint32_t value)
{
    setRegister(34, 1, REGIESTER_BANK_FPGA, 39, value, 0, 1);
}
//static uint8_t registers[_REGISTERS_ARRAY_SIZE] = {
//	0x09, 0x43, 0x03, 0x19, 0x03, 0x03, 0x29, 0x03, 0x03, // 0, 1, 2 - integration time
//	0xb9, 0x03, 0x03, 0xc9, 0x03, 0x03, // 3, 4 - Window row start
//	0xd9, 0x03, 0x8b, 0xe9, 0xc3, 0x13, // 5, 6 - Window row length
//	//0x51, 0x47, 0x03, // 7 - fpga b00, a21, v1 ?  Window column sel
//	0x51, 0x07, 0x03, // 7 - fpga b00, a21, v1 ?  Window column sel
//	0x81, 0x47, 0x07, // 8 - fpga b00, a24, v6 ? Clock main sel
//	//0x91, 0x87, 0x03, // 9 - fpga b00, a25, v2 ? Frames
//	0x91, 0x87, 0x03, // 9 - fpga b00, a25, v2 ? Frames
//	0xb1, 0x07, 0x03,  //Test image en
//	0x81, 0x4b, 0x03,  // LED en
//	0x61, 0x8b, 0x87, // training word
//	0x39, 0x03, 0x03, // 13 - Binning mode
//	0x61, 0x8b, 0x87, // training word
//	0xb9, 0x83, 0x03, 0xc9, 0x03, 0x03, // rep
//	0xd9, 0x03, 0x87, 0xe9, 0xc3, 0x13, // rep
//	0x71, 0x4b, 0x03,  // b00, a39, v1 G1205_en
//	0xf5, 0xc7, 0xa7, // b01, a31, v(10, 7) I_COL_PC, I_COL_LOAD
//	0xf5, 0xc7, 0xa7,
//	0xe5, 0xc7, 0x77, // b01, a30, v(7, 7)  I_INTERNAL_DRIV, I_COMP_INV
//	0xe5, 0xc7, 0x77,
//	0xd5, 0xc7, 0x77, // b01, a29, v(7, 7) I_COMP, I_PGA
//	0xd5, 0xc7, 0x77,
//	0x35, 0x47, 0x03, // b01, a19, v1 I_TEST_DRIV
//	0x25, 0xc7, 0xef, // b01, a18, v7 (5bit not 6bit?) I_VREF_DRIV
//	0x35, 0x47, 0x03, // rep
//	0xe5, 0x43, 0xdf, // b01, a14, v(3+?, 7, 2+?) I_RAMP_OTA(6,7bit), I_ROWLOGIC(2-5bit), RAMP_SLOPE_P(0,1bit)
//	0xf5, 0x43, 0x7b, // b01, a15, v(12+?, 30)  I_RAMP_OTA(0,1bit) V_BLSUN(2-7bit)
//	0xe5, 0x43, 0xdf, // rep
//	0xc5, 0xc7, 0x7b, // b01, a28, v(30, 48+?) V_VTSIG(2-7bit), V_VTREF(0,1)
//	0xb5, 0xc7, 0x27, // b01, a27, v(2+?, 28+?) V_VTREF(4-7bit), V_VREF(0-3)
//	0xc5, 0xc7, 0x7b, // rep
//	0xa5, 0x07, 0x87, // b01, a26, v (2+?)V_VREF
//	0xb5, 0xc7, 0x27, // rep
//	0x25, 0xc7, 0xef, // rep
//	0x15, 0x87, 0x5f, // b01, a17, v(0+?) VRAMP_REF(7bit)
//	0x25, 0xc7, 0xef, // b01, a18, v(1, 30+?)  VRAMP_REF_EN(5bit), VRAMP_REF(0-4bit)
//	0x15, 0x87, 0x5f, // rep
//	0x15, 0x87, 0x5f, // rep
//	0xf5, 0x43, 0x7b, // rep
//	0xa5, 0x07, 0x87, // rep
//	0xa5, 0x07, 0x87, // rep
//	0x75, 0xc7, 0xcf, // b01, a23, v(0, 1, 1) ADC_13BIT_EN(5bit), ADC_CLK_GATE_EN(6bit), LVDS_DRIV_EN(2bit)
//	0x75, 0xc7, 0xcf, // rep
//	0x35, 0x47, 0x03, // rep
//	0x45, 0xc7, 0x67, // b01, a20, v() ??
//	0x55, 0xc7, 0x5f, // b01, a21, v(11, 0+) TDIG(3-6bit), TRAIN_PATTERN(7bit)
//	0x95, 0xc7, 0x5f, // b01, a25, v(15, 1, 2, 0) - PLL_DIV(0-3bit), PLL_CPC(4bit), PLL_CTRL(5,6bit), FLIP(1bit)
//	0x95, 0xc7, 0x5f, // rep
//	0x95, 0xc7, 0x5f, // rep
//	0x85, 0x47, 0x93, // b01, a24, v(1, 2, 0, 0, 1) - LVDS_REC_EN(7bit), LVDS_REC_IBIAS(3-6bit), PIX_HG_EN(2bit), PIX_BIN_EN(1bit), PIX_DIR(0bit)
//	0x85, 0x47, 0x93, // rep
//	0x75, 0xc7, 0xcf, // rep
//	0x65, 0xc7, 0xc3, // b01, a22, v(1) - LVDS_LC_EN(7bit)
//	0x05, 0xc7, 0x7b, // b01, a16, v(7, 11) - LVDS_DRIV_IBIAS(4-7bit), Control TANA(0-3bit)
//	0xa5, 0x07, 0x87, // rep
//	0xa5, 0x07, 0x87, // rep
//	0x95, 0xc7, 0x5f, // rep
//	0x85, 0x47, 0x93, // rep
//	0x85, 0x47, 0x93, // rep
//	0x85, 0x47, 0x93, // rep
//	0x75, 0xc7, 0xcf,  //rep
//	0x75, 0xc7, 0xcf,  //rep
//	0x75, 0xc7, 0xcf,  //rep
//	0x75, 0xc7, 0xcf,  //rep
//	0x75, 0xc7, 0xcf,  //rep
//	0x55, 0xc7, 0x5f, // rep
//	0x65, 0xc7, 0xc3, // rep
//	0x55, 0xc7, 0x5f, // rep
//	0x05, 0xc7, 0x7b, // rep
//	0xd5, 0x03, 0x53, // b01, a13, v(0, 5, 0+?) - GRST_EN(3bit), RAMP_SLOPE_N(4-6bit), RAMP_SLOPE_P(7bit)
//	0xd5, 0x03, 0x53, // rep
//	0xd5, 0x03, 0x53, // rep
//	0xe5, 0x43, 0xdf, // rep
//	0x00
//};
#endif // GMAX_REGISTERS_H
