#ifndef GMAX_SYNCH_H
#define GMAX_SYNCH_H

#include <windows.h>

#define SYNCH_MASTER_PORT 5858
#define SYNCH_TIME_MILISEC 20000
#define SYNCH_TIMEOUT SYNCH_TIME_MILISEC*1000*5

typedef struct SlaveCtx {
    WSADATA wsaData;
    SOCKET socket;
    SOCKADDR_IN saddr;
} SlaveCtx;

typedef struct MasterCtx {
    WSADATA wsaData;
    int32_t mainPort;
    SOCKET mainSock;
    SOCKADDR_IN saddr;
    fd_set rs;
    fd_set ws;

    int timeout;
} MasterCtx;

static __int64 getCurrTime() {
    SYSTEMTIME st;
    FILETIME ft;
    ULARGE_INTEGER timeTmp;
    GetSystemTime(&st);
    SystemTimeToFileTime (&st, &ft);
    timeTmp.HighPart = ft.dwHighDateTime;
    timeTmp.LowPart = ft.dwLowDateTime;
    return (__int64)timeTmp.QuadPart;
}

static int initMaster(MasterCtx *ctx) {
    int optval = 1;
    ctx->timeout = SYNCH_TIMEOUT;
    ctx->mainPort = SYNCH_MASTER_PORT;
    WSAStartup(MAKEWORD(2,2), &ctx->wsaData);

    ctx->mainSock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    memset((void*)&ctx->saddr, 0, sizeof(ctx->saddr));
    ctx->saddr.sin_family = AF_INET;
    ctx->saddr.sin_port = htons(ctx->mainPort);
    ctx->saddr.sin_addr.s_addr = htonl(INADDR_ANY);

    if (setsockopt(ctx->mainSock, SOL_SOCKET, SO_REUSEADDR, (char*)&optval, sizeof(optval)) == SOCKET_ERROR) {
        printf("[SYNCH] Main socket error - set OPT: %lu", GetLastError());
        return -1;
    }
    if (bind(ctx->mainSock, (struct sockaddr*)&ctx->saddr, sizeof(ctx->saddr)) == SOCKET_ERROR) {
        printf("[SYNCH] Main socket error - bind: %lu", GetLastError());
        return -1;
    }
    if (listen(ctx->mainSock, 1) == SOCKET_ERROR) {
        printf("[SYNCH] Main socket error - listen: %lu", GetLastError());
        return -1;
    }
    return 0;
}

static void waitForSlaves(MasterCtx *ctx, __int64 *time) {
    SOCKET clientSocket;
    __int64 t = getCurrTime();
    __int64 endTime = t + ctx->timeout;
    *time = t + ctx->timeout + SYNCH_TIME_MILISEC*1000;

    while(endTime - t > 0) {
        struct timeval tval;
        int e = SOCKET_ERROR;
        FD_ZERO(&ctx->rs);
        FD_ZERO(&ctx->ws);

        FD_SET(ctx->mainSock, &ctx->rs);
        tval.tv_sec = 1;
        tval.tv_usec = 0;

        e = select(0, &ctx->rs, &ctx->ws, NULL, &tval);
        if (e != SOCKET_ERROR) {
            if (FD_ISSET(ctx->mainSock, &ctx->rs)) {
                printf("accept\n");
                clientSocket = accept(ctx->mainSock, NULL, NULL);
                printf("send %d\n", sizeof(__int64));
                e = send(clientSocket, time, sizeof(__int64), 0);
                if (e == SOCKET_ERROR) {
                    closesocket(clientSocket);
                    printf("[error][send] %d\n", e);
                } else {
                    printf("send time to client: [%ld]\n", *time);
                }
            }
        } else {
            printf("[error][select]\n");
            closesocket(clientSocket);
        }
        t = getCurrTime();
        printf("waiting... %ld\t\r", endTime - t);
    }
}

static int initSlave(SlaveCtx *ctx, char *ip, __int64 *time) {
    int res = WSAStartup(MAKEWORD(2,2), &ctx->wsaData);
    int counter = 3;

    if (res) {
        printf("[error][WSAStartup] %d\n", res);
        return 1;
    }
    ctx->socket = socket(AF_INET, SOCK_STREAM, 0);
    if(ctx->socket == INVALID_SOCKET) {
        printf("[error][socket] %d\n", (int)ctx->socket);
        return 1;
    }

    ctx->saddr.sin_port=htons(SYNCH_MASTER_PORT);
    ctx->saddr.sin_family=AF_INET;
    ctx->saddr.sin_addr.s_addr = inet_addr(ip);

    while(counter) {
        printf("connect %d\r", counter);
        res = connect(ctx->socket, &ctx->saddr, sizeof(ctx->saddr));
        if (!res) {
            break;
        }
        counter--;
        Sleep(120);
    }

    if (res) {
        printf("[error][connect] %d\n", res);
        return 1;
    }
    *time = 0;
    printf("recv\t\t\n");
    res = recv(ctx->socket, time, sizeof(__int64), 0);

    if (res == SOCKET_ERROR) {
        *time = 0;
        printf("[error][recv] %d\n", res);
        return 1;
    }
    printf("recv %I64d\n", *time);
    return 0;
}

static void waitSince(__int64 deadline) {
    if (0 == deadline)
        return;
    __int64 time = getCurrTime();
//    Sleep(1); // 20000?????
//    unsigned __int64 time2 = getCurrTime();

    //printf("%lu - %lu\n", time, time2);

    printf("%I64d - %I64d\n", deadline, time);
    while(deadline - time > 0) {
        printf("wait %ld...\r", (deadline - time)/SYNCH_TIME_MILISEC);
        Sleep(200);
        time = getCurrTime();
    }
    printf("wait..done\n.");
}

#endif
