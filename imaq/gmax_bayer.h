#ifndef GMAX_BAYER_H
#define GMAX_BAYER_H

static void gmax12_bayer16_rgb_copy(const uint8_t *src, int src_stride, uint8_t *dst, int dst_stride, int width)
{
//    int i;
//    for (i = 0 ; i < width; i+= 2) {
//        BAYER_TO_RGB24_COPY
//        src += 2 * BAYER_SIZEOF;
//        dst += 6;
    //}
}

static void gmax12_bayer16_rgb_interpolate(const uint8_t *src, int src_stride, uint8_t *dst, int dst_stride, int width)
{
    int x;

//    BAYER_TO_RGB24_COPY
//    src += 2 * BAYER_SIZEOF;
//    dst += 6;

//    for (i = 2 ; i < width - 2; i+= 2) {
//        BAYER_TO_RGB24_INTERPOLATE
//        src += 2 * BAYER_SIZEOF;
//        dst += 6;
//    }
    uint16_t *pS = (uint16_t*)src;
    for (x = 2; x < width - 2; x+=2)
    {
        uint8_t c1, c2;
        int index1 = x;
        int index2 = x + 1;
        dst[3 * index1] = (pS[index1 - 1] + pS[index1 + 1]) / 2;
        dst[3 * index1 + 1] = pS[index1];
        dst[3 * index1 + 2] = (pS[index1 - width] + pS[index1 + width]) / 2;
        dst[3 * index2] = pS[index2];
        dst[3 * index2 + 1] = (pS[index2 - 1] + pS[index2 + 1] + pS[index2-width] + pS[index2+width]) / 4;
        int comp = abs(pS[index2-2] - pS[index2 + 2]) - abs(pS[index2 -2 * width] - pS[index2+2 * width]);
        if (comp > 0)
        {
            dst[3 * index2 + 1]  = (pS[index2-width] + pS[index2+width]) / 2;
        }
        else if (comp < 0)
        {
            dst[3 * index2 + 1]  = (pS[index2-1] + pS[index2+ 1]) / 2;
        }
        dst[3 * index2 + 2] = (pS[index2 - 1 - width] + pS[index2 + 1 + width] + pS[index2 + 1 - width] + pS[index2 - 1 + width]) / 4;;
        //
        index1 = width + x + 1;
        index2 = width + x;
        dst[3 * index1] = (pS[index1 - width] + pS[index1 + width]) / 2;
        dst[3 * index1 + 1] = pS[index1];
        dst[3 * index1 + 2] = (pS[index1 - 1] + pS[index1 + 1]) / 2;
        dst[3 * index2] = (pS[index2 - 1 - width] + pS[index2 + 1 + width] + pS[index2 + 1 - width] + pS[index2 - 1 + width]) / 4;
        dst[3 * index2 + 1] = (pS[index2 - 1] + pS[index2 + 1] + pS[index2-width] + pS[index2+width]) / 4;
        comp = abs(pS[index2-2] - pS[index2 + 2]) - abs(pS[index2 -2 * width] - pS[index2+2 * width]);
        if (comp > 0)
        {
            dst[3 * index2 + 1]  = (pS[index2-width] + pS[index2+width]) / 2;
        }
        else if (comp < 0)
        {
            dst[3 * index2 + 1]  = (pS[index2-1] + pS[index2+ 1]) / 2;
        }
        dst[3 * index2 + 2] = pS[index2];
    }

//    if (width > 2) {
//        BAYER_TO_RGB24_COPY
//    }
}

#endif
