#ifndef GMAX_H
#define GMAX_H

#include "windows.h"
#include "..\hd3\pf_tools.h"
#include "gmax_global.h"

typedef uint32_t INTERFACE_ID;
typedef uint32_t SESSION_ID;
typedef uint32_t BUFLIST_ID;

typedef int (__stdcall *f_gmaxInit)(uint32_t, char*, uint8_t);
const char* FUN_NAME_gmaxInit = "gmaxInit";
typedef int (__stdcall *f_startAq)();
const char* FUN_NAME_startAq = "start";
typedef int (__stdcall *f_stopAq)();
const char* FUN_NAME_stopAq = "stop";
typedef int (__stdcall *f_loadRegisters)(char*);
const char* FUN_NAME_loadRegisters = "loadRegisters";
typedef int (__stdcall *f_getQueue)(int8_t ***data, uint8_t **flags, uint16_t *number);
const char* FUN_NAME_getQueue = "getQueue";

static f_gmaxInit funGmaxInit;
static f_startAq funStartAq;
static f_stopAq funStopAq;
static f_loadRegisters funLoadRegisters;
static f_getQueue funGetQueue;
static int loss = 0;

static int checkFunctionLoad(void* p, const char * name)
{
    if (NULL == p) {
        printf("could not locate the function: %s\n", name);
        return 1;
    }
    return 0;
}
int start()
{
    return funStartAq();
}
int stop()
{
    printf("lost: %d\n", loss);
    return funStopAq();
}
static void gmax_buffer_free2(void *opaque, uint8_t *data)
{
    //av_free(data);
    printf("freeeeeeee2\n");
}
int gmaxInit(GmaxCtx *ctx, uint32_t timeout, char *filename, uint8_t format, char* synch, uint8_t commPort)
{
    HINSTANCE hGetProcIDDLL = LoadLibrary("GmaxApi.dll");
    int loadResult = 0;

    if (hGetProcIDDLL == NULL) {
        printf("cannot load the .dll file %ld\n", GetLastError());
        return -1;
    }
    memset(ctx, 0, sizeof(GmaxCtx));

    ctx->synch = synch;
    ctx->commPort = commPort;
    ctx->timeout = timeout;
    ctx->format = format;
    funGmaxInit = (f_gmaxInit)GetProcAddress(hGetProcIDDLL, FUN_NAME_gmaxInit);
    loadResult = loadResult | checkFunctionLoad(funGmaxInit, FUN_NAME_gmaxInit);
    funStartAq = (f_startAq)GetProcAddress(hGetProcIDDLL, FUN_NAME_startAq);
    loadResult = loadResult | checkFunctionLoad(funStartAq, FUN_NAME_startAq);
    funStopAq = (f_stopAq)GetProcAddress(hGetProcIDDLL, FUN_NAME_stopAq);
    loadResult = loadResult | checkFunctionLoad(funStopAq, FUN_NAME_stopAq);
    funLoadRegisters = (f_loadRegisters)GetProcAddress(hGetProcIDDLL, FUN_NAME_loadRegisters);
    loadResult = loadResult | checkFunctionLoad(funLoadRegisters, FUN_NAME_loadRegisters);
    funGetQueue = (f_getQueue)GetProcAddress(hGetProcIDDLL, FUN_NAME_getQueue);
    loadResult = loadResult | checkFunctionLoad(funGetQueue, FUN_NAME_getQueue);

    if (0 == loadResult) {
        if (filename != NULL) {
            loadResult = loadResult | funLoadRegisters(filename);
        }
        loadResult = funGmaxInit(ctx->timeout, ctx->synch, ctx->commPort);
        if (loadResult) {
            return loadResult;
        }
        loadResult = start();
        if (loadResult) {
            return loadResult;
        }
        Sleep(10000); //kacziro - todo - gmaxInit
        ctx->queue.index = 0;
        ctx->queue.step = 1;
        loadResult = funGetQueue(&ctx->queue.frames, &ctx->queue.flags, &ctx->queue.number);
        printf("open: queue: %d, %d\n", loadResult, ctx->queue.frames);
        if (NULL == ctx->queue.frames)
        {
            loadResult = -1;
        } else {
            ctx->queue.bufs = malloc(sizeof(AVBufferRef*)*ctx->queue.number);
            for (int i = 0; i < ctx->queue.number; i++)
            {
                //ctx->queue.bufs[i] = av_buffer_create(ctx->queue.frames[i], 12000*4996*2, gmax_buffer_free2, NULL, 0);
                ctx->queue.bufs[i] = av_buffer_create(ctx->queue.frames[i], 12000*4996*2, NULL, NULL, 0);
            }
        }
    }
    printf("result: %d\n", loadResult);

    return loadResult;
}
int getFrame(GmaxCtx *ctx, int8_t **frame)
{
    if (1 == ctx->queue.flags[ctx->queue.index])
    {
        int index = ctx->queue.index;
        for (int i = 0; i < ctx->queue.number && ctx->queue.flags[(ctx->queue.index + i)%ctx->queue.number]; i++) {
            index = (ctx->queue.index + i)%ctx->queue.number;
        }
        index = (index - ctx->queue.index + ctx->queue.number) % ctx->queue.number;
        ctx->queue.step = 1 + index/100;
        loss += ctx->queue.step - 1;
        *frame = ctx->queue.frames[ctx->queue.index];
        return 1;
    }
    return 0;
}
int getLastFrame(GmaxCtx *ctx, int8_t **frame)
{
    int index = ctx->queue.index;
    for (int i = 0; i < ctx->queue.number; i++) {
        if (0 == ctx->queue.flags[(ctx->queue.index + i)%ctx->queue.number])
            break;
        index = (ctx->queue.index + i)%ctx->queue.number;
    }
    if (1 == ctx->queue.flags[index])
    {
        memset(ctx->queue.flags, 0, ctx->queue.number);
        ctx->queue.flags[index] = 1;
        printf("%d - > %d\n", ctx->queue.index, index);
        loss += index - ctx->queue.index;
        ctx->queue.index = index;
        *frame = ctx->queue.frames[ctx->queue.index];
        return 1;
    }
    return 0;
}

void unlock(GmaxCtx *ctx)
{
    for (int i = 0; i < ctx->queue.step; i++)
    {
        ctx->queue.flags[ctx->queue.index + i] = 0;
    }
    ctx->queue.index += ctx->queue.step;
    if (ctx->queue.index >= ctx->queue.number) {
        ctx->queue.index = 0;
    }
}


#endif
