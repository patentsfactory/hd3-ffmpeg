#ifndef GMAX_GLOBAL_H
#define GMAX_GLOBAL_H

#include <stdint.h>
#include "libavcodec/avcodec.h"
#include "libavutil/internal.h"
#include "libavutil/opt.h"
#include "libavformat/avio.h"
#include "libavformat/avformat.h"
#include "libavformat/url.h"

#include "imaq/synch.h"

typedef struct FramesQueue
{
    int8_t **frames;
    uint8_t *flags;
    AVBufferRef **bufs;
    uint16_t number;
    int index;
    int step;
} FramesQueue;

typedef struct GmaxCtx
{
    AVClass *class;
    FramesQueue queue;
    uint32_t timeout;
    uint8_t format;
    char* synch;
    uint8_t commPort;
} GmaxCtx;
static GmaxCtx gctx;

#define OFFSET(x) offsetof(GmaxCtx, x)
#define V AV_OPT_FLAG_VIDEO_PARAM
#define D AV_OPT_FLAG_DECODING_PARAM
#define E AV_OPT_FLAG_ENCODING_PARAM
static const AVOption gmaxOptions[] = {
    { "gmaxsynch", "synchronize start with <<ip address>> or wait as MASTER", OFFSET(synch), AV_OPT_TYPE_STRING, { .str = NULL }, 0, 0, V | D | E },
    { "commport", "get time from gps module", OFFSET(commPort), AV_OPT_TYPE_INT, { .i64 = 0 }, 0, 255, V | D | E  },
    { NULL }
};


#define GMAX_WIDTH 12000
#define GMAX_HEIGHT 4996
#define GMAX_STREAM_NAME "GMAX"

#define GMAX_OPTION_DEFAULT GMAX_STREAM_NAME
#define GMAX_OPTION_GRAY GMAX_OPTION_DEFAULT
#define GMAX_OPTION_COLOR "GMAX_COLOR"
static uint8_t isGmax = 0;
static int32_t gmaxFormat = 0;

static const AVClass gmax_class = {
    .class_name = GMAX_STREAM_NAME,
    .item_name  = av_default_item_name,
    .option     = gmaxOptions,
    .version    = LIBAVUTIL_VERSION_INT,
};

int gmaxInit(GmaxCtx *ctx, uint32_t timeout, char *filename, uint8_t format, char* synch, uint8_t commPort);
int getFrame(GmaxCtx *ctx, int8_t **frame);
int getLastFrame(GmaxCtx *ctx, int8_t **frame);
void unlock(GmaxCtx *ctx);
int start();
int stop();

static uint8_t itIsGmax(char* arg) {
    if (!strcmp(GMAX_OPTION_GRAY, arg)) {
        isGmax = 1;
        gmaxFormat = AV_PIX_FMT_GRAY8;
    }
    if (!strcmp(GMAX_OPTION_COLOR, arg)) {
        isGmax = 1;
        gmaxFormat = AV_PIX_FMT_BAYER_GBRG8;
    }
    return itIsGmax;
}
static int initCounter = -1;
static int8_t initSingleton = 0;
static int decode_gmax_init(AVCodecContext *avctx)
{
    int res = 0;
    GmaxCtx *ctx = (GmaxCtx*)avctx->priv_data;
    avctx->priv_data = &gctx; // kacziro - fixme - memory leap (sizeof(void*));
    if (initSingleton) {
        initSingleton = 0;
        res = gmaxInit(&gctx, 1000, "config_gmax.ini", AV_PIX_FMT_GRAY8, ctx->synch, ctx->commPort);
        printf("gmaxInit %d\n", res);
    }
    gctx.class = ctx->class;
    return res;
}
static int decode_gmax_destroy(AVCodecContext *avctx)
{
    printf("gmax destroy %d\n", initCounter); // x1 before question and xN - N = threads count
    if (initCounter == 0) {
        stop();
        printf("finish\n");
    }

    initCounter--;
    return 0;
}
static int decode_gmax_thread_copy(AVCodecContext *avctx)
{
    printf("gmax thread %d\n", initCounter);
    if (-1 == initCounter)
    {
        initCounter = 0;
    }
    initCounter++;
    return 0;
}

static int decode_gmax_frame(AVCodecContext *avctx,
                             void *data, int *got_frame,
                             AVPacket *avpkt)
{
    AVFrame *pict = data;
    *got_frame = 0;
    if (avpkt->data)
    {
        *got_frame = 1;
        pict->width = GMAX_WIDTH;
        pict->height = GMAX_HEIGHT;
        pict->linesize[0] = pict->width*2;
        pict->data[0] = avpkt->data;
        pict->format = AV_PIX_FMT_GRAY16BE;//avctx->format; kacziro - fixme

        pict->buf[0] = av_buffer_ref(avpkt->buf);
        pict->pkt_pts = pict->pts = avpkt->pts;
        pict->pkt_dts = avpkt->dts;
    }
    return 0;
}
static void decode_gmax_flush(AVCodecContext *avctx)
{
    printf("gmax flush\n");
}
static int gmax_read_packet(void *opaque, int64_t offset, int whence)
{
    //GmaxCtx *pCtx = opaque;
    printf("gmax read\n");
    return whence;
}
static int gmax_write_packet(void *opaque, int64_t offset, int whence)
{
    printf("gmax write\n");
    return 0;
}
static int64_t gmax_seek(void *opaque, int64_t offset, int whence)
{
    printf("gmax seek\n");
    return whence;
}

typedef struct GmaxIOInternal {
    URLContext *h;
} GmaxIOInternal;

static const enum AVPixelFormat gmax_pixfmt_list[] = {
    AV_PIX_FMT_GRAY16,
    AV_PIX_FMT_NONE
};

#if HAVE_THREADS
static int gmax_update_thread_context(AVCodecContext *dst,
                                       const AVCodecContext *src)
{
//    Mpeg4DecContext *s = dst->priv_data;
//    const Mpeg4DecContext *s1 = src->priv_data;
//    int init = s->m.context_initialized;
//
//    int ret = ff_mpeg_update_thread_context(dst, src);
//
//    if (ret < 0)
//        return ret;
//
//    memcpy(((uint8_t*)s) + sizeof(MpegEncContext), ((uint8_t*)s1) + sizeof(MpegEncContext), sizeof(Mpeg4DecContext) - sizeof(MpegEncContext));
//
//    if (CONFIG_MPEG4_DECODER && !init && s1->xvid_build >= 0)
//        ff_xvid_idct_init(&s->m.idsp, dst);

    return 0;
}
#endif

static AVCodec ff_gmax_decoder = {
    .name                  = GMAX_STREAM_NAME,
    .long_name             = NULL,
    .type                  = AVMEDIA_TYPE_VIDEO,
    .id                    = AV_CODEC_ID_GMAX,
    .priv_data_size        = sizeof(GmaxCtx),
    .init                  = decode_gmax_init,
    .close                 = decode_gmax_destroy,
    .decode                = decode_gmax_frame,
    .init_thread_copy      = decode_gmax_thread_copy,
    .capabilities          = AV_CODEC_CAP_DRAW_HORIZ_BAND | AV_CODEC_CAP_DR1 |
                             AV_CODEC_CAP_TRUNCATED | AV_CODEC_CAP_DELAY |
                             AV_CODEC_CAP_FRAME_THREADS,
    .flush                 = decode_gmax_flush,
    .max_lowres            = 3,
    .pix_fmts              = gmax_pixfmt_list,
    .profiles              = NULL,
    .update_thread_context = ONLY_IF_THREADS_ENABLED(gmax_update_thread_context),
    .priv_class            = &gmax_class,
};
static int gmax_mux_read_header(AVFormatContext *s) {
    AVStream *st = avformat_new_stream(s, &ff_gmax_decoder);
    printf("gmax_mux_read_header\n");
    if (st && st->codecpar) {
        //st->time_base.num = 1;
        //st->time_base.den = 5;
        st->codecpar->codec_type = AVMEDIA_TYPE_VIDEO;
        st->codecpar->width = GMAX_WIDTH;
        st->codecpar->height = GMAX_HEIGHT;
        st->codecpar->format = AV_PIX_FMT_GRAY16BE;
    }
    return 0;
}
static int gmax_mux_read_packet(AVFormatContext *context, AVPacket *pkt) {
    static uint64_t pts = 0;
    if (pts) {
        unlock(&gctx);
    }
    if (NULL == gctx.queue.frames) {
        return -1;
    }
    {
        int index = gctx.queue.index;
        if(getFrame(&gctx, &pkt->data)) {
            pkt->size = GMAX_WIDTH*GMAX_HEIGHT*2;
            pkt->dts = pkt->pts = pts++;
            pkt->pos = -1;
            pkt->buf = av_buffer_ref(gctx.queue.bufs + index);
        }
    }
    return 0;
}
static AVInputFormat ff_gmax_demuxer = {
    .name           = GMAX_STREAM_NAME,
    .long_name      = NULL,
    .read_header    = gmax_mux_read_header,
    .read_packet    = gmax_mux_read_packet,
    .extensions     = NULL,
    .flags          = 0,
    .raw_codec_id   = AV_CODEC_ID_GMAX,
    .priv_data_size = sizeof(GmaxCtx),
    .priv_class     = NULL,
};

static int decode_gmax_open(AVFormatContext *s, AVIOContext **pb,
                           const char *url, int flags, AVDictionary **options)
{
    uint8_t *buffer = NULL;
    int buffer_size;
    initSingleton = 1;
    //GmaxCtx *pCtx = s->pb->opaque;
    URLContext *h;
    GmaxIOInternal *internal = NULL;
    int ret = ffurl_alloc(&h, url, flags, NULL);

    if (!ret) {
        buffer_size = GMAX_WIDTH*(GMAX_HEIGHT+4)*2;
        buffer = av_malloc(buffer_size);
        if (!buffer)
            return AVERROR(ENOMEM);

        internal = av_mallocz(sizeof(*internal));
        if (!internal)
            return -1;

        internal->h = h;

        *pb = avio_alloc_context(buffer, buffer_size, h->flags & AVIO_FLAG_WRITE,
                                internal, gmax_read_packet, gmax_write_packet, gmax_seek);
        if (!*pb)
            return -1;
        s->iformat = &ff_gmax_demuxer;

        s->opaque = NULL;
        s->probesize = 1000000;
    }
    return ret;
}
static int decode_gmax_close(AVFormatContext *s, AVIOContext *pb)
{
    printf("gmax close\n");
    stop();
    return 0;
}

static void register_gmax()
{
    avcodec_register(&ff_gmax_decoder);
    av_register_input_format(&ff_gmax_demuxer);
}


#endif
