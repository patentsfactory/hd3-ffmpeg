#ifndef GMAX_INI_PARSER_H
#define GMAX_INI_PARSER_H

#include <stdint.h>
#include <stdio.h>
#include "registers.h"
#include "gmax_tools.h"

#define PARAM_NB 59

void(*setters[PARAM_NB]) (uint32_t value) =
{
    setIntergrationTime,
    setWindowRowStart,
    setWindowRowLenght,
    selectWindowColumn,
    setBinningMode,
    selectMainClock,
    setFramesNb,
    setTestImageEnabled,
    setLEDEnabled,
    setTrainingWord,
    setG1205Enabled,
    setIColPc,
    setIColLoad,
    setIInternalDriv,
    setICompInv,
    setIComp,
    setIPGA,
    setITestDriv,
    setAdcClkGateEnable,
    setAdc13BitEnable,
    setAdcCntInt,
    setPllCtrl,
    setPllCpc,
    setPllDiv,
    setLvdsRecEn,
    setLvdsRecIbias,
    setLvdsDrivEnable,
    setLvdsLcEnabled,
    setLvdsDrivIbias,
    setFlip,
    setPixHgEnabled,
    setPixBinEnabled,
    setPixDirEnabled,
    setClkOutEnable,
    setDelLoadEnable,
    setDelSampleEnable,
    setClipEnable,
    setFullAdderEnable,
    setTDig,
    setControlTana
};

char* params[PARAM_NB] = {
    "Integration time",
    "Window row start",
    "Window row length",
    "Window column sel",
    "Binning mode",
    "Clock main sel",
    "Frames",
    "Test image en",
    "LED en",
    "training word",
    "G1205_en",
    "I_COL_PC",
    "I_COL_LOAD",
    "I_INTERNAL_DRIV",
    "I_COMP_INV",
    "I_COMP",
    "I_PGA",
    "I_TEST_DRIV",
    //"I_VREF_DRIV",
    //"I_RAMP_OTA",
    //"I_ROWLOGIC",
    //"V_VTSIG",
    //"V_VTREF",
    //"V_VREF",
    //"VRAMP_REF_EN",
    //"VRAMP_REF",
    //"VRAMP_SIG_EN",
    //"VRAMP_SIG",
    //"V_BLSUN",
    //"PGA_GAIN",
    //"PGA_CALI_EN",
    "ADC_CLK_GATE_EN",
    "ADC_13BIT_EN",
    "ADC_CNT_INT",
    "PLL_CTRL",
    "PLL_CPC",
    "PLL_DIV",
    "LVDS_REC_EN",
    "LVDS_REC_IBIAS",
    "LVDS_DRIV_EN",
    "LVDS_LC_EN",
    "LVDS_DRIV_IBIAS",
    //"TEST_EN",
    //"COMP_CALI_EN",
    "FLIP",
    "PIX_HG_EN",
    "PIX_BIN_EN",
    "PIX_DIR",
    "CLK_OUT_EN",
    "DEL_LOAD_EN",
    "DEL_SAMPLE_EN",
    "CLIP_EN",
    "FULL_ADDER_EN",
    //"TRAIN_PATTERN",
    "TDIG",
    "Control TANA",
   //"GRST_EN",
   //"RAMP_SLOPE_N",
   //"RAMP_SLOPE_P"
};
static int readParam(FILE* file, char *name, uint32_t *value)
{
    int res = 0;
    int state = 0;
    *value = 0;
    char c = fgetc(file);
    while (EOF != c && c != '\n')
    {
        if (0 == state)
        {
            if (('0' <= c && c <= '9') ||
                ('a' <= c && c <= 'z') ||
                ('A' <= c && c <= 'Z') ||
                c == ' ' || c == '_')
            {
                *name = c;
                name++;
            }
            else if (c == '=')
            {
                state = 1;
                if (*(name-1) == ' ')
                {
                    name--;
                }
                *name = 0;
            }
        }
        else if (1 == state)
        {
            if ('0' <= c && c <= '9')
            {
                state = 2;
                res = 1;
                *value = c - '0';
            }
        }
        else if (2 == state)
        {
            if ('0' <= c && c <= '9')
            {
                *value *= 10;
                *value += c - '0';
            }
            else
            {
                break;
            }
        }
        c = fgetc(file);
    }
    return res;
}

static void loadIni(FILE* file)
{
    char param[50];
    uint32_t value;
    char c = fgetc(file);
    while (EOF != c)
    {
        if (c == '\n')
        {
            int res = readParam(file, param, &value);
            if (res)
            {
                fprintf(LOG_STREAM, "param: %s = %d\n", param, value);
                int index = -1;
                for (int i = 0; i < 39; i++)
                {
                    if (strcmp(params[i], param) == 0)
                    {
                        index = i;
                        break;
                    }
                }
                if (index != -1)
                {
                    setters[index](value);
                }
                else
                {
                    fprintf(LOG_STREAM, "Error: unknown parameter %s [%d]\n", param, value);
                }
            }
            else
            {
                //printf("//comment: %s\n", param);
            }
        }
        c = fgetc(file);
    }
}

#endif // GMAX_INI_PARSER_H
