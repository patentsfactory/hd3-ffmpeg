clear all
path = 'H:\test3\';
% path = 'H:\test4\';
% path = 'H:\test5\';
% path = 'H:\test6\';
path2 = [path 'HD3\'];
mkdir(path,'Reconstructed');
path3 = [path 'Reconstructed\'];

name_scene = 'frame__scene.mat';
load([path2 name_scene]);
baza = areas';

% block_size = 32;
% m = 812; m = block_size*fix(m/block_size)-1;
SOF = 8;
frate = 5;

names2 = [name '_hd3_'];
ext = '.tiff';

name_out = [name '_reconstructed_'];
name_out2 = [name '_reconstructed_filtered_'];

mody = [16 8 4 2 1];
modx = [1 2 4 8 16];
pixel = [1 1; 1 2; 1 3; 1 4; 2 1; 2 2; 2 3; 2 4; 3 1; 3 2; 3 3; 3 4; 4 1; 4 2; 4 3; 4 4];

for i = 0:fix(m/16)
    toooc = [];
    tic 
    name2 = [names2 num2str(i*16)];
    A_hd3 = imread([path2 name2 ext]);
    n = size(A_hd3); % input resolution
    nn = n(1)*n(2);
    toooc = [toooc toc];    % 1

    boxes = baza{i+1};
%     boxes = boxes.boxes;

%     figure(1); image(A_hd3); axis equal;
%     title(['ramka nr:',num2str(i),' ostatnia:',num2str(fix(m/16))]);
%     pause(0.01);
    toooc = [toooc toc];    % 2

%         C = A_hd3;
    if i==0
        C = A_hd3;
        M = zeros(size(A_hd3),'logical');
        M2 = M;
    end
    for j = 0:15
        x = pixel(j+1,2);
        y = pixel(j+1,1);
        C(y:4:n(1),x:4:n(2),:) = A_hd3(y:4:n(1),x:4:n(2),:);
        if size(boxes,1)>0 && sum(boxes(1,1:end-1))>0
            M = zeros(size(A_hd3),'logical');
            for k = 1:size(boxes,1)
                M(1+(boxes(k,1)-1)*block_size:boxes(k,1)*block_size,1+(boxes(k,2)-1)*block_size:boxes(k,2)*block_size,:) = 1;   %mask
                
                x = 1 + mod(j,modx(boxes(k,3))); y = 1 + fix(j/modx(boxes(k,3)));
                if boxes(k,3)==1
                    dy = block_size/16; dx = block_size;
                elseif boxes(k,3)==2
                    dy = block_size/8; dx = block_size/2;
                elseif boxes(k,3)==3
                    dy = block_size/4; dx = block_size/4;
                elseif boxes(k,3)==4
                    dy = block_size/2; dx = block_size/8;
                else
                    dy = block_size; dx = block_size/16;
                end

                object_bmp = imresize(A_hd3(1+(boxes(k,1)-1)*block_size+(y-1)*dy:(boxes(k,1)-1)*block_size+y*dy,1+(boxes(k,2)-1)*block_size+(x-1)*dx:(boxes(k,2)-1)*block_size+x*dx,:),[block_size block_size],'bicubic');
                
                D = C;
                C(1+(boxes(k,1)-1)*block_size:boxes(k,1)*block_size,1+(boxes(k,2)-1)*block_size:boxes(k,2)*block_size,:) = object_bmp;
                D(1+(boxes(k,1)-1)*block_size:boxes(k,1)*block_size,1+(boxes(k,2)-1)*block_size:boxes(k,2)*block_size,:) = object_bmp;
            end
            MM = M2 & ~M;	%mask showing where objects was and no longer are
            C(MM) = A_hd3(MM);	%copy new data to places where objects no longer are
            
            B = D;
            %loop filter
            for k = 1:size(boxes,1)
                y = 1+(boxes(k,1)-1)*block_size; x = 1+(boxes(k,2)*block_size-1);
                if y>1 && y+block_size+1<n(1) && x+block_size-1<n(2)
                    B(y-2,x:x+block_size-1,:) = B(y-3,x:x+block_size-1,:)*0.3+B(y-2,x:x+block_size-1,:)*0.4+B(y-1,x:x+block_size-1,:)*0.3;
                    B(y-1,x:x+block_size-1,:) = B(y-2,x:x+block_size-1,:)*0.3+B(y-1,x:x+block_size-1,:)*0.4+B(y,x:x+block_size-1,:)*0.3;
                    B(y,x:x+block_size-1,:) = B(y-1,x:x+block_size-1,:)*0.3+B(y,x:x+block_size-1,:)*0.4+B(y+1,x:x+block_size-1,:)*0.3;
                    B(y+1,x:x+block_size-1,:) = B(y,x:x+block_size-1,:)*0.3+B(y+1,x:x+block_size-1,:)*0.4+B(y+2,x:x+block_size-1,:)*0.3;

                    B(y+block_size-3,x:x+block_size-1,:) = B(y+block_size-4,x:x+block_size-1,:)*0.3+B(y+block_size-3,x:x+block_size-1,:)*0.4+B(y+block_size-2,x:x+block_size-1,:)*0.3;
                    B(y+block_size-2,x:x+block_size-1,:) = B(y+block_size-3,x:x+block_size-1,:)*0.3+B(y+block_size-2,x:x+block_size-1,:)*0.4+B(y+block_size-1,x:x+block_size-1,:)*0.3;
                    B(y+block_size-1,x:x+block_size-1,:) = B(y+block_size-2,x:x+block_size-1,:)*0.3+B(y+block_size-1,x:x+block_size-1,:)*0.4+B(y+block_size,x:x+block_size-1,:)*0.3;
                    B(y+block_size,x:x+block_size-1,:) = B(y+block_size-1,x:x+block_size-1,:)*0.3+B(y+block_size,x:x+block_size-1,:)*0.4+B(y+block_size+1,x:x+block_size-1,:)*0.3;
                end
                if x>1 && x+block_size+1<n(2)
                    B(y:y+block_size-1,x-2,:) = B(y:y+block_size-1,x-3,:)*0.3+B(y:y+block_size-1,x-2,:)*0.4+B(y:y+block_size-1,x-1,:)*0.3;
                    B(y:y+block_size-1,x-1,:) = B(y:y+block_size-1,x-2,:)*0.3+B(y:y+block_size-1,x-1,:)*0.4+B(y:y+block_size-1,x,:)*0.3;
                    B(y:y+block_size-1,x,:) = B(y:y+block_size-1,x-1,:)*0.3+B(y:y+block_size-1,x,:)*0.4+B(y:y+block_size-1,x+1,:)*0.3;
                    B(y:y+block_size-1,x+1,:) = B(y:y+block_size-1,x,:)*0.3+B(y:y+block_size-1,x+1,:)*0.4+B(y:y+block_size-1,x+2,:)*0.3;

                    B(y:y+block_size-1,x+block_size-3,:) = B(y:y+block_size-1,x+block_size-4,:)*0.3+B(y:y+block_size-1,x+block_size-3,:)*0.4+B(y:y+block_size-1,x+block_size-2,:)*0.3;
                    B(y:y+block_size-1,x+block_size-2,:) = B(y:y+block_size-1,x+block_size-3,:)*0.3+B(y:y+block_size-1,x+block_size-2,:)*0.4+B(y:y+block_size-1,x+block_size-1,:)*0.3;
                    B(y:y+block_size-1,x+block_size-1,:) = B(y:y+block_size-1,x+block_size-2,:)*0.3+B(y:y+block_size-1,x+block_size-1,:)*0.4+B(y:y+block_size-1,x+block_size,:)*0.3;
                    B(y:y+block_size-1,x+block_size,:) = B(y:y+block_size-1,x+block_size-1,:)*0.3+B(y:y+block_size-1,x+block_size,:)*0.4+B(y:y+block_size-1,x+block_size+1,:)*0.3;
                end
            end
            D = B;
        end
        toooc = [toooc toc];    % 3
        M2 = M;
        figure(2); image(C); axis equal;
        title(['frame:',num2str(i*16+j),' last:',num2str(m)]);
        pause(0.01);
        figure(3); image(D); axis equal;
        title(['frame:',num2str(i*16+j),' last:',num2str(m)]);
        pause(0.01);

        name3 = [path3 name_out num2str(i*16+j)];
        imwrite(C,[name3 '.jpg']);
        name4 = [path3 name_out2 num2str(i*16+j)];
        imwrite(D,[name4 '.jpg']);
        toooc = [toooc toc];    % 4
        toooc
    end
end
    