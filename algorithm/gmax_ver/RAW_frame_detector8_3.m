clear all
% path = 'H:\test3\'; m = 799;
% path = 'H:\test4\'; m = 809;
path = 'H:\test5\'; m = 812;
% path = 'H:\test6\'; m = 526;
mkdir(path,'HD3');
path2 = [path 'HD3\'];
mkdir(path,'RGBin');
path3 = [path 'RGBin\'];
name = 'frame_';

ext = '.tiff';

omin = 32;
block_size = 32;
m = block_size*fix(m/block_size)-1;

km = 2; ks = 5; ks2 = 3;
margin = 0;
SOF = 1;
areas = [];
V = 5.8;
video_out = 1;
frate = 5;

name_scene = [path2 name '_scene.mat'];

pixel = [1 1; 1 2; 1 3; 1 4; 2 1; 2 2; 2 3; 2 4; 3 1; 3 2; 3 3; 3 4; 4 1; 4 2; 4 3; 4 4];

surf = [];
    
for i = 0:16:m
    tic
    toooc = [];
    name2 = [name num2str(i)];
    A0 = imread([path name2 ext]);
    if i==0
        n = size(A0); n = block_size*fix(n/block_size); n(3) = 3; n2 = n/block_size; n2(3) = 3;
    end
    A0 = A0(1:n(1),1:n(2));
    A0_RGB = raw2rgb2(A0);
    if video_out==1
        name2 = [name num2str(i)];
        name2 = [path3 name2 '_RGB'];
        imwrite(uint8(A0_RGB/16),[name2 '.jpg']);
    end
    DIT = A0_RGB;
    toooc = toc;    % 1
    
    minA = A0; maxA = A0;
    for j = 1:15
        name2 = [name num2str(i+j)];
    %read RAW data
        A = imread([path name2 ext]);
        A = A(1:n(1),1:n(2));
    %convert to RGB
        A_RGB = raw2rgb2(A);
	    if video_out==1
            name2 = [path3 name2 '_RGB'];
            imwrite(uint8(A_RGB/16),[name2 '.jpg']);
        end
    %dithering        
        y = pixel(j+1,1); x = pixel(j+1,2);
        DIT(y:4:end,x:4:end,:) = A_RGB(y:4:end,x:4:end,:);
    %minmax
        minA = min(A,minA); maxA = max(A,maxA);
        eval(['A' num2str(j) '_RGB = A_RGB;']);
    end
    %detected movement
    deltaA = double(maxA-minA);
    ddA = deltaA(1:end/2,:);    % g�rne 50% obrazu
    mA = mean(ddA(:)); sA = std(ddA(:));    
    th = km*mA+ks2*sA;
    masks = deltaA>th;
    toooc = [toooc toc];    % 2
    
    %selection of blocks for scalling
    maseczka = uint8(deltaA); maseczka(:) = 0;
    boxes = [];
    mody = [16 8 4 2 1];
    modx = [1 2 4 8 16];
    for y = 1:n2(1)
        for x = 1:n2(2)
            S = masks(1+(y-1)*block_size:y*block_size,1+(x-1)*block_size:x*block_size);
            S = double(S);
            S = sum(S(:));
            if S>omin
                boxes = [boxes; y x 0 0];
                maseczka(1+(y-1)*block_size:y*block_size,1+(x-1)*block_size:x*block_size) = 127;
            end
        end
    end
    toooc = [toooc toc];    % 3

    surface = 0;
    Sblock = block_size^2;
    %select optimal scalling
    for k = 1:size(boxes,1)
        y = boxes(k,1); x = boxes(k,2);
        S = A0_RGB(1+(y-1)*block_size:y*block_size,1+(x-1)*block_size:x*block_size,:);
        %scalling 16x1
        S16x1 = imresize(S,[block_size/16 block_size],'bicubic'); S16x1 = imresize(S16x1,[block_size block_size],'bicubic'); S16x1 = abs(double(S)-double(S16x1)); S16x1 = sum(S16x1(:));
        %scalling 8x2
        S8x2 = imresize(S,[block_size/8 block_size/2],'bicubic'); S8x2 = imresize(S8x2,[block_size block_size],'bicubic'); S8x2 = abs(double(S)-double(S8x2)); S8x2 = sum(S8x2(:));
        %scalling 4x4
        S4x4 = imresize(S,[block_size/4 block_size/4],'bicubic'); S4x4 = imresize(S4x4,[block_size block_size],'bicubic'); S4x4 = abs(double(S)-double(S4x4)); S4x4 = sum(S4x4(:));
        %scalling 2x8
        S2x8 = imresize(S,[block_size/2 block_size/8],'bicubic'); S2x8 = imresize(S2x8,[block_size block_size],'bicubic'); S2x8 = abs(double(S)-double(S2x8)); S2x8 = sum(S2x8(:));
        %scalling 1x16
        S1x16 = imresize(S,[block_size block_size/16],'bicubic'); S1x16 = imresize(S1x16,[block_size block_size],'bicubic'); S1x16 = abs(double(S)-double(S1x16)); S1x16 = sum(S1x16(:));
        Errors = [S16x1 S8x2 S4x4 S2x8 S1x16]; Errors = Errors/sum(Errors);
        Emin = min(Errors); metoda = find(Errors==Emin);
        boxes(k,3) = metoda(1);
        surface = surface + Sblock;
    end
    toooc = [toooc toc];    % 4

    areas{fix(i/16+1)} = boxes;
    surf = [surf; fix(i/16+1) surface];
    
% scalling part    
    tframe = DIT;
    
    for k = 1:size(boxes,1)
        for j = 0:15                                % place reduced windows with moving objects taken from input frames into single HD3 frame
            eval(['A = A' num2str(j) '_RGB;']);         % A = Aj;
            if boxes(k,3)==1
                dy = block_size/16; dx = block_size;
            elseif boxes(k,3)==2
                dy = block_size/8; dx = block_size/2;
            elseif boxes(k,3)==3
                dy = block_size/4; dx = block_size/4;
            elseif boxes(k,3)==4
                dy = block_size/2; dx = block_size/8;
            else
                dy = block_size; dx = block_size/16;
            end
            object_bmp = imresize(A(1+(boxes(k,1)-1)*block_size:boxes(k,1)*block_size,1+(boxes(k,2)-1)*block_size:boxes(k,2)*block_size,:),[dy,dx],'bicubic');
            x = 1 + mod(j,modx(boxes(k,3)));
            y = 1 + fix(j/modx(boxes(k,3)));
            tframe(1+(boxes(k,1)-1)*block_size+(y-1)*dy:(boxes(k,1)-1)*block_size+y*dy,1+(boxes(k,2)-1)*block_size+(x-1)*dx:(boxes(k,2)-1)*block_size+x*dx,:) = object_bmp;

%             figure(3); image(uint8(tframe(boxes(k,1)*32:boxes(k,1)*32+31,boxes(k,2)*32:boxes(k,2)*32+31,:)/16));
        end
    end
% end of scalling part    
    toooc = [toooc toc];    % 5
    
    
%     RGB = imadjust(A_RGB,[],[],0.5)/4;
    RGB = uint8(A_RGB/16);
    tframe = uint8(tframe/16);
    maseczka(:,:,2) = maseczka(:,:,1);
    maseczka(:,:,3) = maseczka(:,:,1);
    RGB2 = tframe/2+maseczka;

    figure(1); image(tframe); axis equal;
    title(['frame no: ' num2str(i) '   th= ' num2str(th)]);
    pause(0.01);
    
    figure(2); image(RGB2); axis equal;
    title(['frame no: ' num2str(i) '   surface= ' num2str(surface/(n(1)*n(2)))]);
    pause(0.01);
    toooc = [toooc toc];    % 7
    
    if video_out==1
        name3 = [path2 name '_hd3_' num2str(i)];
        imwrite(tframe,[name3 '.tiff']);
        name4 = [path2 name '_composite_' num2str(i)];
        imwrite(RGB2,[name4 '.jpg']);
    end
    toooc = [toooc toc];    %8
    toooc
end
save(name_scene,'name','n','m','areas','surf','km','ks2','omin','block_size');
