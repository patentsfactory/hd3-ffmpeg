function RGB = raw2rgb2(A)

    n = size(A);
    n(3) = 3;
    
    RGB = A; RGB(:,:,2) = A; RGB(:,:,3) = A;
    RGB = [zeros(n(1),1,3,'uint16') RGB zeros(n(1),1,3,'uint16')];
    RGB = [zeros(1,n(2)+2,3,'uint16'); RGB; zeros(1,n(2)+2,3,'uint16')];
    
    A = [zeros(n(1),1,'uint16') A zeros(n(1),1,'uint16')];
    A = [zeros(1,n(2)+2,'uint16'); A; zeros(1,n(2)+2,'uint16')];
    
    RGB(2:2:end-1,2:2:end-1,1) = A(3:2:end-1,2:2:end-1)/2+A(1:2:end-3,2:2:end-1)/2;
    RGB(2:2:end-1,2:2:end-1,3) = A(2:2:end-1,3:2:end-1)/2+A(2:2:end-1,1:2:end-3)/2;

    RGB(2:2:end-1,3:2:end-1,1) = A(3:2:end,2:2:end-1)/4+A(3:2:end,4:2:end)/4+A(1:2:end-3,2:2:end-1)/4+A(1:2:end-3,4:2:end)/4;
    RGB(2:2:end-1,3:2:end-1,2) = A(3:2:end,3:2:end)/4+A(1:2:end-3,3:2:end)/4+A(2:2:end-1,2:2:end-1)/4+A(2:2:end-1,4:2:end)/4;
    
    RGB(3:2:end,2:2:end-1,2) = A(2:2:end-1,2:2:end-1)/4+A(4:2:end,2:2:end-1)/4+A(3:2:end,1:2:end-2)/4+A(3:2:end,3:2:end)/4;
    RGB(3:2:end,2:2:end-1,3) = A(2:2:end-1,3:2:end)/4+A(2:2:end-1,1:2:end-3)/4+A(4:2:end,3:2:end)/4+A(4:2:end,1:2:end-2)/4;
    
    RGB(3:2:end,3:2:end-1,1) = A(3:2:end,2:2:end-1)/2+A(3:2:end,4:2:end)/2;
    RGB(3:2:end,3:2:end-1,3) = A(2:2:end-1,3:2:end)/2+A(4:2:end,3:2:end)/2;
    
    RGB(end,:,:) = []; RGB(1,:,:) = []; RGB(:,end,:) = []; RGB(:,1,:) = [];
end
