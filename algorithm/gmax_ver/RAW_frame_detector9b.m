clear all
tic
% path = 'F:\';
% path = 'D:\HD3_tests\gmax\';
path = 'D:\HD3_tests\';

% path = [path 'test3\']; m = 799;
% path = [path 'test4\']; m = 809;
% path = [path 'test5\'; m = 812;
% path = [path 'test6\']; m = 526;
% path = [path 'tele3\']; m = 3599;
path = [path 'test3\']; m = 799;

name = 'frame_';
% name = 'tele3_';

ext = '.tiff';

mkdir(path,'HD3');
path2 = [path 'HD3\'];

omin = 32;            % minimalna ilo�� pikseli w bloku wskazuj�ca na obecno�� ruchu
reduction = 16;       % ilo�� ramek oryginalnych w ramce HD3
block_size = 64;      % rozmiar bloku:32 lub 64 piksele w poziomie i pionie
m = reduction*fix((m+1)/reduction)-1;
b2 = block_size^2;

km = 2; ks = 5;
areas = [];
video_out = 1;
frate = 5;

name_scene = [path2 name '_scene.mat'];

pixel = [1 1; 1 2; 1 3; 1 4; 2 1; 2 2; 2 3; 2 4; 3 1; 3 2; 3 3; 3 4; 4 1; 4 2; 4 3; 4 4];

surf = [];
parpool;
mr = fix(m/reduction);
    
for iii = 0:mr
    i = iii*reduction;
    name2 = [name num2str(i)];
    A0 = imread([path name2 ext]);
    if i==0
        n = size(A0); n = block_size*fix(n/block_size); n(3) = 3; n2 = n/block_size; n2(3) = 3;
    end
    A0 = A0(1:n(1),1:n(2));
    A0_RGB = raw2rgb2(A0);
    DIT = A0_RGB;
    
    minA = A0; maxA = A0;
    for j = 1:reduction-1
        name2 = [name num2str(i+j)];
    %read RAW data
        A = imread([path name2 ext]);
        A = A(1:n(1),1:n(2));
    %convert to RGB
        A_RGB = raw2rgb2(A);
% 	    if video_out==1
%             name2 = [path3 name2 '_RGB'];
%             imwrite(uint8(A_RGB/16),[name2 '.jpg']);
%         end
    %dithering        
        y = pixel(j+1,1); x = pixel(j+1,2);
        DIT(y:4:end,x:4:end,:) = A_RGB(y:4:end,x:4:end,:);
    %minmax
        minA = min(A,minA); maxA = max(A,maxA);
        eval(['A' num2str(j) '_RGB = A_RGB;']);
    end
    %detected movement
    deltaA = double(maxA-minA);
    ddA = deltaA(1:end/2,:);    % g�rne 50% obrazu
    mA = mean(ddA(:)); sA = std(ddA(:));    
    th = km*mA+ks*sA;
    masks = deltaA>th;
    
    %selection of blocks for scalling
    maseczka = uint8(deltaA); maseczka(:) = 0;
    boxes = [];
    mody = [16 8 4 2 1];
    modx = [1 2 4 8 16];
    ss = zeros(n2(1:2));
    for y = 1:n2(1)
        for x = 1:n2(2)
            S = masks(1+(y-1)*block_size:y*block_size,1+(x-1)*block_size:x*block_size);
            S = double(S);
            S = sum(S(:));
            ss(y,x) = S;
            if S>omin
                boxes = [boxes; y x 0];
                maseczka(1+(y-1)*block_size:y*block_size,1+(x-1)*block_size:x*block_size) = 127;
            elseif (x>1) && (y>1) && (x<n2(2)) && (y<n2(1))
            % zakwalifikuj blok jako ruch je�li wybrani s�siedzi s� zakwalifikowani jako ruch
                if (ss(y-1,x)>omin) && (ss(y,x-1)>omin) && ((ss(y-1,x+1)>omin) || (ss(y,x+1)>omin))
                    ss(y,x) = b2+1;
                    boxes = [boxes; y x 0];
                    maseczka(1+(y-1)*block_size:y*block_size,1+(x-1)*block_size:x*block_size) = 64;
                end
            end
        end
    end

    for y = n2(1):-1:1
        for x = n2(2):-1:1
            if (ss(y,x)<omin) && (x>1) && (y>1) && (x<n2(2)) && (y<n2(1))
            % zakwalifikuj blok jako ruch je�li wybrani s�siedzi s� zakwalifikowani jako ruch
                if (ss(y-1,x)>omin) && (ss(y,x+1)>omin) && ((ss(y-1,x-1)>omin) || (ss(y,x-1)>omin))
                    ss(y,x) = b2+1;
                    boxes = [boxes; y x 0];
                    maseczka(1+(y-1)*block_size:y*block_size,1+(x-1)*block_size:x*block_size) = 64;
                end
            end
        end
    end

    
    surface = 0;
    %select optimal scalling
    parfor k = 1:size(boxes,1)
        wers = boxes(k,:);
        y = wers(1); x = wers(2);
        S = A0_RGB(1+(y-1)*block_size:y*block_size,1+(x-1)*block_size:x*block_size,:);
        %scalling 16x1
        S16x1 = imresize(S,[block_size/16 block_size],'bicubic'); S16x1 = imresize(S16x1,[block_size block_size],'bicubic'); S16x1 = abs(double(S)-double(S16x1)); S16x1 = sum(S16x1(:));
        %scalling 8x2
        S8x2 = imresize(S,[block_size/8 block_size/2],'bicubic'); S8x2 = imresize(S8x2,[block_size block_size],'bicubic'); S8x2 = abs(double(S)-double(S8x2)); S8x2 = sum(S8x2(:));
        %scalling 4x4
        S4x4 = imresize(S,[block_size/4 block_size/4],'bicubic'); S4x4 = imresize(S4x4,[block_size block_size],'bicubic'); S4x4 = abs(double(S)-double(S4x4)); S4x4 = sum(S4x4(:));
        %scalling 2x8
        S2x8 = imresize(S,[block_size/2 block_size/8],'bicubic'); S2x8 = imresize(S2x8,[block_size block_size],'bicubic'); S2x8 = abs(double(S)-double(S2x8)); S2x8 = sum(S2x8(:));
        %scalling 1x16
        S1x16 = imresize(S,[block_size block_size/16],'bicubic'); S1x16 = imresize(S1x16,[block_size block_size],'bicubic'); S1x16 = abs(double(S)-double(S1x16)); S1x16 = sum(S1x16(:));
        Errors = [S16x1 S8x2 S4x4 S2x8 S1x16]; Errors = Errors/sum(Errors);
        Emin = min(Errors); metoda = find(Errors==Emin);
        wers(3) = metoda(1);
        surface = surface + b2;
        boxes(k,:) = wers;
    end

    areas{fix(i/16+1)} = boxes;
    surf = [surf; fix(i/16+1) surface];
    
% scalling part    
    tframe = DIT;
    
    for k = 1:size(boxes,1)
        wers = boxes(k,:);
        for j = 0:reduction-1                                % place reduced windows with moving objects taken from input frames into single HD3 frame
            eval(['A = A' num2str(j) '_RGB;']);         % A = Aj;
            if wers(3)==1
                dy = block_size/16; dx = block_size;
            elseif wers(3)==2
                dy = block_size/8; dx = block_size/2;
            elseif wers(3)==3
                dy = block_size/4; dx = block_size/4;
            elseif wers(3)==4
                dy = block_size/2; dx = block_size/8;
            else
                dy = block_size; dx = block_size/16;
            end
            object_bmp = imresize(A(1+(wers(1)-1)*block_size:wers(1)*block_size,1+(wers(2)-1)*block_size:wers(2)*block_size,:),[dy,dx],'bicubic');
            x = 1 + mod(j,modx(wers(3)));
            y = 1 + fix(j/modx(wers(3)));
            tframe(1+(wers(1)-1)*block_size+(y-1)*dy:(wers(1)-1)*block_size+y*dy,1+(wers(2)-1)*block_size+(x-1)*dx:(wers(2)-1)*block_size+x*dx,:) = object_bmp;
        end
    end
% end of scalling part    
    
    
%     RGB = imadjust(A_RGB,[],[],0.5)/4;
    RGB = uint8(A_RGB/16);
    tframe = uint8(tframe/16);
    maseczka(:,:,2) = maseczka(:,:,1);
    maseczka(:,:,3) = maseczka(:,:,1);
    RGB2 = tframe/2+maseczka;

    name3 = [path2 name '_hd3_' num2str(i)];
    imwrite(tframe,[name3 '.tiff']);
    name4 = [path2 name '_composite_' num2str(i)];
    imwrite(RGB2,[name4 '.jpg']);
end
delete(gcp('nocreate'))
save(name_scene,'name','n','m','areas','surf','km','ks','omin','block_size');
toc

