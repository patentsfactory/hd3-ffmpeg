function B = imdensity(A)
% compute number of details in the image
n = size(A);
A1 = A(:,1:n(2)-2,:);
A2 = A(:,3:n(2),:);
dX = (double(A2)-double(A1))/2;
% dX = [zeros(n(1),1,3) dX zeros(n(1),1,3)];
dX = [dX zeros(n(1),1,3) zeros(n(1),1,3)];
A1 = A(1:n(1)-2,:,:);
A2 = A(3:n(1),:,:);
dY = (double(A2)-double(A1))/2;
% dY = [zeros(1,n(2),3); dY; zeros(1,n(2),3)];
dY = [dY; zeros(1,n(2),3); zeros(1,n(2),3)];
dXY = abs(dX)+abs(dY);
B = double(A);
B(:) = 0;
B(3:n(1)-2,3:n(2)-2,:) = dXY(1:n(1)-4,1:n(2)-4,:);
B(3:n(1)-2,3:n(2)-2,:) = B(3:n(1)-2,3:n(2)-2,:) + dXY(1:n(1)-4,2:n(2)-3,:);
B(3:n(1)-2,3:n(2)-2,:) = B(3:n(1)-2,3:n(2)-2,:) + dXY(1:n(1)-4,3:n(2)-2,:);
B(3:n(1)-2,3:n(2)-2,:) = B(3:n(1)-2,3:n(2)-2,:) + dXY(1:n(1)-4,4:n(2)-1,:);
B(3:n(1)-2,3:n(2)-2,:) = B(3:n(1)-2,3:n(2)-2,:) + dXY(1:n(1)-4,5:n(2),:);

B(3:n(1)-2,3:n(2)-2,:) = B(3:n(1)-2,3:n(2)-2,:) + dXY(2:n(1)-3,1:n(2)-4,:);
B(3:n(1)-2,3:n(2)-2,:) = B(3:n(1)-2,3:n(2)-2,:) + dXY(2:n(1)-3,2:n(2)-3,:);
B(3:n(1)-2,3:n(2)-2,:) = B(3:n(1)-2,3:n(2)-2,:) + dXY(2:n(1)-3,3:n(2)-2,:);
B(3:n(1)-2,3:n(2)-2,:) = B(3:n(1)-2,3:n(2)-2,:) + dXY(2:n(1)-3,4:n(2)-1,:);
B(3:n(1)-2,3:n(2)-2,:) = B(3:n(1)-2,3:n(2)-2,:) + dXY(2:n(1)-3,5:n(2),:);

B(3:n(1)-2,3:n(2)-2,:) = B(3:n(1)-2,3:n(2)-2,:) + dXY(3:n(1)-2,1:n(2)-4,:);
B(3:n(1)-2,3:n(2)-2,:) = B(3:n(1)-2,3:n(2)-2,:) + dXY(3:n(1)-2,2:n(2)-3,:);
B(3:n(1)-2,3:n(2)-2,:) = B(3:n(1)-2,3:n(2)-2,:) + dXY(3:n(1)-2,3:n(2)-2,:);
B(3:n(1)-2,3:n(2)-2,:) = B(3:n(1)-2,3:n(2)-2,:) + dXY(3:n(1)-2,4:n(2)-1,:);
B(3:n(1)-2,3:n(2)-2,:) = B(3:n(1)-2,3:n(2)-2,:) + dXY(3:n(1)-2,5:n(2),:);

B(3:n(1)-2,3:n(2)-2,:) = B(3:n(1)-2,3:n(2)-2,:) + dXY(4:n(1)-1,1:n(2)-4,:);
B(3:n(1)-2,3:n(2)-2,:) = B(3:n(1)-2,3:n(2)-2,:) + dXY(4:n(1)-1,2:n(2)-3,:);
B(3:n(1)-2,3:n(2)-2,:) = B(3:n(1)-2,3:n(2)-2,:) + dXY(4:n(1)-1,3:n(2)-2,:);
B(3:n(1)-2,3:n(2)-2,:) = B(3:n(1)-2,3:n(2)-2,:) + dXY(4:n(1)-1,4:n(2)-1,:);
B(3:n(1)-2,3:n(2)-2,:) = B(3:n(1)-2,3:n(2)-2,:) + dXY(4:n(1)-1,5:n(2),:);

B(3:n(1)-2,3:n(2)-2,:) = B(3:n(1)-2,3:n(2)-2,:) + dXY(5:n(1),1:n(2)-4,:);
B(3:n(1)-2,3:n(2)-2,:) = B(3:n(1)-2,3:n(2)-2,:) + dXY(5:n(1),2:n(2)-3,:);
B(3:n(1)-2,3:n(2)-2,:) = B(3:n(1)-2,3:n(2)-2,:) + dXY(5:n(1),3:n(2)-2,:);
B(3:n(1)-2,3:n(2)-2,:) = B(3:n(1)-2,3:n(2)-2,:) + dXY(5:n(1),4:n(2)-1,:);
B(3:n(1)-2,3:n(2)-2,:) = B(3:n(1)-2,3:n(2)-2,:) + dXY(5:n(1),5:n(2),:);

B(1,:,:) = B(3,:,:);
B(2,:,:) = B(3,:,:);
B(n(1),:,:) = B(n(1)-2,:,:);
B(n(1)-1,:,:) = B(n(1)-2,:,:);

B(:,1,:) = B(:,3,:);
B(:,2,:) = B(:,3,:);
B(:,n(2),:) = B(:,n(2)-2,:);
B(:,n(2)-1,:) = B(:,n(2)-2,:);

B = uint8(B/25);
end