function [mmin, mmax, smin, smax, maxmean] = stats2(A,n)
    rrr = 2*rand(n);
    A = A+rrr;
    mm = zeros(16);
    ss = mm;
    nn = 16*(n(1:2)/16-fix(n(1:2)/16));
    A = A(1:end-nn(1),1:end-nn(2),:);
    n = size(A);
    for y = 1:16
        for x = 1:16
            AA = A(1+(y-1)*n(1)/16:y*n(1)/16,1+(x-1)*n(2)/16:x*n(2)/16,:);
            mm(y,x) = mean(AA(:));
            ss (y,x) = std(AA(:));
        end
    end
    maxmean = mean(mm(:));
    mmm = mm(:);
    ll = mmm==0;
    mmm(ll) = [];
    sss = ss(:);
    ll = sss==0;
    sss(ll) = [];
    mmm = sort(mmm);
    sss = sort(sss);
    mmin = mean(mmm(1:8));
    smin = mean(sss(1:8));
    mmm = sort(mmm,'descend');
    sss = sort(sss,'descend');
    mmax = mean(mmm(1:8));
    smax = mean(sss(1:8));
end

 