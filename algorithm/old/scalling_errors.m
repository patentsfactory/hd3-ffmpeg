function SE = scalling_errors(A,ky,kx,th)
    % compute scalling errors of image A with scalling ratios kx,ky
    %th is threshold level 
    % th = 0.35;
    n = size(A);
    %scalling down and up in X axis
    AAx0 = imresize(A,[n(1) n(2)/kx],'bicubic'); AAx0 = imresize(AAx0,[n(1) n(2)],'bicubic');
    %compare with original and select maximum error
    AAx0 = abs(A-AAx0); AAx0 = max(max(AAx0(:,:,1),AAx0(:,:,2)),AAx0(:,:,3));
    %is error bigger than 1?
    AAx = double(AAx0>1);
    %reduce resolution 8 times in X and Y axis summing values
    AAx = AAx(:,1:2:end)+AAx(:,2:2:end); AAx = AAx(:,1:2:end)+AAx(:,2:2:end); AAx = AAx(:,1:2:end)+AAx(:,2:2:end);
    AAx = AAx(1:2:end,:)+AAx(2:2:end,:); AAx = AAx(1:2:end,:)+AAx(2:2:end,:); AAx = AAx(1:2:end,:)+AAx(2:2:end,:);
    %divide by 64 (8*8) to normalize results [0..1]
    AAx = AAx/64;
    %compare normalized value with threshold
    AAx = AAx>th;

    %scalling down and up in Y axis
    AAy0 = imresize(A,[n(1)/ky n(2)],'bicubic'); AAy0 = imresize(AAy0,[n(1) n(2)],'bicubic');
    %compare with original and select maximum error
    AAy0 = abs(A-AAy0); AAy0 = max(max(AAy0(:,:,1),AAy0(:,:,2)),AAy0(:,:,3));
    %is error bigger than 1?
    AAy = double(AAy0>1);
    %reduce resolution 8 times in X and Y axis summing values
    AAy = AAy(:,1:2:end)+AAy(:,2:2:end); AAy = AAy(:,1:2:end)+AAy(:,2:2:end); AAy = AAy(:,1:2:end)+AAy(:,2:2:end);
    AAy = AAy(1:2:end,:)+AAy(2:2:end,:); AAy = AAy(1:2:end,:)+AAy(2:2:end,:); AAy = AAy(1:2:end,:)+AAy(2:2:end,:);
    %divide by 64 (8*8) to normalize results [0..1]
    AAy = AAy/64;
    %compare normalized value with threshold
    AAy = AAy>th;

    %create output matrix with errors in X axis placed on R component,
    %errors on Y axis placed on G component and conjunction of both on B component
    SE = zeros(n,'logical');
    for y = 1:8
        for x = 1:8
            SE(y:8:end,x:8:end,1) = AAx;
            SE(y:8:end,x:8:end,2) = AAy;
            SE(y:8:end,x:8:end,3) = AAx & AAy;
        end
    end
end
