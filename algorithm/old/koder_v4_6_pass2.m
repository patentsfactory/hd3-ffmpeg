function [out_name, areas] = koder_v4_6_pass2(names, GRR, LRR)
    name_scene = [names '_scene.mat'];
    load(name_scene);   %input boxes data
    areas = areas2';

    v = VideoReader(name);
    fstart = 1;
    fstop = v.NumberOfFrames;     % number of frames in movie
    frate = v.FrameRate;

    back = find(names=='\');
    namem = names(back(end)+1:end);
    back = find(namem=='_');
    namem = namem(1:back(1)-1);

    out_name = [names '_L' num2str(LRR(1)) 'x' num2str(LRR(2))  '_hd3.avi'];
    v4 = VideoWriter(out_name,'Motion JPEG AVI');
    v4.FrameRate = frate;
    v4.Quality = 100;
    open(v4);
    
%     name3 = [names '_maska.avi'];
%     v5 = VideoReader(name3);
%     mstop = v5.NumberOfFrames;     % number of frames in movie

    name_boxes = [names '_boxes.mat'];

%     grr = GRR(1)*GRR(2); %global redution ratio
    grr = GRR(1);
%     lrr = LRR(1)*LRR(2); %local reduction ratio
    lrr = LRR(1);
    dframe = GRR(1)*GRR(2);       %number of frames taken to analysis
    
    %indexes for columns in boxes matrix
    ymin = 1; xmin = 2; ymax = 3; xmax = 4; deltay = 5; deltax = 6; dy = 7; dx = 8; lry = 9; lrx = 10; methode = 11;
    
    B = read(v,fstart);
    n = size(B); % input resolution
%     if n(1)/GRR(1)>fix(n(1)/GRR(1)) || n(2)/GRR(2)>fix(n(2)/GRR(2))
    if n(1)/(GRR(1)*8)>fix(n(1)/(GRR(1)*8)) || n(2)/(GRR(2)*8)>fix(n(2)/(GRR(2)*8))
        unmatched = 1;
    else
        unmatched = 0;
    end
    if unmatched==1
        n(1) = GRR(1)*8*fix(n(1)/(GRR(1)*8));
        n(2) = GRR(2)*8*fix(n(2)/(GRR(2)*8));
        B = B(1:n(1),1:n(2),:);
    end
    tframe = B;
    divider = grr/lrr;
    delta = fstop-fstart+1-dframe;
    delta = fix(delta/grr);
    delta = grr*delta;
    fstop = size(areas,1);

    methodes = {'nearest'; 'bicubic'};
    thZ = 5;      %level of selection of interpolation method
    kk = 3;
    grr = 6;
    
%     pat6 = [1 2; 2 1; 1 3; 2 2; 1 1; 2 3];  % pattern for 6 frames
    pat6 = [2 1; 1 2; 3 1; 2 2; 1 1; 3 2];  % pattern for 6 frames
    
    baza = [];
%     for i = fstart:grr:fstart+delta
    for i = fstart:grr:fstop
        boxes2 = areas{i};  %read boxes data from file genereted during pass1
        if ~isempty(boxes2)
            if max(boxes2(:,1))>size(B,1) || max(boxes2(:,3))>size(B,1)
                kk = kk;
            end
        end
        for j = 0:grr-1    % creates n matrixes named A1..An where n=dframe
            eval(['B' num2str(j) ' = read(v,i+j);']);  %Bj = read(v,i+j);
            eval(['if unmatched==1 B' num2str(j) '=B' num2str(j) '(1:n(1),1:n(2),:); end']);
        end
        for j = 0:grr-1     % create single HD3 frame containig pixels from grr input frames
            eval(['B = B' num2str(j) ';']); % B = Bj;
%             x = 1 + mod(j,GRR(2));
%             y = 1 + fix(j/GRR(2));
            x = pat6(j+1,2);
            y = pat6(j+1,1);
            tframe(y:GRR(1):n(1),x:GRR(2):n(2),:) = B(y:GRR(1):n(1),x:GRR(2):n(2),:);
        end
        figure(1); image(tframe);

        if size(boxes2,1)>0 && sum(boxes2(1,1:end-1))>0
            AAx0 = imresize(B0,[n(1) n(2)/3],'bicubic'); AAx0 = imresize(AAx0,[n(1) n(2)],'bicubic');
            AAx0 = abs(B0-AAx0); AAx0 = max(max(AAx0(:,:,1),AAx0(:,:,2)),AAx0(:,:,3));
            AAx = double(AAx0>1);
            AAx = AAx(:,1:2:end)+AAx(:,2:2:end); AAx = AAx(:,1:2:end)+AAx(:,2:2:end); AAx = AAx(:,1:2:end)+AAx(:,2:2:end);
            AAx = AAx(1:2:end,:)+AAx(2:2:end,:); AAx = AAx(1:2:end,:)+AAx(2:2:end,:); AAx = AAx(1:2:end,:)+AAx(2:2:end,:);
            AAx = AAx/64;
            AAx = AAx>0.35;

            AAy0 = imresize(B0,[n(1)/3 n(2)],'bicubic'); AAy0 = imresize(AAy0,[n(1) n(2)],'bicubic');
            AAy0 = abs(B0-AAy0); AAy0 = max(max(AAy0(:,:,1),AAy0(:,:,2)),AAy0(:,:,3));
            AAy = double(AAy0>1);
            AAy = AAy(:,1:2:end)+AAy(:,2:2:end); AAy = AAy(:,1:2:end)+AAy(:,2:2:end); AAy = AAy(:,1:2:end)+AAy(:,2:2:end);
            AAy = AAy(1:2:end,:)+AAy(2:2:end,:); AAy = AAy(1:2:end,:)+AAy(2:2:end,:); AAy = AAy(1:2:end,:)+AAy(2:2:end,:);
            AAy = AAy/64;
            AAy = AAy>0.35;

            AAxy = double(AAx);
            AAxy(:,:,2) = double(AAy);
            AAxy(:,:,3) = double(AAx & AAy);
            AAx8 = zeros(n(1:2));
            AAy8 = zeros(n(1:2));
            AAxy8 = zeros(n(1:2));
            for y = 1:8
                for x = 1:8
                    AAx8(y:8:end,x:8:end,:) = double(AAx);
                    AAy8(y:8:end,x:8:end,:) = double(AAy);
                    AAxy8(y:8:end,x:8:end,:) = double(AAx & AAy);
                end
            end
        end
        
        if size(boxes2,1)>0 && sum(boxes2(1,1:end-1))>0     %if there is movement in current input frame then
            for k = 1:size(boxes2,1)    %for every box in current input frame
%                 eval(['BB = B' num2str(j) '(boxes2(k,ymin):boxes2(k,ymax),boxes2(k,xmin):boxes2(k,xmax),:);']);
                BB = B0(boxes2(k,ymin):boxes2(k,ymax),boxes2(k,xmin):boxes2(k,xmax),:);
                nn = size(BB);
                
                mx = AAx8(boxes2(k,ymin):boxes2(k,ymax),boxes2(k,xmin):boxes2(k,xmax));
                my = AAy8(boxes2(k,ymin):boxes2(k,ymax),boxes2(k,xmin):boxes2(k,xmax));
                mxy = AAxy8(boxes2(k,ymin):boxes2(k,ymax),boxes2(k,xmin):boxes2(k,xmax));
                mx = mean(mx(:));
                my = mean(my(:));
                mxy = mean(mxy(:));
                
                if mxy < 0.4
                    method = 2;    %bicubic if smooth areas detected
                else
                    method = 1;    %nearest if rough areas detected
                end
                
                if my > mx
                    kkx = 3; kky = 2;
                else
                    kkx = 2; kky = 3;
                end
                    
                boxes2(k,deltay) = boxes2(k,ymax) - boxes2(k,ymin) +1;  %compute difference between max and min in Y axis
                boxes2(k,dy) = ceil(boxes2(k,deltay)/kky);              %round it up
                if boxes2(k,dy)*kky>boxes2(k,deltay)                    %if rounded is bigger then
                    boxes2(k,deltay) = boxes2(k,dy)*kky;                %correct difference to rounded
                    boxes2(k,ymax) = boxes2(k,ymin) + boxes2(k,deltay) -1;  %compute new max value
                    if boxes2(k,ymax)>n(1)                              %correct max value if bigger than image size
                        boxes2(k,ymax) = n(1);
                        boxes2(k,ymin) = n(1)-boxes2(k,deltay) +1;
                    end
                end
                boxes2(k,deltax) = boxes2(k,xmax) - boxes2(k,xmin) +1;  %compute diference between max and min for X axis
                boxes2(k,dx) = ceil(boxes2(k,deltax)/kkx);              %round it up
                if boxes2(k,dx)*kkx>boxes2(k,deltax)                    %if rounded is bigger then
                    boxes2(k,deltax) = boxes2(k,dx)*kkx;                %correct difference to rounded
                    boxes2(k,xmax) = boxes2(k,xmin) + boxes2(k,deltax) -1;  %compute new max value
                    if boxes2(k,xmax)>n(2)                              %correct max value if bigger than image size
                        boxes2(k,xmax) = n(2);
                        boxes2(k,xmin) = n(2)-boxes2(k,deltax) +1;
                    end
                end
                boxes2(k,lry) = kky;
                boxes2(k,lrx) = kkx;
                boxes2(k,methode) = method;
                for j = 0:kky*kkx-1     % place reduced windows with moving objects taken from input frames into single HD3 frame
                    eval(['B = B' num2str(j) ';']); % B = Bj;
                    x = 1 + mod(j,kkx);
                    y = 1 + fix(j/kkx);
                    object_bmp = imresize(B(boxes2(k,ymin):boxes2(k,ymax),boxes2(k,xmin):boxes2(k,xmax),:),[boxes2(k,dy) boxes2(k,dx)],methodes{boxes2(k,methode)});
                    tframe(boxes2(k,ymin)+(y-1)*boxes2(k,dy):boxes2(k,ymin)+y*boxes2(k,dy)-1,boxes2(k,xmin)+(x-1)*boxes2(k,dx):boxes2(k,xmin)+x*boxes2(k,dx)-1,:) = object_bmp;
                end
            end
        end
        baza{i}.boxes = boxes2;

        writeVideo(v4,tframe);

        figure(1); image(tframe);
        title([namem ' Pass2 G',num2str(grr),' L',num2str(LRR(1)),'x',num2str(LRR(2)),' frame:',num2str(i),' last:',num2str(fstop),'  boxes:',num2str(size(boxes2,1))]);
        pause(0.01);
        if i==1255
            zzz = 1;
        end
    end
    close(v4);
    save(name_boxes,'baza');
end
