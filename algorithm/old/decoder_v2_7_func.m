function [out_file] = decoder_v2_7_func(name, names ,GRR, LRR) 
    v = VideoReader(name);
    fstart = 1;
    fstop = v.NumberOfFrames;     % number of frames in movie
    frate = v.FrameRate;
    
    back = find(names=='\');
    namem = names(back(end)+1:end);
    back = find(namem=='_');
    namem = namem(1:back(1)-1);

    name_scene = [names '_boxes.mat'];
    load(name_scene);
    baza = baza';

    grr = GRR(1)*GRR(2); %global redution ratio
    lrr = LRR(1)*LRR(2); %local reduction ratio
    ratio = GRR./LRR;
    ratio2 = ratio(1)*ratio(2);
    baza = baza(1:grr:end);

    out_file = [names '_L' num2str(LRR(1)) 'x' num2str(LRR(2)) '_reconstructed.avi'];

    v2 = VideoWriter(out_file,'Motion JPEG AVI');
    v2.FrameRate = frate;
    v2.Quality = 100;
    open(v2);

    A = read(v,fstart);
    n = size(A); % input resolution
    nn = n(1)*n(2);
    
    %indexes for columns in boxes matrix
    ymin = 1; xmin = 2; ymax = 3; xmax = 4; deltay = 5; deltax = 6; dy = 7; dx = 8; lry = 9; lrx = 10; methode = 11;
    methodes = {'nearest'; 'bicubic'};
    
    pat6 = [2 1; 1 2; 3 1; 2 2; 1 1; 3 2];  % pattern for 6 frames

    nframe = 1;
    for i =fstart:fstop
        B = read(v,i);
%         boxes = areas{i};
        boxes = baza{i};
        boxes = boxes.boxes;

        figure(1); image(B);
        title(['Coded G',num2str(grr),' L',num2str(LRR(1)),'x',num2str(LRR(2)),' ramka nr:',num2str(i),' ostatnia:',num2str(fstop)]);
        pause(0.01);

        if i==1
            C = B;
            M = C>0;
            M(:) = 0;
        end
        
        for j = 0:grr-1
%             x = 1 + mod(j,GRR(2));
%             y = 1 + fix(j/GRR(2));
            x = pat6(j+1,2);
            y = pat6(j+1,1);
            C(y:GRR(1):n(1),x:GRR(2):n(2),:) = B(y:GRR(1):n(1),x:GRR(2):n(2),:);

            if size(boxes,1)>0 && sum(boxes(1,1:end-1))>0
                M = C>0;
                M(:) = 0;
                if i+j==fstart
                    M2 = M;
                end
                for k = 1:size(boxes,1)
                    M(boxes(k,ymin):boxes(k,ymax),boxes(k,xmin):boxes(k,xmax),:) = 1;   %mask

                    j2 = fix(j*(boxes(k,lry)*boxes(k,lrx))/grr);
                    x2 = 1 + mod(j2,boxes(k,lrx));
                    y2 = 1 + fix(j2/boxes(k,lrx));
                    object_bmp = imresize(B(boxes(k,ymin)+(y2-1)*boxes(k,dy):boxes(k,ymin)+y2*boxes(k,dy)-1,boxes(k,xmin)+(x2-1)*boxes(k,dx):boxes(k,xmin)+x2*boxes(k,dx)-1,:),[boxes(k,deltay) boxes(k,deltax)],methodes{boxes(k,methode)});
                    C(boxes(k,ymin):boxes(k,ymax),boxes(k,xmin):boxes(k,xmax),:) = object_bmp;
                end
                MM = M2 & ~M;	%mask showing where objects was and no longer are
                C(MM) = B(MM);	%copy new data to places where objects no longer are
            end
            M2 = M;
            figure(4); image(C);
            title([namem ' Decode G',num2str(grr),' L',num2str(LRR(1)),'x',num2str(LRR(2)),' frame:',num2str(nframe),' last:',num2str(fstop*grr)]);
            pause(0.01);

            writeVideo(v2,C);
            nframe = nframe +1;
        end
    end
    close(v2);
end