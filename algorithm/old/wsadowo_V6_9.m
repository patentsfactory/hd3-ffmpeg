clear all

V = 6.8;            %version number
file = 9;          %select file for convertion
gop = 15;
sof = 6;
th2 = 0.35;         %threshold for surface type detection (higher than th2 => flora or high detail surface)
    
if file==1
    name = 'D:\HD3_tests\IPTECNO\IPTECNO.mp4';
    omin = 32;          %ignore objects smaller than that value
    edges_level = 0.03;   %reduce edges in mask - higher value : more edged
    mix = 20;           %multiplier for threshold
    margin = 8;         %add such margin to detected objects
    gamma = 0.66;
elseif file==2
    name = 'D:\HD3_tests\P1000027\P1000027.mp4';
    omin = 64;          %ignore objects smaller than that value
    edges_level = 0.03;   %reduce edges in mask - higher value : more edged
    mix = 35;           %multiplier for threshold
    margin = 8;         %add such margin to detected objects
    gamma = 0.66;
elseif file==3
    name = 'D:\HD3_tests\Office\Office.mp4';
    omin = 64;          %ignore objects smaller than that value
    edges_level = 0.03;   %reduce edges in mask - higher value : more edged
    mix = 40;           %multiplier for threshold
    margin = 8;         %add such margin to detected objects
    gamma = 0.66;
elseif file==4
    name = 'D:\HD3_tests\Epic\Epic_4k.mp4';
    omin = 64;          %ignore objects smaller than that value
    edges_level = 0.03;   %reduce edges in mask - higher value : more edged
    mix = 30;           %multiplier for threshold
    margin = 8;         %add such margin to detected objects
    gamma = 0.66;
elseif file==5
    name = 'D:\HD3_tests\indoor\indoor.mp4';
    omin = 32;          %ignore objects smaller than that value
    edges_level = 0.03;   %reduce edges in mask - higher value : more edged
    mix = 30;           %multiplier for threshold
    margin = 8;         %add such margin to detected objects
    gamma = 0.66;
elseif file==6
    name = 'D:\HD3_tests\vintage_aircraft\vintage_aircraft.mp4';
    omin = 64;          %ignore objects smaller than that value
    edges_level = 0.03;   %reduce edges in mask - higher value : more edged
    mix = 30;           %multiplier for threshold
    margin = 8;         %add such margin to detected objects
    gamma = 0.66;
elseif file==7
    name = 'D:\HD3_tests\P3500594\P3500594.MP4';
    omin = 32;          %ignore objects smaller than that value
    edges_level = 0.03;   %reduce edges in mask - higher value : more edged
    mix = 20;           %multiplier for threshold
    margin = 8;         %add such margin to detected objects
    gamma = 0.66;
elseif file==8
    name = 'D:\HD3_tests\P3500595\P3500595.MP4';
    omin = 32;          %ignore objects smaller than that value
    edges_level = 0.03;   %reduce edges in mask - higher value : more edged
    mix = 30;           %multiplier for threshold
    margin = 8;         %add such margin to detected objects
    gamma = 0.66;
elseif file==9
    name = 'D:\HD3_tests\P3500597\P3500597.MP4';
    omin = 64;          %ignore objects smaller than that value
    edges_level = 0.03;   %reduce edges in mask - higher value : more edged
    mix = 30;           %multiplier for threshold
    margin = 8;         %add such margin to detected objects
    gamma = 0.66;
elseif file==10
    name = 'D:\HD3_tests\Waterfall\Waterfall.MP4';
    omin = 32;          %ignore objects smaller than that value
    edges_level = 0.03;   %reduce edges in mask - higher value : more edged
    mix = 15;           %multiplier for threshold
    margin = 8;         %add such margin to detected objects
    gamma = 1.0;
elseif file==11
    name = 'D:\HD3_tests\Rice\Rice.MP4';
    omin = 32;          %ignore objects smaller than that value
    edges_level = 0.03;   %reduce edges in mask - higher value : more edged
    mix = 20;           %multiplier for threshold
    margin = 8;         %add such margin to detected objects
    gamma = 1.0;
elseif file==12
    name = 'D:\HD3_tests\low_illumination\Low_Illumination.mp4';
    omin = 32;          %ignore objects smaller than that value
    edges_level = 0.03;   %reduce edges in mask - higher value : more edged
    mix = 15;           %multiplier for threshold
    margin = 8;         %add such margin to detected objects
    gamma = 0.5;
end

back = find(name=='\');
names = name(back(end)+1:end-4);
names2 = [name(1:end-4) '_V' num2str(V) '_mx' num2str(mix) '_GOP' num2str(gop) '_gm' num2str(gamma) '_SOF' num2str(sof)];
back = find(names2=='.');
names2(back) = '_';
name_var = [names2 '_variables.mat'];

vv = V;
ff = file;
p1 = 0;
if p1 == 0 load(name_var); end
    V = vv;
    file = ff;
    GOP = gop;           %Group of Pictures
    SOF = sof;            %Set of Frames
    pass1 = p1;
    clear vv ff gop sof p1
    filter = 0;
    descriptor = 1;
    pass2 = 0;
    decoder = 0;
if pass1 == 0 save(name_var); end

if pass1==1
    disp([names ' ' num2str(SOF) ' ' num2str(GOP) ' encoder pass1']);
    tic
    [pass1_file, areas_pass1] = koder_v5_7_pass1(name, omin, edges_level, mix, V, margin, gamma, GOP, SOF, th2);
    save(name_var);
    toc
    beep; pause(0.5); beep; pause(0.5); beep;
end
if filter==1
    disp([names ' ' num2str(SOF) ' ' num2str(GOP) ' GOP filter']);
    tic
    load(name_var);
    GOP_filter_1_1(pass1_file, SOF, GOP);
    save(name_var);
    toc
    beep; pause(0.5); beep; pause(0.5); beep;
end
if descriptor==1
    disp([names ' ' num2str(SOF) ' ' num2str(GOP) ' descriptor']);
    tic
    load(name_var);
    bbox_descriptor_v1_3(names2, SOF);
    toc
    beep; pause(0.5); beep; pause(0.5); beep;
end
if pass2==1
    disp([names ' ' num2str(SOF) ' ' num2str(GOP) ' encoder pass2']);
    tic
    load(name_var);
    GRR = [3 2]; LRR = [3 2];
    [pass2_file, areas_pass2] = koder_v4_6_pass2(pass1_file, GRR, LRR);
    save(name_var);
    toc
    beep; pause(0.5); beep; pause(0.5); beep;
end
if decoder==1
    disp([names ' ' num2str(SOF) ' ' num2str(GOP) ' decoder']);
    tic
    load(name_var);
    GRR = [3 2]; LRR = [3 2];
%     pass2_file = [pass2_file(1:end-3) 'mp4'];
    [output_file] = decoder_v2_7_func(pass2_file, pass1_file ,GRR, LRR);
    save(name_var);
    toc
    beep; pause(0.5); beep; pause(0.5); beep;
end

