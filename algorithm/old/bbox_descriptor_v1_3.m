function bbox_descriptor_v1_3(names2, SOF)
    name_scene = [names2 '_scene.mat'];
    name_txt = [names2 '_boxes.txt'];
    load(name_scene);   %input boxes data
    areas2 = areas2';
    
    file_ID = fopen(name_txt,'w');

    dframe = SOF;       %number of frames taken to analysis
    
    %indexes for columns in boxes matrix
    iymin = 1; ixmin = 2; iymax = 3; ixmax = 4; ideltay = 5; ideltax = 6; idy = 7; idx = 8; ilry = 9; ilrx = 10; imethod = 11; isurff = 12; idframe = 13; iscene_change = 14;
    
    fstop = size(areas2,1);

    k = 1;
    for i = 1:dframe:fstop
        
        boxes = areas2{i};  %read boxes data from file genereted during pass1
        if ~isempty(boxes)
            for j = 1:size(boxes,1)
                bbox = boxes(j,:);
                ymin = bbox(iymin);
                xmin = bbox(ixmin);
                ymax = bbox(iymax);
                xmax = bbox(ixmax);
                deltay = bbox(ideltay);
                deltax = bbox(ideltax);
                dy = bbox(idy);
                dx = bbox(idx);
                lry = bbox(ilry);
                lrx = bbox(ilrx);
                metoda = bbox(imethod);
                dframe = bbox(idframe);
                scene = bbox(iscene_change);
                box = ['frame = ' num2str(k) '; bbox = ' num2str(j) '; ymin = ' num2str(ymin) '; xmin = ' num2str(xmin) '; ymax = ' num2str(ymax) '; xmax = ' num2str(xmax) '; dy = ' num2str(dy) '; dx = ' num2str(dx) '; lry = ' num2str(lry) '; lrx = ' num2str(lrx) '; metoda = ' num2str(metoda) '; deltay = ' num2str(deltay) '; deltax = ' num2str(deltax) '; dframe = ' num2str(dframe) '; scene_change = ' num2str(scene) ';'];
                fprintf(file_ID,'%s\r\n', box);
            end
        end
        k = k+1;
    end
    fclose(file_ID);
end
