function GOP_filter_1_1(names, SOF, GOP)    %SOF - Set of Frames, GOP - Group of Pictures
    name_scene = [names '_scene.mat'];
    load(name_scene);   %input boxes data
    if size(areas,1)<size(areas,2)
        areas = areas';
    end
    areas2 = areas;
    for i = 1:size(areas2,1)
        areas2{i} = [];
    end

    v = VideoReader(name);
    fstart = 1;
    fstop = v.NumberOfFrames;     % number of frames in movie
    frate = v.FrameRate;

    name_boxes = [names '_boxes.mat'];

    dframe = SOF;       %number of frames taken to analysis
    
    %indexes for columns in boxes matrix
%     ymin = 1; xmin = 2; ymax = 3; xmax = 4; deltay = 5; deltax = 6; dy = 7; dx = 8;
%     lry = 9; lrx = 10; methode = 11; surff = 12; dframe = 13; scene_change = 14;
    
    B = read(v,fstart);
    n = size(B); % input resolution
    if n(1)/SOF>fix(n(1)/SOF) || n(2)/SOF>fix(n(2)/SOF)
        unmatched = 1;
    else
        unmatched = 0;
    end
    if unmatched==1
        n(1) = SOF*fix(n(1)/SOF);
        n(2) = SOF*fix(n(2)/SOF);
        B = B(1:n(1),1:n(2),:);
    end
    fstop = size(areas,1);

    msize = 16; %macroblock size - H264

    for i = 1:SOF*GOP:fstop
        B = read(v,i);
        if unmatched==1
            B = B(1:n(1),1:n(2),:);
        end
        
        ref = areas{i};
        if ~isempty(ref)
            ref = ref(1,:);
        else
            ref = zeros(1,14);
        end
        
        LLmask2 = zeros(n(1:2));
        
        if fstop-i<SOF*GOP
            last = fstop-i;
        else
            last = SOF*GOP-1;
        end
        for dd = 1:last
            BB = areas{i+dd};  %read boxes data from file genereted during pass1
            if ~isempty(BB)
                for j = 1:size(BB,1)
                    bb = BB(j,:);
                    LLmask2(bb(1):bb(3),bb(2):bb(4)) = LLmask2(bb(1):bb(3),bb(2):bb(4)) + 1;
                end
            end
        end
        
        LLmask2 = LLmask2>0;
        LLmask1 = LLmask2;
        
% analytical part        
        sss = regionprops(LLmask2,'BoundingBox');
        LLmask2 = zeros(n(1:2),'logical');
        for j = 1:size(sss,1)
            bb = sss(j);
            bb = round(bb.BoundingBox);
            xmin = bb(1);
            ymin = bb(2);
            xmax = xmin + bb(3) - 1;
            ymax = ymin + bb(4) - 1;
            LLmask2(ymin:ymax,xmin:xmax) = 1; % bondaries of objects
        end
        boxes = [];
        while true
            boxes2 = boxes;
            sss = regionprops(LLmask2,'BoundingBox');
            LLmask2 = zeros(n(1:2),'logical');
            boxes = [];
            for j = 1:size(sss,1)
                bb = sss(j);
                bb = round(bb.BoundingBox);
                xmin = bb(1);
                ymin = bb(2);
                xmax = xmin + bb(3) - 1;
                ymax = ymin + bb(4) - 1;
                if size(sss,1)>0
                    deltay = ymax - ymin +1;        %correction to make area the multiplication of SOF factor
                    dy = ceil(deltay/(16*SOF));
                    if dy*(16*SOF)>deltay
                        deltay = dy*(16*SOF);
                        ymax = ymin + deltay -1;
                        if ymax>n(1)
                            ymax = n(1);
                            ymin = n(1)-deltay +1;
                        end
                    end
                    deltax = xmax - xmin +1;        %correction to make area the multiplication of SOF factor
                    dx = ceil(deltax/(16*SOF));
                    if dx*(16*SOF)>deltax
                        deltax = dx*(16*SOF);
                        xmax = xmin + deltax -1;
                        if xmax>n(2)
                            xmax = n(2);
                            xmin = n(2)-deltax +1;
                        end
                    end
                    LLmask2(ymin:ymax,xmin:xmax) = 1;   % merged bondaries
                    surff = (ymax-ymin+1)*(xmax-xmin+1);
                    boxes = [boxes; ymin xmin ymax xmax deltay deltax dy dx ref(9) ref(10) ref(11) surff ref(13) ref(14)];
                end
            end
            if size(boxes2,1)==size(boxes,1)
                if isempty(boxes)
                    break
                end
                if boxes2==boxes
                    break
                end
            end
        end
        if ~isempty(boxes)
            if max(boxes(:,1))>size(B,1) || max(boxes(:,3))>size(B,1)
                n = n;
            end
        end
        boxes1 = boxes;
        boxes1(:,14) = 1;
        
        if last<SOF*GOP-1
            last = fix(last/SOF);
        else
            last = GOP-1;
        end
        for j = 0:last
            if j==0
                areas2{i+j*SOF} = boxes1;
            else
                areas2{i+j*SOF} = boxes;
            end
        end
        
        M = 255*double(LLmask2);
        M(:,:,2) = M(:,:,1);
        M(:,:,3) = 255*double(LLmask1);
        mmm = max(M(:));
        figure(1); image(uint8(double(B/2) + M/2));
        title(['   i = ',num2str(i),'  last = :',num2str(fstop)]);
%         title([namem ' Pass2 G',num2str(grr),' L',num2str(LRR(1)),'x',num2str(LRR(2)),' frame:',num2str(i),' last:',num2str(fstop),'  boxes:',num2str(size(boxes2,1))]);
        pause(2);
    end
    areas2 = areas2';
    areas = areas';
    save(name_scene,'name','n','areas','areas2');
end
