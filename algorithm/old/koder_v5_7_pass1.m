function [names, areas] = koder_v5_7_pass1(name, omin, edges_level, mix, V, margin, gamma, GOP, SOF, th2)
% fast version

%     gamma = 0.75;
    LUT = 0:1/255:1;
    LUT = LUT.^gamma;
    LUT = uint8(255*LUT);
    
    v = VideoReader(name);
    fstart = 1;
    fstop = v.NumberOfFrames;     % number of frames in movie
    frate = v.FrameRate;
    
    LRR = [6 6];          %12 because bonding boxes adjusted to 12 will be good also for 1, 2, 3, 4, 6
    lrr = 6;    %local reduction ratio
    dframe = SOF;           %number of frames taken to analysis
    
    lry = 0;
    lrx = 0;
    method = 0;

    fstop = SOF*fix(fstop/SOF);
    
    video_out = 1;
    
    back = find(name=='\');
    namem = name(back(end)+1:end-4);
    
    names = [name(1:end-4) '_V' num2str(V) '_mx' num2str(mix) '_GOP' num2str(GOP) '_gm' num2str(gamma) '_SOF' num2str(SOF)];
    back = find(names=='.');
    names(back) = '_';

    name2 = [names '_composite.avi'];
    v2 = VideoWriter(name2,'Motion JPEG AVI');
    v2.FrameRate = frate;
    v2.Quality = 100;
    if video_out==1
        open(v2);
    end

    name_scene = [names '_scene.mat'];

    A = read(v,fstart);
    n = size(A); % input resolution
    
    if n(1)/(SOF*8)>fix(n(1)/(SOF*8)) || n(2)/(SOF*8)>fix(n(2)/(SOF*8))
        unmatched = 1;
    else
        unmatched = 0;
    end
    if unmatched==1
        n(1) = SOF*8*fix(n(1)/(SOF*8));
        n(2) = SOF*8*fix(n(2)/(SOF*8));
        A = A(1:n(1),1:n(2),:);
    end
    nn = n(1)*n(2);
    dA = imdensity(A);

    statt = []; areas = []; mmesy = [];
    mmin = 0; mmax = 0; smin = 0; smax = 0; surf = 0;
    
    for j = 1:dframe    % creates n matrixes named A1..An where n=dframe
        eval(['A' num2str(j) ' = read(v,fstart+j-1);']);
        eval(['if unmatched==1 A' num2str(j) '=A' num2str(j) '(1:n(1),1:n(2),:); end']);
    end

    gmeans = [0 0];
% scan to find objects
%     for i =fstart:dframe:fstop
    scene_change = 0;
        i = fstart;
    while i<fstop
        B = read(v,i);
        B = LUT(B+1);
        if unmatched==1         B = B(1:n(1),1:n(2),:);         end
        dA = imdensity(B);
        SEB = scalling_errors(B,3,3,th2);

        if i>fstart
            current = B(1:4:end,1:4:end,:);
            delta = double(current)-double(last);
            delta2 = abs(delta);
            gmean = mean(delta(:));
            gmean2 = mean(delta2(:));
            gmeans = [gmeans; gmean gmean2];
            if gmean2>40
%                 dframe = 1;
                scene_change = 1;
            end
            last = current;
        else
            last = B(1:4:end,1:4:end,:);
        end

        for j = 2:dframe
            A = read(v,i+j-1);
            A = LUT(A+1);
            if unmatched==1         A = A(1:n(1),1:n(2),:);         end
            eval(['A' num2str(j-1) ' = A;']);
            
            current = A(1:4:end,1:4:end,:);
            delta = double(current)-double(last);
            delta2 = abs(delta);
            last = current;
            gmean = mean(delta(:));
            gmean2 = mean(delta2(:));
            gmeans = [gmeans; gmean gmean2];
            if gmean2>40
                dframe = j-1;
                scene_change = 1;
                break;
            end
        end
        if dframe<SOF
            eval(['A' num2str(j-1) ' = B;']);
        else
            eval(['A' num2str(j) ' = B;']);
        end
        di = dframe;

        minA = A1;
        maxA = A1;
        for j = 2:dframe
            eval(['minA = min(minA,A' num2str(j) ');']);
            eval(['maxA = max(maxA,A' num2str(j) ');']);
        end
        diff = abs(double(maxA)-double(minA));          % copute difference between min and max matrixes
        [mmin, mmax, smin, smax, maxmean] = stats2(diff,n);
        diff = abs(double(LUT(maxA+1))-double(LUT(minA+1)));    % copute difference between min and max matrixes with gamma correction
        if dframe==SOF
            eval(['dB = imdensity(A' num2str(dframe-1) ');']);
            eval(['SEA = scalling_errors(A' num2str(dframe-1) ',3,3,th2);']);
            SE = SEA | SEB;
        end
        fdelay = round(dframe/2);

        th = mmin+mix*smin;
        if i==fstart thlast = th; end
        if th>mmax th = thlast; end
        thlast = th;
        ddif = diff>th;
        ddif = ddif(:,:,1) | ddif(:,:,2) | ddif(:,:,3);
        surf = 100*sum(ddif(:))/nn;

        ddif_low = ddif;
        ddif_low(:,:,2) = ddif;
        ddif_low(:,:,3) = ddif;

        statt = [statt; mmin mmax smin smax surf 0];

        mask_low = ddif_low;

% analytical part        
        sss = regionprops(mask_low(:,:,1),'BoundingBox');
        LLmask2 = zeros(n(1:2),'logical');
        maxmm = 0;
        mmmm = i;
        mmmy = [];
        for j = 1:size(sss,1)
            bb = sss(j);
            bb = round(bb.BoundingBox);
            xmin = bb(1);
            ymin = bb(2);
            xmax = xmin + bb(3) - 1;
            ymax = ymin + bb(4) - 1;
            surff = (ymax-ymin+1)*(xmax-xmin+1);
            
            if surff>omin       %ignore objects smaller than omin
                aa = dA(ymin:ymax,xmin:xmax,:);
                bb = dB(ymin:ymax,xmin:xmax,:);
                maa = mean(double(aa(:)));
                mbb = mean(double(bb(:)));
                saa = std(double(aa(:)));
                sbb = std(double(bb(:)));
                if abs(max(saa,sbb)/min(saa,sbb)-1)<0.2
                    mab = maa/mbb;
                    aa = aa/mab;
                end
                dd = abs(double(aa)-double(bb));
                dd = double(dd>th);
                cc = double((aa/2 + bb/2)>th);
                if sum(cc(:))==0
                    mm = 0;
                else
                    mm = sum(dd(:))/(sum(cc(:)));
                end
                maxmm = max(maxmm,mm);
                if mm>edges_level
                    LLmask2(ymin:ymax,xmin:xmax) = 1; % bondaries of objects
                    mmmy = [mmmy; i ymin xmin ymax xmax mm];
                end
            end
        end
        
        sss = regionprops(LLmask2,'BoundingBox');
        LLmask2 = zeros(n(1:2),'logical');
        for j = 1:size(sss,1)
            if size(sss,1)>0
                bb = sss(j);
                bb = round(bb.BoundingBox);
                xmin = bb(1);
                ymin = bb(2);
                xmax = xmin + bb(3) - 1;
                ymax = ymin + bb(4) - 1;
                
                ymin = ymin - margin;
                if ymin<1
                    ymin = 1;
                end
                ymax = ymax + margin;
                if ymax>n(1)
                    ymax = n(1);
                end
                xmin = xmin - margin;
                if xmin<1
                    xmin = 1;
                end
                xmax = xmax + margin;
                if xmax>n(2)
                    xmax = n(2);
                end
                LLmask2(ymin:ymax,xmin:xmax) = 1; % bondaries of objects with margins
            end
        end
        boxes = [];
        while true
            boxes2 = boxes;
            sss = regionprops(LLmask2,'BoundingBox');
            LLmask2 = zeros(n(1:2),'logical');
            boxes = [];
            for j = 1:size(sss,1)
                if size(sss,1)>0
                    bb = sss(j);
                    bb = round(bb.BoundingBox);
                    xmin = bb(1);
                    ymin = bb(2);
                    xmax = xmin + bb(3) - 1;
                    ymax = ymin + bb(4) - 1;

                    deltay = ymax - ymin +1;        %correction to make area the multiplication of LRR factors
                    if deltay < 16*SOF
                        deltay = 16*SOF;
                    end
                    dy = ceil(deltay/(16*SOF));
                    if dy*16*SOF>deltay
                        deltay = dy*16*SOF;
                        ymax = ymin + deltay -1;
                        if ymax>n(1)
                            ymax = n(1);
                            ymin = n(1)-deltay +1;
                            if ymin<1
                                ymin = 1;
                            end
                        end
                    end
                    deltax = xmax - xmin +1;        %correction to make area the multiplication of LRR factors
                    if deltax < 16*SOF
                        deltax = 16*SOF;
                    end
                    dx = ceil(deltax/(16*SOF));
                    if dx*16*SOF>deltax
                        deltax = dx*16*SOF;
                        xmax = xmin + deltax -1;
                        if xmax>n(2)
                            xmax = n(2);
                            xmin = n(2)-deltax +1;
                            if xmin<1
                                xmin = 1;
                            end
                        end
                    end
                    surff = (ymax-ymin+1)*(xmax-xmin+1);
                    LLmask2(ymin:ymax,xmin:xmax) = 1;   % merged bondaries
                    
                    ee = SE(ymin:ymax,xmin:xmax,:);
                    eex = ee(:,:,1);
                    eey = ee(:,:,2);
                    eexy = ee(:,:,3);
                    lry = mean(eey(:));
                    lrx = mean(eex(:));
                    method = mean(eexy(:))>0.7;
                    boxes = [boxes; ymin xmin ymax xmax deltay deltax dy dx lry lrx method surff dframe scene_change];
                end
            end
            if size(boxes2,1)==size(boxes,1)
                if isempty(boxes)
                    break
                end
                if boxes2==boxes
                    break
                end
            end
        end
        scene_change = 0;

        if ~isempty(boxes)
            surf = 100*sum(boxes(:,12))/nn;
        else
            surf = 0;
        end
        statt(end,6) = surf;

        areas{i} = boxes;
        mmesy{i} = mmmy;
        
        maska = LLmask2;
        maska(:,:,2) = LLmask2;
        maska(:,:,3) = LLmask2;
% end of analytical part

        if di==SOF
            eval(['frame2 = A' num2str(fdelay) '/2 + uint8(127*maska);']);
        else
            frame2 = B;
        end
        dframe = SOF;

        figure(1); image(frame2);
        title([ namem ' Pass1 G',num2str(SOF),' frame:',num2str(i),' last:',num2str(fstop),'  boxes:',num2str(size(boxes,1)),'  maxmm:',num2str(maxmm)]);
        pause(0.01);
        figure(2); plot(statt);
        title([' mmin= ',num2str(mmin),' mmax= ',num2str(mmax),' smin= ',num2str(smin),'  smax= ',num2str(smax),'  surf= ',num2str(statt(end,6)),'  surf2= ',num2str(surf)]);
        pause(0.01);
        figure(3); plot(gmeans);
        title([' gmean = ',num2str(gmean),' gmean2 = ',num2str(gmean2)]);
        pause(0.01);

        if video_out==1
            writeVideo(v2,frame2);
%             writeVideo(v3,uint8(255*maska));
        end
        i = i + di;
    end
    if video_out==1
        close(v2);
%         close(v3);
    end
    save(name_scene,'name','n','statt','gmeans','areas','mmesy');
end
