/*
 * Copyright (c) 2013 Paul B Mahol
 *
 * This file is part of FFmpeg.
 *
 * FFmpeg is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * FFmpeg is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with FFmpeg; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include "libavutil/imgutils.h"
#include "libavutil/eval.h"
#include "libavutil/opt.h"
#include "libavutil/pixfmt.h"
#include "avfilter.h"
#include "bufferqueue.h"
#include "formats.h"
#include "internal.h"
#include "dualinput.h"
#include "video.h"
#include "blend.h"

#include "hd3/frame_copy.h"

#include <math.h>
#include <windows.h>
#include <float.h>

#define TOP    0
#define BOTTOM 1

extern void ff_blend_difference_ssse3(const uint8_t *top, ptrdiff_t top_linesize,
                             const uint8_t *bottom, ptrdiff_t bottom_linesize,
                             uint8_t *dst, ptrdiff_t dst_linesize,
                             ptrdiff_t width, ptrdiff_t height,
                             struct FilterParams *param, double *values, int starty);

static FrameCopy *edges0; //  kacziro - mod - only used with stereo
static FrameCopy *edges1;
static FrameCopy *errors;
static FrameCopy *corrected0;
static FrameCopy *corrected1;
static FrameCopy *distortionMap0;
static FrameCopy *distortionMap1;
static int *prevShift;

typedef struct BlendContext {
    const AVClass *class;
    FFDualInputContext dinput;
    int hsub, vsub;             ///< chroma subsampling values
    int nb_planes;
    char *all_expr;
    enum BlendMode all_mode;
    double all_opacity;

    FilterParams params[4];
    int tblend;
    AVFrame *prev_frame;        /* only used with tblend */

    FrameCopy edges0; // kacziro - mod - only used with stereo
    FrameCopy edges1;
    FrameCopy errors;
    FrameCopy corrected0;
    FrameCopy corrected1;
    FrameCopy distortionMap0;
    FrameCopy distortionMap1;
    int prevShift;
} BlendContext;

static const char *const var_names[] = {   "X",   "Y",   "W",   "H",   "SW",   "SH",   "T",   "N",   "A",   "B",   "TOP",   "BOTTOM",        NULL };
enum                                   { VAR_X, VAR_Y, VAR_W, VAR_H, VAR_SW, VAR_SH, VAR_T, VAR_N, VAR_A, VAR_B, VAR_TOP, VAR_BOTTOM, VAR_VARS_NB };

typedef struct ThreadData {
    const AVFrame *top, *bottom;
    AVFrame *dst;
    AVFilterLink *inlink;
    int plane;
    int w, h;
    FilterParams *param;
} ThreadData;

#define COMMON_OPTIONS \
    { "c0_mode", "set component #0 blend mode", OFFSET(params[0].mode), AV_OPT_TYPE_INT, {.i64=0}, 0, BLEND_NB-1, FLAGS, "mode"},\
    { "c1_mode", "set component #1 blend mode", OFFSET(params[1].mode), AV_OPT_TYPE_INT, {.i64=0}, 0, BLEND_NB-1, FLAGS, "mode"},\
    { "c2_mode", "set component #2 blend mode", OFFSET(params[2].mode), AV_OPT_TYPE_INT, {.i64=0}, 0, BLEND_NB-1, FLAGS, "mode"},\
    { "c3_mode", "set component #3 blend mode", OFFSET(params[3].mode), AV_OPT_TYPE_INT, {.i64=0}, 0, BLEND_NB-1, FLAGS, "mode"},\
    { "all_mode", "set blend mode for all components", OFFSET(all_mode), AV_OPT_TYPE_INT, {.i64=-1},-1, BLEND_NB-1, FLAGS, "mode"},\
    { "addition",   "", 0, AV_OPT_TYPE_CONST, {.i64=BLEND_ADDITION},   0, 0, FLAGS, "mode" },\
    { "addition128", "", 0, AV_OPT_TYPE_CONST, {.i64=BLEND_ADDITION128}, 0, 0, FLAGS, "mode" },\
    { "and",        "", 0, AV_OPT_TYPE_CONST, {.i64=BLEND_AND},        0, 0, FLAGS, "mode" },\
    { "average",    "", 0, AV_OPT_TYPE_CONST, {.i64=BLEND_AVERAGE},    0, 0, FLAGS, "mode" },\
    { "burn",       "", 0, AV_OPT_TYPE_CONST, {.i64=BLEND_BURN},       0, 0, FLAGS, "mode" },\
    { "darken",     "", 0, AV_OPT_TYPE_CONST, {.i64=BLEND_DARKEN},     0, 0, FLAGS, "mode" },\
    { "difference", "", 0, AV_OPT_TYPE_CONST, {.i64=BLEND_DIFFERENCE}, 0, 0, FLAGS, "mode" },\
    { "difference128", "", 0, AV_OPT_TYPE_CONST, {.i64=BLEND_DIFFERENCE128}, 0, 0, FLAGS, "mode" },\
    { "divide",     "", 0, AV_OPT_TYPE_CONST, {.i64=BLEND_DIVIDE},     0, 0, FLAGS, "mode" },\
    { "dodge",      "", 0, AV_OPT_TYPE_CONST, {.i64=BLEND_DODGE},      0, 0, FLAGS, "mode" },\
    { "exclusion",  "", 0, AV_OPT_TYPE_CONST, {.i64=BLEND_EXCLUSION},  0, 0, FLAGS, "mode" },\
    { "freeze",     "", 0, AV_OPT_TYPE_CONST, {.i64=BLEND_FREEZE},     0, 0, FLAGS, "mode" },\
    { "glow",       "", 0, AV_OPT_TYPE_CONST, {.i64=BLEND_GLOW},       0, 0, FLAGS, "mode" },\
    { "hardlight",  "", 0, AV_OPT_TYPE_CONST, {.i64=BLEND_HARDLIGHT},  0, 0, FLAGS, "mode" },\
    { "hardmix",    "", 0, AV_OPT_TYPE_CONST, {.i64=BLEND_HARDMIX},    0, 0, FLAGS, "mode" },\
    { "heat",       "", 0, AV_OPT_TYPE_CONST, {.i64=BLEND_HEAT},       0, 0, FLAGS, "mode" },\
    { "lighten",    "", 0, AV_OPT_TYPE_CONST, {.i64=BLEND_LIGHTEN},    0, 0, FLAGS, "mode" },\
    { "linearlight","", 0, AV_OPT_TYPE_CONST, {.i64=BLEND_LINEARLIGHT},0, 0, FLAGS, "mode" },\
    { "multiply",   "", 0, AV_OPT_TYPE_CONST, {.i64=BLEND_MULTIPLY},   0, 0, FLAGS, "mode" },\
    { "multiply128","", 0, AV_OPT_TYPE_CONST, {.i64=BLEND_MULTIPLY128},0, 0, FLAGS, "mode" },\
    { "negation",   "", 0, AV_OPT_TYPE_CONST, {.i64=BLEND_NEGATION},   0, 0, FLAGS, "mode" },\
    { "normal",     "", 0, AV_OPT_TYPE_CONST, {.i64=BLEND_NORMAL},     0, 0, FLAGS, "mode" },\
    { "or",         "", 0, AV_OPT_TYPE_CONST, {.i64=BLEND_OR},         0, 0, FLAGS, "mode" },\
    { "overlay",    "", 0, AV_OPT_TYPE_CONST, {.i64=BLEND_OVERLAY},    0, 0, FLAGS, "mode" },\
    { "phoenix",    "", 0, AV_OPT_TYPE_CONST, {.i64=BLEND_PHOENIX},    0, 0, FLAGS, "mode" },\
    { "pinlight",   "", 0, AV_OPT_TYPE_CONST, {.i64=BLEND_PINLIGHT},   0, 0, FLAGS, "mode" },\
    { "reflect",    "", 0, AV_OPT_TYPE_CONST, {.i64=BLEND_REFLECT},    0, 0, FLAGS, "mode" },\
    { "screen",     "", 0, AV_OPT_TYPE_CONST, {.i64=BLEND_SCREEN},     0, 0, FLAGS, "mode" },\
    { "softlight",  "", 0, AV_OPT_TYPE_CONST, {.i64=BLEND_SOFTLIGHT},  0, 0, FLAGS, "mode" },\
    { "subtract",   "", 0, AV_OPT_TYPE_CONST, {.i64=BLEND_SUBTRACT},   0, 0, FLAGS, "mode" },\
    { "vividlight", "", 0, AV_OPT_TYPE_CONST, {.i64=BLEND_VIVIDLIGHT}, 0, 0, FLAGS, "mode" },\
    { "xor",        "", 0, AV_OPT_TYPE_CONST, {.i64=BLEND_XOR},        0, 0, FLAGS, "mode" },\
    { "stereo",     "", 0, AV_OPT_TYPE_CONST, {.i64=BLEND_STEREO},     0, 0, FLAGS, "mode" },\
    { "black",      "", 0, AV_OPT_TYPE_CONST, {.i64=BLEND_BLACK},     0, 0, FLAGS, "mode" },\
    { "c0_expr",  "set color component #0 expression", OFFSET(params[0].expr_str), AV_OPT_TYPE_STRING, {.str=NULL}, CHAR_MIN, CHAR_MAX, FLAGS },\
    { "c1_expr",  "set color component #1 expression", OFFSET(params[1].expr_str), AV_OPT_TYPE_STRING, {.str=NULL}, CHAR_MIN, CHAR_MAX, FLAGS },\
    { "c2_expr",  "set color component #2 expression", OFFSET(params[2].expr_str), AV_OPT_TYPE_STRING, {.str=NULL}, CHAR_MIN, CHAR_MAX, FLAGS },\
    { "c3_expr",  "set color component #3 expression", OFFSET(params[3].expr_str), AV_OPT_TYPE_STRING, {.str=NULL}, CHAR_MIN, CHAR_MAX, FLAGS },\
    { "all_expr", "set expression for all color components", OFFSET(all_expr), AV_OPT_TYPE_STRING, {.str=NULL}, CHAR_MIN, CHAR_MAX, FLAGS },\
    { "c0_opacity",  "set color component #0 opacity", OFFSET(params[0].opacity), AV_OPT_TYPE_DOUBLE, {.dbl=1}, 0, 1, FLAGS },\
    { "c1_opacity",  "set color component #1 opacity", OFFSET(params[1].opacity), AV_OPT_TYPE_DOUBLE, {.dbl=1}, 0, 1, FLAGS },\
    { "c2_opacity",  "set color component #2 opacity", OFFSET(params[2].opacity), AV_OPT_TYPE_DOUBLE, {.dbl=1}, 0, 1, FLAGS },\
    { "c3_opacity",  "set color component #3 opacity", OFFSET(params[3].opacity), AV_OPT_TYPE_DOUBLE, {.dbl=1}, 0, 1, FLAGS },\
    { "all_opacity", "set opacity for all color components", OFFSET(all_opacity), AV_OPT_TYPE_DOUBLE, {.dbl=1}, 0, 1, FLAGS}

#define OFFSET(x) offsetof(BlendContext, x)
#define FLAGS AV_OPT_FLAG_FILTERING_PARAM|AV_OPT_FLAG_VIDEO_PARAM

static const AVOption blend_options[] = {
    COMMON_OPTIONS,
    { "shortest",    "force termination when the shortest input terminates", OFFSET(dinput.shortest), AV_OPT_TYPE_BOOL, {.i64=0}, 0, 1, FLAGS },
    { "repeatlast",  "repeat last bottom frame", OFFSET(dinput.repeatlast), AV_OPT_TYPE_BOOL, {.i64=1}, 0, 1, FLAGS },
    { NULL }
};

AVFILTER_DEFINE_CLASS(blend);

#define COPY(src)                                                            \
static void blend_copy ## src(const uint8_t *top, ptrdiff_t top_linesize,    \
                            const uint8_t *bottom, ptrdiff_t bottom_linesize,\
                            uint8_t *dst, ptrdiff_t dst_linesize,            \
                            ptrdiff_t width, ptrdiff_t height,               \
                            FilterParams *param, double *values, int starty) \
{                                                                            \
    av_image_copy_plane(dst, dst_linesize, src, src ## _linesize,            \
                        width, height);                                 \
}

COPY(top)
COPY(bottom)

#undef COPY

static void blend_normal_8bit(const uint8_t *top, ptrdiff_t top_linesize,
                              const uint8_t *bottom, ptrdiff_t bottom_linesize,
                              uint8_t *dst, ptrdiff_t dst_linesize,
                              ptrdiff_t width, ptrdiff_t height,
                              FilterParams *param, double *values, int starty)
{
    const double opacity = param->opacity;
    int i, j;

    for (i = 0; i < height; i++) {
        for (j = 0; j < width; j++) {
            dst[j] = top[j] * opacity + bottom[j] * (1. - opacity);
        }
        dst    += dst_linesize;
        top    += top_linesize;
        bottom += bottom_linesize;
    }
}

static void blend_normal_16bit(const uint8_t *_top, ptrdiff_t top_linesize,
                                  const uint8_t *_bottom, ptrdiff_t bottom_linesize,
                                  uint8_t *_dst, ptrdiff_t dst_linesize,
                                  ptrdiff_t width, ptrdiff_t height,
                                  FilterParams *param, double *values, int starty)
{
    const uint16_t *top = (uint16_t*)_top;
    const uint16_t *bottom = (uint16_t*)_bottom;
    uint16_t *dst = (uint16_t*)_dst;
    const double opacity = param->opacity;
    int i, j;
    dst_linesize /= 2;
    top_linesize /= 2;
    bottom_linesize /= 2;

    for (i = 0; i < height; i++) {
        for (j = 0; j < width; j++) {
            dst[j] = top[j] * opacity + bottom[j] * (1. - opacity);
        }
        dst    += dst_linesize;
        top    += top_linesize;
        bottom += bottom_linesize;
    }
}

#define DEFINE_BLEND8(name, expr)                                              \
static void blend_## name##_8bit(const uint8_t *top, ptrdiff_t top_linesize,         \
                                 const uint8_t *bottom, ptrdiff_t bottom_linesize,   \
                                 uint8_t *dst, ptrdiff_t dst_linesize,               \
                                 ptrdiff_t width, ptrdiff_t height,                \
                                 FilterParams *param, double *values, int starty) \
{                                                                              \
    double opacity = param->opacity;                                           \
    int i, j;                                                                  \
                                                                               \
    for (i = 0; i < height; i++) {                                             \
        for (j = 0; j < width; j++) {                                          \
            dst[j] = top[j] + ((expr) - top[j]) * opacity;                     \
        }                                                                      \
        dst    += dst_linesize;                                                \
        top    += top_linesize;                                                \
        bottom += bottom_linesize;                                             \
    }                                                                          \
}

#define DEFINE_BLEND16(name, expr)                                             \
static void blend_## name##_16bit(const uint8_t *_top, ptrdiff_t top_linesize,       \
                                  const uint8_t *_bottom, ptrdiff_t bottom_linesize, \
                                  uint8_t *_dst, ptrdiff_t dst_linesize,             \
                                  ptrdiff_t width, ptrdiff_t height,           \
                                  FilterParams *param, double *values, int starty)         \
{                                                                              \
    const uint16_t *top = (uint16_t*)_top;                                     \
    const uint16_t *bottom = (uint16_t*)_bottom;                               \
    uint16_t *dst = (uint16_t*)_dst;                                           \
    double opacity = param->opacity;                                           \
    int i, j;                                                                  \
    dst_linesize /= 2;                                                         \
    top_linesize /= 2;                                                         \
    bottom_linesize /= 2;                                                      \
                                                                               \
    for (i = 0; i < height; i++) {                                             \
        for (j = 0; j < width; j++) {                                          \
            dst[j] = top[j] + ((expr) - top[j]) * opacity;                     \
        }                                                                      \
        dst    += dst_linesize;                                                \
        top    += top_linesize;                                                \
        bottom += bottom_linesize;                                             \
    }                                                                          \
}

#define A top[j]
#define B bottom[j]

#define MULTIPLY(x, a, b) ((x) * (((a) * (b)) / 255))
#define SCREEN(x, a, b)   (255 - (x) * ((255 - (a)) * (255 - (b)) / 255))
#define BURN(a, b)        (((a) == 0) ? (a) : FFMAX(0, 255 - ((255 - (b)) << 8) / (a)))
#define DODGE(a, b)       (((a) == 255) ? (a) : FFMIN(255, (((b) << 8) / (255 - (a)))))

DEFINE_BLEND8(addition,   FFMIN(255, A + B))
DEFINE_BLEND8(addition128, av_clip_uint8(A + B - 128))
DEFINE_BLEND8(average,    (A + B) / 2)
DEFINE_BLEND8(subtract,   FFMAX(0, A - B))
DEFINE_BLEND8(multiply,   MULTIPLY(1, A, B))
DEFINE_BLEND8(multiply128,av_clip_uint8((A - 128) * B / 32. + 128))
DEFINE_BLEND8(negation,   255 - FFABS(255 - A - B))
DEFINE_BLEND8(difference, FFABS(A - B))
DEFINE_BLEND8(difference128, av_clip_uint8(128 + A - B))
DEFINE_BLEND8(screen,     SCREEN(1, A, B))
DEFINE_BLEND8(overlay,    (A < 128) ? MULTIPLY(2, A, B) : SCREEN(2, A, B))
DEFINE_BLEND8(hardlight,  (B < 128) ? MULTIPLY(2, B, A) : SCREEN(2, B, A))
DEFINE_BLEND8(hardmix,    (A < (255 - B)) ? 0: 255)
DEFINE_BLEND8(heat,       (A == 0) ? 0 : 255 - FFMIN(((255 - B) * (255 - B)) / A, 255))
DEFINE_BLEND8(freeze,     (B == 0) ? 0 : 255 - FFMIN(((255 - A) * (255 - A)) / B, 255))
DEFINE_BLEND8(darken,     FFMIN(A, B))
DEFINE_BLEND8(lighten,    FFMAX(A, B))
DEFINE_BLEND8(divide,     av_clip_uint8(B == 0 ? 255 : 255 * A / B))
DEFINE_BLEND8(dodge,      DODGE(A, B))
DEFINE_BLEND8(burn,       BURN(A, B))
DEFINE_BLEND8(softlight,  (A > 127) ? B + (255 - B) * (A - 127.5) / 127.5 * (0.5 - fabs(B - 127.5) / 255): B - B * ((127.5 - A) / 127.5) * (0.5 - fabs(B - 127.5)/255))
DEFINE_BLEND8(exclusion,  A + B - 2 * A * B / 255)
DEFINE_BLEND8(pinlight,   (B < 128) ? FFMIN(A, 2 * B) : FFMAX(A, 2 * (B - 128)))
DEFINE_BLEND8(phoenix,    FFMIN(A, B) - FFMAX(A, B) + 255)
DEFINE_BLEND8(reflect,    (B == 255) ? B : FFMIN(255, (A * A / (255 - B))))
DEFINE_BLEND8(glow,       (A == 255) ? A : FFMIN(255, (B * B / (255 - A))))
DEFINE_BLEND8(and,        A & B)
DEFINE_BLEND8(or,         A | B)
DEFINE_BLEND8(xor,        A ^ B)
DEFINE_BLEND8(vividlight, (A < 128) ? BURN(2 * A, B) : DODGE(2 * (A - 128), B))
DEFINE_BLEND8(linearlight,av_clip_uint8((B < 128) ? B + 2 * A - 255 : B + 2 * (A - 128)))

#undef MULTIPLY
#undef SCREEN
#undef BURN
#undef DODGE

#define MULTIPLY(x, a, b) ((x) * (((a) * (b)) / 65535))
#define SCREEN(x, a, b)   (65535 - (x) * ((65535 - (a)) * (65535 - (b)) / 65535))
#define BURN(a, b)        (((a) == 0) ? (a) : FFMAX(0, 65535 - ((65535 - (b)) << 16) / (a)))
#define DODGE(a, b)       (((a) == 65535) ? (a) : FFMIN(65535, (((b) << 16) / (65535 - (a)))))

DEFINE_BLEND16(addition,   FFMIN(65535, A + B))
DEFINE_BLEND16(addition128, av_clip_uint16(A + B - 32768))
DEFINE_BLEND16(average,    (A + B) / 2)
DEFINE_BLEND16(subtract,   FFMAX(0, A - B))
DEFINE_BLEND16(multiply,   MULTIPLY(1, A, B))
DEFINE_BLEND16(multiply128, av_clip_uint16((A - 32768) * B / 8192. + 32768))
DEFINE_BLEND16(negation,   65535 - FFABS(65535 - A - B))
DEFINE_BLEND16(difference, FFABS(A - B))
DEFINE_BLEND16(difference128, av_clip_uint16(32768 + A - B))
DEFINE_BLEND16(screen,     SCREEN(1, A, B))
DEFINE_BLEND16(overlay,    (A < 32768) ? MULTIPLY(2, A, B) : SCREEN(2, A, B))
DEFINE_BLEND16(hardlight,  (B < 32768) ? MULTIPLY(2, B, A) : SCREEN(2, B, A))
DEFINE_BLEND16(hardmix,    (A < (65535 - B)) ? 0: 65535)
DEFINE_BLEND16(heat,       (A == 0) ? 0 : 65535 - FFMIN(((65535 - B) * (65535 - B)) / A, 65535))
DEFINE_BLEND16(freeze,     (B == 0) ? 0 : 65535 - FFMIN(((65535 - A) * (65535 - A)) / B, 65535))
DEFINE_BLEND16(darken,     FFMIN(A, B))
DEFINE_BLEND16(lighten,    FFMAX(A, B))
DEFINE_BLEND16(divide,     av_clip_uint16(B == 0 ? 65535 : 65535 * A / B))
DEFINE_BLEND16(dodge,      DODGE(A, B))
DEFINE_BLEND16(burn,       BURN(A, B))
DEFINE_BLEND16(softlight,  (A > 32767) ? B + (65535 - B) * (A - 32767.5) / 32767.5 * (0.5 - fabs(B - 32767.5) / 65535): B - B * ((32767.5 - A) / 32767.5) * (0.5 - fabs(B - 32767.5)/65535))
DEFINE_BLEND16(exclusion,  A + B - 2 * A * B / 65535)
DEFINE_BLEND16(pinlight,   (B < 32768) ? FFMIN(A, 2 * B) : FFMAX(A, 2 * (B - 32768)))
DEFINE_BLEND16(phoenix,    FFMIN(A, B) - FFMAX(A, B) + 65535)
DEFINE_BLEND16(reflect,    (B == 65535) ? B : FFMIN(65535, (A * A / (65535 - B))))
DEFINE_BLEND16(glow,       (A == 65535) ? A : FFMIN(65535, (B * B / (65535 - A))))
DEFINE_BLEND16(and,        A & B)
DEFINE_BLEND16(or,         A | B)
DEFINE_BLEND16(xor,        A ^ B)
DEFINE_BLEND16(vividlight, (A < 32768) ? BURN(2 * A, B) : DODGE(2 * (A - 32768), B))
DEFINE_BLEND16(linearlight,av_clip_uint16((B < 32768) ? B + 2 * A - 65535 : B + 2 * (A - 32768)))

#define DEFINE_BLEND_EXPR(type, name, div)                                     \
static void blend_expr_## name(const uint8_t *_top, ptrdiff_t top_linesize,          \
                               const uint8_t *_bottom, ptrdiff_t bottom_linesize,    \
                               uint8_t *_dst, ptrdiff_t dst_linesize,                \
                               ptrdiff_t width, ptrdiff_t height,              \
                               FilterParams *param, double *values, int starty) \
{                                                                              \
    const type *top = (type*)_top;                                             \
    const type *bottom = (type*)_bottom;                                       \
    type *dst = (type*)_dst;                                                   \
    AVExpr *e = param->e;                                                      \
    int y, x;                                                                  \
    dst_linesize /= div;                                                       \
    top_linesize /= div;                                                       \
    bottom_linesize /= div;                                                    \
                                                                               \
    for (y = 0; y < height; y++) {                                             \
        values[VAR_Y] = y + starty;                                            \
        for (x = 0; x < width; x++) {                                          \
            values[VAR_X]      = x;                                            \
            values[VAR_TOP]    = values[VAR_A] = top[x];                       \
            values[VAR_BOTTOM] = values[VAR_B] = bottom[x];                    \
            dst[x] = av_expr_eval(e, values, NULL);                            \
        }                                                                      \
        dst    += dst_linesize;                                                \
        top    += top_linesize;                                                \
        bottom += bottom_linesize;                                             \
    }                                                                          \
}

DEFINE_BLEND_EXPR(uint8_t, 8bit, 1)
DEFINE_BLEND_EXPR(uint16_t, 16bit, 2)

static int filter_slice(AVFilterContext *ctx, void *arg, int jobnr, int nb_jobs)
{
    ThreadData *td = arg;
    int slice_start = (td->h *  jobnr   ) / nb_jobs;
    int slice_end   = (td->h * (jobnr+1)) / nb_jobs;
    int height      = slice_end - slice_start;
    const uint8_t *top    = td->top->data[td->plane];
    const uint8_t *bottom = td->bottom->data[td->plane];
    uint8_t *dst    = td->dst->data[td->plane];
    double values[VAR_VARS_NB];

    values[VAR_N]  = td->inlink->frame_count_out;
    values[VAR_T]  = td->dst->pts == AV_NOPTS_VALUE ? NAN : td->dst->pts * av_q2d(td->inlink->time_base);
    values[VAR_W]  = td->w;
    values[VAR_H]  = td->h;
    values[VAR_SW] = td->w / (double)td->dst->width;
    values[VAR_SH] = td->h / (double)td->dst->height;

    td->param->blend(top + slice_start * td->top->linesize[td->plane],
                     td->top->linesize[td->plane],
                     bottom + slice_start * td->bottom->linesize[td->plane],
                     td->bottom->linesize[td->plane],
                     dst + slice_start * td->dst->linesize[td->plane],
                     td->dst->linesize[td->plane],
                     td->w, height, td->param, &values[0], slice_start);
    return 0;
}
static void detect_distances(AVFrame *dst_frame, AVFrame *left_frame, AVFrame *right_frame) { // kacziro -  start here

}

//12cm - distance between cameras (Test_zary.mp4)
static void disparityShift(uint8_t *_left,  int left_linesize,
                           uint8_t *_right, int right_linesize,
                           uint8_t *_dst,   int dst_linesize,
                           int width,       int height) {

    uint16_t errors[12000];
    uint16_t edges[12000];
    uint16_t shifts[256];
    int vshift = 25;//30;//10;//
    memset(edges0->data[0], 0, height*edges0->linesize[0]);
    memset(shifts, 0, sizeof(shifts));
    for (int y = 0; y < height-vshift; y++) {
        int nb = 1;
        uint8_t *pL = _left + (y)*left_linesize;
        uint8_t *pR = _right + (y+vshift)*right_linesize;
        uint8_t *pD = _dst + y*dst_linesize;
        uint8_t *pE = edges0->data[0] + y*edges0->linesize[0];
        edges[0] = 0;
        for(int x = 1; x < width-1; x++) {
            int edge = abs(pR[x-1] - pR[x+1]);
            if (edge > 20) {
                pE[x] = edge;
                edges[nb] = x;
                nb++;
            }
        }
        edges[nb] = width;
        nb++;
        for (int i = 0; i < nb-1; i++) {
            float minE = 255;
            int maxShift = 40;
            int minShift = -40;
            int bestShift = maxShift;
            int sBegin = edges[i] + minShift > 0? minShift : -edges[i];
            memset(errors, 255, sizeof(errors));
            for (int shift = sBegin; shift < maxShift; shift++) {
                float sum = 0;
                int c = 0;
                for (int x = 1 + edges[i]; x < edges[i+1] - 1 && x + shift + 1 < width; x++) {
                    sum += abs(pR[x-1] + pR[x] + pR[x+1] - pL[x + shift - 1] - pL[x + shift] - pL[x + shift + 1]);
                    c++;
                }
                sum = sum/c;
                if (sum < minE){
                    minE = sum;
                    bestShift = shift;
                }
            }
            //printf("%d\n", bestShift);
            for (int x = edges[i]; x < edges[i+1]; x++) {
                int dist = 255;
                float bs = bestShift - minShift;
                if (x + bestShift < width && bestShift < maxShift){
                    dist = pL[x + bestShift];
                }
                //float bs = MAX(MIN(bestShift - minShift, 180), 100)-100;
                //bs /= 80;
                bs /= (maxShift - minShift);
                bs *= 255;
                pD[x] = (int32_t)bs;//bestShift + 128;//dist;//MIN(255, bestShift);//
                shifts[pD[x]]++;
            }
        }
    }
}
typedef struct StereoEdges {
    uint16_t nb;
    uint16_t values[6000];
    uint8_t usage[6000];

    uint8_t avgY[6000];
    uint8_t avgU[6000];
    uint8_t avgV[6000];
} StereoEdges;
typedef struct FindMatchArgs {
    int startY;
    int endY;
    AVFrame *left_frame;
    uint8_t *pRY;
    uint8_t *pRU;
    uint8_t *pRV;
    int rLen;
    uint16_t fragmentBegin;
    uint16_t fragmentEnd;
    StereoEdges *edgesL;
    //out
    float bestShift;
    int bestLe;
    int bestLy;
} FindMatchArgs;
static void blend_stereo_yuv420p_findMatch(FindMatchArgs *args) {// kacziro - todo - as thread
    double minE = 255;
    int rLen = args->rLen;
    uint16_t fragmentBegin = args->fragmentBegin;
    uint16_t fragmentEnd = args->fragmentEnd;
    uint8_t *pRY = args->pRY;
    uint8_t *pRU = args->pRU;
    uint8_t *pRV = args->pRV;

    for (int ly = args->startY; ly < args->endY; ly++) {
        StereoEdges lEdges = args->edgesL[ly];
        uint8_t *pLY = args->left_frame->data[0] + ly*args->left_frame->linesize[0];
        uint8_t *pLU = args->left_frame->data[1] + (ly/2)*args->left_frame->linesize[1];
        uint8_t *pLV = args->left_frame->data[2] + (ly/2)*args->left_frame->linesize[2];
        for (int le = 1; le < lEdges.nb; le++) {
            int lLen = lEdges.values[le] - lEdges.values[le-1];

            if (abs(lLen - rLen) < 160) {
                double e = abs(lLen - rLen)/5;
                if (lLen > rLen) {
                    float step = lLen/(float)rLen;
                    for (int i = 0; i < rLen; i++) {
                        e += abs(pRY[i + fragmentBegin] - pLY[(int)(i*step) + lEdges.values[le-1]]);
                    }
                    e /= rLen;
                } else {
                    float step = rLen/(float)lLen;
                    for (int i = 0; i < lLen; i++) {
                        e += abs(pRY[(int)(i*step) + fragmentBegin] - pLY[i + lEdges.values[le-1]]);
                    }
                    e /= lLen;
                }
                if (e < minE) {
                    args->bestShift = abs((fragmentEnd - rLen/2) - (lEdges.values[le] - lLen/2)); //+ abs(ry - ly)
                    minE = e;
                    args->bestLe = le;
                    args->bestLy = ly;
                }
            }
        }
    }
}
static void blend_stereo_yuv420p_v2(AVFrame *dst_frame, AVFrame *left_frame, const AVFrame *right_frame, int vshift) {
    int width = dst_frame->width;
    int height = dst_frame->height;
    StereoEdges *edgesR = malloc(sizeof(StereoEdges)*5000);
    StereoEdges *edgesL = malloc(sizeof(StereoEdges)*5000);

    memset(dst_frame->data[1], 128, dst_frame->linesize[1]*height/2);
    memset(dst_frame->data[2], 128, dst_frame->linesize[2]*height/2);

    int maxNb = 0;
    for (int y = 0; y < height; y++) {
        uint8_t *pRY = right_frame->data[0] + y*right_frame->linesize[0];
        uint8_t *pRU = right_frame->data[1] + (y/2)*right_frame->linesize[1];
        uint8_t *pRV = right_frame->data[2] + (y/2)*right_frame->linesize[2];
        uint8_t *pLY = left_frame->data[0]  + y*left_frame->linesize[0];
        uint8_t *pLU = left_frame->data[1]  + (y/2)*left_frame->linesize[1];
        uint8_t *pLV = left_frame->data[2]  + (y/2)*left_frame->linesize[2];
        int edgeTh = 13;
        float sumRY = 0;
        float sumRU = 0;
        float sumRV = 0;
        float sumLY = 0;
        float sumLU = 0;
        float sumLV = 0;

        edgesR[y].values[0] = 0;
        edgesL[y].values[0] = 0;
        edgesL[y].usage[0] = 0;
        edgesR[y].nb = 1;
        edgesL[y].nb = 1;
        for(int x = 1; x < width-1; x++) {
            int edge = abs(pRY[x-1] - pRY[x+1]) + abs(pRU[(x-1)/2] - pRU[(x+1)/2]) + abs(pRV[(x-1)/2] - pRV[(x+1)/2]);
            uint16_t len = x - edgesR[y].values[edgesR[y].nb-1];
            sumRY += pRY[x];
            sumRU += pRU[x/2];
            sumRV += pRV[x/2];
            if (edge > edgeTh) {
                edgesR[y].values[edgesR[y].nb] = x;
                edgesL[y].usage[edgesR[y].nb] = 0;
                edgesR[y].avgY[edgesR[y].nb] = sumRY/len;
                edgesR[y].avgU[edgesR[y].nb] = sumRU/len;
                edgesR[y].avgV[edgesR[y].nb] = sumRV/len;
                edgesR[y].nb++;
                sumRY = 0;
                sumRU = 0;
                sumRV = 0;
            }
            edge = abs(pLY[x-1] - pLY[x+1]) + abs(pLU[(x-1)/2] - pLU[(x+1)/2]) + abs(pLV[(x-1)/2] - pLV[(x+1)/2]);

            sumLY += pLY[x];
            sumLU += pLU[x/2];
            sumLV += pLV[x/2];
            len = x - edgesL[y].values[edgesL[y].nb-1];
            if (edge > edgeTh) {
                edgesL[y].values[edgesL[y].nb] = x;
                edgesL[y].usage[edgesR[y].nb] = 0;
                edgesL[y].avgY[edgesL[y].nb] = sumLY/len;
                edgesL[y].avgU[edgesL[y].nb] = sumLU/len;
                edgesL[y].avgV[edgesL[y].nb] = sumLV/len;
                edgesL[y].nb++;
                sumLY = 0;
                sumLU = 0;
                sumLV = 0;
            }
        }
        edgesR[y].values[edgesR[y].nb] = width;
        edgesR[y].nb++;
        edgesL[y].values[edgesL[y].nb] = width;
        edgesL[y].nb++;
        if (maxNb < edgesR[y].nb) {
            maxNb = edgesR[y].nb;
        }
    }
// kacziro - start here - set min err (unmatched case)
    printf("edges.. %d\n", maxNb);
    for (int ry = 0; ry < height; ry++) {
        StereoEdges rEdges = edgesR[ry];
        uint8_t *pRY = right_frame->data[0] + ry*right_frame->linesize[0];
        uint8_t *pRU = right_frame->data[1] + (ry/2)*right_frame->linesize[1];
        uint8_t *pRV = right_frame->data[2] + (ry/2)*right_frame->linesize[2];
        printf("%d\r", ry);
        //float prevShift = 0;
        for (int re = 1; re < rEdges.nb; re++) {
            int rLen = rEdges.values[re] - rEdges.values[re-1];
            double minE = 255;
            double bestShift = width;
            int bestLe = 1;
            int bestLy = 0;
            uint8_t *pDY = dst_frame->data[0] + ry*dst_frame->linesize[0];
            uint8_t *pDU = dst_frame->data[1] + (ry/2)*dst_frame->linesize[1];
            uint8_t *pDV = dst_frame->data[2] + (ry/2)*dst_frame->linesize[2];
            for (int ly = MAX(0, ry-110+470); ly < MIN(height, ry+110+470); ly++) {
                StereoEdges lEdges = edgesL[ly];
                uint8_t *pLY = left_frame->data[0] + ly*left_frame->linesize[0];
                uint8_t *pLU = left_frame->data[1] + (ly/2)*left_frame->linesize[1];
                uint8_t *pLV = left_frame->data[2] + (ly/2)*left_frame->linesize[2];
                for (int le = 1; le < lEdges.nb; le++) {
                    int lLen = lEdges.values[le] - lEdges.values[le-1];

                    if (abs(lLen - rLen) < 400) {// && edgesL[ly].usage[le]) {
                        double e = edgesL[ly].usage[le]/2;// abs((rEdges.values[re] - rLen/2) - (lEdges.values[le] - lLen/2)) + 25*edgesL[ly].usage[le];

                        float step = lLen/(float)rLen;
                        for (int i = 0; i < rLen; i++) {
                            e += abs(pRY[i + rEdges.values[re-1]] - pLY[(int)(i*step) + lEdges.values[le-1]]);
                        }
//                        step = rLen/(float)lLen;
//                        for (int i = 0; i < lLen; i++) {
//                            e += abs(pRY[(int)(i*step) + rEdges.values[re-1]] - pLY[i + lEdges.values[le-1]]);
//                        }
                        e /= (rLen);//lLen +
//                        if (lLen > rLen) { // kacziro - start here - check inverse
//                            float step = lLen/(float)rLen;
//                            for (int i = 0; i < rLen; i++) {
//                                e += abs(pRY[i + rEdges.values[re-1]] - pLY[(int)(i*step) + lEdges.values[le-1]]);
//                            }
//                            e /= rLen;
//                        } else {
//                            float step = rLen/(float)lLen;
//                            for (int i = 0; i < lLen; i++) {
//                                e += abs(pRY[(int)(i*step) + rEdges.values[re-1]] - pLY[i + lEdges.values[le-1]]);
//                            }
//                            e /= lLen;
//                        }
                        if (e < minE) {
                            //bestShift = abs(ry - ly) + abs((rEdges.values[re] - rLen/2) - (lEdges.values[le] - lLen/2));
                            bestShift = //abs((rEdges.values[re] - rLen/2) - (lEdges.values[le] - lLen/2))/3 +
                                        abs(rEdges.values[re] - lEdges.values[le])/2 +
                                        abs(rEdges.values[re-1] - lEdges.values[le-1])/2 +
                                        abs(ry - ly);
                            minE = e;
                            bestLe = le;
                            bestLy = ly;
                        }
                    }
                }
            }
           // prevShift = bestShift;
            edgesL[bestLy].usage[bestLe]++;
            bestShift /= width;
            if (bestShift > 1.0) {
                bestShift = 1;
            }
            bestShift *= 255;
            memset(pDY + rEdges.values[re-1], (uint8_t)bestShift, rLen);
//            {
//                memset(pDY + rEdges.values[re-1], 0, rLen);
//                int lLen =  edgesL[bestLy].values[bestLe] -  edgesL[bestLy].values[bestLe-1];
//                uint8_t *pBestLY = left_frame->data[0] + bestLy*left_frame->linesize[0];
//                uint8_t *pBestLU = left_frame->data[1] + (bestLy/2)*left_frame->linesize[1];
//                uint8_t *pBestLV = left_frame->data[2] + (bestLy/2)*left_frame->linesize[2];
//                float step = lLen/(float)rLen;
//                for (int x = rEdges.values[re-1]; x < rEdges.values[re]; x++) {
//                    int i = x - rEdges.values[re-1];
//                    pDY[x] = pBestLY[(int)(i*step) +  edgesL[bestLy].values[bestLe-1]];
//                    pDU[x/2] = pBestLU[((int)(i*step) +  edgesL[bestLy].values[bestLe-1])/2];
//                    pDV[x/2] = pBestLV[((int)(i*step) +  edgesL[bestLy].values[bestLe-1])/2];
//                }
//            }
            edgesL[bestLy].avgY[bestLe] = 255;
            edgesL[bestLy].avgU[bestLe] = 255;
            edgesL[bestLy].avgV[bestLe] = 255;
            //printf("minE: %f / %d, %d | %d |\n", minE, edgesL[bestLy].values[bestLe] - edgesL[bestLy].values[bestLe-1], rLen, bestLy);
        }
    }
    free(edgesR);
    free(edgesL);
}
static void blend_stereo_yuv420p(AVFrame *dst_frame, AVFrame *left_frame, const AVFrame *right_frame, int vshift) {
    int width = dst_frame->width;
    int height = dst_frame->height;
    uint16_t edges[12000];
    memset(edges0->data[0], 0, height*edges0->linesize[0]);
    memset(dst_frame->data[1], 128, dst_frame->linesize[1]*(height/2));
    memset(dst_frame->data[2], 128, dst_frame->linesize[2]*(height/2));
    for (int y = 0; y < height-vshift; y++) {
        int nb = 1;
//        uint8_t *pLY = left_frame->data[0] + (y+vshift)*left_frame->linesize[0];
//        uint8_t *pLU = left_frame->data[1] + ((y+vshift)/2)*left_frame->linesize[1];
//        uint8_t *pLV = left_frame->data[2] + ((y+vshift)/2)*left_frame->linesize[2];
        uint8_t *pRY = right_frame->data[0] + y*right_frame->linesize[0];
        uint8_t *pRU = right_frame->data[1] + (y/2)*right_frame->linesize[1];
        uint8_t *pRV = right_frame->data[2] + (y/2)*right_frame->linesize[2];

        uint8_t *pDY = dst_frame->data[0] + y*dst_frame->linesize[0];
        uint8_t *pDU = dst_frame->data[1] + (y/2)*dst_frame->linesize[1];
        uint8_t *pDV = dst_frame->data[2] + (y/2)*dst_frame->linesize[2];
        edges[0] = 0;
        for(int x = 1; x < width-1; x++) {
            int edge = abs(pRY[x-1] - pRY[x+1]) + abs(pRU[(x-1)/2] - pRU[(x+1)/2]) + abs(pRV[(x-1)/2] - pRV[(x+1)/2]);
            if (edge > 15) {//|| (edge > 8 && x - edges[nb-1] > 5*width/nb)) {
                edges[nb] = x;
                nb++;
            }
        }
        edges[nb] = width;
        nb++;
        for (int i = 0; i < nb-1; i++) {
            float minE = 255;
            int maxShift = 120;
            int minShift = 80;
            int bestShift = maxShift;
            int sBegin = edges[i] + minShift > 0? minShift : -edges[i];

            int bestY = 0;
            for (int vs = MAX(0, y - 15); vs < MIN(height, y+15); vs++) {
                uint8_t *pLY = left_frame->data[0] + (vs+vshift)*left_frame->linesize[0];
                uint8_t *pLU = left_frame->data[1] + ((vs+vshift)/2)*left_frame->linesize[1];
                uint8_t *pLV = left_frame->data[2] + ((vs+vshift)/2)*left_frame->linesize[2];
                for (int shift = sBegin; shift < maxShift; shift++) {
                    float sum = 0;
                    int hshift = shift/2;
                    int c = 0;
                    int endE = edges[i+1];
                    for (int x = 1 + edges[i]; x < endE - 1 && x + shift + 1 < width; x++) {
                        sum += abs(pRY[x-1] + pRY[x] + pRY[x+1] - pLY[x + shift - 1] - pLY[x + shift] - pLY[x + shift + 1]);
                        c++;
                    }
                    for (int x = 1 + edges[i]/2; x < endE/2 - 1 && x + hshift + 1 < width/2; x++) {
                        sum += abs(pRU[x-1] + pRU[x] + pRU[x+1] - pLU[x + hshift - 1] - pLU[x + hshift] - pLU[x + hshift + 1]);
                        sum += abs(pRV[x-1] + pRV[x] + pRV[x+1] - pLV[x + hshift - 1] - pLV[x + hshift] - pLV[x + hshift + 1]);
                        c+=2;
                    }
                    sum = sum/c;
                    if (sum < minE){
                        minE = sum;
                        bestShift = shift;// + abs(vs - y);
                        bestY = vs;
                    }
                }
            }
            uint8_t *pLY = left_frame->data[0] + bestY*left_frame->linesize[0];
            uint8_t *pLU = left_frame->data[1] + (bestY/2)*left_frame->linesize[1];
            uint8_t *pLV = left_frame->data[2] + (bestY/2)*left_frame->linesize[2];
            for (int x = edges[i]; x < edges[i+1]; x++) {
                int chY = 255;
                int chU = 255;
                int chV = 255;
                float bs = bestShift - minShift;
                if (x + bestShift < width && bestShift < maxShift){
                    chY = pLY[x + bestShift];
                    chU = pLU[(x + bestShift)/2];
                    chV = pLV[(x + bestShift)/2];
                    //pBestLY[x + bestShift] = 255;
                }
                bs /= (maxShift - minShift);
                bs *= 255;
                pDY[x] = 255 -(int32_t)bs;//chY;//
//                pDU[x/2] = chU;
//                pDV[x/2] = chV;
            }
        }
    }
    double err = 0.0;
    int c = 0;
    for (int y = 0; y < height; y++) {
        uint8_t *pDY = dst_frame->data[0] + y*dst_frame->linesize[0];
        uint8_t *pLY = left_frame->data[0] + y*left_frame->linesize[0];
        for (int x = 0; x < width; x++) {
            err += abs(pDY[x] - pLY[x]);
            c++;
        }
    }
    for (int y = 0; y < height/2; y++) {
        uint8_t *pDU = dst_frame->data[1] + y*dst_frame->linesize[1];
        uint8_t *pLU = left_frame->data[1] + y*left_frame->linesize[1];
        uint8_t *pDV = dst_frame->data[2] + y*dst_frame->linesize[2];
        uint8_t *pLV = left_frame->data[2] + y*left_frame->linesize[2];
        for (int x = 0; x < width/2; x++) {
            err += abs(pDU[x] - pLU[x]);
            err += abs(pDV[x] - pLV[x]);
            c+=2;
        }
    }
    printf("error: [%f]\n", err/c);
}
static void blend_stereo_gbr24p(AVFrame *dst_frame, AVFrame *left_frame, const AVFrame *right_frame, int vshift) {
    int width = dst_frame->width;
    int height = dst_frame->height;
    uint16_t errors[12000];
    uint16_t edges[12000];
    uint16_t shifts[256];
    memset(edges0->data[0], 0, height*edges0->linesize[0]);
    memset(shifts, 0, sizeof(shifts));
    memset(dst_frame->data[1], 0, dst_frame->linesize[1]*height);
    memset(dst_frame->data[2], 0, dst_frame->linesize[2]*height);
    for (int y = 0; y < height-vshift; y++) {
        int nb = 1;
        uint8_t *pRG = right_frame->data[0] + y*right_frame->linesize[0];
        uint8_t *pRB = right_frame->data[1] + y*right_frame->linesize[1];
        uint8_t *pRR = right_frame->data[2] + y*right_frame->linesize[2];

        uint8_t *pDG = dst_frame->data[0] + y*dst_frame->linesize[0];
        uint8_t *pEG = edges0->data[0] + y*edges0->linesize[0];
        edges[0] = 0;
        for(int x = 1; x < width-1; x++) {
            int edge = MAX(abs(pRR[x-1] - pRR[x+1]), MAX(abs(pRG[x-1] - pRG[x+1]), abs(pRB[x-1] - pRB[x+1])));
            if (edge > 20) {
                pEG[x] = edge;
                edges[nb] = x;
                nb++;
            }
        }
        edges[nb] = width;
        nb++;
        for (int i = 0; i < nb-1; i++) {
            float minE = 255;
            int maxShift = 2000;
            int minShift = 200;
            int deltaShift = 4;
            int bestShift = maxShift;
            int sBegin = edges[i] + minShift > 0? minShift : -edges[i];
            uint8_t *pLGbest = left_frame->data[0] + (y+vshift)*left_frame->linesize[0];
            memset(errors, 255, sizeof(errors));
            for (int vs = MAX(0, y - 15); vs < MIN(height, y+20); vs++) {
                uint8_t *pLG = left_frame->data[0] + (vs+vshift)*left_frame->linesize[0];
                uint8_t *pLB = left_frame->data[1] + (vs+vshift)*left_frame->linesize[1];
                uint8_t *pLR = left_frame->data[2] + (vs+vshift)*left_frame->linesize[2];
                for (int shift = sBegin; shift < maxShift; shift+=deltaShift) {
                    float sum = 0;
                    int c = 0;
                    for (int x = 1 + edges[i]; x < edges[i+1] - 1 && x + shift + 1 < width; x++) {
                        sum += abs(pRR[x-1] + pRR[x] + pRR[x+1] - pLR[x + shift - 1] - pLR[x + shift] - pLR[x + shift + 1]);
                        sum += abs(pRG[x-1] + pRG[x] + pRG[x+1] - pLG[x + shift - 1] - pLG[x + shift] - pLG[x + shift + 1]);
                        sum += abs(pRB[x-1] + pRB[x] + pRB[x+1] - pLB[x + shift - 1] - pLB[x + shift] - pLB[x + shift + 1]);
                        c+=3;
                        //c+=1;
                    }
                    sum = sum/c;
                    if (sum < minE){
                        minE = sum;
                        bestShift = shift;
                        pLGbest = pLG;
                    }
                }
            }
            if (_isnanf(minE)) {
                printf("s: %f\n", minE);
            }
            for (int x = edges[i]; x < edges[i+1]; x++) {
                int out = 255;
                float bs = bestShift - minShift;
                if (x + bestShift < width && bestShift < maxShift){
                    out = pLGbest[x + bestShift];
                }
                bs /= (maxShift - minShift);
                bs *= 255;
                pDG[x] = (int32_t)bs;//bestShift + 128;//out;//MIN(255, bestShift);//
                shifts[pDG[x]]++;
            }
        }
    }
}

static void blend_stereo_yuv444p_v2(AVFrame *dst_frame, AVFrame *left_frame, const AVFrame *right_frame, int vshift) {
    int width = dst_frame->width;
    int height = dst_frame->height;
    StereoEdges *edgesR = malloc(sizeof(StereoEdges)*5000);
    StereoEdges *edgesL = malloc(sizeof(StereoEdges)*5000);
    memset(dst_frame->data[1], 128, dst_frame->linesize[1]*height);
    memset(dst_frame->data[2], 128, dst_frame->linesize[2]*height);

    int maxNb = 0;
    for (int y = 0; y < height; y++) {
        uint8_t *pRY = right_frame->data[0] + y*right_frame->linesize[0];
        uint8_t *pRU = right_frame->data[1] + y*right_frame->linesize[1];
        uint8_t *pRV = right_frame->data[2] + y*right_frame->linesize[2];
        uint8_t *pLY = left_frame->data[0]  + y*left_frame->linesize[0];
        uint8_t *pLU = left_frame->data[1]  + y*left_frame->linesize[1];
        uint8_t *pLV = left_frame->data[2]  + y*left_frame->linesize[2];
        int edgeTh = 3;
        float sumRY = 0;
        float sumRU = 0;
        float sumRV = 0;
        float sumLY = 0;
        float sumLU = 0;
        float sumLV = 0;

        edgesR[y].values[0] = 0;
        edgesL[y].values[0] = 0;
        edgesR[y].nb = 1;
        edgesL[y].nb = 1;
        for(int x = 1; x < width-1; x++) {
            int edge = abs(pRY[x-1] - pRY[x+1]);// + abs(pRU[x-1] - pRU[x+1]) + abs(pRV[x-1] - pRV[x+1]);
            sumRY += pRY[x];
            sumRU += pRU[x];
            sumRV += pRV[x];
            if (edge > edgeTh) {
                uint16_t len = x - edgesR[y].values[edgesR[y].nb-1];
                edgesR[y].values[edgesR[y].nb] = x;
                edgesR[y].avgY[edgesR[y].nb] = sumRY/len;
                edgesR[y].avgU[edgesR[y].nb] = sumRU/len;
                edgesR[y].avgV[edgesR[y].nb] = sumRV/len;
                edgesR[y].nb++;
                sumRY = 0;
                sumRU = 0;
                sumRV = 0;
            }
            edge = abs(pLY[x-1] - pLY[x+1]);// + abs(pRU[x-1] - pRU[x+1]) + abs(pRV[x-1] - pRV[x+1]);
            sumLY += pLY[x];
            sumLU += pLU[x];
            sumLV += pLV[x];
            if (edge > edgeTh) {
                uint16_t len = x - edgesL[y].values[edgesL[y].nb-1];
                edgesL[y].values[edgesL[y].nb] = x;
                edgesL[y].avgY[edgesL[y].nb] = sumLY/len;
                edgesL[y].avgU[edgesL[y].nb] = sumLU/len;
                edgesL[y].avgV[edgesL[y].nb] = sumLV/len;
                edgesL[y].nb++;
                sumLY = 0;
                sumLU = 0;
                sumLV = 0;
            }
        }
        edgesR[y].values[edgesR[y].nb] = width;
        edgesR[y].nb++;
        edgesL[y].values[edgesL[y].nb] = width;
        edgesL[y].nb++;
        if (maxNb < edgesR[y].nb) {
            maxNb = edgesR[y].nb;
        }
    }

    printf("edges.. %d\n", maxNb);
    for (int ry = 0; ry < height; ry++) {
        StereoEdges rEdges = edgesR[ry];
        uint8_t *pRY = right_frame->data[0] + ry*right_frame->linesize[0];
        uint8_t *pRU = right_frame->data[1] + ry*right_frame->linesize[1];
        uint8_t *pRV = right_frame->data[2] + ry*right_frame->linesize[2];
        printf("%d\r", ry);
        for (int re = 1; re < rEdges.nb; re++) {
            int rLen = rEdges.values[re] - rEdges.values[re-1];
            double minE = 255;
            float bestShift = width;
            int bestLe = 1;
            int bestLy = 0;
            uint8_t *pDY = dst_frame->data[0] + ry*dst_frame->linesize[0];
            for (int ly = MAX(0, ry-120); ly < MIN(height, ry+120); ly++) {
                StereoEdges lEdges = edgesL[ly];
                uint8_t *pLY = left_frame->data[0] + ly*left_frame->linesize[0];
                uint8_t *pLU = left_frame->data[1] + ly*left_frame->linesize[1];
                uint8_t *pLV = left_frame->data[2] + ly*left_frame->linesize[2];
                for (int le = 1; le < lEdges.nb; le++) {
                    int lLen = lEdges.values[le] - lEdges.values[le-1];

                    if (abs(lLen - rLen) < 160) {
                        double e = abs(lLen - rLen)/5;
                        if (lLen > rLen) {
                            float step = lLen/(float)rLen;
                            for (int i = 0; i < rLen; i++) {
                                e += abs(pRY[i + rEdges.values[re-1]] - pLY[(int)(i*step) + lEdges.values[le-1]]);
                            }
                            e /= rLen;
                        } else {
                            float step = rLen/(float)lLen;
                            for (int i = 0; i < lLen; i++) {
                                e += abs(pRY[(int)(i*step) + rEdges.values[re-1]] - pLY[i + lEdges.values[le-1]]);
                            }
                            e /= lLen;
                        }
                        //int e = abs(lEdges.avgY[le] - rEdges.avgY[re]) + abs(lEdges.avgU[le] - rEdges.avgU[re]) + abs(lEdges.avgV[le] - rEdges.avgV[re]);
    //                    e += abs(lEdges.avgY[le-1] - rEdges.avgY[re-1]) + abs(lEdges.avgU[le-1] - rEdges.avgU[re-1]) + abs(lEdges.avgV[le-1] - rEdges.avgV[re-1]);
    //                    e += abs(lEdges.avgY[le+1] - rEdges.avgY[re+1]) + abs(lEdges.avgU[le+1] - rEdges.avgU[re+1]) + abs(lEdges.avgV[le+1] - rEdges.avgV[re+1]);
    //                    e /= 3;
                        if (e < minE) {
                            bestShift = abs(ry - ly) + abs((rEdges.values[re] - rLen/2) - (lEdges.values[le] - lLen/2));
                            minE = e;
                            bestLe = le;
                            bestLy = ly;
                        }

                    }
                }
            }
            bestShift /= width;
            bestShift *= 255;
            memset(pDY + rEdges.values[re-1], (uint8_t)bestShift, rLen);
            edgesL[bestLy].avgY[bestLe] = 255;
            edgesL[bestLy].avgU[bestLe] = 255;
            edgesL[bestLy].avgV[bestLe] = 255;
            //printf("minE: %f\n", minE);
        }
    }
    free(edgesR);
    free(edgesL);
}

static void blend_stereo_yuv444p_v1(AVFrame *dst_frame, AVFrame *left_frame, const AVFrame *right_frame, int vshift) {
    int width = dst_frame->width;
    int height = dst_frame->height;
    double meanerr = 0;
    double count = 0;
    uint16_t edges[12000];
    uint32_t errs[256];
    memset(errs, 0, sizeof(errs));
    memset(edges0->data[0], 0, height*edges0->linesize[0]);
    memset(dst_frame->data[1], 128, dst_frame->linesize[1]*height);
    memset(dst_frame->data[2], 128, dst_frame->linesize[2]*height);
    vshift = 0;
    for (int y = 0; y < height-vshift; y++) {
        int nb = 1;
        uint8_t *pRY = right_frame->data[0] + y*right_frame->linesize[0];
        uint8_t *pRU = right_frame->data[1] + y*right_frame->linesize[1];
        uint8_t *pRV = right_frame->data[2] + y*right_frame->linesize[2];

        uint8_t *pDY = dst_frame->data[0] + y*dst_frame->linesize[0];
        uint8_t *pDU = dst_frame->data[1] + y*dst_frame->linesize[1];
        uint8_t *pDV = dst_frame->data[2] + y*dst_frame->linesize[2];
        edges[0] = 0;
        for(int x = 1; x < width-1; x++) {
            int edge = abs(pRY[x-1] - pRY[x+1]);// + abs(pRU[x-1] - pRU[x+1]) + abs(pRV[x-1] - pRV[x+1]);
            if (edge > 3 ){//|| x - edges[nb-1] > 150) {
                edges[nb] = x;
                nb++;
            }
        }
        edges[nb] = width;
        nb++;
        printf("%d\r", y);
        for (int i = 0; i < nb-1; i++) {
            float minE = 255;
            int maxShift = 1250;
            int minShift = -400;
            int deltaShift = 5;
            int bestShift = maxShift;
            int sBegin = edges[i] + minShift > 0? minShift : -edges[i];
            uint8_t *pLYbest = left_frame->data[0] + (y+vshift)*left_frame->linesize[0];
            uint8_t *pLUbest = left_frame->data[1] + (y+vshift)*left_frame->linesize[1];
            uint8_t *pLVbest = left_frame->data[2] + (y+vshift)*left_frame->linesize[2];
            int vs = y;
            //for (int vs = MAX(0, y - 115); vs < MIN(height-vshift, y+28); vs+=8) {
            for (int vs = MAX(0, y - 60); vs < MIN(height-vshift, y+120); vs+=12) {
                uint8_t *pLY = left_frame->data[0] + (vs+vshift)*left_frame->linesize[0];
                uint8_t *pLU = left_frame->data[1] + (vs+vshift)*left_frame->linesize[1];
                uint8_t *pLV = left_frame->data[2] + (vs+vshift)*left_frame->linesize[2];
                for (int shift = sBegin; shift < maxShift; shift+=deltaShift) {
                    float sumY = 0;
                    float sumU = 0;
                    float sumV = 0;
                    int c = 0;
                    sumY += abs(pRY[MAX(0, edges[i] - 4)] - pLY[MAX(0, edges[i] - 4)]);
                    sumY += abs(pRY[MIN(0, edges[i+1] + 4)] - pLY[MIN(0, edges[i+1] + 4)]);
                    sumY += abs(pRY[MAX(0, edges[i] - right_frame->linesize[0])] - pLY[MAX(0, edges[i] - left_frame->linesize[0])]);
                    sumY += abs(pRY[MIN(0, edges[i] + right_frame->linesize[0])] - pLY[MIN(0, edges[i] + left_frame->linesize[0])]);
                    sumY += abs(pRY[MAX(0, edges[i+1] - right_frame->linesize[0])] - pLY[MAX(0, edges[i+1] - left_frame->linesize[0])]);
                    sumY += abs(pRY[MIN(0, edges[i+1] + right_frame->linesize[0])] - pLY[MIN(0, edges[i+1] + left_frame->linesize[0])]);

                    sumU += abs(pRU[MAX(0, edges[i] - 4)] - pLU[MAX(0, edges[i] - 4)]);
                    sumU += abs(pRU[MIN(0, edges[i+1] + 4)] - pLU[MIN(0, edges[i+1] + 4)]);
                    sumU += abs(pRU[MAX(0, edges[i] - right_frame->linesize[0])] - pLU[MAX(0, edges[i] - left_frame->linesize[0])]);
                    sumU += abs(pRU[MIN(0, edges[i] + right_frame->linesize[0])] - pLU[MIN(0, edges[i] + left_frame->linesize[0])]);
                    sumU += abs(pRU[MAX(0, edges[i+1] - right_frame->linesize[0])] - pLU[MAX(0, edges[i+1] - left_frame->linesize[0])]);
                    sumU += abs(pRU[MIN(0, edges[i+1] + right_frame->linesize[0])] - pLU[MIN(0, edges[i+1] + left_frame->linesize[0])]);

                    sumV += abs(pRV[MAX(0, edges[i] - 4)] - pLV[MAX(0, edges[i] - 4)]);
                    sumV += abs(pRV[MIN(0, edges[i+1] + 4)] - pLV[MIN(0, edges[i+1] + 4)]);
                    sumV += abs(pRV[MAX(0, edges[i] - right_frame->linesize[0])] - pLV[MAX(0, edges[i] - left_frame->linesize[0])]);
                    sumV += abs(pRV[MIN(0, edges[i] + right_frame->linesize[0])] - pLV[MIN(0, edges[i] + left_frame->linesize[0])]);
                    sumV += abs(pRV[MAX(0, edges[i+1] - right_frame->linesize[0])] - pLV[MAX(0, edges[i+1] - left_frame->linesize[0])]);
                    sumV += abs(pRV[MIN(0, edges[i+1] + right_frame->linesize[0])] - pLV[MIN(0, edges[i+1] + left_frame->linesize[0])]);
                    c+= 3;
                    for (int x = 1 + edges[i]; x < edges[i+1] - 1 && x + shift + 1 < width; x++) {
                        sumY += abs(pRY[x-1] + pRY[x] + pRY[x+1] - pLY[x + shift - 1] - pLY[x + shift] - pLY[x + shift + 1]);
                        c++;
                        sumU += abs(pRU[x-1] + pRU[x] + pRU[x+1] - pLU[x + shift - 1] - pLU[x + shift] - pLU[x + shift + 1]);
                        c++;
                        sumV += abs(pRV[x-1] + pRV[x] + pRV[x+1] - pLV[x + shift - 1] - pLV[x + shift] - pLV[x + shift + 1]);
                        c++;

//                        sumY += abs(pRY[x] - pLY[x + shift]);
//                        sumU += abs(pRU[x] - pLU[x + shift]);
//                        sumV += abs(pRV[x] - pLV[x + shift]);
//                        c+=3;
                    }
                    sumY = (sumY + sumU + sumV + shift + abs(vs - y))/c;
                    if (sumY < minE){
                        minE = sumY;
                        bestShift = shift + abs(vs - y);
                        pLYbest = pLY;
                        pLUbest = pLU;
                        pLVbest = pLV;
                    }
                }
            }
            meanerr += (uint8_t)minE;
            count++;
            for (int x = edges[i]; x < edges[i+1]; x++) {
                int outY = 255;
                int outU = 128;
                int outV = 128;
                double bs = bestShift - minShift;
                if (x + bestShift < width && bestShift < maxShift){
                    outY = pLYbest[x + bestShift];
                    outU = pLUbest[x + bestShift];
                    outV = pLVbest[x + bestShift];
                    //if (minE < 20) {
//                        pLYbest[x + bestShift] = 255;
//                        pLUbest[x + bestShift] = 255;
//                        pLVbest[x + bestShift] = 255;
                    //}
                }
                bs /= (maxShift - minShift);
                bs *= 256 - 1;//256*256 - 1;
                pDY[x] = outY;// (uint8_t)bs;//
                pDU[x] = outU;
                pDV[x] = outV;
                //uint32_t ibs = (uint32_t)bs;
                //pDY[x] = ibs%256;//out;//
                //pDU[x] = ibs >> 8;
            }
        }
    }
    printf("err: [%f] %f, %f\n", meanerr/count, meanerr, count);
}
static void blend_stereo_yuv444p_v0(AVFrame *dst_frame, AVFrame *left_frame, const AVFrame *right_frame, int vshift) {
    int width = dst_frame->width;
    int height = dst_frame->height;
    float meanerr = 0;
    float count = 0;
    uint16_t edges[12000];
    memset(edges0->data[0], 0, height*edges0->linesize[0]);
    memset(dst_frame->data[1], 128, dst_frame->linesize[1]*height);
    memset(dst_frame->data[2], 128, dst_frame->linesize[2]*height);
//    memcpy(dst_frame->data[1], right_frame->data[1], dst_frame->linesize[1]*height);
//    memcpy(dst_frame->data[2], right_frame->data[2], dst_frame->linesize[2]*height);

    {
        uint8_t minR = 255;
        uint8_t minL = 255;
        uint8_t maxR = 0;
        uint8_t maxL = 0;
        for (int y = 0; y < left_frame->height; y++) {
            uint8_t *pLY = left_frame->data[0] + y*left_frame->linesize[0];
            uint8_t *pRY = right_frame->data[0] + y*right_frame->linesize[0];
            for (int x = 0; x < left_frame->width; x++) {
                maxR = MAX(maxR, pRY[x]);
                maxL = MAX(maxL, pLY[x]);
                minR = MIN(minR, pRY[x]);
                minL = MIN(minL, pLY[x]);
            }
        }
        uint8_t rlen = maxR - minR;
        uint8_t llen = maxL - minL;
        for (int y = 0; y < left_frame->height; y++) {
            uint8_t *pLY = left_frame->data[0] + y*left_frame->linesize[0];
            uint8_t *pRY = right_frame->data[0] + y*right_frame->linesize[0];
            for (int x = 0; x < left_frame->width; x++) {
                float t = pLY[x] - minL;
                pLY[x] = (uint8_t)(t/llen);
                t = pRY[x] - minR;
                pRY[x] = (uint8_t)(t/rlen);
            }
        }
    }

    for (int y = 0; y < height-vshift; y++) {
        int nb = 1;
        uint8_t *pRY = right_frame->data[0] + y*right_frame->linesize[0];
        uint8_t *pRU = right_frame->data[1] + y*right_frame->linesize[1];
        uint8_t *pRV = right_frame->data[2] + y*right_frame->linesize[2];

        uint8_t *pDY = dst_frame->data[0] + y*dst_frame->linesize[0];
        uint8_t *pEY = edges0->data[0] + y*edges0->linesize[0];
        edges[0] = 0;
        for(int x = 1; x < width-1; x++) {
            int edge = abs(pRY[x-1] - pRY[x+1]);// + abs(pRU[x-1] - pRU[x+1]) + abs(pRV[x-1] - pRV[x+1]);
            if (edge > 5) {
                pEY[x] = edge;
                edges[nb] = x;
                nb++;
            }
        }
        edges[nb] = width;
        nb++;
        printf("y: %d\r", y);
        for (int i = 0; i < nb-1; i++) {
            float minE = 255;
            int maxShift = 1000;
            int minShift = -500;
            int deltaShift = 1;
            int bestShift = maxShift;
            int sBegin = edges[i] + minShift > 0? minShift : -edges[i];
            uint8_t *pLYbest = left_frame->data[0] + (y+vshift)*left_frame->linesize[0];
            int vs = y;
            //for (int vs = MAX(0, y - 115); vs < MIN(height-vshift, y+28); vs+=8) {
            for (int vs = MAX(0, y - 28); vs < MIN(height-vshift, y+40); vs+=1) {
                uint8_t *pLY = left_frame->data[0] + (vs+vshift)*left_frame->linesize[0];
                uint8_t *pLU = left_frame->data[1] + (vs+vshift)*left_frame->linesize[1];
                uint8_t *pLV = left_frame->data[2] + (vs+vshift)*left_frame->linesize[2];
                for (int shift = sBegin; shift < maxShift; shift+=deltaShift) {
                    float sumY = 0;
                    float sumU = 0;
                    float sumV = 0;
                    int c = 0;
                    for (int x = 1 + edges[i]; x < edges[i+1] - 1 && x + shift + 1 < width; x++) {
                        sumY += abs(pRY[x-1] + pRY[x] + pRY[x+1] - pLY[x + shift - 1] - pLY[x + shift] - pLY[x + shift + 1]);
                        c++;
                        sumU += abs(pRU[x-1] + pRU[x] + pRU[x+1] - pLU[x + shift - 1] - pLU[x + shift] - pLU[x + shift + 1]);
                        c++;
                        sumV += abs(pRV[x-1] + pRV[x] + pRV[x+1] - pLV[x + shift - 1] - pLV[x + shift] - pLV[x + shift + 1]);
                        c++;
                    }
                    float sum = (sumY + sumU + sumV)/c;
                    if (sum < minE){
                        minE = sum;
                        bestShift = shift + abs(vs - y);
                        pLYbest = pLY;
                    }
                }
            }
            meanerr += minE;
            //meanShift += bestShift;
            count++;
            for (int x = edges[i]; x < edges[i+1]; x++) {
                int out = 255;
                float bs = bestShift - minShift;
                if (x + bestShift < width && bestShift < maxShift){
                    out = pLYbest[x + bestShift];
                }
                bs /= (maxShift - minShift);
                bs *= 255;
                pDY[x] = (int32_t)bs;//bestShift + 128;//out;//MIN(255, bestShift);///
            }
        }
    }
    printf("err: [%f]\n", meanerr/count);
}
static void blend_stereo(AVFrame *dst_frame, AVFrame *left_frame, const AVFrame *right_frame)
{
    int vshift = 0;//10;//25;//10;
    switch (dst_frame->format) {
        case AV_PIX_FMT_YUV420P:
        {
            blend_stereo_yuv420p_v2(dst_frame, left_frame, right_frame, vshift);
        } break;
        case AV_PIX_FMT_GBR24P:
        {
            blend_stereo_gbr24p(dst_frame, left_frame, right_frame, vshift);
        } break;
        case AV_PIX_FMT_YUV444P:
        {
            blend_stereo_yuv444p_v0(dst_frame, left_frame, right_frame, vshift);
        } break;
    }
}

static AVFrame *blend_frame(AVFilterContext *ctx, AVFrame *top_buf,
                            const AVFrame *bottom_buf)
{
    BlendContext *s = ctx->priv;
    AVFilterLink *inlink = ctx->inputs[0];
    AVFilterLink *outlink = ctx->outputs[0];
    AVFrame *dst_buf;
    int plane;

    dst_buf = ff_get_video_buffer(outlink, outlink->w, outlink->h);
    if (!dst_buf)
        return top_buf;
    av_frame_copy_props(dst_buf, top_buf);

    if (s->params[0].mode == BLEND_STEREO) { // kacziro - mod
        blend_stereo(dst_buf, top_buf, bottom_buf);
    } else {
        for (plane = 0; plane < s->nb_planes; plane++) {
            int hsub = plane == 1 || plane == 2 ? s->hsub : 0;
            int vsub = plane == 1 || plane == 2 ? s->vsub : 0;
            int outw = AV_CEIL_RSHIFT(dst_buf->width,  hsub);
            int outh = AV_CEIL_RSHIFT(dst_buf->height, vsub);
            FilterParams *param = &s->params[plane];
            ThreadData td = { .top = top_buf, .bottom = bottom_buf, .dst = dst_buf,
                              .w = outw, .h = outh, .param = param, .plane = plane,
                              .inlink = inlink };

            ctx->internal->execute(ctx, filter_slice, &td, NULL, FFMIN(outh, ff_filter_get_nb_threads(ctx)));
        }
    }

    if (!s->tblend)
        av_frame_free(&top_buf);

    return dst_buf;
}

static av_cold int init(AVFilterContext *ctx)
{
    BlendContext *s = ctx->priv;

    s->tblend = !strcmp(ctx->filter->name, "tblend");

    s->dinput.process = blend_frame;
    return 0;
}

static int query_formats(AVFilterContext *ctx)
{
    static const enum AVPixelFormat pix_fmts[] = {
        AV_PIX_FMT_YUVA444P, AV_PIX_FMT_YUVA422P, AV_PIX_FMT_YUVA420P,
        AV_PIX_FMT_YUVJ444P, AV_PIX_FMT_YUVJ440P, AV_PIX_FMT_YUVJ422P,AV_PIX_FMT_YUVJ420P, AV_PIX_FMT_YUVJ411P,
        AV_PIX_FMT_YUV444P, AV_PIX_FMT_YUV440P, AV_PIX_FMT_YUV422P, AV_PIX_FMT_YUV420P, AV_PIX_FMT_YUV411P, AV_PIX_FMT_YUV410P,
        AV_PIX_FMT_GBRP, AV_PIX_FMT_GBRAP, AV_PIX_FMT_GRAY8,
        AV_PIX_FMT_YUV420P16, AV_PIX_FMT_YUV422P16, AV_PIX_FMT_YUV444P16,
        AV_PIX_FMT_YUVA420P16, AV_PIX_FMT_YUVA422P16, AV_PIX_FMT_YUVA444P16,
        AV_PIX_FMT_GBRP16, AV_PIX_FMT_GRAY16,
        AV_PIX_FMT_NONE
    };

    AVFilterFormats *fmts_list = ff_make_format_list(pix_fmts);
    if (!fmts_list)
        return AVERROR(ENOMEM);
    return ff_set_common_formats(ctx, fmts_list);
}

static av_cold void uninit(AVFilterContext *ctx)
{
    BlendContext *s = ctx->priv;
    int i;

    ff_dualinput_uninit(&s->dinput);
    av_frame_free(&s->prev_frame);

    if (isInitializedFrameCopy(&s->edges0)) {
        edges0 = NULL;
        destroyFrameCopy(&s->edges0);
    }
    if (isInitializedFrameCopy(&s->edges1)) {
        edges1 = NULL;
        destroyFrameCopy(&s->edges1);
    }
    if (isInitializedFrameCopy(&s->errors)) {
        errors = NULL;
        destroyFrameCopy(&s->errors);
    }
    if (isInitializedFrameCopy(&s->distortionMap0)) {
        distortionMap0 = NULL;
        destroyFrameCopy(&s->distortionMap0);
    }
    if (isInitializedFrameCopy(&s->distortionMap1)) {
        distortionMap1 = NULL;
        destroyFrameCopy(&s->distortionMap1);
    }
    if (isInitializedFrameCopy(&s->corrected0)) {
        corrected0 = NULL;
        destroyFrameCopy(&s->corrected0);
    }
    if (isInitializedFrameCopy(&s->corrected1)) {
        corrected1 = NULL;
        destroyFrameCopy(&s->corrected1);
    }

    for (i = 0; i < FF_ARRAY_ELEMS(s->params); i++)
        av_expr_free(s->params[i].e);
}
static void blend_black_8bit(const uint8_t *_left, ptrdiff_t left_linesize,
                             const uint8_t *_right, ptrdiff_t right_linesize,
                               uint8_t *_dst, ptrdiff_t dst_linesize,
                               ptrdiff_t width, ptrdiff_t height,
                               FilterParams *param, double *values, int starty) {

    uint8_t *dst = (uint8_t*)_dst;
    memset(dst, 128, dst_linesize*height);
    //memcpy(dst, _left, dst_linesize*height);
//    for (int i = 0; i < height; i++) {
//        memcpy(dst + i*dst_linesize, _left + i*left_linesize, dst_linesize);
//        memcpy(dst + i*dst_linesize, _right + i*right_linesize, dst_linesize);
//    }
}


#define MAX(a,b) ((a) < (b) ? (b) : (a))
#define MIN(a,b) ((a) > (b) ? (b) : (a))

static void detectEdges(uint8_t *_right, int right_linesize,
                        uint8_t *_left, int left_linesize,
                        uint8_t *_dst, int dst_linesize,
                        int width, int height) {
    const uint8_t *rightDown = (uint8_t*)_right;
    const uint8_t *right = _right + right_linesize;
    const uint8_t *rightUp = right + 2*right_linesize;

    const uint8_t *leftDown = (uint8_t*)_left;
    const uint8_t *left = _left + left_linesize;
    const uint8_t *leftUp = _left + 2*left_linesize;

    uint8_t *dst = (uint8_t*)_dst;

    uint8_t minLeft, maxLeft, minRight, maxRight;
    int y, x;
    uint8_t* eRight = (uint8_t*)edges0->data[0];
    uint8_t* eLeft = (uint8_t*)edges1->data[0];
    minRight = minLeft = 255;
    maxRight = maxLeft = 0;

    for (y = 1; y < height - 1; y++) {
        for (x = 1; x < width - 1; x++) {
            dst[x] = abs(rightDown[x] - rightUp[x]);
            dst[x] = MAX(dst[x], abs(rightDown[x-1] - rightUp[x+1]));
            dst[x] = MAX(dst[x], abs(rightDown[x+1] - rightUp[x-1]));
            dst[x] = MAX(dst[x], abs(right[x+1] - right[x-1]));
            minRight = MIN(minRight, dst[x]);
            maxRight = MAX(maxRight, dst[x]);
        }
        dst    += dst_linesize;
        rightDown  = right;
        right     = rightUp;
        rightUp += right_linesize;
    }
    memcpy(edges0->data[0], _dst, dst_linesize*height);
    dst = (uint8_t*)_dst;
    for (y = 1; y < height - 1; y++) {
        for (x = 1; x < width - 1; x++) {
            dst[x] = abs(leftDown[x] - leftUp[x]);
            dst[x] = MAX(dst[x], abs(leftDown[x-1] - leftUp[x+1]));
            dst[x] = MAX(dst[x], abs(leftDown[x+1] - leftUp[x-1]));
            dst[x] = MAX(dst[x], abs(left[x+1] - left[x-1]));
            minLeft = MIN(minLeft, dst[x]);
            maxLeft = MAX(maxLeft, dst[x]);
        }
        dst    += dst_linesize;
        leftDown  = left;
        left  = leftUp;
        leftUp += left_linesize;
    }
    memcpy(edges1->data[0], _dst, dst_linesize*height);
    printf("min :%d, max: %d\n", minLeft, maxLeft);
    printf("min :%d, max: %d\n", minRight, maxRight);
    for (y = 1; y < height - 1; y++) {
        for (x = 1; x < width - 1; x++) {
            eLeft[x] = (uint8_t)(((eLeft[x] - minLeft)/(float)(maxLeft - minLeft))*255);
            eRight[x] = (uint8_t)(((eRight[x] - minRight)/(float)(maxRight - minRight))*255);
        }
        eLeft     += dst_linesize;
        eRight    += dst_linesize;
    }
}
typedef struct ImgBlock {
    Point2u16 begin;
    uint16_t horizontalEdges[512];
    uint16_t verticalEdges[512];
    uint16_t hist[256];
} ImgBlock;

static void histNormalize(uint16_t *hist, int size) {
    int min = -1;
    int max = -1;
    uint16_t norm[512];
    float len = max - min;
    memset(norm, 0, sizeof(uint16_t)*size);
    for (int i = 0; i < size && (min == -1 || max == -1); i++) {
        if (min == -1 && hist[i] != 0)
            min = i;
        if (max == -1 && hist[size -1 - i] != 0)
            max = size -1 - i;
    }
    if (len > 0) {
        int prevNi = 0;
        int prevVal = hist[min];
        for (int i = min; i < max + 1; i++) {
            int ni = (int) ((size-1)*(i - min)/len);
            float a = (prevVal - hist[i])/(float)(prevNi - ni);
            float b = prevVal - a*prevNi;
            for (int n = prevNi; n < ni+1; n++) {
                norm[n] = (uint16_t)(a*n + b);
            }
            prevVal = hist[i];
            prevNi = ni;
        }
//        for (int i = 0; i < 256; i++) {
//            printf("%d) %d | %d\n", i, block->hist[i], vv  [i]);
//        }
//        printf("end\n");
        memcpy(hist, norm, sizeof(uint16_t)*size);
    }
}
//static void histNormalize(ImgBlock *b) {
//    int min = -1;
//    int max = -1;
//    uint16_t norm[256];
//    memset(norm, 0, sizeof(uint16_t)*256);
//    for (int i = 0; i < 256 && (min == -1 || max == -1); i++) {
//        if (min == -1 && b->hist[i] != 0)
//            min = i;
//        if (max == -1 && b->hist[255 - i] != 0)
//            max = 255 - i;
//    }
//    float len = max - min;
//    if (len > 0) {
//        for (int i = min; i < max + 1; i++) {
//            int ni = (int) (255*(i - min)/len);
//            if (ni > 0)
//                norm[ni - 1] += b->hist[i]/4;
//            if (ni < 255)
//                norm[ni + 1] += b->hist[i]/4;
//            norm[ni] += b->hist[i]/2;
//        }
//        memcpy(b->hist, norm, sizeof(uint16_t)*256);
//    }
////    for (int i = 0; i < 256; i++) {
////        printf("%d) %d | %d\n", i, b->hist[i], norm[i]);
////    }
////    printf("end\n");
//}

static int compareImgBlocks(ImgBlock *b0, ImgBlock *b1) {
    int error = 0;
    for (int i = 1; i < 256 - 1; i++) {
        error += abs(b0->hist[i - 1] + b0->hist[i] + b0->hist[i + 1] - b1->hist[i - 1] - b1->hist[i] - b1->hist[i + 1]);
    }
    return error;
}
ImgBlock *rightBlocks = NULL;
ImgBlock *leftBlocks = NULL;
static void disparityBlockImg (uint8_t *_left,  int left_linesize,
                               uint8_t *_right, int right_linesize,
                               uint8_t *_dst,   int dst_linesize,
                               int width,       int height) {
    int blockSize = 10;
    int gridW = width/blockSize;
    int gridH = height/blockSize;
    int blocksNb = gridW*gridH;
    int w = width/blockSize;
    uint8_t *rdown   = _right;
    uint8_t *rcenter = _right + right_linesize;
    uint8_t *rup     = rcenter + right_linesize;
    uint8_t *ldown   = _left;
    uint8_t *lcenter = _left + left_linesize;
    uint8_t *lup     = lcenter + left_linesize;
    int maxErr = 500;//500*256;
    //ImgBlock left[6000];  //120x50
    //ImgBlock right[6000]; //12000x5000 / 100x100
    if (!rightBlocks) {
        rightBlocks = malloc(blocksNb*sizeof(ImgBlock));
    }
    if (!leftBlocks) {
        leftBlocks = malloc(blocksNb*sizeof(ImgBlock));
    }

    memset(rightBlocks, 0, blocksNb*sizeof(ImgBlock));
    memset(leftBlocks, 0, blocksNb*sizeof(ImgBlock));

    for (int y = 0; y < height/blockSize; y++) {
        for (int x = 0; x < w; x++) {
            leftBlocks [x + y*w].begin.x = x;
            leftBlocks [x + y*w].begin.y = y;
            rightBlocks[x + y*w].begin.x = x;
            rightBlocks[x + y*w].begin.y = y;
        }
    }
    //todo halfblocks for right
    for (int y = 0; y < height; y++) {
        for (int x = 0; x < width; x++) {
            int index = x/blockSize + (y/blockSize)*gridW;
            leftBlocks[index].hist[lcenter[x]]++;
            rightBlocks[index].hist[rcenter[x]]++;
//            leftBlocks[index].horizontalEdges[lcenter[x - 1] - lcenter[x + 1] + 128]++;
//            rightBlocks[index].horizontalEdges[rcenter[x - 1] - rcenter[x + 1] + 128]++;
//            leftBlocks[index].verticalEdges[ldown[x] - lup[x] + 128]++;
//            rightBlocks[index].verticalEdges[rdown[x] - rup[x] + 128]++;

//            leftBlocks[index].horizontalEdges += abs(lcenter[x - 1] - lcenter[x + 1]);
//            leftBlocks[index].verticalEdges += abs(ldown[x] - lup[x]);
//            rightBlocks[index].horizontalEdges += abs(rcenter[x - 1] - rcenter[x + 1]);
//            rightBlocks[index].verticalEdges += abs(rdown[x] - rup[x]);
        }
        rdown   += right_linesize;
        rcenter += right_linesize;
        rup     += right_linesize;
        ldown   += left_linesize;
        lcenter += left_linesize;
        lup     += left_linesize;
    }
    for (int i = 0; i < blocksNb; i++) {
        rightBlocks[i].horizontalEdges[127] = 0;
        rightBlocks[i].horizontalEdges[128] = 0;
        rightBlocks[i].horizontalEdges[129] = 0;

        rightBlocks[i].verticalEdges[127] = 0;
        rightBlocks[i].verticalEdges[128] = 0;
        rightBlocks[i].verticalEdges[129] = 0;

        leftBlocks[i].horizontalEdges[127] = 0;
        leftBlocks[i].horizontalEdges[128] = 0;
        leftBlocks[i].horizontalEdges[129] = 0;

        leftBlocks[i].verticalEdges[127] = 0;
        leftBlocks[i].verticalEdges[128] = 0;
        leftBlocks[i].verticalEdges[129] = 0;
//        histNormalize(leftBlocks[i].hist, 256);
//        histNormalize(leftBlocks[i].horizontalEdges, 512);
//        histNormalize(leftBlocks[i].verticalEdges, 512);
//        histNormalize(rightBlocks[i].hist, 256);
//        histNormalize(rightBlocks[i].horizontalEdges, 512);
//        histNormalize(rightBlocks[i].verticalEdges, 512);
    }
    for (int ry = 5; ry < gridH; ry++) {
        int bestlx = 0;
        for (int rx = 0; rx < gridW; rx++) {
            int ri = rx + ry*gridW;
            uint16_t *rhist  = rightBlocks[ri].hist;
            //uint16_t *rvhist = rightBlocks[ri].verticalEdges;
            //uint16_t *rhhist = rightBlocks[ri].horizontalEdges;
            int minErr = maxErr;
            int dist = 255;
            if (rightBlocks[ri].hist[0] < blockSize*blockSize) {
                int startx = bestlx;
                for (int lx = MAX(0, startx); lx < MIN(gridW, startx+14); lx++) {
                    int li = ry*gridW + lx;
                    uint16_t *lhist  = leftBlocks[li].hist;
//                    uint16_t *lvhist = leftBlocks[li].verticalEdges;
//                    uint16_t *lhhist = leftBlocks[li].horizontalEdges;
                    int err = 0;
                    if (leftBlocks[li].hist[0] >= blockSize*blockSize)
                    {
                        continue;
                    }
                    for (int h = 1; h < 255; h++) {
                        err += abs(lhist[h-1] + lhist[h] + lhist[h+1] - rhist[h-1] - rhist[h] - rhist[h+1]);
                    }
    //                for (int h = 1; h < 511; h++) {
    //                    err += abs(lvhist[h-1] + lvhist[h] + lvhist[h+1] - rvhist[h-1] - rvhist[h] - rvhist[h+1]);
    //                    err += abs(lhhist[h-1] + lhhist[h] + lhhist[h+1] - rhhist[h-1] - rhhist[h] - rhhist[h+1]);
    //                }
                    //err += err*0.05f*abs(7 - abs(rx - lx));
                    if (err < minErr) {
                        bestlx = lx;
                        minErr = err;
                        dist = abs(rx - lx)*2;
                    }
                }

                //dist = sqrt(dist);
            }

            if (dist == 255) {
//                for (int i = 0; i < 512; i++) {
//                    if (lvhist[i] || rvhist[i])
//                        printf("%d) %d\t%d\n", i, lvhist[i], rvhist[i]);
//                }
//                printf("-\n");
//                for (int i = 0; i < 512; i++) {
//                    if (lhhist[i] || rhhist[i])
//                        printf("%d) %d\t%d\n", i, lhhist[i], rhhist[i]);
//                }
//                printf("---\n");
                for (int y = rightBlocks[ri].begin.y*blockSize; y < MIN(blockSize*rightBlocks[ri].begin.y + blockSize, height); y++) {
                    uint8_t *pD = _dst + y*dst_linesize;
                    for (int x = 0; x < blockSize; x++) {
                        pD[MIN(blockSize*rightBlocks[ri].begin.x + x, dst_linesize)] = dist;
                    }
                }
            }
            else {
                int li = ry*gridW + bestlx;
//                uint16_t *lhist  = leftBlocks[li].hist;
//                uint16_t *lvhist = leftBlocks[li].verticalEdges;
//                uint16_t *lhhist = leftBlocks[li].horizontalEdges;
                printf("-%d-%d-%d-[%d]\n", rx, bestlx, minErr, dist);
//                for (int i = 0; i < 256; i++) {
//                    if (lhist[i] || rhist[i])
//                        printf("%d) %d\t%d\n", i, lhist[i], rhist[i]);
//                }
                for (int y = rightBlocks[ri].begin.y*blockSize; y < MIN(blockSize*rightBlocks[ri].begin.y + blockSize, height); y++) {
                    uint8_t *pD = _dst + y*dst_linesize;
                    uint8_t *pL = _left + y*left_linesize;
                    for (int x = 0; x < blockSize; x++) {
                        pD[MIN(blockSize*rightBlocks[ri].begin.x + x, dst_linesize)] = pL[MIN(blockSize*leftBlocks[li].begin.x + x, left_linesize)];
                    }
                }

            }
            if (minErr < maxErr) {
                bestlx++;
            }
        }
    }
//    for (int li = 0; li < 6000; li++) {
//        uint8_t *lhist = leftBlocks[li].hist;
//        int minErr = 900;//3*256
//        int dist = 255*255;
//        for (int ri = 0; ri < 6000; ri++) {
//            uint8_t *rhist = rightBlocks[ri].hist;
//            int err = 0;
//            for (int h = 1; h < 255; h++) {
//                err += abs(lhist[h-1] + lhist[h] + lhist[h+1] - rhist[h-1] - rhist[h] - rhist[h+1]);
//            }
//            err += abs(rightBlocks[ri].horizontalEdges - leftBlocks[li].horizontalEdges)/2;
//            err += abs(rightBlocks[ri].verticalEdges - leftBlocks[li].verticalEdges)/2;
//            if (err < minErr) {
//                minErr = err;
//                dist = (leftBlocks[li].begin.x - rightBlocks[ri].begin.x)*(leftBlocks[li].begin.x - rightBlocks[ri].begin.x) +
//                       (leftBlocks[li].begin.y - rightBlocks[ri].begin.y)*(leftBlocks[li].begin.y - rightBlocks[ri].begin.y);
//            }
//        }
//        dist = sqrt(dist);
//        dists[MIN(dist, 255)]++;
//        errs[MIN(minErr, 255)]++;
//        for (int y = leftBlocks[li].begin.y*blockSize; y < MIN(blockSize*leftBlocks[li].begin.y + blockSize, height); y++) {
//            uint8_t *pD = _dst + y*dst_linesize;
//            for (int x = blockSize*leftBlocks[li].begin.x; x < MIN(blockSize*leftBlocks[li].begin.x + blockSize, dst_linesize); x++) {
//                pD[x] = dist;
//            }
//        }
//    }
//    for (int i = 0; i < 256; i++) {
//        printf("%d) %d \t|\t %d\n", i, dists[i], errs[i]);
//    }
//    printf("end\n");
//    for (int ly = 0; ly < height/blockSize; ly++) {
//        for (int lx = 0; lx < width/blockSize; lx++) {
//            int index = lx + ly*120;
//
//        }
//    }

}
typedef struct ComparationBlock {

    uint8_t* lines[200];
    uint8_t size;
    uint16_t x;

} ComparationBlock;

static int compareBlocks(ComparationBlock *p0, ComparationBlock *p1) {
    int error = 0;
    for (int i  = 0; i < p0->size; i++) {
        uint8_t *d0 = p0->lines[i] + p0->x;
        uint8_t *d1 = p1->lines[i] + p1->x;
        for (int x = 0; x < p0->size; x++) {
            error += abs(d0[x] - d1[x]);
        }
    }
    return error;
}

static void disparityBlockCompartation(uint8_t *_right, int right_linesize,
                                       uint8_t *_left,  int left_linesize,
                                       uint8_t *_dst,   int dst_linesize,
                                       int width,       int height) {

    uint8_t blockR = 5;
    uint8_t blockSize = blockR*2+1;
    int searchStart = 0;
    int searchRange = 30;
    uint8_t *err = (uint8_t*)errors->data[0];
    int step = 1;//blockR;

    memset(_dst, 255, height*dst_linesize);
    memset(err, 255, height*dst_linesize);

    ComparationBlock lBlock;
    ComparationBlock rBlock;
    rBlock.size = lBlock.size = blockSize;
    for (int ly = 0; ly < height - lBlock.size; ly += lBlock.size) {
        for (int i = 0; i < lBlock.size; i++) {
            lBlock.lines[i] = _left + (ly*left_linesize);
        }
        for (int lx = 0; lx < width - lBlock.size; lx += lBlock.size) {
            int minErr = 255*blockSize*blockSize;
            float dist = 255*5;
            lBlock.x = lx;
//            for (int by = searchStart; by < searchRange; by++) {
//                int ry = ly + by*(step);
//                if (ry > height - rBlock.size){
//                    break;
//                }
                for (int i = 0; i < lBlock.size; i++) {
                    rBlock.lines[i] = _right + (ly*right_linesize);
                }
                for (int bx = searchStart; bx < searchRange; bx++) {
                    int rx = lx + bx*(step);
                    if (rx > width - rBlock.size) {
                        break;
                    }
                    rBlock.x = rx;
                    {
                        int error = compareBlocks(&lBlock, &rBlock);
                        if (error < minErr) {
                            minErr = error;
                            //dist = sqrt((bx-searchStart)*(bx-searchStart) + (by-searchStart)*(by-searchStart));//sqrt((rx - lx) * (rx - lx) + (ry - ly) * (ry - ly));
                            dist = abs(lx - rx);//sqrt((bx-searchStart)*(bx-searchStart) + (ly-searchStart)*(ly-searchStart));//sqrt((rx - lx) * (rx - lx) + (ry - ly) * (ry - ly));
                        }
                    }
                }
            //}
            //printf("e %d, %d\n", width, height);
            //printf("%d, %d) %f, %d, %d\n", lx, ly, dist, minErr, dst_linesize);
            for (int i  = 0; i < lBlock.size; i++) {
                uint8_t *d = _dst + (ly+i)*dst_linesize;
//                printf("%d, %d (%d, %d, %d, %d)\n",
//                                                    ly+i,
//                                                    dst_linesize*(height - (ly+i)) - lx - lBlock.size,
//                                                    ly,
//                                                    lx,
//                                                    lBlock.size,
//                                                    height);
                //memset(d + lx, (uint8_t)(minErr/(blockSize*blockSize)), lBlock.size);
                memset(d + lx, (uint8_t)(dist), lBlock.size);
            }
        }
    }
}
static void detectVerticalEdges(uint8_t *_right, int right_linesize,
                                uint8_t *_left, int left_linesize,
                                int width, int height) {
    const uint8_t *right = _right;
    const uint8_t *left = _left;
    int8_t *dst = edges0->data[0];

    int y, x;
    int min = 127;
    int max = -128;
    for (y = 0; y < height; y++) {
        for (x = 0; x < width; x++) {
            dst[x] = MAX(-128, MIN(127, left[x+1] - (int)left[x-1]));
            min = MIN(min, dst[x]);
            max = MAX(max, dst[x]);
        }
        dst   += edges0->linesize[0];
        left  += left_linesize;
    }
    dst = edges0->data[0];
    for (y = 0; y < height; y++) {
        for (x = 0; x < width; x++) {
            dst[x] = (255*dst[x])/abs(max - min);
        }
        dst   += edges0->linesize[0];
    }
    min = 127;
    max = -128;
    dst = edges1->data[0];
    for (y = 0; y < height; y++) {
        for (x = 0; x < width; x++) {
            dst[x] = MAX(-128, MIN(127, right[x+1] - (int)right[x-1]));
            min = MIN(min, dst[x]);
            max = MAX(max, dst[x]);
        }
        dst    += edges1->linesize[0];
        right  += right_linesize;
    }
    dst = edges1->data[0];
    for (y = 0; y < height; y++) {
        for (x = 0; x < width; x++) {
            dst[x] = (255*dst[x])/abs(max - min);
        }
        dst   += edges0->linesize[0];
    }
}
static int verticalShift(uint8_t *_right, int right_linesize,
                         uint8_t *_left, int left_linesize,
                         int width,      int height,
                         int rangeStart, int rangeEnd) {

    int minR = rangeStart;
    double minErr = 0.0;
    for (int y = 0; y < height - abs(minR); y++){
        uint8_t *pL = _left + (y + MAX(minR, 0))*left_linesize;
        uint8_t *pR = _right + (y - MIN(0, minR))*right_linesize;
        for (int x = 0; x < width; x++) {
            minErr += abs(pL[x] - pR[x]);
        }
    }
    minErr /= (height - abs(minR));
    for (int r = minR; r < rangeEnd; r++) {
        double err = 0.0;
        for (int i = 0; i < height - abs(r); i++){
            uint8_t *pL = _left + (i + MAX(r, 0))*left_linesize;
            uint8_t *pR = _right + (i - MIN(0, r))*right_linesize;
            for (int x = 0; x < width; x++) {
                err += abs(pL[x] - pR[x]);
            }
        }
        err /= height - abs(r);
        if (err < minErr) {
            minErr = err;
            minR = r;
        }
    }
    printf("!minR = %d, %f!\n", minR, minErr);
    return minR;
}
typedef struct Pick {
    int x;
    int value;
    int length;
} Pick;
static void disparityLines(uint8_t *_right, int right_linesize,
                           uint8_t *_left,  int left_linesize,
                           uint8_t *_dst,   int dst_linesize,
                           int width,       int height) {
    uint8_t *err = (uint8_t*)errors->data[0];
    detectVerticalEdges(_right, right_linesize,
                        _left,  left_linesize,
                        width,  height);

    memset(_dst, 0, height*dst_linesize);
    memset(err,  255, height*errors->linesize[0]);
//    for (int y = 0; y < height; y++)
//        memcpy(_dst + y*dst_linesize, edges0->data[0] + y*edges0->linesize[0], dst_linesize);
    for (int y = 0; y < height; y++) {
        uint8_t *d = _dst + y*dst_linesize;
        int8_t *e0 = edges0->data[0] + y*edges0->linesize[0];
        int8_t *e1 = edges1->data[0] + y*edges1->linesize[0];
        for (int x = 0; x < width; x++) {
            d[x] = abs(e0[x] - e1[x]);
        }
    }

    printf("disp\n");
    for (int y = 0; y < height; y++) {
        Pick leftPicks[2000];
        Pick rightPicks[2000];
        int lIndex = 0;
        uint8_t lState = 0;
        int rIndex = 0;
        uint8_t rState = 0;
        //dst = (int8_t*)(_dst + y*dst_linesize);
        int8_t *pE0 = (int8_t*)(edges0->data[0] + y*edges0->linesize[0]);
        int8_t *pE1 = (int8_t*)(edges1->data[0] + y*edges1->linesize[0]);
        for (int lx = 0; lx < width; lx++) {
            if (abs(pE0[lx]) > 1) {
                if (lState == 0) {
                    leftPicks[lIndex].x = lx;
                    leftPicks[lIndex].value = pE0[lx];
                    leftPicks[lIndex].length = 1;
                    lState = 1;
                } else {
                    if (abs(leftPicks[lIndex].value) < abs(pE0[lx])) {
                        leftPicks[lIndex].x = lx;
                        leftPicks[lIndex].value = pE0[lx];
                    }
                    leftPicks[lIndex].length++;
                }
            } else {
                if (lState == 1) {
                    //printf("%d) %d) lpic: %d, %d, %d\n", y, lIndex, leftPicks[lIndex].x, leftPicks[lIndex].value, leftPicks[lIndex].length);
                    lIndex++;
                    lState = 0;
                }
            }
            if (abs(pE1[lx]) > 1) {
                if (rState == 0) {
                    rightPicks[rIndex].x = lx;
                    rightPicks[rIndex].value = pE1[lx];
                    rightPicks[rIndex].length = 1;
                    rState = 1;
                } else {
                    if (abs(rightPicks[rIndex].value) < abs(pE1[lx])) {
                        rightPicks[rIndex].x = lx;
                        rightPicks[rIndex].value = pE1[lx];
                    }
                    rightPicks[rIndex].length++;
                }
            } else {
                if (rState == 1) {
                    //printf("%d) %d) rpic: %d, %d, %d\n", y, rIndex, rightPicks[rIndex].x, rightPicks[rIndex].value, rightPicks[rIndex].length);
                    rIndex++;
                    rState = 0;
                }
            }
        }
        printf("left: %d | right: %d\n", lIndex, rIndex);
//        if(y == 1800) {
//            for (int i = 0; i < rIndex; i++) {
//                int bx = MAX(0, rightPicks[i].x - rightPicks[i].length/2);
//                int yy = height/2 + rightPicks[i].value*2;
//                memset(_dst + bx + yy*dst_linesize, 160, rightPicks[i].length);
//            }
//            printf("1800\n");
//            for (int i = 0; i < lIndex; i++) {
//                int bx = MAX(0, leftPicks[i].x - leftPicks[i].length/2);
//                int yy = height/2 + leftPicks[i].value*2;
//
//                memset(_dst + bx + yy*dst_linesize, 0, leftPicks[i].length);
//            }
//        }
    }

}

static void fillDistortionMap(uint8_t *pMap, int mapLinesize,
                             int width, int height,
                             double strength, double zoom) {
    int halfW = width/2;
    int halfH = height/2;
    double corRadius = sqrt(width*width + height*height)/strength;
    for (int y = 0; y < height; y++) {
        uint16_t *pM = (uint16_t*)(pMap + mapLinesize*y);
        for (int x = 0; x < width; x++) {
            int nX = x - halfW;
            int nY = y - halfH;

            double dist = sqrt(nX*nX + nY*nY);
            double r = dist/corRadius;
            double theta = 1.0;
            if (nX != 0 || nY != 0) {
                theta = atan(r)/r;
            }
            pM[2*x]     = MAX(0, MIN((int)(halfW + theta * nX * zoom), width-1));
            pM[2*x + 1] = MAX(0, MIN((int)(halfH + theta * nY * zoom), height-1));
        }
    }
}

static void correctDistorition(uint8_t *pDst, int dstLinesize,
                               uint8_t *pSrc, int srcLinesize,
                               uint8_t *pMap, int mapLinesize,
                               int width, int height) {
    for (int y = 0; y < height; y++) {
        uint16_t *pM = (uint16_t*)(pMap + mapLinesize*y);
        uint8_t  *pD = (uint8_t*)(pDst + dstLinesize*y);
        for (int x = 0; x < width; x++) {
            pD[x] = pSrc[pM[2*x] + srcLinesize*pM[2*x + 1]];
        }
    }
}
static void colorNormalization(uint8_t *src, int src_linesize,
                               uint8_t *dst, int dst_linesize,
                               int width, int height) {
    uint8_t min = 256;
    uint8_t max = -1;
    float len = max - min;
    for (int i = 0; i < height*src_linesize; i++) {
        min = MIN(src[i], min);
        max = MAX(src[i], max);
    }
    //printf("|%d - %d|\n", max, min);
    if (len > 0) {
        for (int y = 0; y < height; y++) {
            uint8_t *pS = src + src_linesize*y;
            uint8_t *pD = dst + dst_linesize*y;
            for (int x = 0; x < width; x++) {
                pD[x] = 255*(pS[x] - min)/len;
            }
        }
    } else {
        for (int y = 0; y < height; y++) {
            memcpy(dst + dst_linesize*y, src + src_linesize*y, dst_linesize);
        }
    }
}
static void blend_stereo_8bit(const uint8_t *_left, ptrdiff_t left_linesize,
                              const uint8_t *_right, ptrdiff_t right_linesize,
                               uint8_t *_dst, ptrdiff_t dst_linesize,
                               ptrdiff_t width, ptrdiff_t height,
                               FilterParams *param, double *values, int starty)
{
    static int tmp = 0;
//    int rStart = -500;
//    int rEnd = -200;
//    if (tmp > 0) {
//        rStart = *prevShift - 30;
//        rEnd = *prevShift + 30;
//    }
    if (tmp > 8){
        return;
    }
    tmp++;
//    *prevShift = verticalShift(_right, right_linesize,
//                               _left, left_linesize,
//                               width, height,
//                               rStart, rEnd);
    //*prevShift = -140;
//    correctDistorition(corrected0->data[0], corrected0->linesize[0],//_dst, dst_linesize,//
//                       (uint8_t*) _right, right_linesize,
//                       distortionMap0->data[0], distortionMap0->linesize[0],
//                       width, height);
    //memset(corrected0->data[0], 0, corrected0->linesize[0]*height);
//    for (int y = 0; y < height - abs(*prevShift); y++) {
//        uint8_t *pSrc = _dst + (y - MIN(0, *prevShift))*dst_linesize;//corrected0->data[0] + (y - MIN(0, *prevShift))*corrected0->linesize[0];
//        uint8_t *pDst = corrected0->data[0] + (y + MAX(0, *prevShift))*corrected0->linesize[0];//_dst + (y + MAX(0, *prevShift))*dst_linesize;//
//        memcpy(pDst, pSrc, dst_linesize);
//    }
//    correctDistorition(corrected1->data[0], corrected1->linesize[0],
//                       (uint8_t*) _left, left_linesize,
//                       distortionMap1->data[0], distortionMap1->linesize[0],
//                       width, height);
//    disparityBlockImg(corrected0->data[0], corrected0->linesize[0],
//                      corrected1->data[0], corrected1->linesize[0],
//                      _dst, dst_linesize,
//                      width, height);
//    colorNormalization((uint8_t*) _right, right_linesize,
//                       corrected0->data[0], corrected0->linesize[0],
//                       width, height);
//    colorNormalization((uint8_t*) _left, left_linesize,
//                       corrected1->data[0], corrected0->linesize[0],
//                       width, height);
//    disparityBlockImg(corrected1->data[0], corrected1->linesize[0],
//                      corrected0->data[0], corrected0->linesize[0],
//                      _dst, dst_linesize,
//                      width, height);
//    disparityBlockImg((uint8_t*) _left, left_linesize,
//                      (uint8_t*) _right, right_linesize,
//                      _dst, dst_linesize,
//                      width, height);
    disparityShift(_left, left_linesize,
                    _right, right_linesize,
                    _dst, dst_linesize,
                    width, height);
//    disparityBlockCompartation(_left, left_linesize,
//                                _right, right_linesize,
//                                _dst, dst_linesize,
//                                width, height);
//    disparityBlockCompartation(corrected0->data[0], corrected0->linesize[0],
//                                corrected1->data[0], corrected1->linesize[0],
//                                _dst, dst_linesize,
//                                width, height);
//    disparityLines(corrected0->data[0], corrected0->linesize[0],
//                   corrected1->data[0], corrected1->linesize[0],
//                   _dst, dst_linesize,
//                   width, height);
//    detectEdges(corrected1->data[0], corrected1->linesize[0],//_left, left_linesize,
//                corrected0->data[0], corrected0->linesize[0],//_right, right_linesize,
//                _dst, dst_linesize,
//                width, height);

}

void ff_blend_init(FilterParams *param, int is_16bit)
{
    switch (param->mode) {
    case BLEND_ADDITION:   param->blend = is_16bit ? blend_addition_16bit   : blend_addition_8bit;   break;
    case BLEND_ADDITION128: param->blend = is_16bit ? blend_addition128_16bit : blend_addition128_8bit; break;
    case BLEND_AND:        param->blend = is_16bit ? blend_and_16bit        : blend_and_8bit;        break;
    case BLEND_AVERAGE:    param->blend = is_16bit ? blend_average_16bit    : blend_average_8bit;    break;
    case BLEND_BURN:       param->blend = is_16bit ? blend_burn_16bit       : blend_burn_8bit;       break;
    case BLEND_DARKEN:     param->blend = is_16bit ? blend_darken_16bit     : blend_darken_8bit;     break;
    case BLEND_DIFFERENCE: param->blend = is_16bit ? blend_difference_16bit : blend_difference_8bit; break;
    case BLEND_DIFFERENCE128: param->blend = is_16bit ? blend_difference128_16bit: blend_difference128_8bit; break;
    case BLEND_DIVIDE:     param->blend = is_16bit ? blend_divide_16bit     : blend_divide_8bit;     break;
    case BLEND_DODGE:      param->blend = is_16bit ? blend_dodge_16bit      : blend_dodge_8bit;      break;
    case BLEND_EXCLUSION:  param->blend = is_16bit ? blend_exclusion_16bit  : blend_exclusion_8bit;  break;
    case BLEND_FREEZE:     param->blend = is_16bit ? blend_freeze_16bit     : blend_freeze_8bit;     break;
    case BLEND_GLOW:       param->blend = is_16bit ? blend_glow_16bit       : blend_glow_8bit;       break;
    case BLEND_HARDLIGHT:  param->blend = is_16bit ? blend_hardlight_16bit  : blend_hardlight_8bit;  break;
    case BLEND_HARDMIX:    param->blend = is_16bit ? blend_hardmix_16bit    : blend_hardmix_8bit;    break;
    case BLEND_HEAT:       param->blend = is_16bit ? blend_heat_16bit       : blend_heat_8bit;       break;
    case BLEND_LIGHTEN:    param->blend = is_16bit ? blend_lighten_16bit    : blend_lighten_8bit;    break;
    case BLEND_LINEARLIGHT:param->blend = is_16bit ? blend_linearlight_16bit: blend_linearlight_8bit;break;
    case BLEND_MULTIPLY:   param->blend = is_16bit ? blend_multiply_16bit   : blend_multiply_8bit;   break;
    case BLEND_MULTIPLY128:param->blend = is_16bit ? blend_multiply128_16bit: blend_multiply128_8bit;break;
    case BLEND_NEGATION:   param->blend = is_16bit ? blend_negation_16bit   : blend_negation_8bit;   break;
    case BLEND_NORMAL:     param->blend = param->opacity == 1 ? blend_copytop :
                                          param->opacity == 0 ? blend_copybottom :
                                          is_16bit ? blend_normal_16bit     : blend_normal_8bit;     break;
    case BLEND_OR:         param->blend = is_16bit ? blend_or_16bit         : blend_or_8bit;         break;
    case BLEND_OVERLAY:    param->blend = is_16bit ? blend_overlay_16bit    : blend_overlay_8bit;    break;
    case BLEND_PHOENIX:    param->blend = is_16bit ? blend_phoenix_16bit    : blend_phoenix_8bit;    break;
    case BLEND_PINLIGHT:   param->blend = is_16bit ? blend_pinlight_16bit   : blend_pinlight_8bit;   break;
    case BLEND_REFLECT:    param->blend = is_16bit ? blend_reflect_16bit    : blend_reflect_8bit;    break;
    case BLEND_SCREEN:     param->blend = is_16bit ? blend_screen_16bit     : blend_screen_8bit;     break;
    case BLEND_SOFTLIGHT:  param->blend = is_16bit ? blend_softlight_16bit  : blend_softlight_8bit;  break;
    case BLEND_SUBTRACT:   param->blend = is_16bit ? blend_subtract_16bit   : blend_subtract_8bit;   break;
    case BLEND_VIVIDLIGHT: param->blend = is_16bit ? blend_vividlight_16bit : blend_vividlight_8bit; break;
    case BLEND_XOR:        param->blend = is_16bit ? blend_xor_16bit        : blend_xor_8bit;        break;

    case BLEND_STEREO:     param->blend = blend_stereo_8bit;        break;
    case BLEND_BLACK:      param->blend = blend_black_8bit;         break;//blend_stereo_8bit; break;//
    }

    if (param->opacity == 0 && param->mode != BLEND_NORMAL) {
        param->blend = blend_copytop;
    }

    if (ARCH_X86)
        ff_blend_init_x86(param, is_16bit);
}

static int config_output(AVFilterLink *outlink)
{
    AVFilterContext *ctx = outlink->src;
    AVFilterLink *toplink = ctx->inputs[TOP];
    BlendContext *s = ctx->priv;
    const AVPixFmtDescriptor *pix_desc = av_pix_fmt_desc_get(toplink->format);
    int ret, plane, is_16bit;
    uint8_t singletone = 1;

    if (!s->tblend) {
        AVFilterLink *bottomlink = ctx->inputs[BOTTOM];

        if (toplink->format != bottomlink->format) {
            av_log(ctx, AV_LOG_ERROR, "inputs must be of same pixel format\n");
            return AVERROR(EINVAL);
        }
        if (toplink->w                       != bottomlink->w ||
            toplink->h                       != bottomlink->h ||
            toplink->sample_aspect_ratio.num != bottomlink->sample_aspect_ratio.num ||
            toplink->sample_aspect_ratio.den != bottomlink->sample_aspect_ratio.den) {
            av_log(ctx, AV_LOG_ERROR, "First input link %s parameters "
                   "(size %dx%d, SAR %d:%d) do not match the corresponding "
                   "second input link %s parameters (%dx%d, SAR %d:%d)\n",
                   ctx->input_pads[TOP].name, toplink->w, toplink->h,
                   toplink->sample_aspect_ratio.num,
                   toplink->sample_aspect_ratio.den,
                   ctx->input_pads[BOTTOM].name, bottomlink->w, bottomlink->h,
                   bottomlink->sample_aspect_ratio.num,
                   bottomlink->sample_aspect_ratio.den);
            return AVERROR(EINVAL);
        }
    }

    outlink->w = toplink->w;
    outlink->h = toplink->h;
    outlink->time_base = toplink->time_base;
    outlink->sample_aspect_ratio = toplink->sample_aspect_ratio;
    outlink->frame_rate = toplink->frame_rate;

    s->hsub = pix_desc->log2_chroma_w;
    s->vsub = pix_desc->log2_chroma_h;

    is_16bit = pix_desc->comp[0].depth == 16;
    s->nb_planes = av_pix_fmt_count_planes(toplink->format);

    if (!s->tblend)
        if ((ret = ff_dualinput_init(ctx, &s->dinput)) < 0)
            return ret;

    for (plane = 0; plane < FF_ARRAY_ELEMS(s->params); plane++) {
        FilterParams *param = &s->params[plane];

        //kacziro - mod
        if (param->mode == BLEND_STEREO) {
            if (singletone) {
                singletone = 0;
                initFrameCopy(NULL, &s->edges0,        toplink->w, toplink->h, toplink->format);
                initFrameCopy(NULL, &s->edges1,        toplink->w, toplink->h, toplink->format);
                initFrameCopy(NULL, &s->errors,        toplink->w, toplink->h, toplink->format);
                initFrameCopy(NULL, &s->corrected0,    toplink->w, toplink->h, toplink->format);
                initFrameCopy(NULL, &s->corrected1,    toplink->w, toplink->h, toplink->format);
                initFrameCopy(NULL, &s->distortionMap0, toplink->w, toplink->h, AV_PIX_FMT_BGRA); // AV_PIX_FMT_BGRA - 32bpp - 2x16bits for coords
                initFrameCopy(NULL, &s->distortionMap1, toplink->w, toplink->h, AV_PIX_FMT_BGRA); // AV_PIX_FMT_BGRA - 32bpp - 2x16bits for coords

                edges0         = &s->edges0;
                edges1         = &s->edges1;
                errors         = &s->errors;
                corrected0     = &s->corrected0;
                corrected1     = &s->corrected1;
                distortionMap0 = &s->distortionMap0;
                distortionMap1 = &s->distortionMap1;
                prevShift      = &s->prevShift;

                fillDistortionMap(distortionMap0->data[0], distortionMap0->linesize[0],
                                  toplink->w, toplink->h,
                                  4.48, 1.84);
                fillDistortionMap(distortionMap1->data[0], distortionMap1->linesize[0],
                                  toplink->w, toplink->h,
                                  4.46, 1.83);
            }
        }
        if (plane != 0 && !singletone && param->mode == BLEND_NORMAL) {
            param->mode = BLEND_BLACK;
        }

        if (s->all_mode >= 0)
            param->mode = s->all_mode;
        if (s->all_opacity < 1)
            param->opacity = s->all_opacity;

        ff_blend_init(param, is_16bit);

        if (s->all_expr && !param->expr_str) {
            param->expr_str = av_strdup(s->all_expr);
            if (!param->expr_str)
                return AVERROR(ENOMEM);
        }
        if (param->expr_str) {
            ret = av_expr_parse(&param->e, param->expr_str, var_names,
                                NULL, NULL, NULL, NULL, 0, ctx);
            if (ret < 0)
                return ret;
            param->blend = is_16bit? blend_expr_16bit : blend_expr_8bit;
        }
    }

    return 0;
}

#if CONFIG_BLEND_FILTER

static int request_frame(AVFilterLink *outlink)
{
    BlendContext *s = outlink->src->priv;
    return ff_dualinput_request_frame(&s->dinput, outlink);
}

static int filter_frame(AVFilterLink *inlink, AVFrame *buf)
{
    BlendContext *s = inlink->dst->priv;
    return ff_dualinput_filter_frame(&s->dinput, inlink, buf);
}

static const AVFilterPad blend_inputs[] = {
    {
        .name          = "top",
        .type          = AVMEDIA_TYPE_VIDEO,
        .filter_frame  = filter_frame,
    },{
        .name          = "bottom",
        .type          = AVMEDIA_TYPE_VIDEO,
        .filter_frame  = filter_frame,
    },
    { NULL }
};

static const AVFilterPad blend_outputs[] = {
    {
        .name          = "default",
        .type          = AVMEDIA_TYPE_VIDEO,
        .config_props  = config_output,
        .request_frame = request_frame,
    },
    { NULL }
};

AVFilter ff_vf_blend = {
    .name          = "blend",
    .description   = NULL_IF_CONFIG_SMALL("Blend two video frames into each other."),
    .init          = init,
    .uninit        = uninit,
    .priv_size     = sizeof(BlendContext),
    .query_formats = query_formats,
    .inputs        = blend_inputs,
    .outputs       = blend_outputs,
    .priv_class    = &blend_class,
    .flags         = AVFILTER_FLAG_SUPPORT_TIMELINE_INTERNAL | AVFILTER_FLAG_SLICE_THREADS,
};

#endif

#if CONFIG_TBLEND_FILTER

static int tblend_filter_frame(AVFilterLink *inlink, AVFrame *frame)
{
    BlendContext *s = inlink->dst->priv;
    AVFilterLink *outlink = inlink->dst->outputs[0];

    if (s->prev_frame) {
        AVFrame *out = blend_frame(inlink->dst, frame, s->prev_frame);
        av_frame_free(&s->prev_frame);
        s->prev_frame = frame;
        return ff_filter_frame(outlink, out);
    }
    s->prev_frame = frame;
    return 0;
}

static const AVOption tblend_options[] = {
    COMMON_OPTIONS,
    { NULL }
};

AVFILTER_DEFINE_CLASS(tblend);

static const AVFilterPad tblend_inputs[] = {
    {
        .name          = "default",
        .type          = AVMEDIA_TYPE_VIDEO,
        .filter_frame  = tblend_filter_frame,
    },
    { NULL }
};

static const AVFilterPad tblend_outputs[] = {
    {
        .name          = "default",
        .type          = AVMEDIA_TYPE_VIDEO,
        .config_props  = config_output,
    },
    { NULL }
};

AVFilter ff_vf_tblend = {
    .name          = "tblend",
    .description   = NULL_IF_CONFIG_SMALL("Blend successive frames."),
    .priv_size     = sizeof(BlendContext),
    .priv_class    = &tblend_class,
    .query_formats = query_formats,
    .init          = init,
    .uninit        = uninit,
    .inputs        = tblend_inputs,
    .outputs       = tblend_outputs,
    .flags         = AVFILTER_FLAG_SLICE_THREADS,
};

#endif
